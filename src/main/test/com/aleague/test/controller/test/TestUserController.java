/**
 * 
 */
package com.aleague.test.controller.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.aleague.test.controller.TestAbstractController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intentlabs.aleague.user.view.UserView;

/**
 * @author Dhruvang
 *
 */
public class TestUserController extends TestAbstractController  {
	
	private ObjectMapper objectMapper;

	@Override
	public void prepareData() throws Exception {
		
		objectMapper = new ObjectMapper();
	 }

	@Override
	public void removeData() throws Exception {
	}
	
	@Test
	public void testUserLogin() {
		try {
			final String requestUri = "/user/login.htm";
			request.setContentType("application/json");
			request.setMethod("POST");
			request.setRequestURI(requestUri);
			
			UserView userView = new UserView();
			userView.setId(4l);
			userView.setEmailId("bhavik.bhuva@intentlabs.com");
			userView.setPassword("123");
			
			ModelMap modelMap = new ModelMap();
			
			modelMap.put("userView",userView);
			
			request.setContent(objectMapper.writeValueAsString(modelMap).getBytes());
			
			Object handler = requestMappingHandlerMapping.getHandler(request).getHandler();
			
			ModelAndView modelAndView = requestMappingHandlerAdapter.handle(request, response, handler);
			Assert.assertTrue("response Message", response.getContentAsString().contains("1000"));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue("Exception", false);
		}
	}

}
