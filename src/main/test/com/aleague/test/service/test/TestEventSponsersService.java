
package com.aleague.test.service.test;

import java.util.Date;

import org.junit.Test;

import com.aleague.test.service.TestAbstractService;
import com.intentlabs.aleague.event.model.EventSponsorsModel;
import com.intentlabs.aleague.event.services.EventService;
import com.intentlabs.aleague.event.services.EventSponsersService;
import com.intentlabs.aleague.user.services.UserService;

/**
 * This is used to test data for Event Sponsers.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class TestEventSponsersService extends TestAbstractService
{

	UserService userService;
	EventService eventService;
	EventSponsersService eventSponsersService;
	
	@Override
	public void prepareData() throws Exception {
		eventService = (EventService) applicationContext.getBean("eventService");
		userService = (UserService) applicationContext.getBean("userService");
		eventSponsersService = (EventSponsersService) applicationContext.getBean("eventSponsersService");
	}

	@Override
	public void removeData() throws Exception {
	
	}
	
	@Test
	public void addEventSponsersTest(){
		
		try {
			EventSponsorsModel eventSponsorsModel = new EventSponsorsModel();
			eventSponsorsModel.setFkEventId(eventService.get(1l));
			eventSponsorsModel.setTxtName("Sponser 1");
			eventSponsorsModel.setTxtImagePath("/home/aleague/abc.png");
			eventSponsorsModel.setDateUpload(new Date());
			eventSponsorsModel.setFkUploadBy(userService.get(1l));
			eventSponsersService.create(eventSponsorsModel);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
		
	}

}
