
package com.aleague.test.service.test;

import org.junit.Test;

import com.aleague.test.service.TestAbstractService;
import com.intentlabs.aleague.event.model.EventMemberModel;
import com.intentlabs.aleague.event.services.EventMemberService;
import com.intentlabs.aleague.event.services.EventService;
import com.intentlabs.aleague.user.services.UserService;

/**
 * This is used to test data for Event Sponsers.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class TestEventMemberService extends TestAbstractService
{

	UserService userService;
	EventService eventService;
	EventMemberService eventMemberService;
	
	@Override
	public void prepareData() throws Exception {
		eventService = (EventService) applicationContext.getBean("eventService");
		userService = (UserService) applicationContext.getBean("userService");
		eventMemberService = (EventMemberService) applicationContext.getBean("eventMemberService");
	}

	@Override
	public void removeData() throws Exception {
	
	}
	
	@Test
	public void addEventMembersTest(){
		
		try {
			EventMemberModel eventMemberModel = new EventMemberModel();
			eventMemberModel.setFkEventId(eventService.get(1l));
			eventMemberModel.setFkUserId(userService.get(1l));
			eventMemberService.create(eventMemberModel);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
		
	}

}
