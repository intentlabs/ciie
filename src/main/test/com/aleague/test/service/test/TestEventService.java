
package com.aleague.test.service.test;

import java.util.Date;

import org.junit.Test;

import com.aleague.test.service.TestAbstractService;
import com.intentlabs.aleague.event.enums.EnumEventType;
import com.intentlabs.aleague.event.enums.EnumPeriod;
import com.intentlabs.aleague.event.model.EventModel;
import com.intentlabs.aleague.event.services.EventService;
import com.intentlabs.aleague.user.services.UserService;

/**
 * This is used to test data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class TestEventService extends TestAbstractService 
{
	
	UserService userService;
	EventService eventService;

	@Override
	public void prepareData() throws Exception {
		eventService = (EventService) applicationContext.getBean("eventService");
		userService = (UserService) applicationContext.getBean("userService");
		
	}

	@Override
	public void removeData() throws Exception {
		
	}
	
	@Test
	public void addEventTest(){
		
		try {
			EventModel event = new EventModel();
			event.setCreateBy(userService.get(1l));
			event.setCreateDate(new Date());
			event.setTxtName("Event 1");
			event.setActivationStatus("1");
			event.setTxtAddress("Ahmedabad");
			event.setTxtDescription("event 1");
			event.setEnumType(EnumEventType.OWN_COLLEGE);
			event.setEnumPeriod(EnumPeriod.PERIOD);
			event.setDateStartDate(new Date());
			eventService.create(event);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
		
	}

}
