package com.aleague.test.service.test;

import java.util.Date;

import org.junit.Test;

import com.aleague.test.service.TestAbstractService;
import com.intentlabs.aleague.rights.Role;
import com.intentlabs.aleague.role.services.RoleService;
import com.intentlabs.aleague.user.services.UserService;

public class TestRoleService extends TestAbstractService {
	
	UserService userService;
	RoleService roleService; 

	@Override
	public void prepareData() throws Exception {
		userService = (UserService) applicationContext.getBean("userService");
		roleService = (RoleService) applicationContext.getBean("roleService");

		
	}

	@Override
	public void removeData() throws Exception {
	
		
	}
	
	
	@Test
	public void addRoleBySuperAdminTest(){
		
		try {
			
			Role role = new Role();
			role.setCreateBy(userService.get(1l));
			role.setCreateDate(new Date());
			role.setActivationDate(new Date());
			role.setTxtName("Admin");
			role.setDescription("admin level user");
			role.setActivationStatus("1");
			role.setEnumArchive("1");
			roleService.create(role);
			} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
	}

}
