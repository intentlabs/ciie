
package com.aleague.test.service.test;

import java.util.Date;

import org.junit.Test;

import com.aleague.test.service.TestAbstractService;
import com.intentlabs.aleague.event.model.EventGalleryModel;
import com.intentlabs.aleague.event.services.EventGalleryService;
import com.intentlabs.aleague.event.services.EventService;
import com.intentlabs.aleague.user.services.UserService;

/**
 * This is used to test data for Event gallery.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class TestEventGalleryService extends TestAbstractService 
{

	UserService userService;
	EventService eventService;
	EventGalleryService eventGalleryService;
	
	@Override
	public void prepareData() throws Exception {
		eventService = (EventService) applicationContext.getBean("eventService");
		userService = (UserService) applicationContext.getBean("userService");
		eventGalleryService = (EventGalleryService) applicationContext.getBean("eventGalleryService");
	}

	@Override
	public void removeData() throws Exception {
	
	}

	@Test
	public void addEventGalleryTest(){
		
		try {
			EventGalleryModel eventGalleryModel = new EventGalleryModel();
			eventGalleryModel.setFkEventId(eventService.get(1l));
			eventGalleryModel.setTxtImagePath("/home/aleague/abc.txt");
			eventGalleryModel.setDateUpload(new Date());
			eventGalleryModel.setFkUploadBy(userService.get(1l));
			eventGalleryService.create(eventGalleryModel);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
		
	}
	
}
