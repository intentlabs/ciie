package com.aleague.test.service.test;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

import com.aleague.test.service.TestAbstractService;
import com.intentlabs.aleague.enums.EnumProfile;
import com.intentlabs.aleague.role.services.RoleService;
import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.user.model.UserPassword;
import com.intentlabs.aleague.user.services.UserService;
import com.intentlabs.common.enums.CommonStatus;

public class TestUserService extends TestAbstractService  {
	
	UserService userService;
	RoleService roleService; 
	@Override
	public void prepareData() throws Exception {
		
		userService = (UserService) applicationContext.getBean("userService");
		roleService = (RoleService) applicationContext.getBean("roleService");

		
	}

	@Override
	public void removeData() throws Exception {
	}
		
		
		@Test
		public void addUserBySuperAdmin(){
			try {
				
				User user = new User();
				user.setCreateBy(userService.get(1l));
				user.setCreateDate(new Date());
				user.setTxtEmail("bhavik@intentlabs.com");
				user.setTxtFirstName("Bhavik");
				user.setTxtLastName("Bhuva");
				user.setFkRoleId(roleService.get(2l));
				user.setTxtCountryCode("91");
				user.setTxtContactNumber("9429517040");
				user.setTxtActToken("dhruvang1271");
				user.setActivationStatus("1");
				user.setEnumActTokenUsed("1");
				user.setEnumProfilel(EnumProfile.SEVENTY);
				user.setEnumArchive("1");
				setUserPassword(user);
				userService.create(user);
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}

			
	
		
		
	}
	
	private void setUserPassword(User user){
		Set<UserPassword> lastPassList = user.getUserPassword();
		
		if (user.getNoOfPasswords() == null) {
			UserPassword newPassword = new UserPassword();
			newPassword.setTxtPassword("123456");
			newPassword.setDatePasswordChange(new Date());
			newPassword.setFkUserId(user);
			lastPassList.add(newPassword);
		} else if (user.getNoOfPasswords() < 5) {
			UserPassword newPassword = new UserPassword();
			newPassword.setTxtPassword("123456");
			newPassword.setDatePasswordChange(new Date());
			newPassword.setFkUserId(user);
			lastPassList.add(newPassword);
		} else {
			Iterator<UserPassword> iterator = lastPassList.iterator();
			if (iterator.hasNext()) {
				UserPassword oldestPass = iterator.next();
				oldestPass.setTxtPassword("123456");
				oldestPass.setDatePasswordChange(new Date());
			}
		}
		user.setUserPassword(lastPassList);
		
	}

}
