
package com.intentlabs.aleague.tag.services;

import java.util.List;

import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface TagService extends BaseService<TagModel> {
	List<TagModel> getAllTags(String tagname);
}
