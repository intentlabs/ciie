
package com.intentlabs.aleague.tag.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class TagServiceImpl extends AbstractService<TagModel> implements TagService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<TagModel> getModelClass() {
		return TagModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<TagModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(TagModel model, Criteria commonCriteria) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TagModel> getAllTags(String tagname) {
		Criteria criteria = getSession().createCriteria(getModelClass());
				
		if(tagname != null && !"".equals(tagname)){
			criteria.add(Restrictions.like("txtTagName", tagname, MatchMode.START));
		}
		
		return criteria.list();
	}
}
