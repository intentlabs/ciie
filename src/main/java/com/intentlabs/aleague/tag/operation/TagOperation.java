
package com.intentlabs.aleague.tag.operation;

import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is operation for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface TagOperation extends BaseOperation<TagView> {

	String TAG_OPERATION = "tagOperation";	
	
	Response doDisplayAllOperation(String tagname);
}
