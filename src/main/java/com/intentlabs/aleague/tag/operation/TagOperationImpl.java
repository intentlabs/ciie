
package com.intentlabs.aleague.tag.operation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.jobpost.enums.EnumJobType;
import com.intentlabs.aleague.jobpost.model.JobPostModel;
import com.intentlabs.aleague.jobpost.view.JobPostView;
import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.aleague.tag.services.TagService;
import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.View;

/**
 * This is operation for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Component(value = "tagOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class TagOperationImpl extends AbstractOperation<TagModel, TagView> implements TagOperation {

	private static final long serialVersionUID = 741391010890205612L;

	@Autowired
	private TagService tagService;

	@Override
	public BaseService getService() {
		return tagService;
	}

	@Override
	protected TagModel getNewModel(TagView view) {
		return new TagModel();
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		return null;
	}

	@Override
	protected void checkInactive(TagModel model) throws Exception {
	}

	@Override
	public TagModel toModel(TagModel tagModel, TagView tagView) throws Exception {
		if (tagView.getId() == null || tagView.getId().longValue() <= 0) {
			tagModel.setCreateBy(Auditor.getAuditor());
			tagModel.setActivationStatus(ActiveInActive.ACTIVE);
			tagModel.setActivationDate(DateUtility.getCurrentDate());
			tagModel.setEnumArchive("1");
		}

		tagModel.setUpdateBy(Auditor.getAuditor());
		tagModel.setUpdateDate(DateUtility.getCurrentDate());

		tagModel.setTxtTagName(tagView.getTagName());

		return tagModel;
	}

	@Override
	public TagView fromModel(TagModel tagModel) {
		TagView tagView = new TagView();
		tagView.setId(tagModel.getId());
		tagView.setTagName(tagModel.getTxtTagName());
		
		return tagView;
	}

	@Override
	public Response doDisplayAllOperation(String tagname) {
		List<TagModel> listTags = tagService.getAllTags(tagname);

		List<View> listTagViews = new ArrayList<>();

		for (TagModel model : listTags) {
			listTagViews.add(fromModel(model));
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_TAGS,
				listTagViews.size(), listTagViews);
	}
}
