
package com.intentlabs.aleague.tag.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.tag.operation.TagOperation;
import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@SuppressWarnings("serial")
@Controller
@RequestMapping("/tag")
public class TagControllerImpl extends AbstractController<TagView> implements TagController {

	@Autowired
	TagOperation tagOperation;

	@Override
	public BaseOperation<TagView> getOperation() {
		return tagOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(TagView webView) throws Exception {
	}

	@Override
	public Response getAllTags(@RequestBody (required = false) TagView tagView)
			throws InvalidDataException, Exception {
		if(tagView != null){
			return tagOperation.doDisplayAllOperation(tagView.getTagName());	
		}else{
			return tagOperation.doDisplayAllOperation(null);
		}		
	}

}
