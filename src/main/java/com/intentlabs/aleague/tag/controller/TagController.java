package com.intentlabs.aleague.tag.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface TagController extends WebController<TagView>
{

	String TAG_CONTROLLER = "TagController";
	
	@RequestMapping(value = "/getalltags.htm", method = RequestMethod.GET)
	@ResponseBody
	Response getAllTags(@RequestBody (required = false) TagView tagView) throws InvalidDataException, Exception;
}
