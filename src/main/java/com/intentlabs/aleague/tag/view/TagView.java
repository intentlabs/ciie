
package com.intentlabs.aleague.tag.view;

import com.intentlabs.common.view.IdentifierView;

/**
 * This is View used to get data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class TagView extends IdentifierView {


	private static final long serialVersionUID = 8692674149531174388L;
	
	private String tagName;

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
}
