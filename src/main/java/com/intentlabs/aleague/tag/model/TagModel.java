
package com.intentlabs.aleague.tag.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.intentlabs.aleague.blogpost.model.BlogPostModel;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tbltag")
public class TagModel extends ArchiveModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;
	
	@Column(name = "txtTagName", length = 100)
	private String txtTagName;
	
	public String getTxtTagName() {
		return txtTagName;
	}

	public void setTxtTagName(String txtTagName) {
		this.txtTagName = txtTagName;
	}

	public static TagModel getSaveModel(){
		TagModel tag = new TagModel();
		tag.setCreateDate(new Date());
		return tag;
	}
}
