/**
 * 
 */
package com.intentlabs.aleague.usersession.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.model.IdentifierModel;

/**
 * @author Dhruvang
 *
 */

@Entity
@Table(name = "tblUserSession" , uniqueConstraints = @UniqueConstraint(columnNames = {"txtSession", "txtDeviceCookie"}))
public class UserJsessions extends IdentifierModel {


	private static final long serialVersionUID = -2950420391918498866L;

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkUserId")
	private User fkUserId;
	
	@Column(name = "txtSession", nullable = false, length = 100)
    private String Jsession;
	
	@Column(name = "txtDevice")
	private String deviceBrowser;
	
	@Column(name = "txtDeviceCookie")
	private String deviceCookie;
	
	@Column(name = "txtIp")
	private String ipAddress;
	
	@Column(name = "dateCookieExpireTime")
   private Date dateCookieExpireTime;	

	public User getFkUserId() {
		return fkUserId;
	}

	public void setFkUserId(User fkUserId) {
		this.fkUserId = fkUserId;
	}

	public String getJsession() {
		return Jsession;
	}

	public void setJsession(String jsession) {
		Jsession = jsession;
	}

	public String getDeviceBrowser() {
		return deviceBrowser;
	}

	public void setDeviceBrowser(String deviceBrowser) {
		this.deviceBrowser = deviceBrowser;
	}

	public String getDeviceCookie() {
		return deviceCookie;
	}

	public void setDeviceCookie(String deviceCookie) {
		this.deviceCookie = deviceCookie;
	}


	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	
	
}
