package com.intentlabs.aleague.usersession.service;

import java.util.List;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.usersession.model.UserJsessions;
import com.intentlabs.common.service.BaseService;

public interface SessionService extends BaseService<UserJsessions> {
	
	UserJsessions userSessionValidate(User user, String jssionId);
	UserJsessions userCookiValidate(User user,String cookis);
	UserJsessions userCookiValidate(String jssionId,String cookis);
	User getUserBySessionId(String sessionId);
	List<UserJsessions> getSessionByUser(long userId);
}
