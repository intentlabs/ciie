package com.intentlabs.aleague.usersession.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Qualifier;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.usersession.model.UserJsessions;
import com.intentlabs.common.service.AbstractService;



public class SessionServiceImpl extends AbstractService<UserJsessions> implements SessionService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6418282978716258680L;

	public SessionServiceImpl(@Qualifier("sessionFactory") SessionFactory sessionFactory){
		this.setSessionFactory(sessionFactory);
		System.out.println("Call" + sessionFactory);
	}
	
	@Override
	public Class<UserJsessions> getModelClass() {
		return UserJsessions.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<UserJsessions> modelClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria setSearchCriteria(UserJsessions model, Criteria commonCriteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserJsessions userSessionValidate(User user, String jssionId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("fkUserId",user));
		criteria.add(Restrictions.eq("txtJsessionId",jssionId));
		return (UserJsessions ) criteria.uniqueResult();
	}

	@Override
	public UserJsessions userCookiValidate(User user,String cookis) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("fkUserId",user));
		criteria.add(Restrictions.eq("deviceCookie",cookis));
		return (UserJsessions ) criteria.uniqueResult();
	}

	@Override
	public UserJsessions userCookiValidate(String jssionId, String cookis) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("txtJsessionId",jssionId));
		criteria.add(Restrictions.eq("txtCookie",cookis));
		return (UserJsessions) criteria.uniqueResult();
	}

	@Override
	public User getUserBySessionId(String sessionId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setProjection(Projections.property("fkUserId"));
		criteria.add(Restrictions.eq("Jsession",sessionId));
		return (User) criteria.uniqueResult();
	}

	@Override
	public List<UserJsessions> getSessionByUser(long userId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("fkUserId.id",userId));
		return (List<UserJsessions>) criteria.list();
	}

}
