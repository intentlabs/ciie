/**
 * 
 */
package com.intentlabs.aleague.role.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.role.view.RoleView;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * @author Dhruvang
 *
 */
public interface RoleController extends WebController<RoleView>  {
	
	
	String ROLE_CONTROLLER = "RoleController";
	
	@RequestMapping(value = "/getAllRoles.htm", method = RequestMethod.GET)
	@ResponseBody
	Response getAllRoles() throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getRoslesForCirdinators.htm", method = RequestMethod.GET)
	@ResponseBody
	Response getRoslesForCirdinators() throws InvalidDataException, Exception;

}
