package com.intentlabs.aleague.role.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.role.operation.RoleOperation;
import com.intentlabs.aleague.role.view.RoleView;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;

@Controller
@RequestMapping("/role")
public class RoleControllerImpl extends AbstractController<RoleView> implements RoleController  {

	private static final long serialVersionUID = -4509704082303229777L;
	@Autowired
	private RoleOperation roleOperation;
	
	@Override
	public BaseOperation<RoleView> getOperation() {
		return roleOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {
	
		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {
	
		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {
	
		return null;
	}

	@Override
	public void isValidSaveData(RoleView webView) throws Exception {
			
	}

	@Override
	public Response getAllRoles() throws InvalidDataException, Exception {
	
		try{
			LoggerService.info(ROLE_CONTROLLER, OperationName.GET_AllROLES, LogMessage.BEFORE_CALLING_OPERATION);
			
			return roleOperation.getAllRoles();
		}
		finally {
			LoggerService.info(ROLE_CONTROLLER, OperationName.GET_AllROLES, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response getRoslesForCirdinators() throws InvalidDataException, Exception {
		try{
			LoggerService.info(ROLE_CONTROLLER, OperationName.GET_ROL_FOR_CORDINATIRS, LogMessage.BEFORE_CALLING_OPERATION);
			
			return roleOperation.getRolesForCordinator();
		}
		finally {
			LoggerService.info(ROLE_CONTROLLER, OperationName.GET_ROL_FOR_CORDINATIRS, LogMessage.REQUEST_COMPLETED);			
		}
	}
}
