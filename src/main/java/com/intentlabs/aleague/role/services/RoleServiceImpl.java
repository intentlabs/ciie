/**
 * 
 */
package com.intentlabs.aleague.role.services;

import java.util.List;

import org.apache.tools.ant.types.resources.Restrict;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.rights.Role;
import com.intentlabs.common.service.AbstractService;

/**
 * @author Dhruvang
 *
 */
public class RoleServiceImpl extends AbstractService<Role> implements RoleService {

	
	private static final long serialVersionUID = 6553698107249592738L;

	@Override
	public Class<Role> getModelClass() {
		return Role.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<Role> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(Role model, Criteria commonCriteria) {
		return null;
	}

	@Override
	public List<Role> fetchRoleforDropdown() {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.in("id", new Long[] {2l,3l,4l}));
		return criteria.list();
	}
	
	@Override
	public List<Role> fetchRoleforCordinator() {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.in("id", new Long[] {2l,3l,4l,5l,6l}));
		return criteria.list();
	}

	@Override
	public Role getRoleByName(String name) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("txtName", name));
		return (Role) criteria.uniqueResult();
	}
	
	

}
