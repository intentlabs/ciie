/**
 * 
 */
package com.intentlabs.aleague.role.services;

import java.util.List;

import com.intentlabs.aleague.rights.Role;
import com.intentlabs.common.service.BaseService;

/**
 * @author Dhruvang
 *
 */
public interface RoleService extends BaseService<Role>  {
	
	public List<Role> fetchRoleforDropdown();
	public List<Role> fetchRoleforCordinator();
	
	public Role getRoleByName(String name);
	

}
