/**
 * 
 */
package com.intentlabs.aleague.role.view;

import com.intentlabs.common.view.ArciveView;

/**
 * @author Dhruvang
 *
 */
public class RoleView extends ArciveView {

	
	private static final long serialVersionUID = -357076072802968506L;
	
    private String name;



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
