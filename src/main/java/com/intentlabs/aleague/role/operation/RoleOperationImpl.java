
package com.intentlabs.aleague.role.operation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.rights.Role;
import com.intentlabs.aleague.role.services.RoleService;
import com.intentlabs.aleague.role.view.RoleView;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.PageResultResponse;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.view.KeyValueView;

/**
 * @author Dhruvang
 *
 */

@Component(value = "roleOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class RoleOperationImpl extends AbstractOperation<Role, RoleView> implements RoleOperation {

	@Autowired
	private RoleService roleService;
	
	@Override
	public Role toModel(Role model, RoleView view) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Role getNewModel(RoleView view) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RoleView fromModel(Role model) {
		RoleView roleView = new RoleView();
		roleView.setId(model.getId());
		roleView.setName(model.getTxtName());
		return roleView;
	}

	@Override
	public BaseService getService() {
		return roleService;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void checkInactive(Role model) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Response getAllRoles() {

		List<Role> roles = roleService.fetchRoleforDropdown();
		List<KeyValueView> roleList = new ArrayList<>();
		for (Role role : roles) {
			roleList.add(KeyValueView.create(role.getId(), role.getTxtName()));
		}
	
		return PageResultResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Get All roles is Successfull", roleList.size(), roleList);
	}

	@Override
	public Response getRolesForCordinator() {
		
		List<Role> roles = roleService.fetchRoleforCordinator();
		List<KeyValueView> roleList = new ArrayList<>();
		for (Role role : roles) {
			roleList.add(KeyValueView.create(role.getId(), role.getTxtName()));
		}
	
		return PageResultResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Get roles for cordinator", roleList.size(), roleList);
	}
		
}
