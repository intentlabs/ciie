/**
 * 
 */
package com.intentlabs.aleague.role.operation;

import com.intentlabs.aleague.role.view.RoleView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * @author Dhruvang
 *
 */
public interface RoleOperation extends BaseOperation<RoleView> {
	
	Response getAllRoles();
	Response getRolesForCordinator();

}
