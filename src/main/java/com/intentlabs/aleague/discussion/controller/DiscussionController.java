
package com.intentlabs.aleague.discussion.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.discussion.view.DiscussionView;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface DiscussionController extends WebController<DiscussionView>
{	
	/*@RequestMapping(value = "/saveblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response saveBlogPost(@ModelAttribute BlogPostView view) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/getallblogposts.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getAllBlogPosts(@RequestBody (required=false) BlogPostView blogPost) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/followblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response followBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/likeblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response likeBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/search.htm", method = RequestMethod.GET)
	@ResponseBody
	Response search(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end,
			@RequestParam(required = false, value="tags") String tags, @RequestParam(required = false, value="orderType") String orderType) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/deleteblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response deleteBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception;*/
	
	@RequestMapping(value = "/addQuestion.htm", method = RequestMethod.POST)
	@ResponseBody
	Response addQuestion(@RequestBody DiscussionView discussionView) throws Exception;
	
	@RequestMapping(value = "/addAnswer.htm", method = RequestMethod.POST)
	@ResponseBody
	Response addAnswer(@RequestBody DiscussionView discussionView) throws Exception;
	
	@RequestMapping(value = "/reportQuestion.htm", method = RequestMethod.POST)
	@ResponseBody
	Response reportQuestion(@RequestBody DiscussionView discussionView) throws Exception;
	
	@RequestMapping(value = "/reportAnswer.htm", method = RequestMethod.POST)
	@ResponseBody
	Response reportAnswer(@RequestBody DiscussionView discussionView) throws Exception;
	
	@RequestMapping(value = "/search.htm", method = RequestMethod.GET)
	@ResponseBody
	Response search(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/view.htm", method = RequestMethod.POST)
	@ResponseBody
	Response view(@RequestBody DiscussionView discussionView) throws Exception;
	
	@RequestMapping(value = "/like.htm", method = RequestMethod.POST)
	@ResponseBody
	Response like(@RequestBody DiscussionView discussionView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/deleteQuestion.htm", method = RequestMethod.POST)
	@ResponseBody
	Response deleteQuestion(@RequestBody DiscussionView discussionView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/deleteAnswer.htm", method = RequestMethod.POST)
	@ResponseBody
	Response deleteAnswer(@RequestBody DiscussionView discussionView) throws InvalidDataException, Exception;
}
