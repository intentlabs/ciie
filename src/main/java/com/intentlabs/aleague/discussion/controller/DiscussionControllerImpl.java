
package com.intentlabs.aleague.discussion.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.intentlabs.aleague.discussion.operation.DiscussionOperation;
import com.intentlabs.aleague.discussion.view.DiscussionView;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Blog Post to handle request.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Controller
@RequestMapping("/discussion")
public class DiscussionControllerImpl extends AbstractController<DiscussionView> implements DiscussionController
{
	@Autowired
	private DiscussionOperation discussionOperation;
	
	@Override
	public Response addQuestion(@RequestBody DiscussionView discussionView) throws Exception {
		return discussionOperation.doAddQuestion(discussionView);
	}

	@Override
	public BaseOperation<DiscussionView> getOperation() {
		return discussionOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void isValidSaveData(DiscussionView webView) throws Exception {
		return;
	}

	@Override
	public Response reportQuestion(@RequestBody DiscussionView discussionView) throws Exception {
		return discussionOperation.doReportQuestion(discussionView);
	}

	@Override
	public Response reportAnswer(@RequestBody DiscussionView discussionView) throws Exception {
		return discussionOperation.doReportAnswer(discussionView);
	}

	@Override
	public Response search(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception {
		if(start == null){
			start = 0;
		}
		if(end == null){
			end = 9;
		}
		return discussionOperation.doSearch(start, end);
	}

	@Override
	public Response addAnswer(@RequestBody DiscussionView discussionView) throws Exception {
		return discussionOperation.doAddAnswer(discussionView);
	}

	@Override
	public Response view(@RequestBody DiscussionView discussionView) throws Exception {
		return discussionOperation.doView(discussionView.getId());
	}

	@Override
	public Response like(@RequestBody DiscussionView discussionView) throws InvalidDataException, Exception{
		return discussionOperation.doLike(discussionView.getId());
	}

	@Override
	public Response deleteQuestion(@RequestBody DiscussionView discussionView) throws InvalidDataException, Exception {
		return discussionOperation.doDeleteQuestion(discussionView.getId());
	}

	@Override
	public Response deleteAnswer(@RequestBody DiscussionView discussionView) throws InvalidDataException, Exception {
		return discussionOperation.doDeleteAnswer(discussionView.getId());
	}

	/*@Autowired
	BlogPostOperation blogPostOperation;
	
	@Override
	public BaseOperation<BlogPostView> getOperation() {
		return blogPostOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(BlogPostView webView) throws Exception {

		
	}

	@Override
	public Response getBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception {
		return blogPostOperation.doDisplayOperation(blogPost.getId());
	}

	@Override
	public Response getAllBlogPosts(@RequestBody (required=false) BlogPostView blogPost) throws InvalidDataException, Exception {
		if(blogPost != null){
			return blogPostOperation.doDisplayAllOperation(blogPost.getTags(), blogPost.getOrderType());	
		}else{
			return blogPostOperation.doDisplayAllOperation(null, null);
		}		
	}

	@Override
	public Response followBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception {
		try{
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.FOLLOW_BLOG_POST, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.FOLLOW_BLOG_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return blogPostOperation.doFollowOperation(blogPost.getId());
		}
		finally {
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.FOLLOW_BLOG_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response likeBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception {
		try{
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.LIKE_BLOG_POST, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.LIKE_BLOG_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return blogPostOperation.doLikeOperation(blogPost.getId());
		}
		finally {
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.LIKE_BLOG_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response saveBlogPost(@ModelAttribute BlogPostView view) throws InvalidDataException, Exception {
		try{
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.SAVE_BLOG_POST, LogMessage.BEFORE_VALIDATION);
			isValidSaveData(view);
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.SAVE_BLOG_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return blogPostOperation.doSaveOperation(view);
		}
		finally {
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.SAVE_BLOG_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response search(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end,
			@RequestParam(required = false, value="tags") String tags, @RequestParam(required = false, value="orderType") String orderType) throws InvalidDataException, Exception {
		if(start == null){
			start = 0;
		}
		if(end == null){
			end = 10;
		}
		return blogPostOperation.doSearch(start, end, tags, orderType);
	}

	@Override
	public Response deleteBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception {
		try{
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.DELETE_BLOG_POST, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.DELETE_BLOG_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return blogPostOperation.doDeleteOperation(blogPost.getId());
		}
		finally {
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.DELETE_BLOG_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}*/
}
