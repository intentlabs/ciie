
package com.intentlabs.aleague.discussion.operation;

import com.intentlabs.aleague.discussion.view.DiscussionView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is operation for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface DiscussionOperation extends BaseOperation<DiscussionView> {

	Response doAddQuestion(DiscussionView discussionView) throws Exception;
	
	Response doReportQuestion(DiscussionView discussionView) throws Exception;
	
	Response doReportAnswer(DiscussionView discussionView) throws Exception;
	
	Response doAddAnswer(DiscussionView discussionView)  throws Exception;
	
	Response doSearch(int start, int end) throws Exception;
	
	Response doView(Long id) throws Exception;
	
	Response doLike(Long id) throws Exception;
	
	Response doDeleteQuestion(Long id) throws Exception;
	
	Response doDeleteAnswer(Long id) throws Exception;
	/*Response doDisplayOperation(Long id);

	Response doDisplayAllOperation(String tags, String ordertype);
	
	Response doFollowOperation(Long id);
	
	Response doLikeOperation(Long id);
	
	Response doSearch(int start, int end, String tags, String orderType);
	
	Response doDeleteOperation(Long blogPostId);*/
}
