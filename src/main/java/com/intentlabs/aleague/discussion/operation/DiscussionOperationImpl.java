
package com.intentlabs.aleague.discussion.operation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.discussion.enums.AppropriateEnum;
import com.intentlabs.aleague.discussion.model.AnswerModel;
import com.intentlabs.aleague.discussion.model.QuestionLikeModel;
import com.intentlabs.aleague.discussion.model.QuestionModel;
import com.intentlabs.aleague.discussion.service.AnswerService;
import com.intentlabs.aleague.discussion.service.QuestionLikeService;
import com.intentlabs.aleague.discussion.service.QuestionService;
import com.intentlabs.aleague.discussion.view.AnswerView;
import com.intentlabs.aleague.discussion.view.DiscussionView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.model.PageModel;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;

/**
 * This is operation for Event to handle request.
 * 
 * @author Dhruvang Joshi.
 * @version 1.0
 * @since 14/07/2017
 */

@Component(value = "discussionOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class DiscussionOperationImpl extends AbstractOperation<QuestionModel, DiscussionView> implements DiscussionOperation {

	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private QuestionLikeService questionLikeService;
	
	@Override
	public Response doAddQuestion(DiscussionView discussionView) throws Exception {
		QuestionModel question = toModel(getNewModel(discussionView), discussionView);
		questionService.create(question);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}

	@Override
	public QuestionModel toModel(QuestionModel question, DiscussionView discussionView) throws Exception {
		question.setAppropriate(AppropriateEnum.APPROPRIATE.getId());
		question.setQuestion(discussionView.getQuestion());
		return question;
	}

	@Override
	public Response doReportQuestion(DiscussionView discussionView) throws Exception {
		QuestionModel questionModel = questionService.get(discussionView.getId());
		if(questionModel == null){
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), ResponseCode.INVALID_REQUEST.getMessage());
		}
		if(AppropriateEnum.INAPPROPRIATE.getId().equals(questionModel.getAppropriate())){
			questionModel.setModeratedBy(Auditor.getAuditor());
			questionModel.setModeratedDate(DateUtility.getCurrentDate());
			questionModel.setAppropriate(AppropriateEnum.APPROPRIATE.getId());
		}else{
			questionModel.setAppropriate(AppropriateEnum.INAPPROPRIATE.getId());
			questionModel.setReportBy(Auditor.getAuditor());
			questionModel.setReportDate(DateUtility.getCurrentDate());
		}
		questionService.update(questionModel);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}
	
	@Override
	protected QuestionModel getNewModel(DiscussionView view) {
		return new QuestionModel();
	}

	@Override
	public DiscussionView fromModel(QuestionModel questionModel) {
		DiscussionView discussionView = new DiscussionView();
		discussionView.setCreateByUserName(questionModel.getCreateBy().getTxtFirstName() + " "+questionModel.getCreateBy().getTxtLastName());
		if (questionModel.getCreateBy().getTxtProfilePicPath() != null && !"".equals(questionModel.getCreateBy().getTxtProfilePicPath())) {
			discussionView.setProfilePicPath(FileUtility.setPath(questionModel.getCreateBy().getTxtProfilePicPath()));
		} else {
			discussionView.setProfilePicPath(FileUtility.setPath("defaultuserprofilepic.PNG"));
		}
		discussionView.setCreateDate(DateUtility.toDateStringForDiscussion(questionModel.getCreateDate()));
		discussionView.setId(questionModel.getId());
		discussionView.setVersion(questionModel.getLockVersion());
		discussionView.setQuestion(questionModel.getQuestion());
		discussionView.setIsAppropriate(questionModel.getAppropriate());
		long totalAnswer = answerService.getCount(questionModel.getId());
		discussionView.setTotalAnswer(totalAnswer);
		
		List<AnswerView> anserViewList = new ArrayList<>();
		List<AnswerModel> answerModelList = answerService.getLatestAnswerByLimit(2, questionModel.getId(), (int)totalAnswer);
		for(AnswerModel answerModel : answerModelList){
			AnswerView answerView = new AnswerView();
			
			if (answerModel.getCreateBy().getTxtProfilePicPath() != null && !"".equals(answerModel.getCreateBy().getTxtProfilePicPath())) {
				answerView.setProfilePicPath(FileUtility.setPath(answerModel.getCreateBy().getTxtProfilePicPath()));
			} else {
				answerView.setProfilePicPath(FileUtility.setPath("defaultuserprofilepic.PNG"));
			}
			
			//answerView.setProfilePicPath(FileUtility.setPath(answerModel.getCreateBy().getTxtProfilePicPath()));
			answerView.setCreateByUserName(answerModel.getCreateBy().getTxtFirstName() + " "+answerModel.getCreateBy().getTxtLastName());
			answerView.setCreateDate(DateUtility.toDateStringForDiscussion(answerModel.getCreateDate()));
			answerView.setId(answerModel.getId());
			Boolean canEditDelete = false;
			if (Auditor.getAuditor().getId().longValue() == answerModel.getCreateBy().getId().longValue()) {
				canEditDelete = true;
			}
			answerView.setCanEditDelete(canEditDelete);
			discussionView.setVersion(answerModel.getLockVersion());
			answerView.setAnswer(answerModel.getAnswer());
			answerView.setIsAppropriate(answerModel.getAppropriate());
			anserViewList.add(answerView);
		}
		discussionView.setAnswerViewList(anserViewList);
	
		Boolean canEditDelete = false;
		if (Auditor.getAuditor().getId().longValue() == questionModel.getCreateBy().getId().longValue()) {
			canEditDelete = true;
		}
		discussionView.setCanEditDelete(canEditDelete);
		discussionView.setTotalLike(questionModel.getCountLikes());
		QuestionLikeModel questionLikeModel = questionLikeService.getByQuestionAndUser(questionModel.getId());
		if(questionLikeModel != null){
			discussionView.setIsLiked(true);
		}else{
			discussionView.setIsLiked(false);
		}
		
		return discussionView;
		
	}

	@Override
	public BaseService getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void checkInactive(QuestionModel model) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Response doReportAnswer(DiscussionView discussionView) throws Exception {
		AnswerModel answerModel = answerService.get(discussionView.getAnswerId());
		if(answerModel == null){
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), ResponseCode.INVALID_REQUEST.getMessage());
		}
		if(AppropriateEnum.INAPPROPRIATE.getId().equals(answerModel.getAppropriate())){
			answerModel.setModeratedBy(Auditor.getAuditor());
			answerModel.setModeratedDate(DateUtility.getCurrentDate());
			answerModel.setAppropriate(AppropriateEnum.APPROPRIATE.getId());
		}else{
			answerModel.setAppropriate(AppropriateEnum.INAPPROPRIATE.getId());
			answerModel.setReportBy(Auditor.getAuditor());
			answerModel.setReportDate(DateUtility.getCurrentDate());	
		}
		answerService.update(answerModel);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}

	@Override
	public Response doAddAnswer(DiscussionView discussionView) throws Exception {
		QuestionModel questionModel = questionService.get(discussionView.getId());
		AnswerModel answerModel = new AnswerModel();
		answerModel.setAppropriate(AppropriateEnum.APPROPRIATE.getId());
		answerModel.setQuestion(questionModel);
		answerModel.setAnswer(discussionView.getAnswer());
		answerService.create(answerModel);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}

	@Override
	public Response doSearch(int start, int end) {
		PageModel pageModel = questionService.search(new QuestionModel(), start, end);
		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), "Question List",
				pageModel.getRecords(), fromModelList((List<QuestionModel>) pageModel.getList()));
	}

	@Override
	public Response doView(Long id) throws Exception {
		QuestionModel questionModel = questionService.get(id);
		DiscussionView discussionView = view(questionModel);
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage(), discussionView);
	}
	
	public DiscussionView view(QuestionModel questionModel){
		DiscussionView discussionView = new DiscussionView();
		discussionView.setCreateByUserName(questionModel.getCreateBy().getTxtFirstName() + " "+questionModel.getCreateBy().getTxtLastName());
		discussionView.setProfilePicPath(FileUtility.setPath(questionModel.getCreateBy().getTxtProfilePicPath()));
		discussionView.setCreateDate(DateUtility.toDateStringForDiscussion(questionModel.getCreateDate()));
		discussionView.setId(questionModel.getId());
		discussionView.setVersion(questionModel.getLockVersion());
		discussionView.setQuestion(questionModel.getQuestion());
		discussionView.setIsAppropriate(questionModel.getAppropriate());
		long totalAnswer = answerService.getCount(questionModel.getId());
		discussionView.setTotalAnswer(totalAnswer);
		
		List<AnswerView> anserViewList = new ArrayList<>();
		
		List<AnswerModel> answerModelList = answerService.getAnswerByQuestion(questionModel.getId());
		for(AnswerModel answerModel : answerModelList){
			AnswerView answerView = new AnswerView();
			answerView.setProfilePicPath(FileUtility.setPath(answerModel.getCreateBy().getTxtProfilePicPath()));
			answerView.setCreateByUserName(answerModel.getCreateBy().getTxtFirstName() + " "+answerModel.getCreateBy().getTxtLastName());
			answerView.setCreateDate(DateUtility.toDateStringForDiscussion(answerModel.getCreateDate()));
			answerView.setId(answerModel.getId());
			discussionView.setVersion(answerModel.getLockVersion());
			Boolean canEditDelete = false;
			if (Auditor.getAuditor().getId().longValue() == answerModel.getCreateBy().getId().longValue()) {
				canEditDelete = true;
			}
			answerView.setCanEditDelete(canEditDelete);
			answerView.setAnswer(answerModel.getAnswer());
			answerView.setIsAppropriate(answerModel.getAppropriate());
			anserViewList.add(answerView);
		}
		discussionView.setAnswerViewList(anserViewList);
		Boolean canEditDelete = false;
		if (Auditor.getAuditor().getId().longValue() == questionModel.getCreateBy().getId().longValue()) {
			canEditDelete = true;
		}
		discussionView.setCanEditDelete(canEditDelete);
		discussionView.setTotalLike(questionModel.getCountLikes());
		QuestionLikeModel questionLikeModel = questionLikeService.getByQuestionAndUser(questionModel.getId());
		if(questionLikeModel != null){
			discussionView.setIsLiked(true);
		}else{
			discussionView.setIsLiked(false);
		}
		return discussionView;
	}

	@Override
	public Response doLike(Long id) throws Exception {
		QuestionLikeModel questionLikeModel = questionLikeService.getByQuestionAndUser(id);
		if (questionLikeModel == null) {
			QuestionModel questionModel = questionService.get(id);
			questionLikeModel = new QuestionLikeModel();
			questionLikeModel.setId(id);
			questionLikeModel.setLike(1);
			questionLikeModel.setQuestionModel(questionModel);
			questionLikeModel.setUser(Auditor.getAuditor());
			questionLikeService.create(questionLikeModel);
			return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
		} else {
			return CommonResponse.create(ResponseCode.ALREADY_EXISTS.getCode(), "Question is already liked");
		}
	}

	@Override
	public Response doDeleteQuestion(Long id) throws Exception {
		QuestionModel questionModel = questionService.get(id);
		questionModel.setEnumArchive("0");
		questionModel.setDateArchive(DateUtility.getCurrentDate());
		questionModel.setFkArchiveBy(Auditor.getAuditor());
		questionService.update(questionModel);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}

	@Override
	public Response doDeleteAnswer(Long id) throws Exception {
		AnswerModel answerModel = answerService.get(id);
		answerModel.setEnumArchive("0");
		answerModel.setDateArchive(DateUtility.getCurrentDate());
		answerModel.setFkArchiveBy(Auditor.getAuditor());
		answerService.update(answerModel);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}

	/*private static final long serialVersionUID = 741391010890205612L;

	@Autowired
	private BlogPostService blogPostService;

	@Autowired
	private BlogPostTagService blogPostTagService;

	@Autowired
	private BlogPostUserLikeService blogPostUserLikeService;

	@Autowired
	private BlogPostUserFollowService blogPostUserFollowService;

	@Autowired
	private TagService tagService;

	@Override
	public BaseService getService() {
		return blogPostService;
	}

	@Override
	protected BlogPostModel getNewModel(BlogPostView view) {
		BlogPostModel model = new BlogPostModel();
		model.setCreateDate(DateUtility.getCurrentDate());
		return model;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		return null;
	}

	@Override
	protected void checkInactive(BlogPostModel model) throws Exception {
	}

	@Override
	public BlogPostModel toModel(BlogPostModel blogPostModel, BlogPostView blogPostView) throws Exception {
		if (blogPostView.getId() == null || blogPostView.getId().longValue() <= 0) {
			blogPostModel.setCreateBy(Auditor.getAuditor());
			blogPostModel.setActivationStatus(ActiveInActive.ACTIVE);
			blogPostModel.setActivationDate(DateUtility.getCurrentDate());
			blogPostModel.setEnumArchive("1");
			blogPostModel.setCountFollowers(0l);
			blogPostModel.setCountLikes(0l);
		} else {
			blogPostModel.setId(blogPostView.getId());
		}

		blogPostModel.setUpdateBy(Auditor.getAuditor());
		blogPostModel.setUpdateDate(DateUtility.getCurrentDate());

		blogPostModel.setTxtAuthor(blogPostView.getAuthor());
		blogPostModel.setTxtBlogTitle(blogPostView.getBlogTitle());
		blogPostModel.setTxtDescription(blogPostView.getDescription());

		if (blogPostView.getLinkblog().startsWith("https:") || blogPostView.getLinkblog().startsWith("http:")) {
			blogPostModel.setTxtLinkBlog(blogPostView.getLinkblog());
		} else {
			blogPostModel.setTxtLinkBlog(Constant.HTTP_URL + blogPostView.getLinkblog());
		}

		blogPostModel
				.setDatePostedOn(DateUtility.toDate(blogPostView.getPostedOn(), new SimpleDateFormat("dd-MM-yyyy")));

		if (blogPostView.getFile() != null) {
			blogPostModel.setTxtPostFilePath(FileUtility.storeFile(blogPostView.getFile(), "blogpost"));
		} else if (blogPostModel.getTxtPostFilePath() == null) {
			blogPostModel.setTxtPostFilePath("");
		}

		Set<BlogPostTagModel> setBlogPostTagModel = new HashSet<>();

		Map<Long, BlogPostTagModel> mapTagModel = new HashMap<>();

		if (blogPostModel.getSetBlogPostTagModel() != null && !blogPostModel.getSetBlogPostTagModel().isEmpty()) {
			for (BlogPostTagModel blogPostTag : blogPostModel.getSetBlogPostTagModel()) {
				mapTagModel.put(blogPostTag.getTag().getId(), blogPostTag);
			}
		}

		if (blogPostView.getTags() != null && !"".equals(blogPostView.getTags())) {
			JSONArray jsonArrayBlogPostTag = new JSONArray(blogPostView.getTags());

			List<TagModel> listTags = tagService.getAllTags(null);

			for (Integer i = 0; i < jsonArrayBlogPostTag.length(); i++) {
				Long tagId = jsonArrayBlogPostTag.getLong(i);

				BlogPostTagModel blogPostTag;
				if (mapTagModel.containsKey(tagId)) {
					blogPostTag = mapTagModel.get(tagId);

					if (blogPostTag.getActivationStatus().getId() == ActiveInActive.INACTIVE.getId()) {
						blogPostTag.setActivationStatus(ActiveInActive.ACTIVE);
					}

					mapTagModel.remove(tagId);
				} else {
					blogPostTag = new BlogPostTagModel();
					blogPostTag.setCreateBy(Auditor.getAuditor());
					blogPostTag.setCreateDate(DateUtility.getCurrentDate());
					blogPostTag.setActivationStatus(ActiveInActive.ACTIVE);
					blogPostTag.setActivationChangeBy(Auditor.getAuditor());
					blogPostTag.setActivationDate(DateUtility.getCurrentDate());
					blogPostTag.setEnumArchive("1");

					for (TagModel instanceTag : listTags) {
						if (instanceTag.getId().longValue() == tagId.longValue()) {
							blogPostTag.setTag(instanceTag);
							break;
						}
					}
				}

				blogPostTag.setUpdateBy(Auditor.getAuditor());
				blogPostTag.setUpdateDate(DateUtility.getCurrentDate());

				setBlogPostTagModel.add(blogPostTag);
			}
		}

		if (!mapTagModel.keySet().isEmpty()) {
			for (Long key : mapTagModel.keySet()) {
				BlogPostTagModel blogPostTag = mapTagModel.get(key);

				if (blogPostTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					blogPostTag.setActivationStatus(ActiveInActive.INACTIVE);
					blogPostTag.setActivationChangeBy(Auditor.getAuditor());
					blogPostTag.setActivationDate(DateUtility.getCurrentDate());
					setBlogPostTagModel.add(blogPostTag);
				}
			}
		}

		blogPostModel.setSetBlogPostTagModel(setBlogPostTagModel);

		return blogPostModel;
	}

	@Override
	public BlogPostView fromModel(BlogPostModel blogPostModel) {
		BlogPostView blogPostView = new BlogPostView();
		blogPostView.setId(blogPostModel.getId());
		blogPostView.setAuthor(blogPostModel.getTxtAuthor());
		blogPostView.setBlogTitle(blogPostModel.getTxtBlogTitle());
		blogPostView.setDescription(blogPostModel.getTxtDescription());
		blogPostView.setLinkblog(blogPostModel.getTxtLinkBlog());
		blogPostView.setPostFilePath(FileUtility.setPath(blogPostModel.getTxtPostFilePath()));

		Set<TagView> setTags = new HashSet<>();
		if (blogPostModel.getSetBlogPostTagModel() != null && !blogPostModel.getSetBlogPostTagModel().isEmpty()) {
			for (BlogPostTagModel blogPostTag : blogPostModel.getSetBlogPostTagModel()) {
				if (blogPostTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					TagView tagView = new TagView();
					tagView.setId(blogPostTag.getTag().getId());
					tagView.setTagName(blogPostTag.getTag().getTxtTagName());
					setTags.add(tagView);
				}
			}
		}

		blogPostView.setSettags(setTags);
		blogPostView.setPostedOn(
				DateUtility.toDateTimeString(blogPostModel.getDatePostedOn(), new SimpleDateFormat("dd-MM-yyyy")));

		blogPostView.setCountFollowers(blogPostModel.getCountFollowers());
		blogPostView.setCountLikes(blogPostModel.getCountLikes());

		return blogPostView;
	}

	@Override
	public Response doSaveOperation(BlogPostView view) throws Exception {
		BlogPostModel model = null;

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = getNewModel(view);
		} else {
			model = blogPostService.getById(view.getId());
		}

		model = toModel(model, view);

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = blogPostService.create(model);
		} else {
			blogPostService.update(model);
		}

		for (BlogPostTagModel blogPostTag : model.getSetBlogPostTagModel()) {
			if (blogPostTag.getBlogPost() == null) {
				blogPostTag.setBlogPost(model);
				blogPostTagService.create(blogPostTag);
			} else {
				blogPostTagService.update(blogPostTag);
			}
		}

		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Blog Post has been saved.");
	}

	@Override
	public Response doDisplayOperation(Long id) {
		BlogPostModel model = blogPostService.load(id);
		BlogPostView blogPostView = fromModel(model);
		
		Boolean canEditDelete = false;			
		if(Auditor.getAuditor().getId().longValue() == model.getCreateBy().getId().longValue()){
			canEditDelete = true;
		}			
		blogPostView.setCanEditDelete(canEditDelete);

		BlogPostUserFollowModel follow = blogPostUserFollowService.getByBlogPostAndUser(id);

		if (follow != null) {
			blogPostView.setIsFollowed(true);
		} else {
			blogPostView.setIsFollowed(false);
		}

		BlogPostUserLikeModel like = blogPostUserLikeService.getByBlogPostAndUser(id);

		if (like != null) {
			blogPostView.setIsLiked(true);
		} else {
			blogPostView.setIsLiked(false);
		}

		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_BLOG_POST, blogPostView);
	}

	@Override
	public Response doDisplayAllOperation(String tags, String ordertype) {
		List<BlogPostModel> listBlogPosts = blogPostService.getAllBlogPosts(tags, ordertype);

		List<View> listBlogViews = new ArrayList<>();

		List<BlogPostUserFollowModel> listBlogPostUserFollow = blogPostUserFollowService.getAllByUser();

		List<BlogPostUserLikeModel> listBlogPostUserLike = blogPostUserLikeService.getAllByUser();

		for (BlogPostModel model : listBlogPosts) {
			BlogPostView blogPostView = fromModel(model);
			
			Boolean canEditDelete = false;			
			if(Auditor.getAuditor().getId().longValue() == model.getCreateBy().getId().longValue()){
				canEditDelete = true;
			}			
			blogPostView.setCanEditDelete(canEditDelete);

			Boolean isFollowed = false;
			for (BlogPostUserFollowModel follow : listBlogPostUserFollow) {
				if (follow.getBlogPost().getId() == model.getId()) {
					isFollowed = true;
					break;
				}
			}
			blogPostView.setIsFollowed(isFollowed);

			Boolean isLiked = false;
			for (BlogPostUserLikeModel follow : listBlogPostUserLike) {
				if (follow.getBlogPost().getId() == model.getId()) {
					isLiked = true;
					break;
				}
			}
			blogPostView.setIsLiked(isLiked);

			listBlogViews.add(blogPostView);
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_BLOG_POSTS,
				listBlogViews.size(), listBlogViews);
	}

	@Override
	public Response doFollowOperation(Long id) {
		BlogPostUserFollowModel model = blogPostUserFollowService.getByBlogPostAndUser(id);

		if (model == null) {
			BlogPostModel blogPost = blogPostService.getById(id);
			model = new BlogPostUserFollowModel();
			model.setCreateBy(Auditor.getAuditor());
			model.setActivationStatus(ActiveInActive.ACTIVE);
			model.setActivationDate(DateUtility.getCurrentDate());
			model.setEnumArchive("1");
			model.setUpdateBy(Auditor.getAuditor());
			model.setUpdateDate(DateUtility.getCurrentDate());
			model.setBlogPost(blogPost);
			model.setBlogPostFollow(EnumBlogPostFollow.YES);
			model.setUser(Auditor.getAuditor());
			blogPostUserFollowService.create(model);

			return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Blog Post is followed successfully.");
		} else {
			return CommonResponse.create(ResponseCode.ALREADY_EXISTS.getCode(), "Blog Post already followed.");
		}
	}

	@Override
	public Response doLikeOperation(Long id) {
		BlogPostUserLikeModel model = blogPostUserLikeService.getByBlogPostAndUser(id);

		if (model == null) {
			BlogPostModel blogPost = blogPostService.getById(id);
			model = new BlogPostUserLikeModel();
			model.setCreateBy(Auditor.getAuditor());
			model.setActivationStatus(ActiveInActive.ACTIVE);
			model.setActivationDate(DateUtility.getCurrentDate());
			model.setEnumArchive("1");
			model.setUpdateBy(Auditor.getAuditor());
			model.setUpdateDate(DateUtility.getCurrentDate());
			model.setBlogPost(blogPost);
			model.setBlogPostLike(EnumBlogPostLike.YES);
			model.setUser(Auditor.getAuditor());
			blogPostUserLikeService.create(model);

			return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Blog Post is liked successfully.");
		} else {
			return CommonResponse.create(ResponseCode.ALREADY_EXISTS.getCode(), "Blog Post already liked.");
		}
	}

	@Override
	public Response doSearch(int start, int end, String tags, String orderType) {
		BlogPostModel blogPostModel = new BlogPostModel();
		PageModel pageModel = blogPostService.search(new BlogPostModel(), start, end);
		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_BLOG_POSTS,
				pageModel.getRecords(), fromModelList((List<BlogPostModel>) pageModel.getList()));
	}
	
	@Override
	public Response doDeleteOperation(Long blogPostId){
		BlogPostModel model= blogPostService.getById(blogPostId);
		model.setActivationStatus(ActiveInActive.INACTIVE);
		model.setActivationDate(DateUtility.getCurrentDate());
		model.setActivationChangeBy(Auditor.getAuditor());
		blogPostService.update(model);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Blog Post has been deleted.");
	}*/
}
