
package com.intentlabs.aleague.discussion.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.discussion.enums.AppropriateEnum;
import com.intentlabs.aleague.discussion.model.AnswerModel;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class AnswerServiceImpl extends AbstractService<AnswerModel> implements AnswerService
{

	private static final long serialVersionUID = -5554281255091289184L;

	@Override
	public Class<AnswerModel> getModelClass() {
		return AnswerModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<AnswerModel> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(AnswerModel answerModel, Criteria commonCriteria) {
		return commonCriteria;
	}
	
	@Override
	public long getCount(long questionId){
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("question.id", questionId));
		criteria.add(Restrictions.eq("enumArchive", "1"));
		return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
	}
	
	@Override
	public List<AnswerModel> getLatestAnswerByLimit(int limit, long questionId, int totalRecord) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("question.id", questionId));
		criteria.add(Restrictions.eq("enumArchive", "1"));
		if(Auditor.getAuditor().getFkRoleId().getId() == 1 || Auditor.getAuditor().getFkRoleId().getId() == 5 ||
				Auditor.getAuditor().getFkRoleId().getId() == 6){
						
		}else{
			criteria.add(Restrictions.eq("appropriate", AppropriateEnum.APPROPRIATE.getId()));	
		}
		criteria.setFirstResult(totalRecord - limit);
		criteria.setMaxResults(limit);
		return (List<AnswerModel>) criteria.list();
	}

	@Override
	public List<AnswerModel> getAnswerByQuestion(long questionId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("question.id", questionId));
		criteria.add(Restrictions.eq("enumArchive", "1"));
		if(Auditor.getAuditor().getFkRoleId().getId() == 1 || Auditor.getAuditor().getFkRoleId().getId() == 5 ||
				Auditor.getAuditor().getFkRoleId().getId() == 6){
						
		}else{
			criteria.add(Restrictions.eq("appropriate", AppropriateEnum.APPROPRIATE.getId()));	
		}
		return (List<AnswerModel>) criteria.list();
	}
	
}