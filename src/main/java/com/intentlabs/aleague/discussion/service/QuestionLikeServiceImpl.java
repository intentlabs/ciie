
package com.intentlabs.aleague.discussion.service;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.discussion.model.QuestionLikeModel;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class QuestionLikeServiceImpl extends AbstractService<QuestionLikeModel> implements QuestionLikeService
{

	private static final long serialVersionUID = -5554281255091289184L;

	@Override
	public Class<QuestionLikeModel> getModelClass() {
		return QuestionLikeModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<QuestionLikeModel> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(QuestionLikeModel questionLikeModel, Criteria commonCriteria) {
		return commonCriteria;
	}

	@Override
	public QuestionLikeModel getByQuestionAndUser(Long id) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("questionModel.id", id));
		criteria.add(Restrictions.eq("user.id", Auditor.getAuditor().getId()));		
		return (QuestionLikeModel) criteria.uniqueResult();
	}
}