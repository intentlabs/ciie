
package com.intentlabs.aleague.discussion.service;

import java.util.List;

import com.intentlabs.aleague.discussion.model.AnswerModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Dhruvang Joshi.
 * @version 1.0
 * @since 14/07/2017
 */

public interface AnswerService extends BaseService<AnswerModel> {
	
	List<AnswerModel> getLatestAnswerByLimit(int limit, long questionId, int totalRecord);
	
	long getCount(long questionId);
	
	List<AnswerModel> getAnswerByQuestion(long questionId);
} 
