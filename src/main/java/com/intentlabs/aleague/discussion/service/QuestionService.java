
package com.intentlabs.aleague.discussion.service;

import com.intentlabs.aleague.discussion.model.QuestionModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface QuestionService extends BaseService<QuestionModel> {
}
