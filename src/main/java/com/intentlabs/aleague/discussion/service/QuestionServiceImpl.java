
package com.intentlabs.aleague.discussion.service;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.discussion.enums.AppropriateEnum;
import com.intentlabs.aleague.discussion.model.QuestionModel;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class QuestionServiceImpl extends AbstractService<QuestionModel> implements QuestionService
{

	private static final long serialVersionUID = -5554281255091289184L;

	@Override
	public Class<QuestionModel> getModelClass() {
		return QuestionModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<QuestionModel> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(QuestionModel question, Criteria commonCriteria) {
		if(Auditor.getAuditor().getFkRoleId().getId() == 1 || Auditor.getAuditor().getFkRoleId().getId() == 5 ||
				Auditor.getAuditor().getFkRoleId().getId() == 6){
						
		}else{
			commonCriteria.add(Restrictions.eq("appropriate", AppropriateEnum.APPROPRIATE.getId()));
		}
		
		commonCriteria.add(Restrictions.eq("enumArchive", "1"));
		return commonCriteria;
	}
}