
package com.intentlabs.aleague.discussion.service;

import com.intentlabs.aleague.discussion.model.QuestionLikeModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Dhruvang Joshi.
 * @version 1.0
 * @since 14/07/2017
 */

public interface QuestionLikeService extends BaseService<QuestionLikeModel> {
	QuestionLikeModel getByQuestionAndUser(Long id);
} 
