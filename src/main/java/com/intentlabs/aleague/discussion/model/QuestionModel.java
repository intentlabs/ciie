package com.intentlabs.aleague.discussion.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Formula;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.model.ArchiveModel;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tblQuestion")
public class QuestionModel extends ArchiveModel {
	
	private static final long serialVersionUID = -5764068071467332650L;
	
	@Column(name = "txtQuestion", nullable = false)
	private String question;
	
	@Column(name = "enumAppropriate")
	@Access(AccessType.FIELD)
	private String appropriate;
	
	@Column(name = "dateReportedDate")
	private Date reportDate;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fkReportedBy")
	private User reportBy;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fkModeratedBy")
	private User moderatedBy;
	
	@Column(name = "dateModeratedDate")
	private Date moderatedDate;
	
	@Formula("(SELECT COUNT(*) FROM tblQuestionLike e WHERE e.fkQuestionId = pkId)")
	private long countLikes;
	
	@OneToMany(cascade = CascadeType.ALL, fetch =FetchType.LAZY)
	@JoinColumn(name = "fkQuestionId")
	private Set<AnswerModel> answers;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAppropriate() {
		return appropriate;
	}

	public void setAppropriate(String appropriate) {
		this.appropriate = appropriate;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public User getReportBy() {
		return reportBy;
	}

	public void setReportBy(User reportBy) {
		this.reportBy = reportBy;
	}

	public User getModeratedBy() {
		return moderatedBy;
	}

	public void setModeratedBy(User moderatedBy) {
		this.moderatedBy = moderatedBy;
	}

	public Date getModeratedDate() {
		return moderatedDate;
	}

	public void setModeratedDate(Date moderatedDate) {
		this.moderatedDate = moderatedDate;
	}

	public Set<AnswerModel> getAnswers() {
		return answers;
	}

	public void setAnswers(Set<AnswerModel> answers) {
		this.answers = answers;
	}

	public long getCountLikes() {
		return countLikes;
	}

	public void setCountLikes(long countLikes) {
		this.countLikes = countLikes;
	}
}
