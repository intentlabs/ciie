
package com.intentlabs.aleague.discussion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.model.IdentifierModel;

/**
 * This is model for Event sponsers.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Entity
@Table(name = "tblQuestionLike")
@DynamicInsert
@DynamicUpdate
public class QuestionLikeModel extends IdentifierModel {

	private static final long serialVersionUID = -3528610774449650642L;

	@Column(name = "enumLike")
	private int like;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkQuestionId")
	private QuestionModel questionModel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkUserId")
	private User user;

	public int getLike() {
		return like;
	}

	public void setLike(int like) {
		this.like = like;
	}

	public QuestionModel getQuestionModel() {
		return questionModel;
	}

	public void setQuestionModel(QuestionModel questionModel) {
		this.questionModel = questionModel;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}