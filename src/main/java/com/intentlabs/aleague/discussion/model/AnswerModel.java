package com.intentlabs.aleague.discussion.model;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.model.ArchiveModel;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tblAnswer")
public class AnswerModel extends ArchiveModel {
	
	private static final long serialVersionUID = -5764068071467332650L;
	
	@Column(name = "txtAnswer", nullable = false)
	private String answer;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fkQuestionId", nullable=false)
	private QuestionModel question;
	
	@Column(name = "dateReportedDate")
	private Date reportDate;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fkReportedBy")
	private User reportBy;
		
	@Column(name = "enumAppropriate")
	@Access(AccessType.FIELD)
	private String appropriate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fkModeratedBy")
	private User moderatedBy;
	
	@Column(name = "dateModeratedDate")
	private Date moderatedDate;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public QuestionModel getQuestion() {
		return question;
	}

	public void setQuestion(QuestionModel question) {
		this.question = question;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public User getReportBy() {
		return reportBy;
	}

	public void setReportBy(User reportBy) {
		this.reportBy = reportBy;
	}

	public String getAppropriate() {
		return appropriate;
	}

	public void setAppropriate(String appropriate) {
		this.appropriate = appropriate;
	}

	public User getModeratedBy() {
		return moderatedBy;
	}

	public void setModeratedBy(User moderatedBy) {
		this.moderatedBy = moderatedBy;
	}

	public Date getModeratedDate() {
		return moderatedDate;
	}

	public void setModeratedDate(Date moderatedDate) {
		this.moderatedDate = moderatedDate;
	}
}
