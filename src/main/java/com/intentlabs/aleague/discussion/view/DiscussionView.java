
package com.intentlabs.aleague.discussion.view;

import java.util.List;

import com.intentlabs.common.view.ArciveView;

/**
 * This is View used to get data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class DiscussionView extends ArciveView {


	private static final long serialVersionUID = 8692674149531174388L;
	
	private String question;
	private String answer;
	private long answerId;
	private String profilePicPath;
	private long totalAnswer;
	private Boolean canEditDelete;
	private long totalLike;
	private Boolean isLiked;
	private String isAppropriate;
	
	private List<AnswerView> answerViewList;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(long answerId) {
		this.answerId = answerId;
	}

	public List<AnswerView> getAnswerViewList() {
		return answerViewList;
	}

	public void setAnswerViewList(List<AnswerView> answerViewList) {
		this.answerViewList = answerViewList;
	}

	public String getProfilePicPath() {
		return profilePicPath;
	}

	public void setProfilePicPath(String profilePicPath) {
		this.profilePicPath = profilePicPath;
	}

	public long getTotalAnswer() {
		return totalAnswer;
	}

	public void setTotalAnswer(long totalAnswer) {
		this.totalAnswer = totalAnswer;
	}

	public Boolean getCanEditDelete() {
		return canEditDelete;
	}

	public void setCanEditDelete(Boolean canEditDelete) {
		this.canEditDelete = canEditDelete;
	}

	public long getTotalLike() {
		return totalLike;
	}

	public void setTotalLike(long totalLike) {
		this.totalLike = totalLike;
	}

	public Boolean getIsLiked() {
		return isLiked;
	}

	public void setIsLiked(Boolean isLiked) {
		this.isLiked = isLiked;
	}

	public String getIsAppropriate() {
		return isAppropriate;
	}

	public void setIsAppropriate(String isAppropriate) {
		this.isAppropriate = isAppropriate;
	}
}
