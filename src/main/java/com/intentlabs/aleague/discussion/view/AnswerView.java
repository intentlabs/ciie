
package com.intentlabs.aleague.discussion.view;

import com.intentlabs.common.view.ArciveView;

/**
 * This is View used to get data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class AnswerView extends ArciveView {


	private static final long serialVersionUID = 8692674149531174388L;
	
	private String answer;
	private String profilePicPath;
	private Boolean canEditDelete;
	private String isAppropriate;
	
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getProfilePicPath() {
		return profilePicPath;
	}

	public void setProfilePicPath(String profilePicPath) {
		this.profilePicPath = profilePicPath;
	}

	public Boolean getCanEditDelete() {
		return canEditDelete;
	}

	public void setCanEditDelete(Boolean canEditDelete) {
		this.canEditDelete = canEditDelete;
	}

	public String getIsAppropriate() {
		return isAppropriate;
	}
	
	public void setIsAppropriate(String isAppropriate) {
		this.isAppropriate = isAppropriate;
	}
}
