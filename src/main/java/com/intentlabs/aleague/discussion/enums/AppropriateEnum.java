/**
 * 
 */
package com.intentlabs.aleague.discussion.enums;

/**
 * @author Dhruvang
 *
 */
public enum AppropriateEnum {
	APPROPRIATE("0", "appropriate"),
	INAPPROPRIATE("1","Inappropriate");
	
	private final String id;
    private final String name;
    
    
    AppropriateEnum(String id, String name) {
        this.id = id;
        this.name = name;
    }

	public String getId() {
	
		return id;
	}

	public String getName() {
		return name;
	}
	
	
	   public static AppropriateEnum fromId(String id) {
	        for (AppropriateEnum status : values()) {
	            if (status.getId() == id) {
	                return status;
	            }
	        }
	        return null;
	    }

}
