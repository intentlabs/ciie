package com.intentlabs.aleague.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.aleague.rights.Role;
import com.intentlabs.common.model.ArchiveModel;

@Entity
@Table(name = "tblUser", uniqueConstraints = { @UniqueConstraint(columnNames = "txtEmail")})
public class UserBasic extends ArchiveModel {


	private static final long serialVersionUID = -5764068071467332650L;
	
	
	@Column(name = "txtEmail", length = 100, nullable = false)
	private String txtEmail;
	
	@Column(name = "txtFirstName", length = 30, nullable = false)
	private String txtFirstName;
	
	@Column(name = "txtLastName", length = 30, nullable = true)
	private String txtLastName;
	
	@Column(name = "txtContactNumber", length = 12, nullable = true)
	private String txtContactNumber;
	
	@Column(name = "txtProfilePicPath", length = 100, nullable = true)
	private String txtProfilePicPath;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkInstituteId")
	private InstituteModel fkInstituteId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkRoleId")
	private Role fkRoleId;

	@Column(name = "enumApprove")
	private String enumApprove;
	
	@Column(name = "enumActTokenUsed")
	private String enumActTokenUsed;
	
	@Column(name = "enrolmentNumber")
	private String enrolmentNumber;
	
	@Column(name = "txtLinkdinLink", length = 500, nullable = true)
	private String txtLinkdinLink;
	
	@Column(name = "txtUserprofileLink", length = 1000, nullable = true)
	private String txtUserprofileLink;
	
	@Column(name = "txtAbout", length = 1000, nullable = true)
	private String about;
	
	@Column(name = "txtCourse", length = 100, nullable = true)
	private String course;
	
	@Column(name = "txtBatch", length = 100, nullable = true)
	private String batch;
	
	@Column(name = "numberBatchYear")
	private Integer numberBatchYear;
	
	public String getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(String txtEmail) {
		this.txtEmail = txtEmail;
	}

	public String getTxtFirstName() {
		return txtFirstName;
	}

	public void setTxtFirstName(String txtFirstName) {
		this.txtFirstName = txtFirstName;
	}

	public String getTxtLastName() {
		return txtLastName;
	}

	public void setTxtLastName(String txtLastName) {
		this.txtLastName = txtLastName;
	}

	public String getTxtContactNumber() {
		return txtContactNumber;
	}

	public void setTxtContactNumber(String txtContactNumber) {
		this.txtContactNumber = txtContactNumber;
	}

	public InstituteModel getFkInstituteId() {
		return fkInstituteId;
	}

	public void setFkInstituteId(InstituteModel fkInstituteId) {
		this.fkInstituteId = fkInstituteId;
	}

	public String getTxtProfilePicPath() {
		return txtProfilePicPath;
	}

	public void setTxtProfilePicPath(String txtProfilePicPath) {
		this.txtProfilePicPath = txtProfilePicPath;
	}

	public Role getFkRoleId() {
		return fkRoleId;
	}

	public void setFkRoleId(Role fkRoleId) {
		this.fkRoleId = fkRoleId;
	}

	public String getEnumApprove() {
		return enumApprove;
	}

	public void setEnumApprove(String enumApprove) {
		this.enumApprove = enumApprove;
	}

	public String getEnumActTokenUsed() {
		return enumActTokenUsed;
	}

	public void setEnumActTokenUsed(String enumActTokenUsed) {
		this.enumActTokenUsed = enumActTokenUsed;
	}

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	/**
	 * @return the about
	 */
	public String getAbout() {
		return about;
	}

	/**
	 * @param about the about to set
	 */
	public void setAbout(String about) {
		this.about = about;
	}

	/**
	 * @return the txtLinkdinLink
	 */
	public String getTxtLinkdinLink() {
		return txtLinkdinLink;
	}

	/**
	 * @param txtLinkdinLink the txtLinkdinLink to set
	 */
	public void setTxtLinkdinLink(String txtLinkdinLink) {
		this.txtLinkdinLink = txtLinkdinLink;
	}

	/**
	 * @return the txtUserprofileLink
	 */
	public String getTxtUserprofileLink() {
		return txtUserprofileLink;
	}

	/**
	 * @param txtUserprofileLink the txtUserprofileLink to set
	 */
	public void setTxtUserprofileLink(String txtUserprofileLink) {
		this.txtUserprofileLink = txtUserprofileLink;
	}

	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}

	/**
	 * @param course the course to set
	 */
	public void setCourse(String course) {
		this.course = course;
	}

	/**
	 * @return the batch
	 */
	public String getBatch() {
		return batch;
	}

	/**
	 * @param batch the batch to set
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * @return the numberBatchYear
	 */
	public Integer getNumberBatchYear() {
		return numberBatchYear;
	}

	/**
	 * @param numberBatchYear the numberBatchYear to set
	 */
	public void setNumberBatchYear(Integer numberBatchYear) {
		this.numberBatchYear = numberBatchYear;
	}
}
