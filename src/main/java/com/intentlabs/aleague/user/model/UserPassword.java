package com.intentlabs.aleague.user.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.intentlabs.common.model.IdentifierModel;


@Entity
@Table(name = "tblUserPassword")
public class UserPassword extends IdentifierModel {

	
	private static final long serialVersionUID = -9208371529796270071L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkUserId")
	private User fkUserId;
	
	@Column(name = "txtPassword")
	private String txtPassword;
	
	@Column(name = "datePasswordChange")
	private Date datePasswordChange;

	public User getFkUserId() {
		return fkUserId;
	}

	public void setFkUserId(User fkUserId) {
		this.fkUserId = fkUserId;
	}

	public String getTxtPassword() {
		return txtPassword;
	}

	public void setTxtPassword(String txtPassword) {
		this.txtPassword = txtPassword;
	}

	public Date getDatePasswordChange() {
		return datePasswordChange;
	}

	public void setDatePasswordChange(Date datePasswordChange) {
		this.datePasswordChange = datePasswordChange;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datePasswordChange == null) ? 0 : datePasswordChange.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserPassword other = (UserPassword) obj;
		if (datePasswordChange == null) {
			if (other.datePasswordChange != null)
				return false;
		} else if (!datePasswordChange.equals(other.datePasswordChange))
			return false;
		return true;
	}
}
