package com.intentlabs.aleague.user.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Formula;

import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.aleague.rights.Role;
import com.intentlabs.aleague.usersession.model.UserJsessions;
import com.intentlabs.common.model.ArchiveModel;

@Entity
@Table(name = "tblUser", uniqueConstraints = { @UniqueConstraint(columnNames = "txtEmail")})
public class User extends ArchiveModel {


	private static final long serialVersionUID = -5764068071467332650L;
	
	
	@Column(name = "txtEmail", length = 100, nullable = false)
	private String txtEmail;
	
	@Column(name = "txtFirstName", length = 30, nullable = false)
	private String txtFirstName;
	
	@Column(name = "txtLastName", length = 30, nullable = true)
	private String txtLastName;
	
	@Column(name = "txtProfilePicPath", length = 100, nullable = true)
	private String txtProfilePicPath;
	
	@Column(name = "txtContactNumber", length = 12, nullable = true)
	private String txtContactNumber;
	
	@Column(name = "numberBatchYear")
	private Integer numberBatchYear;
	
	@Column(name = "enrolmentNumber")
	private String enrolmentNumber;
	
	@Column(name = "txtLinkdinLink", length = 500, nullable = true)
	private String txtLinkdinLink;
	
	@Column(name = "txtUserprofileLink", length = 1000, nullable = true)
	private String txtUserprofileLink;
	
	@Column(name = "txtAbout", length = 1000, nullable = true)
	private String about;
	
	@Column(name = "txtCourse", length = 100, nullable = true)
	private String course;
	
	@Column(name = "txtBatch", length = 100, nullable = true)
	private String batch;
	
	@Column(name = "txtActToken", length = 32, nullable = false)
	private String  txtActToken;
	
	@Column(name = "enumActTokenUsed")
	private String enumActTokenUsed;
	
	@Column(name = "enumApprove")
	private String enumApprove;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkRoleId")
	private Role fkRoleId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkInstituteId")
	private InstituteModel fkInstituteId;
	
	@OneToMany(cascade = CascadeType.ALL, fetch =FetchType.LAZY)
	@JoinColumn(name = "fkUserId")
	private Set<UserPassword> userPassword = new HashSet<>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch =FetchType.EAGER)
	@JoinColumn(name = "fkUserId")
	private Set<UserJsessions> userJsessions = new HashSet<>();
	
	@Formula("(SELECT COUNT(*) FROM tblUserPassword t where t.fkUserId = pkId)")
	private Integer noOfPasswords;
	
	@Formula("(SELECT COUNT(*) FROM tblUserSession t where t.fkUserId = pkId)")
	private Integer noOfSessions;

	public String getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(String txtEmail) {
		this.txtEmail = txtEmail;
	}

	public String getTxtFirstName() {
		return txtFirstName;
	}

	public void setTxtFirstName(String txtFirstName) {
		this.txtFirstName = txtFirstName;
	}

	public String getTxtLastName() {
		return txtLastName;
	}

	public void setTxtLastName(String txtLastName) {
		this.txtLastName = txtLastName;
	}

	public String getTxtProfilePicPath() {
		return txtProfilePicPath;
	}

	public void setTxtProfilePicPath(String txtProfilePicPath) {
		this.txtProfilePicPath = txtProfilePicPath;
	}

	public String getTxtContactNumber() {
		return txtContactNumber;
	}

	public void setTxtContactNumber(String txtContactNumber) {
		this.txtContactNumber = txtContactNumber;
	}

	public Integer getNumberBatchYear() {
		return numberBatchYear;
	}

	public void setNumberBatchYear(Integer numberBatchYear) {
		this.numberBatchYear = numberBatchYear;
	}
		
	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	

	public String getTxtLinkdinLink() {
		return txtLinkdinLink;
	}

	public void setTxtLinkdinLink(String txtLinkdinLink) {
		this.txtLinkdinLink = txtLinkdinLink;
	}


	public String getTxtActToken() {
		return txtActToken;
	}

	public void setTxtActToken(String txtActToken) {
		this.txtActToken = txtActToken;
	}

	public String  getEnumActTokenUsed() {
		return enumActTokenUsed;
	}

	public void setEnumActTokenUsed(String enumActTokenUsed) {
		this.enumActTokenUsed = enumActTokenUsed;
	}

	public Role getFkRoleId() {
		return fkRoleId;
	}

	public void setFkRoleId(Role fkRoleId) {
		this.fkRoleId = fkRoleId;
	}

	public InstituteModel getFkInstituteId() {
		return fkInstituteId;
	}

	public void setFkInstituteId(InstituteModel fkInstituteId) {
		this.fkInstituteId = fkInstituteId;
	}

	public Set<UserPassword> getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(Set<UserPassword> userPassword) {
		this.userPassword = userPassword;
	}
	

	public Set<UserJsessions> getUserJsessions() {
		return userJsessions;
	}

	public void setUserJsessions(Set<UserJsessions> userJsessions) {
		this.userJsessions = userJsessions;
	}

	public Integer getNoOfPasswords() {
		return noOfPasswords;
	}

	public void setNoOfPasswords(Integer noOfPasswords) {
		this.noOfPasswords = noOfPasswords;
	}

	public Integer getNoOfSessions() {
		return noOfSessions;
	}

	public void setNoOfSessions(Integer noOfSessions) {
		this.noOfSessions = noOfSessions;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	/**
	 * @return the batch
	 */
	public String getBatch() {
		return batch;
	}

	/**
	 * @param batch the batch to set
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getEnumApprove() {
		return enumApprove;
	}

	public void setEnumApprove(String enumApprove) {
		this.enumApprove = enumApprove;
	}

	/**
	 * @return the txtUserprofileLink
	 */
	public String getTxtUserprofileLink() {
		return txtUserprofileLink;
	}

	/**
	 * @param txtUserprofileLink the txtUserprofileLink to set
	 */
	public void setTxtUserprofileLink(String txtUserprofileLink) {
		this.txtUserprofileLink = txtUserprofileLink;
	}
	
	
}
