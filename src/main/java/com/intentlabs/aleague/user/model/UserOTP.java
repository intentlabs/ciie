/* Copyright (C) Intentlabs, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Intentlabs, January 2017
 */
package com.intentlabs.aleague.user.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.intentlabs.common.enums.CommonStatus;
import com.intentlabs.common.model.IdentifierModel;


/**
 * This model maps user otp related information.
 * @author Nirav.Shah
 * @since  12/07/2017
 */
@Entity
@Table(name = "tblUserOTP")
public class UserOTP extends IdentifierModel {
	
	private static final long serialVersionUID = -5764068071467332650L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkUserId")
	private User user;
	
	@Column(name = "txtForgotPassOtp", length = 8, nullable = true)
	private String forgotPassOtp;
	
	@Column(name = "dateForgotOtpGeneration", nullable = true)
	private Date forgotOtpGeneration;
	
	@Column(name = "enumForgotPassOtpUsed", nullable = true)
	private long forgotPassOtpUsed;
	
	@Column(name = "txtTwoFactorOtpGeneration", length = 8, nullable = true)
	private String twoFactorOtpGeneration;
	
	@Column(name = "dateTwoFactorOtp", nullable = true)
	private Date twoFactorOtp;
	
	@Column(name = "enumTwoFactorOtpUsed", nullable = true)
	private long twoFactorOtpUsed;

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the forgotPassOtp
	 */
	public String getForgotPassOtp() {
		return forgotPassOtp;
	}

	/**
	 * @param forgotPassOtp the forgotPassOtp to set
	 */
	public void setForgotPassOtp(String forgotPassOtp) {
		this.forgotPassOtp = forgotPassOtp;
	}

	/**
	 * @return the forgotOtpGeneration
	 */
	public Date getForgotOtpGeneration() {
		return forgotOtpGeneration;
	}

	/**
	 * @param forgotOtpGeneration the forgotOtpGeneration to set
	 */
	public void setForgotOtpGeneration(Date forgotOtpGeneration) {
		this.forgotOtpGeneration = forgotOtpGeneration;
	}

	/**
	 * @return the forgotPassOtpUsed
	 */
	public CommonStatus getForgotPassOtpUsed() {
		return CommonStatus.fromId(forgotPassOtpUsed);
	}

	/**
	 * @param forgotPassOtpUsed the forgotPassOtpUsed to set
	 */
	public void setForgotPassOtpUsed(long forgotPassOtpUsed) {
		this.forgotPassOtpUsed = forgotPassOtpUsed;
	}

	/**
	 * @return the twoFactorOtpGeneration
	 */
	public String getTwoFactorOtpGeneration() {
		return twoFactorOtpGeneration;
	}

	/**
	 * @param twoFactorOtpGeneration the twoFactorOtpGeneration to set
	 */
	public void setTwoFactorOtpGeneration(String twoFactorOtpGeneration) {
		this.twoFactorOtpGeneration = twoFactorOtpGeneration;
	}

	/**
	 * @return the twoFactorOtp
	 */
	public Date getTwoFactorOtp() {
		return twoFactorOtp;
	}

	/**
	 * @param twoFactorOtp the twoFactorOtp to set
	 */
	public void setTwoFactorOtp(Date twoFactorOtp) {
		this.twoFactorOtp = twoFactorOtp;
	}

	/**
	 * @return the twoFactorOtpUsed
	 */
	public CommonStatus getTwoFactorOtpUsed() {
		return CommonStatus.fromId(twoFactorOtpUsed);
	}

	/**
	 * @param twoFactorOtpUsed the twoFactorOtpUsed to set
	 */
	public void setTwoFactorOtpUsed(long twoFactorOtpUsed) {
		this.twoFactorOtpUsed = twoFactorOtpUsed;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (getId() ^ (getId() >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdentifierModel other = (IdentifierModel) obj;
		if (getId() != other.getId())
			return false;
		return true;
	}
}
