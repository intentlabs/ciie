package com.intentlabs.aleague.user.services;

import java.util.List;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.service.BaseService;

public interface UserService extends BaseService<User> {
	
	User userLogin(String emailId);
	
	/*List<User> fetchInstituteFaculty(List<Long> roleId, long instituteId, Integer start, Integer end);*/
	
	List<User> findByIds(String ids);
	
	User getUsingToken(String token);
	
	int userCount(long instituteId);
	
	List<User> getStudentCo(long instituteId);
	
	List<User> getFacultyCo(long instituteId);

}
