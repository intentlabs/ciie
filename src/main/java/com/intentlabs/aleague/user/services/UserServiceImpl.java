package com.intentlabs.aleague.user.services;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.service.AbstractService;

public class UserServiceImpl extends AbstractService<User> implements UserService {

	
	private static final long serialVersionUID = -6908968849333062085L;

	@Override
	public Class<User> getModelClass() {
		return User.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<User> modelClass) {
	    Criteria criteria = getSession().createCriteria(getModelClass());
	    return criteria;
	}

	@Override
	public Criteria setSearchCriteria(User model, Criteria commonCriteria) {
		if(model.getFkRoleId() != null){
			if(Auditor.getAuditor().getFkInstituteId() != null){
				commonCriteria.add(Restrictions.eq("fkInstituteId.id", Auditor.getAuditor().getFkInstituteId().getId()));
			}
			commonCriteria.add(Restrictions.eq("fkRoleId", model.getFkRoleId()));
			if(model.getEnumActTokenUsed() != null && model.getEnumApprove() != null){
				Criterion rest1= Restrictions.eq("enumActTokenUsed", model.getEnumActTokenUsed());
				Criterion rest2= Restrictions.eq("enumApprove", model.getEnumApprove());
				commonCriteria.add(Restrictions.or(rest1, rest2));
			}
			return commonCriteria;
		}else{
			return commonCriteria;
		}
		
         
	}

	@Override
	public User userLogin(String txtEmail) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("txtEmail", txtEmail));
		return (User) criteria.uniqueResult();
	}

	/*@Override
	public List<User> fetchInstituteFaculty(List<Long> roleId, long instituteId, Integer start, Integer end) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.in("fkRoleId.id", roleId));
		criteria.add(Restrictions.eq("fkInstituteId.id", instituteId));
		criteria.add(Restrictions.eq("activationStatus", 1l));
		if(start != null && end != null){
			criteria.setFirstResult(start);
			criteria.setMaxResults(end - start);
		}
		
		return (List<User>)criteria.list();
	}*/
	
	@Override
	public User getUsingToken(String token) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("txtActToken", token));
		return (User) criteria.uniqueResult();
	}

	@Override
	public int userCount(long instituteId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("fkInstituteId.id", instituteId));
		return ((Long)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@Override
	public List<User> getStudentCo(long instituteId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("fkInstituteId.id", instituteId));
		criteria.add(Restrictions.eq("fkRoleId.id", 6l));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<User>)criteria.list();
	}

	@Override
	public List<User> getFacultyCo(long instituteId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("fkInstituteId.id", instituteId));
		criteria.add(Restrictions.eq("fkRoleId.id", 5l));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<User>)criteria.list();
	}

	@Override
	public List<User> findByIds(String ids) {
		ids = ids.substring(1, ids.length() - 1);
		String[] userIds = ids.split(",");
		
		List<Long> longIds = new ArrayList<>();
		for(String id : userIds){
			longIds.add(Long.valueOf(id));
		}
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.in("id", longIds));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<User>)criteria.list();
	}
}
