package com.intentlabs.aleague.user.services;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.user.model.UserBasic;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.service.AbstractService;

public class UserBasicServiceImpl extends AbstractService<UserBasic> implements UserBasicService {

	@Override
	public Class<UserBasic> getModelClass() {
		return UserBasic.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<UserBasic> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(UserBasic model, Criteria commonCriteria) {
		if(model.getFkRoleId() != null){
			if(Auditor.getAuditor().getFkInstituteId() != null){
				commonCriteria.add(Restrictions.eq("fkInstituteId.id", Auditor.getAuditor().getFkInstituteId().getId()));
			}
			commonCriteria.add(Restrictions.eq("fkRoleId", model.getFkRoleId()));
			if(model.getEnumActTokenUsed() != null && model.getEnumApprove() != null){
				Criterion rest1= Restrictions.eq("enumActTokenUsed", model.getEnumActTokenUsed());
				Criterion rest2= Restrictions.eq("enumApprove", model.getEnumApprove());
				commonCriteria.add(Restrictions.or(rest1, rest2));
			}
			return commonCriteria;
		}else{
			return commonCriteria;
		}
	}

	@Override
	public List<UserBasic> getUser(List<Long> userIdList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserBasic> getUser(long roleId, String enumApprove, String activationTokenUsed, Long instituteId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		if(instituteId != null){
			criteria.add(Restrictions.eq("fkInstituteId.id", instituteId));
		}
		if(! StringUtils.isBlank(enumApprove)){
			criteria.add(Restrictions.eq("enumApprove", enumApprove));
		}
		if(! StringUtils.isBlank(activationTokenUsed)){
			criteria.add(Restrictions.eq("enumActTokenUsed", activationTokenUsed));
		}
		criteria.add(Restrictions.eq("fkRoleId.id", roleId));
		criteria.addOrder(Order.desc("createDate"));
		return (List<UserBasic>)criteria.list();
	}
	
	@Override
	public List<UserBasic> fetchInstituteFaculty(List<Long> roleId, long instituteId, Integer start, Integer end) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.in("fkRoleId.id", roleId));
		criteria.add(Restrictions.eq("fkInstituteId.id", instituteId));
		criteria.add(Restrictions.eq("activationStatus", 1l));
		if(start != null && end != null){
			criteria.setFirstResult(start);
			criteria.setMaxResults(end - start);
		}
		
		return (List<UserBasic>)criteria.list();
	}

}
