package com.intentlabs.aleague.user.services;

import java.util.List;

import com.intentlabs.aleague.user.model.UserBasic;
import com.intentlabs.common.service.BaseService;

public interface UserBasicService extends BaseService<UserBasic> {
	
	List<UserBasic> getUser(List<Long> userIdList);
	
	List<UserBasic> getUser(long roleId, String enumApprove, String activationTokenUsed, Long instituteId);
	
	List<UserBasic> fetchInstituteFaculty(List<Long> roleId, long instituteId, Integer start, Integer end);
}
