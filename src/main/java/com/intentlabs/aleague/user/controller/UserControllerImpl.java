package com.intentlabs.aleague.user.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.intentlabs.aleague.exception.CapsException;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.exception.InvalidRequestException;
import com.intentlabs.aleague.user.oparation.UserOperation;
import com.intentlabs.aleague.user.view.UserView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.util.CustomMessage;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.util.Utility;

@SuppressWarnings("serial")
@Controller
@RequestMapping("/user")
public class UserControllerImpl extends AbstractController<UserView> implements UserController {

	@Autowired
	private UserOperation userOperation;

	@Override
	public BaseOperation<UserView> getOperation() {

		return userOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		try {
			LoggerService.info(USER_CONTROLLER, OperationName.EDIT, LogMessage.BEFORE_CALLING_OPERATION);
			if (id == null) {
				LoggerService.info(USER_CONTROLLER, OperationName.EDIT, CustomMessage.INVALID_FORM);
				return ViewResponse.create(ResponseCode.INVALID_REQUEST.getCode(), CustomMessage.INVALID_FORM, null);
			}
			return userOperation.doEditOpeartion(id);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.EDIT, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response delete(Long id) {

		try {
			LoggerService.info(USER_CONTROLLER, OperationName.DELETE, LogMessage.BEFORE_CALLING_OPERATION);
			if (id == null) {
				LoggerService.info(USER_CONTROLLER, OperationName.EDIT, CustomMessage.INVALID_FORM);
				return ViewResponse.create(ResponseCode.INVALID_REQUEST.getCode(), CustomMessage.INVALID_FORM, null);
			}
			return userOperation.doDeleteOperation(id);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.DELETE, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response displayGrid(Integer start, Integer end) {
		try {
			LoggerService.info(USER_CONTROLLER, OperationName.DISPLAY_GRID, LogMessage.BEFORE_CALLING_OPERATION);

			return userOperation.doDisplayGridOperation(start, end);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.DELETE, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(UserView webView) throws Exception {

	}

	@Override
	public Response save(@RequestBody UserView view) throws Exception {

		try {
			LoggerService.info(USER_CONTROLLER, OperationName.USER_REGISTRATION, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(USER_CONTROLLER, OperationName.USER_REGISTRATION, LogMessage.BEFORE_CALLING_OPERATION);
			return userOperation.doSaveOperation(view);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.USER_REGISTRATION, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response login(@RequestBody UserView userView, HttpServletRequest request, HttpServletResponse response)
			throws InvalidDataException, Exception {

		try {
			LoggerService.info(USER_CONTROLLER, OperationName.USER_LOGIN, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(USER_CONTROLLER, OperationName.USER_LOGIN, LogMessage.BEFORE_CALLING_OPERATION);
			return userOperation.doUserLogin(userView, request, response);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.USER_LOGIN, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response userForgotPassword(@RequestBody UserView userView) throws Exception {

		LoggerService.info(USER_CONTROLLER, OperationName.USER_SET_NEW_PASSWOED,
				LogMessage.BEFORE_CALLING_OPERATION);
		if (userView.getEmailId() != null) {
			return userOperation.doGenerateOtpForForgetPassword(userView);

		}else{
			LoggerService.info(USER_CONTROLLER, OperationName.USER_OTP, LogMessage.REQUEST_COMPLETED);
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "Email is not found");
		}
	}

	@Override
	public Response userupdatenewPassword(@RequestBody UserView userView, HttpServletResponse response) throws InvalidRequestException {
		return userOperation.doUpdateUserPassword(userView, response);
	}

	@Override
	public Response logoutuser(HttpServletRequest request, RedirectAttributes redirectAttributes) throws Exception {

		HttpSession session = request.getSession(true);

		try {

			return userOperation.logOut(session, redirectAttributes);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.SI_USER_LOGOUT, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response checkEmailid(@RequestBody UserView userView) throws Exception {

		try {
			LoggerService.info(USER_CONTROLLER, OperationName.CHECK_EMAIL, LogMessage.BEFORE_VALIDATION);
			return userOperation.doValidateEmailIdOperation(userView);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.CHECK_EMAIL, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response getloggeddinUser(@RequestBody String sessionId) {
		return userOperation.getLoggeedinUser(sessionId);
	}

	@Override
	public Response getUserProfile(@RequestBody(required = false) UserView userView)
			throws InvalidDataException, Exception {
		if(userView != null){
			return userOperation.doGetUserProfileOperation(userView.getId());
		}else{
			return userOperation.doGetUserProfileOperation(Auditor.getAuditor().getId());
		}
	}

	@Override
	public Response saveUserProfile(@ModelAttribute UserView userView) throws InvalidDataException, Exception {
		return userOperation.doSaveUserProfileOperation(userView);
	}

	@Override
	public Response verification(@RequestParam("token") String token) throws CapsException {
		if(StringUtils.isBlank(token)){
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), ResponseCode.INVALID_REQUEST.getMessage());
		}
		return userOperation.doVerification(token);
	}
	
	@Override
	public Response search(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end,@RequestBody (required= false) UserView userview) throws InvalidDataException, Exception {
		if(start == null){
			start = 0;
		}
		if(end == null){
			end = 9;
		}
		return userOperation.doSearch(start, end, userview);
	}

	@Override
	public Response inviteUser(@RequestBody UserView userView) throws InvalidDataException, Exception {
	
		try {
			LoggerService.info(USER_CONTROLLER, OperationName.INVITE_USER, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(USER_CONTROLLER, OperationName.INVITE_USER, LogMessage.BEFORE_CALLING_OPERATION);
			return userOperation.doInviteOperation(userView);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.INVITE_USER, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response approveUser(@RequestBody UserView userView) throws InvalidDataException, Exception {
		try {
			LoggerService.info(USER_CONTROLLER, OperationName.APPROVE_USER, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(USER_CONTROLLER, OperationName.APPROVE_USER, LogMessage.BEFORE_CALLING_OPERATION);
			return userOperation.doApproveOperation(userView);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.APPROVE_USER, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response enableDisableUser(@RequestBody UserView userView) throws InvalidDataException, Exception {
		try {
			LoggerService.info(USER_CONTROLLER, OperationName.ENABLE_DISABLE_USER, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(USER_CONTROLLER, OperationName.ENABLE_DISABLE_USER, LogMessage.BEFORE_CALLING_OPERATION);
			return userOperation.doEnableDisableOperation(userView);
		} finally {
			LoggerService.info(USER_CONTROLLER, OperationName.ENABLE_DISABLE_USER, LogMessage.REQUEST_COMPLETED);
		}
	}

	@Override
	public Response getAllFaculties(@RequestParam(required = true, value="instituteid") long instituteid, @RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception {
		return userOperation.doSearchFaculty(instituteid, start, end);
	}

	@Override
	public Response setCoordinatorUser(@RequestBody (required = true) UserView userView) throws InvalidDataException, Exception {
		return userOperation.doSetCoordinatorUserOperation(userView.getSelectedUsers());
	}
	
	@Override
	public Response approveUsers(@RequestBody (required = true) UserView userView) throws InvalidDataException, Exception {
		return userOperation.doApproveOperation(userView.getSelectedUsers());
	}

	@Override
	public Response exportUserList(@RequestBody UserView userView, HttpServletResponse response) throws InvalidDataException, Exception {
		File file = userOperation.doExport(userView);
		
		response.setContentType("application/xls");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ file.getName() + "\"");
		
		InputStream stream = new FileInputStream(file);
		FileCopyUtils.copy(stream, response.getOutputStream());
		
		Utility.deleteFile(file);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}

}
