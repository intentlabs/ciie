/**
 * 
 */
package com.intentlabs.aleague.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.intentlabs.aleague.exception.CapsException;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.exception.InvalidRequestException;
import com.intentlabs.aleague.user.view.UserView;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * @author Dhruvang
 *
 */
public interface UserController extends WebController<UserView> {

	String USER_CONTROLLER = "UserController";

	@RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	@ResponseBody
	Response login(UserView userView, HttpServletRequest request, HttpServletResponse response)
			throws InvalidDataException, Exception;

	@RequestMapping(value = "/forgotpassword.htm", method = RequestMethod.POST)
	@ResponseBody
	Response userForgotPassword(@RequestBody UserView userView) throws Exception;

	@RequestMapping(value = "/updatenewPassword.htm", method = RequestMethod.POST)
	@ResponseBody
	Response userupdatenewPassword(@RequestBody UserView userView, HttpServletResponse response)
			throws InvalidRequestException;

	@RequestMapping(value = "logout.htm")
	@ResponseBody
	public Response logoutuser(HttpServletRequest request, RedirectAttributes redirectAttributes) throws Exception;

	@RequestMapping(value = "/loggedinUser.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getloggeddinUser(@RequestBody String sessionId);

	@RequestMapping(value = "/checkEmailid.htm", method = RequestMethod.POST)
	@ResponseBody
	Response checkEmailid(@RequestBody UserView userView) throws Exception;

	@RequestMapping(value = "/getuserprofile.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getUserProfile(@RequestBody (required= false) UserView userView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/saveuserprofile.htm", method = RequestMethod.POST)
	@ResponseBody
	Response saveUserProfile(@ModelAttribute UserView userView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/inviteUser.htm", method = RequestMethod.POST)
	@ResponseBody
	Response inviteUser(@RequestBody UserView userView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/approveUser.htm", method = RequestMethod.POST)
	@ResponseBody
	Response approveUser(@RequestBody UserView userView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/enableDisableUser.htm", method = RequestMethod.POST)
	@ResponseBody
	Response enableDisableUser(@RequestBody UserView userView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/setCoordinatorUser.htm", method = RequestMethod.POST)
	@ResponseBody
	Response setCoordinatorUser(@RequestBody (required = true) UserView userView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/approveUsers.htm", method = RequestMethod.POST)
	@ResponseBody
	Response approveUsers(@RequestBody (required = true) UserView userView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/verification.htm", method = RequestMethod.GET)
	@ResponseBody
	Response verification(@RequestParam("token") String token) throws CapsException;
	
	@RequestMapping(value = "/search.htm", method = RequestMethod.POST)
	@ResponseBody
	Response search(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end, @RequestBody (required= false) UserView userview) throws InvalidDataException, Exception;	

	@RequestMapping(value = "/getallfaculties.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getAllFaculties(@RequestParam(required = true, value="instituteid") long instituteid, @RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/export.htm", method = RequestMethod.POST)
	@ResponseBody
	Response exportUserList(@RequestBody UserView userView, HttpServletResponse response) throws InvalidDataException, Exception;
}
