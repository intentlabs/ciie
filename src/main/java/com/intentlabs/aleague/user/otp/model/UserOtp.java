package com.intentlabs.aleague.user.otp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.model.IdentifierModel;

/**
 * @author Dhruvang
 *
 */

@Entity
@Table(name = "tblUserOTP")
public class UserOtp extends IdentifierModel {

	private static final long serialVersionUID = 3935962409208820210L;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkUserId")
	private User fkUserId;
	
	@Column(name = "txtForgotPassOtp")
	private Integer fotgotPassOtp;
	
	@Column(name = "dateForgotOtpGeneration")
	private Date forgotOtpGenerationDate;
	
	@Column(name = "enumForgotPassOtpUsed")
	private String enumForgotPassOtpUsed;
	
	@Column(name = "txtTwoFactorOtpGeneration")
	private String twoFactorOtpGeneration;
	
	@Column(name = "dateTwoFactorOtp")
	private Date dateTwoFactorOtp;
	
	@Column(name = "enumTwoFactorOtpUsed")
	private String enumTwoFactorOtpUsed;

	public User getFkUserId() {
		return fkUserId;
	}

	public void setFkUserId(User fkUserId) {
		this.fkUserId = fkUserId;
	}

	public Integer getFotgotPassOtp() {
		return fotgotPassOtp;
	}

	public void setFotgotPassOtp(Integer fotgotPassOtp) {
		this.fotgotPassOtp = fotgotPassOtp;
	}

	public Date getForgotOtpGenerationDate() {
		return forgotOtpGenerationDate;
	}

	public void setForgotOtpGenerationDate(Date forgotOtpGenerationDate) {
		this.forgotOtpGenerationDate = forgotOtpGenerationDate;
	}

	public String getEnumForgotPassOtpUsed() {
		return enumForgotPassOtpUsed;
	}

	public void setEnumForgotPassOtpUsed(String enumForgotPassOtpUsed) {
		this.enumForgotPassOtpUsed = enumForgotPassOtpUsed;
	}

	public String getTwoFactorOtpGeneration() {
		return twoFactorOtpGeneration;
	}

	public void setTwoFactorOtpGeneration(String twoFactorOtpGeneration) {
		this.twoFactorOtpGeneration = twoFactorOtpGeneration;
	}

	public Date getDateTwoFactorOtp() {
		return dateTwoFactorOtp;
	}

	public void setDateTwoFactorOtp(Date dateTwoFactorOtp) {
		this.dateTwoFactorOtp = dateTwoFactorOtp;
	}

	public String getEnumTwoFactorOtpUsed() {
		return enumTwoFactorOtpUsed;
	}

	public void setEnumTwoFactorOtpUsed(String enumTwoFactorOtpUsed) {
		this.enumTwoFactorOtpUsed = enumTwoFactorOtpUsed;
	}
	
}
