package com.intentlabs.aleague.user.otp.service;

import com.intentlabs.aleague.user.otp.model.UserOtp;
import com.intentlabs.common.service.BaseService;


/**
 * @author Dhruvang
 *
 */
public interface UserOtpService extends BaseService<UserOtp> {
	
	
	UserOtp doGetOtpUser(long userid, Integer otp);

}
