/**
 * 
 */
package com.intentlabs.aleague.user.otp.service;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.user.otp.model.UserOtp;
import com.intentlabs.common.service.AbstractService;

/**
 * @author Dhruvang
 *
 */
public class UserOtpServiceImpl extends AbstractService<UserOtp> implements UserOtpService {


	private static final long serialVersionUID = -9212124763921534277L;

	@Override
	public UserOtp doGetOtpUser(long userid, Integer otp) {
	
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("fkUserId.id",userid));
		criteria.add(Restrictions.eq("fotgotPassOtp",otp));
		return (UserOtp) criteria.uniqueResult();
	}

	@Override
	public Class<UserOtp> getModelClass() {
		
		return UserOtp.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<UserOtp> modelClass) {
		
		return null;
	}

	@Override
	public Criteria setSearchCriteria(UserOtp model, Criteria commonCriteria) {
		
		
	
		return null;
	}

}
