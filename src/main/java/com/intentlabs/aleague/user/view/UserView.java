/**
 * 
 */
package com.intentlabs.aleague.user.view;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.common.view.ArciveView;
import com.intentlabs.common.view.KeyValueView;

/**
 * @author Dhruvang
 *
 */
public class UserView extends ArciveView {

	
	private static final long serialVersionUID = 2133154225206222090L;
	
 	private String emailId;
	
	private String txtFirstName;
	
	private String txtLastName;
	
	private String fullName;
	
	private String oldpassword;  
	
	private String password;
	
	private String txtProfilePicPath;
	
	private String txtInstitutePicPath;
	
	private String txtCountryCode;
	
	private String txtContactNumber;
	
	private int numberBatchYear;
	
	private String enrolmentNumber;
	
	private String txtFbLink;
	
	private String txtTwLink;
	
	private String txtInstaLink;
	
	private String txtLinkdinLink;
	
	private String txtUserprofileLink;
	
	private KeyValueView enumProfilel;
	
	private String txtActToken;
	
	private String enumActTokenUsed;
	
	private KeyValueView roleType;

	private KeyValueView fkInstituteId;
	
	private String otp;
	
	private String about;
	
	private String course;
	
	private MultipartFile file;
	
	private String instituteName;
	
	private String batch;
	
	private String userRoleType;
	
	private Boolean canCreate;
	
	private Long saInstituteId;

	private Boolean isselected;
	
	private Boolean isActive;
	
	private Boolean isRegistered;
	
	private Boolean isApproved;
	
	private String selectedUsers;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getTxtFirstName() {
		return txtFirstName;
	}

	public void setTxtFirstName(String txtFirstName) {
		this.txtFirstName = txtFirstName;
	}

	public String getTxtLastName() {
		return txtLastName;
	}

	public void setTxtLastName(String txtLastName) {
		this.txtLastName = txtLastName;
	}
	
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTxtProfilePicPath() {
		return txtProfilePicPath;
	}

	public void setTxtProfilePicPath(String txtProfilePicPath) {
		this.txtProfilePicPath = txtProfilePicPath;
	}

	public String getTxtCountryCode() {
		return txtCountryCode;
	}

	public void setTxtCountryCode(String txtCountryCode) {
		this.txtCountryCode = txtCountryCode;
	}

	public String getTxtContactNumber() {
		return txtContactNumber;
	}

	public void setTxtContactNumber(String txtContactNumber) {
		this.txtContactNumber = txtContactNumber;
	}

	public int getNumberBatchYear() {
		return numberBatchYear;
	}

	public void setNumberBatchYear(int numberBatchYear) {
		this.numberBatchYear = numberBatchYear;
	}

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	public String getTxtFbLink() {
		return txtFbLink;
	}

	public void setTxtFbLink(String txtFbLink) {
		this.txtFbLink = txtFbLink;
	}

	public String getTxtTwLink() {
		return txtTwLink;
	}

	public void setTxtTwLink(String txtTwLink) {
		this.txtTwLink = txtTwLink;
	}

	public String getTxtInstaLink() {
		return txtInstaLink;
	}

	public void setTxtInstaLink(String txtInstaLink) {
		this.txtInstaLink = txtInstaLink;
	}

	public String getTxtLinkdinLink() {
		return txtLinkdinLink;
	}

	public void setTxtLinkdinLink(String txtLinkdinLink) {
		this.txtLinkdinLink = txtLinkdinLink;
	}



	/**
	 * @return the txtUserprofileLink
	 */
	public String getTxtUserprofileLink() {
		return txtUserprofileLink;
	}

	/**
	 * @param txtUserprofileLink the txtUserprofileLink to set
	 */
	public void setTxtUserprofileLink(String txtUserprofileLink) {
		this.txtUserprofileLink = txtUserprofileLink;
	}

	public KeyValueView getEnumProfilel() {
		return enumProfilel;
	}

	public void setEnumProfilel(KeyValueView enumProfilel) {
		this.enumProfilel = enumProfilel;
	}

	public String getTxtActToken() {
		return txtActToken;
	}

	public void setTxtActToken(String txtActToken) {
		this.txtActToken = txtActToken;
	}

	public String getEnumActTokenUsed() {
		return enumActTokenUsed;
	}

	public void setEnumActTokenUsed(String enumActTokenUsed) {
		this.enumActTokenUsed = enumActTokenUsed;
	}

	public KeyValueView getRoleType() {
		return roleType;
	}

	public void setRoleType(KeyValueView roleType) {
		this.roleType = roleType;
	}

	public KeyValueView getFkInstituteId() {
		return fkInstituteId;
	}

	public void setFkInstituteId(KeyValueView fkInstituteId) {
		this.fkInstituteId = fkInstituteId;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * @return the instituteName
	 */
	public String getInstituteName() {
		return instituteName;
	}

	/**
	 * @param instituteName the instituteName to set
	 */
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	/**
	 * @return the batch
	 */
	public String getBatch() {
		return batch;
	}

	/**
	 * @param batch the batch to set
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * @return the txtInstitutePicPath
	 */
	public String getTxtInstitutePicPath() {
		return txtInstitutePicPath;
	}

	/**
	 * @param txtInstitutePicPath the txtInstitutePicPath to set
	 */
	public void setTxtInstitutePicPath(String txtInstitutePicPath) {
		this.txtInstitutePicPath = txtInstitutePicPath;
	}
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the userRoleType
	 */
	public String getUserRoleType() {
		return userRoleType;
	}

	/**
	 * @param userRoleType the userRoleType to set
	 */
	public void setUserRoleType(String userRoleType) {
		this.userRoleType = userRoleType;
	}

	/**
	 * @return the canCreate
	 */
	public Boolean getCanCreate() {
		return canCreate;
	}

	/**
	 * @param canCreate the canCreate to set
	 */
	public void setCanCreate(Boolean canCreate) {
		this.canCreate = canCreate;
	}

	/**
	 * @return the saInstituteId
	 */
	public Long getSaInstituteId() {
		return saInstituteId;
	}

	/**
	 * @param saInstituteId the saInstituteId to set
	 */
	public void setSaInstituteId(Long saInstituteId) {
		this.saInstituteId = saInstituteId;
	}

	/**
	 * @return the isselected
	 */
	public Boolean getIsselected() {
		return isselected;
	}

	/**
	 * @param isselected the isselected to set
	 */
	public void setIsselected(Boolean isselected) {
		this.isselected = isselected;
	}

	/**
	 * @return the isActive
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the isRegistered
	 */
	public Boolean getIsRegistered() {
		return isRegistered;
	}

	/**
	 * @param isRegistered the isRegistered to set
	 */
	public void setIsRegistered(Boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	/**
	 * @return the isApproved
	 */
	public Boolean getIsApproved() {
		return isApproved;
	}

	/**
	 * @param isApproved the isApproved to set
	 */
	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}

	/**
	 * @return the selectedUsers
	 */
	public String getSelectedUsers() {
		return selectedUsers;
	}

	/**
	 * @param selectedUsers the selectedUsers to set
	 */
	public void setSelectedUsers(String selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	/**
	 * @return the oldpassword
	 */
	public String getOldpassword() {
		return oldpassword;
	}

	/**
	 * @param oldpassword the oldpassword to set
	 */
	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}
	
}
