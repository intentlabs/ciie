
package com.intentlabs.aleague.user.oparation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.intentlabs.aleague.email.enums.Status;
import com.intentlabs.aleague.email.model.EmailAccountModel;
import com.intentlabs.aleague.email.model.EmailContentModel;
import com.intentlabs.aleague.email.model.TransactionalEmailModel;
import com.intentlabs.aleague.email.service.TransactionalEmailService;
import com.intentlabs.aleague.exception.CapsException;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.exception.InvalidRequestException;
import com.intentlabs.aleague.institute.services.InstituteService;
import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.aleague.password.service.PasswordService;
import com.intentlabs.aleague.rights.Role;
import com.intentlabs.aleague.role.services.RoleService;
import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.user.model.UserBasic;
import com.intentlabs.aleague.user.model.UserPassword;
import com.intentlabs.aleague.user.otp.service.UserOtpService;
import com.intentlabs.aleague.user.services.UserBasicService;
import com.intentlabs.aleague.user.services.UserService;
import com.intentlabs.aleague.user.view.UserView;
import com.intentlabs.aleague.usersession.model.UserJsessions;
import com.intentlabs.aleague.usersession.service.SessionService;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.CommonStatus;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.model.PageModel;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.PageResultResponse;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.Constant;
import com.intentlabs.common.util.CookieUtility;
import com.intentlabs.common.util.CustomMessage;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.util.HttpRequestParam;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.util.PasswordUtil;
import com.intentlabs.common.util.Utility;
import com.intentlabs.common.util.WebUtil;
import com.intentlabs.common.view.KeyValueView;
import com.intentlabs.common.view.View;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * @author Dhruvang
 *
 */

@Component(value = "userOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class UserOperationImpl extends AbstractOperation<User, UserView> implements UserOperation {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1710313391976023172L;

	@Autowired
	private UserService userService;

	@Autowired
	TransactionalEmailService transactionalEmailService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private UserOtpService userOtpService;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private PasswordService passwordService;
	
	@Autowired 
	private UserBasicService userBasicService;

	@Override
	public User toModel(User user, UserView userview) {
		user.setTxtFirstName(userview.getTxtFirstName());
		user.setTxtLastName(userview.getTxtLastName());
		user.setTxtEmail(userview.getEmailId().trim());
		user.setId(userview.getId());
		// user.setTxtActToken(userview.getTxtActToken());
		// user.setCreateDate(new Date());
		/*
		 * user.setActivationStatus(1l); user.setCreateBy(userService.get(1l));
		 * user.setFkRoleId(roleService.get(2l));
		 * user.setEnumProfilel(EnumProfile.SEVENTY); user.setEnumArchive("0");
		 * setUserPassword(user, userview);
		 */
		return user;
	}

	@Override
	protected User getNewModel(UserView view) {
		return new User();
	}

	@Override
	public UserView fromModel(User user) {

		UserView userView = new UserView();

		if (user.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
			userView.setIsActive(true);
		} else {
			userView.setIsActive(false);
		}

		userView.setId(user.getId());
		userView.setTxtFirstName(user.getTxtFirstName());
		userView.setTxtLastName(user.getTxtLastName());
		userView.setEmailId(user.getTxtEmail());
		if (user.getFkInstituteId() != null) {
			userView.setFkInstituteId(
					KeyValueView.create(user.getFkInstituteId().getId(), user.getFkInstituteId().getTxtName()));
			userView.setTxtInstitutePicPath(FileUtility.setPath(user.getFkInstituteId().getTxtLogoPath()));
		} else {
			userView.setTxtInstitutePicPath(FileUtility.setPath("defaultinstitutepic.PNG"));
		}
		userView.setAbout(user.getAbout());

		if (user.getTxtProfilePicPath() != null && !"".equals(user.getTxtProfilePicPath())) {
			userView.setTxtProfilePicPath(FileUtility.setPath(user.getTxtProfilePicPath()));
		} else {
			userView.setTxtProfilePicPath(FileUtility.setPath("defaultuserprofilepic.PNG"));
		}

		if (user.getNumberBatchYear() != null) {
			userView.setNumberBatchYear(user.getNumberBatchYear());
		}
		if(user.getTxtUserprofileLink() !=null){
			userView.setTxtUserprofileLink(user.getTxtUserprofileLink());
		}
		userView.setTxtContactNumber(user.getTxtContactNumber());
		userView.setTxtLinkdinLink(user.getTxtLinkdinLink());
		userView.setRoleType(KeyValueView.create(user.getFkRoleId().getId(), user.getFkRoleId().getTxtName()));
		if (user.getEnrolmentNumber() != null) {
			userView.setEnrolmentNumber(user.getEnrolmentNumber());
		}
		userView.setCourse(user.getCourse());

		if ("1".equals(user.getEnumApprove())) {
			userView.setCanCreate(false);
		} else {
			userView.setCanCreate(true);
		}

		/*
		 * userView.setPassword(lastpassword(user.getUserPassword(),
		 * userView.getPassword()));
		 */
		return userView;
	}

	@Override
	public BaseService getService() {
		return userService;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {

		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {

		User user = userService.get(id);

		if (user == null) {
			LoggerService.info(USER_OPERATION, OperationName.EDIT, CustomMessage.INVALID_FORM);
			return ViewResponse.create(ResponseCode.INVALID_REQUEST.getCode(), CustomMessage.INVALID_FORM, null);
		}
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), "", fromModel(user));
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		User user = userService.get(id);
		UserView userView = fromModel(user);
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), "", userView);
	}

	@Override
	protected void checkInactive(User model) throws Exception {
	}

	public Response doSaveOperation(UserView view) throws Exception {
		String email = view.getEmailId();
		String[] split = email.split("@");
		String domainString = split[1];

		User existUser = userService.userLogin(view.getEmailId());

		if (existUser == null) {
			User user = toModel(getModel(view), view);
			InstituteModel instituteModel = instituteService.checkDomainName(domainString);

			if (instituteModel == null) {
				if (view.getEnrolmentNumber() == null || view.getFkInstituteId() == null) {
					return CommonResponse.create(ResponseCode.PROVIDE_INSTITUTE_DETAILS.getCode(),
							ResponseCode.PROVIDE_INSTITUTE_DETAILS.getMessage());
				}
				user.setFkInstituteId(instituteService.load(view.getFkInstituteId().getId()));
				user.setEnumApprove(String.valueOf(CommonStatus.YES.getId()));
			} else {
				user.setFkInstituteId(instituteModel);
				user.setEnumApprove(String.valueOf(CommonStatus.NO.getId()));
			}

			user.setFkRoleId(roleService.get(view.getRoleType().getId()));
			user.setActivationStatus(ActiveInActive.INACTIVE);

			user.setTxtActToken(Utility.generateCode(8));
			user.setEnumActTokenUsed(String.valueOf(CommonStatus.NO.getId()));
			user.setEnumArchive("1");

			user.setEnrolmentNumber(view.getEnrolmentNumber());
			setUserPassword(user, view);
			userService.create(user);
			sendEmail(user);
			return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(),
					"You have been successfully registered on the portal. Please check your email inbox for account verification and activation mail.");
		} else {
			return CommonResponse.create(ResponseCode.EMAIL_ALREADY_EXISTS.getCode(),
					ResponseCode.EMAIL_ALREADY_EXISTS.getMessage());
		}
	}

	@Override
	public Response doDisplayGridOperation(Integer start, Integer end) {

		@SuppressWarnings("deprecation")
		List<User> allUsers = userService.findAll();

		List<UserView> uv = (List<UserView>) fromModelList(allUsers);
		List<View> view = new ArrayList<View>();
		for (View u : uv) {
			view.add(u);
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), "", view.size(), view);
	}

	@Override
	public Response doDeleteOperation(Long id) {

		User user = userService.get(2l);
		if (user == null) {
			LoggerService.info(USER_OPERATION, OperationName.EDIT, CustomMessage.INVALID_FORM);
			return ViewResponse.create(ResponseCode.INVALID_REQUEST.getCode(), CustomMessage.INVALID_FORM, null);
		}

		user.setEnumArchive("0");
		user.setFkArchiveBy(user);
		userService.update(user);
		Role role = roleService.get(2l);
		role.setEnumArchive("0");
		role.setFkArchiveBy(user);
		roleService.update(role);

		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), "", fromModel(user));
	}

	@Override
	public Response doValidateUSeremailid(UserView userview) throws Exception {

		User user = userService.userLogin(userview.getEmailId().trim());
		if (user == null) {
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "Email is not found");
		}

		UserView userView = fromModel(user);
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Email is found", userView);
	}

	@Override
	public Response doUserLogin(UserView userView, HttpServletRequest request, HttpServletResponse response)
			throws InvalidDataException, Exception {

		User user = userService.userLogin(userView.getEmailId().trim());

		if (user == null) {
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "Email is not found");
		}

		if (user.getEnumArchive().equals("0")) {
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(),
					"You account has been disabled by portal admin. Please contact techsupport@a-league.org.");
		}

		if ("0".equals(user.getEnumActTokenUsed())) {
			return CommonResponse.create(ResponseCode.INACTIVE_ACCOUNT.getCode(),
					ResponseCode.INACTIVE_ACCOUNT.getMessage());
		}

		if (ActiveInActive.INACTIVE.equals(user.getActivationStatus())) {
			return CommonResponse.create(ResponseCode.INACTIVE_ACCOUNT.getCode(),
					"You account has been disabled by portal admin. Please contact techsupport@a-league.org.");
		}

		/*
		 * if(CommonStatus.YES.getId() ==
		 * Long.valueOf(user.getEnumApprove()).longValue()){ return
		 * CommonResponse.create(ResponseCode.APPROVAL_REQUIRED.getCode(),
		 * ResponseCode.APPROVAL_REQUIRED.getMessage()); }
		 */
		// String password = PasswordUtil.getmd5(user.getTxtEmail() +
		// Constant.SALT_STRING + userView.getPassword());
		
		/*UserPassword up = passwordService.lsstpassword(user.getId(),
				PasswordUtil.getmd5(user.getTxtEmail() + Constant.SALT_STRING + userView.getPassword()));*/
		
		UserPassword up = passwordService.lsstpassword(user.getId());
		
		if (up == null) {
			LoggerService.info(USER_OPERATION, userView.getEmailId().trim(), CustomMessage.INVALID_USERNAME_PASS);
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "Invalid password");
		}
		
		if(! up.getTxtPassword().equals(PasswordUtil.getmd5(user.getTxtEmail() + Constant.SALT_STRING + userView.getPassword()))){
			LoggerService.info(USER_OPERATION, userView.getEmailId().trim(), CustomMessage.INVALID_USERNAME_PASS);
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "Invalid password");
		}
		/*
		 * Cookie[] cookies = request.getCookies(); if (cookies != null &&
		 * cookies.length > 0) { for (Cookie cookie : cookies) { if
		 * (cookie.getName().equals(CookieUtility.COOKIE_FOR_USER_ID)) {
		 * LoggerService.info(USER_OPERATION, userView.getEmailId(),
		 * "else for cookies not null"); UserJsessions cookiesuser =
		 * sessionService.userCookiValidate(user, cookie.getValue()); if
		 * (cookiesuser != null) {
		 */
		UserJsessions cookiesuser = new UserJsessions();
		String jsessions = Utility.generateSession(userView.getEmailId().trim());
		cookiesuser.setJsession(jsessions);
		cookiesuser.setFkUserId(user);
		cookiesuser.setIpAddress("120");
		sessionService.create(cookiesuser);
		StringBuilder sb = new StringBuilder(Constant.USER_SESSION_STRING);
		sb.append("=");
		sb.append(jsessions);
		sb.append(";path=");
		sb.append("/");
		sb.append(";HttpOnly");
		response.setHeader("SET-COOKIE", sb.toString());
		response.setHeader(Constant.USER_SESSION_STRING, jsessions);
		// break;
		/*
		 * } setuserSession(user, response, request); break; } else {
		 * setuserSession(user, response, request); } } }
		 */

		// user.setEnumActTokenUsed();

		userService.update(user);
		Auditor.setAuditor(user);
		WebUtil.setToCurrentSession(Constant.USER_SESSION_KEY, user);
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Logged in", fromModel(user));
	}

	private void setuserSession(User user, HttpServletResponse response, HttpServletRequest request) {

		Set<UserJsessions> sessionList = user.getUserJsessions();
		UserJsessions usersession = new UserJsessions();
		usersession.setFkUserId(user);
		String jsessions = Utility.generateSession(user.getTxtEmail());
		usersession.setJsession(jsessions);
		WebUtil.setUserSession(Constant.USER_SESSION_STRING, jsessions);
		UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
		String userAgentbrowswer = userAgent.getBrowser().getName();
		usersession.setDeviceBrowser(userAgentbrowswer);
		usersession.setIpAddress(HttpRequestParam.getRemoteAddress());
		String cookieValue = Utility.getMD5(CookieUtility.getCookieValue(UUID.randomUUID().toString()));
		usersession.setDeviceCookie(cookieValue);
		CookieUtility.setCookie(response, CookieUtility.COOKIE_FOR_USER_ID, cookieValue);
		sessionList.add(usersession);
		user.setUserJsessions(sessionList);

	}

	@Override
	public Response doGenerateOtpForForgetPassword(UserView userView) throws Exception {
		User user = userService.userLogin(userView.getEmailId().trim());

		if (user == null) {
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "Email is not found");
		}
		
		TransactionalEmailModel emailModel = new TransactionalEmailModel();
		String newpassword = Utility.generateRandomNumberAndString(10);
		System.out.println(newpassword);
		String body = EmailContentModel.getMAP().get(3l).getContent()
				.replaceAll("\\{ApplicantName}", user.getTxtFirstName() + " " + user.getTxtLastName())
				.replaceAll("\\{password}", newpassword);
		
		emailModel.setBody(body);
		emailModel.setEmailAccountId(EmailAccountModel.getMAP().get(1l).getId());
		emailModel.setEmailTo(user.getTxtEmail());
		emailModel.setStatus(String.valueOf(Status.NEW.getId()));
		emailModel.setSubject("Reset password");
		transactionalEmailService.create(emailModel);
		userView.setPassword(newpassword);
	
		setUserPassword(user, userView);
		userService.update(user);
		
		
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Email sent for forgotpassword");
	}
	
	
	

	@Override
	public Response doUpdateUserPassword(UserView userView, HttpServletResponse response) throws InvalidRequestException {
		User user = userService.userLogin(Auditor.getAuditor().getTxtEmail());
		if (user == null) {
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "Email is not found");
		}
		/*if (checkLsatUsedPassword(user.getUserPassword(), userView.getPassword())) {
			LoggerService.info(USER_OPERATION, OperationName.CHANGE_PASSWORD,
					"This passeord is in used ==>" + userView.getId());
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "password is already used");
		}*/
		userView.setEmailId(Auditor.getAuditor().getTxtEmail());
		setUserPassword(user, userView);
		userService.update(user);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Your new password has been changed");
	}

	private void setUserPassword(User user, UserView view) {
		Set<UserPassword> lastPassList = user.getUserPassword();

		if (user.getNoOfPasswords() == null) {
			UserPassword newPassword = new UserPassword();
			newPassword
					.setTxtPassword(PasswordUtil.getmd5(view.getEmailId() + Constant.SALT_STRING + view.getPassword()));
			newPassword.setDatePasswordChange(new Date());
			newPassword.setFkUserId(user);
			lastPassList.add(newPassword);
		} else if (user.getNoOfPasswords() < 5) {
			UserPassword newPassword = new UserPassword();
			newPassword
					.setTxtPassword(PasswordUtil.getmd5(view.getEmailId() + Constant.SALT_STRING + view.getPassword()));
			newPassword.setDatePasswordChange(new Date());
			newPassword.setFkUserId(user);
			lastPassList.add(newPassword);
		} else {
			Iterator<UserPassword> iterator = lastPassList.iterator();
			if (iterator.hasNext()) {
				UserPassword oldestPass = iterator.next();
				oldestPass.setTxtPassword(
						PasswordUtil.getmd5(view.getEmailId() + Constant.SALT_STRING + view.getPassword()));
				oldestPass.setDatePasswordChange(new Date());
			}
		}
		user.setUserPassword(lastPassList);

	}

	private boolean checkLsatUsedPassword(Set<UserPassword> userPasswordsList, String newPassword) {
		for (UserPassword passwords : userPasswordsList) {
			if (newPassword.equals(passwords.getTxtPassword())) {
				return true;
			}
		}
		return false;
	}

	/*
	 * private String lastpassword(Set<UserPassword> userPasswordsList, String
	 * newPassword){
	 * 
	 * for(UserPassword passwords : userPasswordsList){
	 * if(newPassword.equals(passwords.getTxtPassword())){ return
	 * passwords.getTxtPassword(); } } return null; }
	 */

	@Override
	public Response logOut(HttpSession session, RedirectAttributes redirectAttributes) throws Exception {

		WebUtil.invalidatSession();
		Auditor.setAuditor(null);
		redirectAttributes.addFlashAttribute("from", "logout");
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
		// return new ModelAndView(new
		// RedirectView(HttpRequestParam.getContextPath()));
	}

	@Override
	public Response doValidateEmailIdOperation(UserView userView) throws Exception {
		String email = userView.getEmailId().trim();
		String[] split = email.split("@");
		String domainString = split[1];
		
		InstituteModel domains = instituteService.checkDomainName(","+domainString+",");
		if (domains == null) {
			return CommonResponse.create(ResponseCode.DOMAIN_NAME_IS_NOT_FOUND.getCode(),
					CustomMessage.NO_DOMAIN_FOUND);
		}
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "User found by Email ID");
	}

	@Override
	public Response getLoggeedinUser(String sessionId) {

		User loggedinUser = Auditor.getAuditor();
		if (loggedinUser == null) {
			
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), "User is not avilable");
		} else {
			return PageResultResponse.create(ResponseCode.SUCCESSFUL.getCode(), "" + loggedinUser.getId());
		}
	}

	@Override
	public User getBySessionId(String sessionId) {

		return sessionService.getUserBySessionId(sessionId);
	}

	@Override
	public Response doGetUserProfileOperation(Long id) {
		User user;
		if (Auditor.getAuditor().getId().longValue() == id.longValue()) {
			user = Auditor.getAuditor();
		} else {
			user = userService.load(id);
		}

		UserView view = new UserView();
		view.setId(user.getId());
		view.setTxtFirstName(user.getTxtFirstName());
		view.setTxtLastName(user.getTxtLastName());
		view.setFullName(user.getTxtFirstName() + " " + user.getTxtLastName());
		view.setEmailId(user.getTxtEmail());
		view.setTxtProfilePicPath(FileUtility.setPath(user.getTxtProfilePicPath()));
		view.setTxtContactNumber(user.getTxtContactNumber());
		if (user.getNumberBatchYear() != null) {
			view.setNumberBatchYear(user.getNumberBatchYear());
		}
if(user.getTxtUserprofileLink() !=null){
	view.setTxtUserprofileLink(user.getTxtUserprofileLink());
}
		view.setEnrolmentNumber(user.getEnrolmentNumber());
		view.setTxtLinkdinLink(user.getTxtLinkdinLink());
		view.setFkInstituteId(
				KeyValueView.create(user.getFkInstituteId().getId(), user.getFkInstituteId().getTxtName()));
		view.setAbout(user.getAbout());
		view.setCourse(user.getCourse());
		view.setBatch(user.getBatch());
		if (user.getFkInstituteId() != null) {
			view.setInstituteName(user.getFkInstituteId().getTxtName());
		} else {
			view.setInstituteName("");
		}

		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_USER_PROFILE, view);
	}

	@Override
	public Response doSaveUserProfileOperation(UserView view) {
		User user = Auditor.getAuditor();
		user.setTxtFirstName(view.getTxtFirstName());
		user.setTxtLastName(view.getTxtLastName());

		if (view.getFile() != null) {
			try {
				user.setTxtProfilePicPath(FileUtility.storeFile(view.getFile(), "userprofilepic"));
			} catch (Exception e) {
				LoggerService.exception(e);
			}
		}

		user.setTxtContactNumber(view.getTxtContactNumber());
		user.setNumberBatchYear(view.getNumberBatchYear());
		user.setEnrolmentNumber(view.getEnrolmentNumber());
		user.setTxtLinkdinLink(view.getTxtLinkdinLink());
		user.setAbout(view.getAbout());
		user.setCourse(view.getCourse());
		user.setBatch(view.getBatch());
		user.setTxtUserprofileLink(view.getTxtUserprofileLink());

		userService.update(user);

		Auditor.setAuditor(user);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "User Profile updated successfully.");
	}

	private void sendEmail(User userModel) {
		TransactionalEmailModel emailModel = new TransactionalEmailModel();
		String body = EmailContentModel.getMAP().get(1l).getContent()
				.replaceAll("\\{ApplicantName}", userModel.getTxtFirstName() + " " + userModel.getTxtLastName())
				.replaceAll("\\{link}", Constant.SERVER_URL + userModel.getTxtActToken());
		emailModel.setBody(body);
		emailModel.setEmailAccountId(EmailAccountModel.getMAP().get(1l).getId());
		emailModel.setEmailTo(userModel.getTxtEmail());
		emailModel.setStatus(String.valueOf(Status.NEW.getId()));
		emailModel.setSubject("ALeague - Account verfication");
		transactionalEmailService.create(emailModel);
	}

	private void sendEmailInvitation(User userModel, String pass, String instituteName) {
		TransactionalEmailModel emailModel = new TransactionalEmailModel();
		String creationInfo = "";

		if (Auditor.getAuditor().getFkRoleId().getId().longValue() == 5l
				|| Auditor.getAuditor().getFkRoleId().getId().longValue() == 6l) {
			creationInfo = Auditor.getAuditor().getTxtFirstName() + " " + Auditor.getAuditor().getTxtLastName() + ", "
					+ Auditor.getAuditor().getFkRoleId().getTxtName() + " of " + instituteName;
		} else {
			creationInfo = " its Super Admin";
		}

		String body = EmailContentModel.getMAP().get(2l).getContent()
				.replaceAll("\\{ApplicantName}", userModel.getTxtFirstName() + " " + userModel.getTxtLastName())
				.replaceAll("\\{CreationInfo}", creationInfo).replaceAll("\\{EmailID}", userModel.getTxtEmail())
				.replaceAll("\\{Password}", pass);

		emailModel.setBody(body);
		emailModel.setEmailAccountId(EmailAccountModel.getMAP().get(1l).getId());
		emailModel.setEmailTo(userModel.getTxtEmail());
		emailModel.setStatus(String.valueOf(Status.NEW.getId()));
		emailModel.setSubject("Invitation to ALeague portal");
		transactionalEmailService.create(emailModel);
	}

	@Override
	public Response doVerification(String token) throws CapsException {
		User userModel = userService.getUsingToken(token);
		if (userModel == null) {
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(),
					ResponseCode.INVALID_REQUEST.getMessage());
		}

		if (String.valueOf(CommonStatus.YES.getId()).equals(userModel.getEnumActTokenUsed())) {
			return CommonResponse.create(ResponseCode.LINK_EXPIRED.getCode(), ResponseCode.LINK_EXPIRED.getMessage());
		}
		userModel.setEnumActTokenUsed(String.valueOf(CommonStatus.YES.getId()));
		userModel.setActivationStatus(ActiveInActive.ACTIVE);
		userService.update(userModel);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(),
				"Your account has been activated successfully. Please login to continue.");
	}

	@Override
	public Response doSearch(int start, int end, UserView userview) {
		UserBasic userbasic = new UserBasic();
		if (!userview.getUserRoleType().equals("0")) {
			userbasic.setFkRoleId(roleService.get(Long.valueOf(userview.getUserRoleType())));
		}

		if (userview.getIsRegistered() != null && userview.getIsRegistered()) {
			userbasic.setEnumApprove("1");
			userbasic.setEnumActTokenUsed("0");
		}

		if (userview.getIsApproved() != null && userview.getIsApproved()) {
			userbasic.setEnumApprove("2");
			userbasic.setEnumActTokenUsed("1");
		}

		//PageModel pageModel = userService.search(user, start, end);
		PageModel pageModel = userBasicService.search(userbasic, start, end);
		List<UserBasic> userBasicList = (List<UserBasic>) pageModel.getList();
		List<UserView> userViewList = new ArrayList<>(userBasicList.size());
        for (UserBasic testUserBasic : userBasicList) {
        	userViewList.add(fromModelUserBasic(testUserBasic));
        }
		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), "User List", pageModel.getList().size(),userViewList);
	}
	
	private UserView fromModelUserBasic(UserBasic userBasic){
		UserView userView = new UserView();

		if (userBasic.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
			userView.setIsActive(true);
		} else {
			userView.setIsActive(false);
		}

		userView.setId(userBasic.getId());
		userView.setTxtFirstName(userBasic.getTxtFirstName());
		userView.setTxtLastName(userBasic.getTxtLastName());
		userView.setEmailId(userBasic.getTxtEmail());
		userView.setInstituteName(userBasic.getFkInstituteId().getTxtName());
		
		userView.setAbout(userBasic.getAbout());

		if (userBasic.getTxtProfilePicPath() != null && !"".equals(userBasic.getTxtProfilePicPath())) {
			userView.setTxtProfilePicPath(FileUtility.setPath(userBasic.getTxtProfilePicPath()));
		} else {
			userView.setTxtProfilePicPath(FileUtility.setPath("defaultuserprofilepic.PNG"));
		}

		if (userBasic.getNumberBatchYear() != null) {
			userView.setNumberBatchYear(userBasic.getNumberBatchYear());
		}
		if(userBasic.getTxtUserprofileLink() !=null){
			userView.setTxtUserprofileLink(userBasic.getTxtUserprofileLink());
		}
		userView.setTxtContactNumber(userBasic.getTxtContactNumber());
		userView.setTxtLinkdinLink(userBasic.getTxtLinkdinLink());
		userView.setRoleType(KeyValueView.create(userBasic.getFkRoleId().getId(), userBasic.getFkRoleId().getTxtName()));
		if (userBasic.getEnrolmentNumber() != null) {
			userView.setEnrolmentNumber(userBasic.getEnrolmentNumber());
		}
		userView.setCourse(userBasic.getCourse());

		if ("1".equals(userBasic.getEnumApprove())) {
			userView.setCanCreate(false);
		} else {
			userView.setCanCreate(true);
		}
		return userView;
	}

	@Override
	public Response doInviteOperation(UserView view) throws Exception {
		if ((Auditor.getAuditor().getFkRoleId().getId().longValue() == 1l && view.getSaInstituteId() != null)
				|| Auditor.getAuditor().getFkRoleId().getId().longValue() == 5l
				|| Auditor.getAuditor().getFkRoleId().getId().longValue() == 6l) {
			User existUser = userService.userLogin(view.getEmailId());

			Boolean isFresh = true, canSave = false;
			if (existUser != null && existUser.getId().longValue() == view.getId().longValue()) {
				canSave = true;
				isFresh = false;
			} else if (existUser == null) {
				isFresh = true;
				canSave = true;
			}

			if (canSave) {
				User user = userService.get(view.getId());
				user.setTxtFirstName(view.getTxtFirstName());
				user.setTxtLastName(view.getTxtLastName());
				user.setFkRoleId(roleService.get(view.getRoleType().getId()));
				
				if (isFresh) {
					user.setTxtEmail(view.getEmailId());
					user.setCreateBy(Auditor.getAuditor());
					user.setEnumApprove(String.valueOf(CommonStatus.YES.getId()));					
					user.setActivationStatus(ActiveInActive.ACTIVE);
					user.setTxtActToken(Utility.generateCode(8));
					user.setEnumActTokenUsed(String.valueOf(CommonStatus.YES.getId()));
					user.setEnumArchive("1");
					user.setEnrolmentNumber(view.getEnrolmentNumber());

					InstituteModel institute;
					if (Auditor.getAuditor().getId().longValue() == 1l) {
						institute = instituteService.get(view.getSaInstituteId());
					} else {
						institute = Auditor.getAuditor().getFkInstituteId();
					}

					user.setFkInstituteId(institute);

					String pass = view.getTxtFirstName() + "@" + "123";
					view.setPassword(pass);
					setUserPassword(user, view);
					userService.create(user);
					sendEmailInvitation(user, pass, institute.getTxtName());
					return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(),
							"User has been successfully invited.");
				} else {
					userService.update(user);
					return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(),
							"User has been successfully updated.");
				}
			} else {
				return CommonResponse.create(ResponseCode.INVALID_FORM_DATA.getCode(), "User cannot be saved.");
			}
		} else {
			return CommonResponse.create(ResponseCode.INVALID_FORM_DATA.getCode(), "No Permission.");
		}
	}

	@Override
	public Response doApproveOperation(UserView userview) throws Exception {
		User user = userService.get(userview.getId());
		user.setEnumApprove("2");
		if(user.getUserJsessions() != null && ! user.getUserJsessions().isEmpty()){
			for (Iterator<UserJsessions> i = user.getUserJsessions().iterator(); i.hasNext();) {
				i.next();
			    i.remove();
			}
		}
		/*List<UserJsessions> userJsessionList = sessionService.getSessionByUser(user.getId());
		for(UserJsessions userJsessions : userJsessionList){
			sessionService.delete(userJsessions.getId());
		}*/
		userService.update(user);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "User has been successfully approved.");
	}

	@Override
	public Response doEnableDisableOperation(UserView userview) throws Exception {
		User user = userService.get(userview.getId());

		String msg = "User has been successfully enabled.";

		if (user.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
			msg = "User has been successfully disabled.";
			user.setActivationStatus(ActiveInActive.INACTIVE);
			/*List<UserJsessions> userJsessionList = sessionService.getSessionByUser(user.getId());
			for(UserJsessions userJsessions : userJsessionList){
				sessionService.delete(userJsessions.getId());
			}*/
			
			if(user.getUserJsessions() != null && ! user.getUserJsessions().isEmpty()){
				for (Iterator<UserJsessions> j = user.getUserJsessions().iterator(); j.hasNext();) {
					j.next();
				    j.remove();
				}
			}
		} else {
			user.setActivationStatus(ActiveInActive.ACTIVE);
		}

		user.setActivationChangeBy(Auditor.getAuditor());
		user.setActivationDate(DateUtility.getCurrentDate());
		userService.update(user);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), msg);
	}

	@Override
	public Response doSearchFaculty(long instituteId, int start, int end) {
		List<Long> facultyList = new ArrayList<>();
		facultyList.add(3l);
		facultyList.add(5l);
		List<UserBasic> listUsers = userBasicService.fetchInstituteFaculty(facultyList, instituteId, start, end);
		List<UserView> listUserView = new ArrayList<>();

		for (UserBasic userBasic : listUsers) {
			UserView userView = new UserView();
			userView.setId(userBasic.getId());
			userView.setTxtFirstName(userBasic.getTxtFirstName());
			userView.setTxtLastName(userBasic.getTxtLastName());
			userView.setEmailId(userBasic.getTxtEmail());
			userView.setAbout(userBasic.getAbout());
			if (userBasic.getTxtProfilePicPath() != null && !"".equals(userBasic.getTxtProfilePicPath())) {
				userView.setTxtProfilePicPath(FileUtility.setPath(userBasic.getTxtProfilePicPath()));
			} else {
				userView.setTxtProfilePicPath(FileUtility.setPath("defaultuserprofilepic.PNG"));
			}
			userView.setTxtLinkdinLink(userBasic.getTxtLinkdinLink());

			listUserView.add(userView);
		}
		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_FACULTIES,
				listUserView.size(), listUserView);
	}

	@Override
	public Response doSetCoordinatorUserOperation(String selectedUsers) {
		try {
			String users = selectedUsers.replace("[", "(");
			users = selectedUsers.replace("]", ")");
			List<User> listUsers = userService.findByIds(users);
			Map<Long, User> mapUsers = new HashMap<>();

			for (User user : listUsers) {
				mapUsers.put(user.getId(), user);
			}

			JSONArray jsonArrayUsers = new JSONArray(selectedUsers);
			Role roleFC = roleService.get(5l);

			Role roleSC = roleService.get(6l);

			for (Integer i = 0; i < jsonArrayUsers.length(); i++) {
				try {
					if (mapUsers.containsKey(jsonArrayUsers.getLong(i))) {
						User user = mapUsers.get(jsonArrayUsers.getLong(i));

						if (user.getFkRoleId().getId() == 3) {
							user.setFkRoleId(roleFC);
						} else {
							user.setFkRoleId(roleSC);
						}
						/*List<UserJsessions> userJsessionList = sessionService.getSessionByUser(user.getId());
						for(UserJsessions userJsessions : userJsessionList){
							sessionService.delete(userJsessions.getId());
						}*/
						
						if(user.getUserJsessions() != null && ! user.getUserJsessions().isEmpty()){
							for (Iterator<UserJsessions> j = user.getUserJsessions().iterator(); j.hasNext();) {
								j.next();
							    j.remove();
							}
						}
						userService.update(user);
					}
				} catch (JSONException e) {
					LoggerService.exception(e);
				}
			}

			return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Users has been set as Co-ordinators.");
		} catch (JSONException e) {
			LoggerService.exception(e);
		}

		return CommonResponse.create(ResponseCode.INVALID_FORM_DATA.getCode(), "Users cannot be set as Co-ordinators.");
	}

	@Override
	public Response doApproveOperation(String selectedUsers) {
		try {
			String users = selectedUsers.replace("[", "(");
			users = selectedUsers.replace("]", ")");
			List<User> listUsers = userService.findByIds(users);
			Map<Long, User> mapUsers = new HashMap<>();

			for (User user : listUsers) {
				mapUsers.put(user.getId(), user);
			}

			JSONArray jsonArrayUsers = new JSONArray(selectedUsers);

			for (Integer i = 0; i < jsonArrayUsers.length(); i++) {
				try {
					if (mapUsers.containsKey(jsonArrayUsers.getLong(i))) {
						User user = mapUsers.get(jsonArrayUsers.getLong(i));
						user.setEnumApprove("2");
						/*List<UserJsessions> userJsessionList = sessionService.getSessionByUser(user.getId());
						for(UserJsessions userJsessions : userJsessionList){
							sessionService.delete(userJsessions.getId());
						}*/
						if(user.getUserJsessions() != null && ! user.getUserJsessions().isEmpty()){
							for (Iterator<UserJsessions> j = user.getUserJsessions().iterator(); j.hasNext();) {
								j.next();
							    j.remove();
							}
						}
						userService.update(user);
					}
				} catch (JSONException e) {
					LoggerService.exception(e);
				}
			}

			return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Users has been approved.");
		} catch (JSONException e) {
			LoggerService.exception(e);
		}

		return CommonResponse.create(ResponseCode.INVALID_FORM_DATA.getCode(), "Users cannot be approved.");
	}

	@Override
	public File doExport(UserView userView) throws Exception {
		String enumApprove = null;
		String activationTokenUsed = null;
		Long instituteId = null;
		
		if(Auditor.getAuditor().getFkInstituteId() != null){
			instituteId = Auditor.getAuditor().getFkInstituteId().getId();
		}
		long roleId = Long.valueOf(userView.getUserRoleType());
		
		if (userView.getIsRegistered() != null && userView.getIsRegistered()) {
			enumApprove = "1";
			activationTokenUsed = "0";
		}

		if (userView.getIsApproved() != null && userView.getIsApproved()) {
			enumApprove = "2";
			activationTokenUsed = "1";
		}
		
		List<UserBasic> userBasicList = userBasicService.getUser(roleId, enumApprove, activationTokenUsed, instituteId);
		
	//	Map<String, Object[]> data = new TreeMap<String, Object[]>();
		//data.put("1", new Object[] {"Email", "Name", "Institute Name", "Enrolment Number", "Status"});
		List<Object[]> data = new ArrayList<Object[]>();
		data.add(new Object[] {"Email", "Name", "Institute Name", "Enrolment Number", "Status"});
        
        int i = 2;
		for(UserBasic userBasic : userBasicList){
			i++;
			String status = userBasic.getActivationStatus().getName();
			/*data.put(String.valueOf(i), new Object[] {userBasic.getTxtEmail(), userBasic.getTxtFirstName() + " "+ userBasic.getTxtLastName(),
					userBasic.getFkInstituteId().getTxtName(), userBasic.getEnrolmentNumber(), status});*/
			data.add(new Object[] {userBasic.getTxtEmail(), userBasic.getTxtFirstName() + " "+ userBasic.getTxtLastName(),
					userBasic.getFkInstituteId().getTxtName(), userBasic.getEnrolmentNumber(), status});
		}
		
		return prepareFile(data);
	}
	
	public static File prepareFile(/*Map<String, Object[]> data*/ List<Object[]> data) throws IOException{
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("UserList");
	
      //Iterate over data and write to sheet
       // Set<String> keyset = data.keySet();
        int rownum = 0;
        
        /*for (Map.Entry<String, Object[]> entry : data.entrySet())
        {
            System.out.println(entry.getKey() + "/" + entry.getValue());
        }*/
        for (Object[] key : data)
        {
            Row row = sheet.createRow(rownum++);
           // Object [] objArr = data.get(entry.getValue());
            Object [] objArr = key;
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        FileOutputStream out = null;
        File file = new File("UserList.xlsx");
        try{
        	out = new FileOutputStream(file);
            workbook.write(out);
        }finally {
        	if(out != null){
        		out.close();	
        	}
		}
        return file;
	}
}
