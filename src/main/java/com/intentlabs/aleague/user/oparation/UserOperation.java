/**
 * 
 */
package com.intentlabs.aleague.user.oparation;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.intentlabs.aleague.exception.CapsException;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.exception.InvalidRequestException;
import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.user.view.UserView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * @author Dhruvang
 *
 */
public interface UserOperation extends BaseOperation<UserView> {
	
	
	String USER_OPERATION = "userOperation";
	
	Response doValidateUSeremailid(UserView userview) throws Exception;
	Response doUserLogin(UserView userView,HttpServletRequest request, HttpServletResponse response) throws InvalidDataException,Exception;
    Response doGenerateOtpForForgetPassword(UserView userView) throws Exception;
    Response doUpdateUserPassword(UserView userView, HttpServletResponse response)throws InvalidRequestException;
    Response logOut(HttpSession session,RedirectAttributes redirectAttributes) throws Exception;
    Response doValidateEmailIdOperation(UserView uservies) throws Exception;
    Response getLoggeedinUser(String sessionId);
    User getBySessionId(String sessionId);
    Response doGetUserProfileOperation(Long id);
    Response doSaveUserProfileOperation(UserView view);
    Response doVerification(String token) throws CapsException;
    Response doSearch(int start, int end, UserView userview) throws Exception;
    Response doInviteOperation(UserView userview) throws Exception;
    Response doApproveOperation(UserView userview) throws Exception;
    Response doEnableDisableOperation(UserView userview) throws Exception;
    Response doSearchFaculty(long instituteId, int start, int end);
    Response doSetCoordinatorUserOperation(String selectedUsers);
    Response doApproveOperation(String selectedUsers);
    File doExport(UserView userView)throws Exception;
}
