package com.intentlabs.aleague.event.controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.event.view.AttachmentView;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface AttachmentController extends WebController<AttachmentView>
{

	String ATTACHMENT_CONTROLLER = "AttachmentController";
	
	@RequestMapping(value = "/saveattachment.htm", method = RequestMethod.POST)
	@ResponseBody
	Response saveAttachment(@ModelAttribute AttachmentView view) throws InvalidDataException, Exception;
}
