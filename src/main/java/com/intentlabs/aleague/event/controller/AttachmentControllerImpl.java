
package com.intentlabs.aleague.event.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.intentlabs.aleague.event.operation.AttachmentOperation;
import com.intentlabs.aleague.event.view.AttachmentView;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;

/**
 * This is controller for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@SuppressWarnings("serial")
@Controller
@RequestMapping("/attachment")
public class AttachmentControllerImpl extends AbstractController<AttachmentView> implements AttachmentController {

	@Autowired
	AttachmentOperation attachmentOperation;

	@Override
	public BaseOperation<AttachmentView> getOperation() {
		return attachmentOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(AttachmentView webView) throws Exception {
	}

	@Override
	public Response save(AttachmentView view) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response update(AttachmentView view) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response saveAttachment(@ModelAttribute AttachmentView view) throws InvalidDataException, Exception {
		try{
			LoggerService.info(ATTACHMENT_CONTROLLER, OperationName.SAVE_ATTACHMENT, LogMessage.BEFORE_VALIDATION);
			isValidSaveData(view);
			LoggerService.info(ATTACHMENT_CONTROLLER, OperationName.SAVE_ATTACHMENT, LogMessage.BEFORE_CALLING_OPERATION);
			return attachmentOperation.doSaveOperation(view);
		}
		finally {
			LoggerService.info(ATTACHMENT_CONTROLLER, OperationName.SAVE_ATTACHMENT, LogMessage.REQUEST_COMPLETED);			
		}
	}
}
