
package com.intentlabs.aleague.event.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.event.view.EventView;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface EventController extends WebController<EventView>
{

	String EVENT_CONTROLLER = "EventController";
	
	@RequestMapping(value = "/saveevent.htm", method = RequestMethod.POST)
	@ResponseBody
	Response saveEvent(@ModelAttribute EventView view) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getevent.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getEvent(@RequestBody EventView eventView) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/getallevents.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getAllEvents(@RequestBody (required=false) EventView eventView, @RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/changeeventuserstatus.htm", method = RequestMethod.POST)
	@ResponseBody
	Response changeEventUserStatus(@RequestBody EventView eventView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getalleventusers.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getAllEventUsers(@RequestBody (required=false) EventView eventView, @RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/deleteEvent.htm", method = RequestMethod.POST)
	@ResponseBody
	Response delete(@RequestBody EventView eventView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/export.htm", method = RequestMethod.POST)
	@ResponseBody
	Response exportEventAttendeesList(@RequestBody EventView eventView, HttpServletResponse response) throws InvalidDataException, Exception;
}
