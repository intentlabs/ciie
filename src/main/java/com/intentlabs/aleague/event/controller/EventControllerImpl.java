
package com.intentlabs.aleague.event.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.intentlabs.aleague.event.operation.EventOperation;
import com.intentlabs.aleague.event.view.EventView;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.util.Utility;

/**
 * This is controller for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@SuppressWarnings("serial")
@Controller
@RequestMapping("/event")
public class EventControllerImpl extends AbstractController<EventView> implements EventController {

	@Autowired
	EventOperation eventOperation;

	@Override
	public BaseOperation<EventView> getOperation() {
		return eventOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(EventView webView) throws Exception {
	}

	@Override
	public Response save(EventView view) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response update(EventView view) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response saveEvent(@ModelAttribute EventView view) throws InvalidDataException, Exception {
		try{
			LoggerService.info(EVENT_CONTROLLER, OperationName.SAVE_EVENT, LogMessage.BEFORE_VALIDATION);
			isValidSaveData(view);
			LoggerService.info(EVENT_CONTROLLER, OperationName.SAVE_EVENT, LogMessage.BEFORE_CALLING_OPERATION);
			return eventOperation.doSaveOperation(view);
		}
		finally {
			LoggerService.info(EVENT_CONTROLLER, OperationName.SAVE_EVENT, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response getEvent(@RequestBody EventView eventView) throws InvalidDataException, Exception {
		return eventOperation.doDisplayOperation(eventView.getId());
	}

	@Override
	public Response getAllEvents(@RequestBody (required=false) EventView eventView, @RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception {		
		if(eventView != null){
			return eventOperation.doDisplayAllOperation(eventView.getTags(), eventView.getStatus(), start, end);
		}else{
			return eventOperation.doDisplayAllOperation(null, null, start, end);
		}
	}

	@Override
	public Response changeEventUserStatus(@RequestBody EventView eventView) throws InvalidDataException, Exception {
		try{
			LoggerService.info(EVENT_CONTROLLER, OperationName.CHANGE_EVENT_USER_STATUS, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(EVENT_CONTROLLER, OperationName.CHANGE_EVENT_USER_STATUS, LogMessage.BEFORE_CALLING_OPERATION);
			return eventOperation.doChangeEventUserStatusOperation(eventView.getId(), eventView.getStatus());
		}
		finally {
			LoggerService.info(EVENT_CONTROLLER, OperationName.CHANGE_EVENT_USER_STATUS, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response getAllEventUsers(@RequestBody (required=false) EventView eventView, @RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception {
		if(eventView != null){
			return eventOperation.doDisplayEventUsersOperation(eventView.getId(), eventView.getStatus(), start, end);	
		}else{
			return eventOperation.doDisplayEventUsersOperation(null, null, start, end);
		}
		
	}

	@Override
	public Response delete(@RequestBody EventView eventView) throws InvalidDataException, Exception {
		return eventOperation.doDelete(eventView.getId());
	}

	@Override
	public Response exportEventAttendeesList(@RequestBody EventView eventView, HttpServletResponse response) throws InvalidDataException, Exception {
		
		File file = eventOperation.doExport(eventView.getId());
		
		response.setContentType("application/xls");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ file.getName() + "\"");
		
		InputStream stream = new FileInputStream(file);
		FileCopyUtils.copy(stream, response.getOutputStream());
		
		Utility.deleteFile(file);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}
}
