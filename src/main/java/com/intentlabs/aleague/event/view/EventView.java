
package com.intentlabs.aleague.event.view;

import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.view.CreateView;

/**
 * This is View used to get data for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventView extends CreateView {

	private static final long serialVersionUID = 8692674149531174388L;

	private String name;
	private String eventType;
	private String startDate;
	private String endDate;
	private String duration;
	private String location;
	private String isrsvp;
	private String description;
	private String agenda;
	private String eventPartners;
	private Set<EventPartnerView> setEventPartners;
	private String eventSponsors;
	private Set<EventSponsorView> setEventSponsors;
	private String facebookLink;
	private String instagramLink;
	private String youtubeLink;
	private String eventLink;
	private String tags;
	private Set<TagView> setTags;
	private String capacity;
	private String maxCapacity;
	private String contactPersonName;
	private String contactPersonEmail;
	private String contactNumber;
	private String eventGalleries;
	private Set<EventGalleryView> setEventGalleries;
	private String bannerImagePath;
	private MultipartFile file;
	private Set<EventUserStatusView> setEventUserStatus;
	private String status;
	private String startdatemonth;
	private String startdateday;
	private String startdatedayofweek;
	private Boolean canEditEvent;
	private String instituteName;
	private String eventUserStatus;
	private Boolean canChangeStatus;
	private Long goingUsersCount;
	private Long maybeUsersCount;
	private Long notgoingUsersCount;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the eventType
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * @param eventType
	 *            the eventType to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the duration
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the isrsvp
	 */
	public String getIsrsvp() {
		return isrsvp;
	}

	/**
	 * @param isrsvp
	 *            the isrsvp to set
	 */
	public void setIsrsvp(String isrsvp) {
		this.isrsvp = isrsvp;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the agenda
	 */
	public String getAgenda() {
		return agenda;
	}

	/**
	 * @param agenda
	 *            the agenda to set
	 */
	public void setAgenda(String agenda) {
		this.agenda = agenda;
	}

	/**
	 * @return the setEventPartners
	 */
	public Set<EventPartnerView> getSetEventPartners() {
		return setEventPartners;
	}

	/**
	 * @param setEventPartners
	 *            the setEventPartners to set
	 */
	public void setSetEventPartners(Set<EventPartnerView> setEventPartners) {
		this.setEventPartners = setEventPartners;
	}

	/**
	 * @return the setEventSponsors
	 */
	public Set<EventSponsorView> getSetEventSponsors() {
		return setEventSponsors;
	}

	/**
	 * @param setEventSponsors
	 *            the setEventSponsors to set
	 */
	public void setSetEventSponsors(Set<EventSponsorView> setEventSponsors) {
		this.setEventSponsors = setEventSponsors;
	}

	/**
	 * @return the facebookLink
	 */
	public String getFacebookLink() {
		return facebookLink;
	}

	/**
	 * @param facebookLink
	 *            the facebookLink to set
	 */
	public void setFacebookLink(String facebookLink) {
		this.facebookLink = facebookLink;
	}

	/**
	 * @return the instagramLink
	 */
	public String getInstagramLink() {
		return instagramLink;
	}

	/**
	 * @param instagramLink
	 *            the instagramLink to set
	 */
	public void setInstagramLink(String instagramLink) {
		this.instagramLink = instagramLink;
	}

	/**
	 * @return the youtubeLink
	 */
	public String getYoutubeLink() {
		return youtubeLink;
	}

	/**
	 * @param youtubeLink
	 *            the youtubeLink to set
	 */
	public void setYoutubeLink(String youtubeLink) {
		this.youtubeLink = youtubeLink;
	}

	/**
	 * @return the eventLink
	 */
	public String getEventLink() {
		return eventLink;
	}

	/**
	 * @param eventLink
	 *            the eventLink to set
	 */
	public void setEventLink(String eventLink) {
		this.eventLink = eventLink;
	}

	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * @return the setTags
	 */
	public Set<TagView> getSetTags() {
		return setTags;
	}

	/**
	 * @param setTags
	 *            the setTags to set
	 */
	public void setSetTags(Set<TagView> setTags) {
		this.setTags = setTags;
	}

	/**
	 * @return the capacity
	 */
	public String getCapacity() {
		return capacity;
	}

	/**
	 * @param capacity
	 *            the capacity to set
	 */
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	/**
	 * @return the maxCapacity
	 */
	public String getMaxCapacity() {
		return maxCapacity;
	}

	/**
	 * @param maxCapacity
	 *            the maxCapacity to set
	 */
	public void setMaxCapacity(String maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	/**
	 * @return the contactPersonName
	 */
	public String getContactPersonName() {
		return contactPersonName;
	}

	/**
	 * @param contactPersonName
	 *            the contactPersonName to set
	 */
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	/**
	 * @return the contactPersonEmail
	 */
	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	/**
	 * @param contactPersonEmail
	 *            the contactPersonEmail to set
	 */
	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber
	 *            the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the setEventGalleries
	 */
	public Set<EventGalleryView> getSetEventGalleries() {
		return setEventGalleries;
	}

	/**
	 * @param setEventGalleries
	 *            the setEventGalleries to set
	 */
	public void setSetEventGalleries(Set<EventGalleryView> setEventGalleries) {
		this.setEventGalleries = setEventGalleries;
	}

	/**
	 * @return the bannerImagePath
	 */
	public String getBannerImagePath() {
		return bannerImagePath;
	}

	/**
	 * @param bannerImagePath
	 *            the bannerImagePath to set
	 */
	public void setBannerImagePath(String bannerImagePath) {
		this.bannerImagePath = bannerImagePath;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * @return the setEventUserStatus
	 */
	public Set<EventUserStatusView> getSetEventUserStatus() {
		return setEventUserStatus;
	}

	/**
	 * @param setEventUserStatus
	 *            the setEventUserStatus to set
	 */
	public void setSetEventUserStatus(Set<EventUserStatusView> setEventUserStatus) {
		this.setEventUserStatus = setEventUserStatus;
	}

	/**
	 * @return the eventPartners
	 */
	public String getEventPartners() {
		return eventPartners;
	}

	/**
	 * @param eventPartners
	 *            the eventPartners to set
	 */
	public void setEventPartners(String eventPartners) {
		this.eventPartners = eventPartners;
	}

	/**
	 * @return the eventSponsors
	 */
	public String getEventSponsors() {
		return eventSponsors;
	}

	/**
	 * @param eventSponsors
	 *            the eventSponsors to set
	 */
	public void setEventSponsors(String eventSponsors) {
		this.eventSponsors = eventSponsors;
	}

	/**
	 * @return the eventGalleries
	 */
	public String getEventGalleries() {
		return eventGalleries;
	}

	/**
	 * @param eventGalleries
	 *            the eventGalleries to set
	 */
	public void setEventGalleries(String eventGalleries) {
		this.eventGalleries = eventGalleries;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the startdatemonth
	 */
	public String getStartdatemonth() {
		return startdatemonth;
	}

	/**
	 * @param startdatemonth
	 *            the startdatemonth to set
	 */
	public void setStartdatemonth(String startdatemonth) {
		this.startdatemonth = startdatemonth;
	}

	/**
	 * @return the startdateday
	 */
	public String getStartdateday() {
		return startdateday;
	}

	/**
	 * @param startdateday
	 *            the startdateday to set
	 */
	public void setStartdateday(String startdateday) {
		this.startdateday = startdateday;
	}

	/**
	 * @return the startdatedayofweek
	 */
	public String getStartdatedayofweek() {
		return startdatedayofweek;
	}

	/**
	 * @param startdatedayofweek
	 *            the startdatedayofweek to set
	 */
	public void setStartdatedayofweek(String startdatedayofweek) {
		this.startdatedayofweek = startdatedayofweek;
	}

	/**
	 * @return the canEditEvent
	 */
	public Boolean getCanEditEvent() {
		return canEditEvent;
	}

	/**
	 * @param canEditEvent
	 *            the canEditEvent to set
	 */
	public void setCanEditEvent(Boolean canEditEvent) {
		this.canEditEvent = canEditEvent;
	}

	/**
	 * @return the instituteName
	 */
	public String getInstituteName() {
		return instituteName;
	}

	/**
	 * @param instituteName
	 *            the instituteName to set
	 */
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	/**
	 * @return the eventUserStatus
	 */
	public String getEventUserStatus() {
		return eventUserStatus;
	}

	/**
	 * @param eventUserStatus
	 *            the eventUserStatus to set
	 */
	public void setEventUserStatus(String eventUserStatus) {
		this.eventUserStatus = eventUserStatus;
	}

	/**
	 * @return the canChangeStatus
	 */
	public Boolean getCanChangeStatus() {
		return canChangeStatus;
	}

	/**
	 * @param canChangeStatus
	 *            the canChangeStatus to set
	 */
	public void setCanChangeStatus(Boolean canChangeStatus) {
		this.canChangeStatus = canChangeStatus;
	}

	/**
	 * @return the goingUsersCount
	 */
	public Long getGoingUsersCount() {
		return goingUsersCount;
	}

	/**
	 * @param goingUsersCount
	 *            the goingUsersCount to set
	 */
	public void setGoingUsersCount(Long goingUsersCount) {
		this.goingUsersCount = goingUsersCount;
	}

	/**
	 * @return the maybeUsersCount
	 */
	public Long getMaybeUsersCount() {
		return maybeUsersCount;
	}

	/**
	 * @param maybeUsersCount
	 *            the maybeUsersCount to set
	 */
	public void setMaybeUsersCount(Long maybeUsersCount) {
		this.maybeUsersCount = maybeUsersCount;
	}

	/**
	 * @return the notgoingUsersCount
	 */
	public Long getNotgoingUsersCount() {
		return notgoingUsersCount;
	}

	/**
	 * @param notgoingUsersCount
	 *            the notgoingUsersCount to set
	 */
	public void setNotgoingUsersCount(Long notgoingUsersCount) {
		this.notgoingUsersCount = notgoingUsersCount;
	}
}
