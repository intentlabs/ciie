
package com.intentlabs.aleague.event.view;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.common.view.IdentifierView;

/**
 * This is View used to get data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class AttachmentView extends IdentifierView {


	private static final long serialVersionUID = 8692674149531174388L;
	
	private String attachmentpath;
	private MultipartFile file;

	/**
	 * @return the attachmentpath
	 */
	public String getAttachmentpath() {
		return attachmentpath;
	}

	/**
	 * @param attachmentpath the attachmentpath to set
	 */
	public void setAttachmentpath(String attachmentpath) {
		this.attachmentpath = attachmentpath;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}	
}
