
package com.intentlabs.aleague.event.view;

import com.intentlabs.common.view.IdentifierView;

/**
 * This is View used to get data for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventGalleryView extends IdentifierView {

	private static final long serialVersionUID = 8692674149531174388L;

	private AttachmentView attachment;

	/**
	 * @return the attachment
	 */
	public AttachmentView getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment
	 *            the attachment to set
	 */
	public void setAttachment(AttachmentView attachment) {
		this.attachment = attachment;
	}
}
