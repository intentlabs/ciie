
package com.intentlabs.aleague.event.view;

import com.intentlabs.aleague.user.view.UserView;
import com.intentlabs.common.view.IdentifierView;

/**
 * This is View used to get data for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventUserStatusView extends IdentifierView {

	private static final long serialVersionUID = 8692674149531174388L;

	private String eventStatus;
	private UserView userView;
	
	/**
	 * @return the eventStatus
	 */
	public String getEventStatus() {
		return eventStatus;
	}
	/**
	 * @param eventStatus the eventStatus to set
	 */
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
	/**
	 * @return the userView
	 */
	public UserView getUserView() {
		return userView;
	}
	/**
	 * @param userView the userView to set
	 */
	public void setUserView(UserView userView) {
		this.userView = userView;
	}
}
