
package com.intentlabs.aleague.event.view;

import com.intentlabs.common.view.IdentifierView;

/**
 * This is View used to get data for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventSponsorView extends IdentifierView {

	private static final long serialVersionUID = 8692674149531174388L;

	private String sponsorLink;
	private AttachmentView attachment;

	/**
	 * @return the sponsorLink
	 */
	public String getSponsorLink() {
		return sponsorLink;
	}

	/**
	 * @param sponsorLink
	 *            the sponsorLink to set
	 */
	public void setSponsorLink(String sponsorLink) {
		this.sponsorLink = sponsorLink;
	}

	/**
	 * @return the attachment
	 */
	public AttachmentView getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment
	 *            the attachment to set
	 */
	public void setAttachment(AttachmentView attachment) {
		this.attachment = attachment;
	}
}
