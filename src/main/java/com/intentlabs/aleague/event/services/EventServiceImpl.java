
package com.intentlabs.aleague.event.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.sql.JoinType;
import org.json.JSONArray;

import com.intentlabs.aleague.event.model.EventModel;
import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.service.AbstractService;
import com.intentlabs.common.util.DateUtility;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventServiceImpl extends AbstractService<EventModel> implements EventService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<EventModel> getModelClass() {
		return EventModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<EventModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(EventModel model, Criteria commonCriteria) {
		return null;
	}

	@Override
	public EventModel getById(Long id) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("setEventPartnerModel", "eventPartners", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("setEventSponsorModel", "eventSponsors", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("setEventGalleryModel", "eventGalleries", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("setEventTagModel", "eventTags", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("id", id));		
		return (EventModel) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EventModel> getAllEvents(String tags, String status, Integer start, Integer end) {
		if(tags != null && !"".equals(tags) && !"[]".equals(tags)){
			Criteria criteriaEventIds = getSession().createCriteria(getModelClass());
			criteriaEventIds.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			criteriaEventIds.createAlias("setEventPartnerModel", "eventPartners", JoinType.LEFT_OUTER_JOIN);
			criteriaEventIds.createAlias("setEventSponsorModel", "eventSponsors", JoinType.LEFT_OUTER_JOIN);
			criteriaEventIds.createAlias("setEventGalleryModel", "eventGalleries", JoinType.LEFT_OUTER_JOIN);
			criteriaEventIds.createAlias("setEventTagModel", "eventTags", JoinType.LEFT_OUTER_JOIN);
			criteriaEventIds.createAlias("eventTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);
			criteriaEventIds.add(Restrictions.eq("eventTags.activationStatus", Long.valueOf("1")));
			
			try{
				JSONArray jsonArray = new JSONArray(tags);
				List<Long> listTagids = new ArrayList<>();
				for(Integer i = 0; i < jsonArray.length(); i++){
					listTagids.add(jsonArray.getLong(i));
				}
						
				criteriaEventIds.add(Restrictions.in("tags.id", listTagids));
			}catch (Exception e) {
				LoggerService.exception(e);
			}
			
			if(status != null && !"".equals(status)){
				if("upcoming".equals(status)){
					//Date date = DateUtility.getCurrentDate();
					Date date = DateUtility.getOnlyDate();
					/*SimpleExpression exp1 = Restrictions.gt("dateStartDate", date);
					SimpleExpression exp2 = Restrictions.gt("dateEndDate", date);
					SimpleExpression exp3 = Restrictions.lt("dateStartDate", date);*/		
					SimpleExpression exp1 = Restrictions.ge("dateStartDate", date);
					SimpleExpression exp2 = Restrictions.ge("dateEndDate", date);
					SimpleExpression exp3 = Restrictions.le("dateStartDate", date);
					Criterion criteriaOR = Restrictions.disjunction(exp1, Restrictions.conjunction(exp2, exp3));
					criteriaEventIds.add(criteriaOR);
				}else if("past".equals(status)){
					Date date = DateUtility.getOnlyDate();
					criteriaEventIds.add(Restrictions.lt("dateStartDate", date));
					criteriaEventIds.add(Restrictions.lt("dateEndDate", date));
				}else if("institute".equals(status)){
					User user = Auditor.getAuditor();
					if(user.getFkInstituteId() != null){
						Criteria criteriaUser = getSession().createCriteria(User.class);
						criteriaUser.add(Restrictions.eq("fkInstituteId", user.getFkInstituteId()));
						List<User> listInstituteUsers = criteriaUser.list();
						criteriaEventIds.add(Restrictions.in("createBy", listInstituteUsers));
					}else{
						return new ArrayList<EventModel>();
					}
				}else{
					criteriaEventIds.add(Restrictions.eq("createBy", Auditor.getAuditor()));
				}
			}
			
			criteriaEventIds.setProjection(Projections.property("id"));
			
			List<Long> listEvents = criteriaEventIds.list();
			
			if(!listEvents.isEmpty()){
				Criteria criteria = getSession().createCriteria(getModelClass());
				criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
				criteria.createAlias("setEventPartnerModel", "eventPartners", JoinType.LEFT_OUTER_JOIN);
				criteria.createAlias("setEventSponsorModel", "eventSponsors", JoinType.LEFT_OUTER_JOIN);
				criteria.createAlias("setEventGalleryModel", "eventGalleries", JoinType.LEFT_OUTER_JOIN);
				criteria.createAlias("setEventTagModel", "eventTags", JoinType.LEFT_OUTER_JOIN);
				criteria.add(Restrictions.eq("eventTags.activationStatus", Long.valueOf("1")));
				criteria.add(Restrictions.in("id", listEvents));
				criteria.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
				criteria.addOrder(Order.desc("id"));
				criteria.setFirstResult(start);
				criteria.setMaxResults(end - start);
				
				return criteria.list();
			}else{
				return new ArrayList<EventModel>();
			}
		}else{
			Criteria criteria = getSession().createCriteria(getModelClass());
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			criteria.createAlias("setEventPartnerModel", "eventPartners", JoinType.LEFT_OUTER_JOIN);
			criteria.createAlias("setEventSponsorModel", "eventSponsors", JoinType.LEFT_OUTER_JOIN);
			criteria.createAlias("setEventGalleryModel", "eventGalleries", JoinType.LEFT_OUTER_JOIN);
			criteria.createAlias("setEventTagModel", "eventTags", JoinType.LEFT_OUTER_JOIN);
			criteria.setFirstResult(start);
			criteria.setMaxResults(end - start);
			if(status != null && !"".equals(status)){
				if("upcoming".equals(status)){
					//Date date = DateUtility.getCurrentDate();
					Date date = DateUtility.getOnlyDate();
					/*SimpleExpression exp1 = Restrictions.gt("dateStartDate", date);
					SimpleExpression exp2 = Restrictions.gt("dateEndDate", date);
					SimpleExpression exp3 = Restrictions.lt("dateStartDate", date);	*/
					SimpleExpression exp1 = Restrictions.ge("dateStartDate", date);
					SimpleExpression exp2 = Restrictions.ge("dateEndDate", date);
					SimpleExpression exp3 = Restrictions.le("dateStartDate", date);
					Criterion criteriaOR = Restrictions.disjunction(exp1, Restrictions.conjunction(exp2, exp3));
					criteria.add(criteriaOR);
				}else if("past".equals(status)){
					criteria.add(Restrictions.lt("dateStartDate", DateUtility.getOnlyDate()));
					criteria.add(Restrictions.lt("dateEndDate", DateUtility.getOnlyDate()));
				}else if("institute".equals(status)){
					User user = Auditor.getAuditor();
					if(user.getFkInstituteId() != null){
						Criteria criteriaUser = getSession().createCriteria(User.class);
						criteriaUser.add(Restrictions.eq("fkInstituteId", user.getFkInstituteId()));
						List<User> listInstituteUsers = criteriaUser.list();
						criteria.add(Restrictions.in("createBy", listInstituteUsers));
					}else{
						return new ArrayList<EventModel>();
					}
				}else{
					criteria.add(Restrictions.eq("createBy", Auditor.getAuditor()));
				}
			}
			criteria.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
			criteria.addOrder(Order.desc("id"));
			
			return criteria.list();
		}		
	}

	@Override
	public String getName(Long id) {
		Query query = getSession().createQuery("SELECT txtName from EventModel where id = :id");
		query.setParameter("id", id);
		return (String)query.uniqueResult();
	}
}
