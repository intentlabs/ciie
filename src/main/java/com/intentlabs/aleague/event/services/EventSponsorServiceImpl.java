
package com.intentlabs.aleague.event.services;

import org.hibernate.Criteria;

import com.intentlabs.aleague.event.model.EventSponsorModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventSponsorServiceImpl extends AbstractService<EventSponsorModel> implements EventSponsorService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<EventSponsorModel> getModelClass() {
		return EventSponsorModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<EventSponsorModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(EventSponsorModel model, Criteria commonCriteria) {
		return null;
	}
}
