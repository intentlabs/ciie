
package com.intentlabs.aleague.event.services;

import com.intentlabs.aleague.event.model.EventTagModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface EventTagService extends BaseService<EventTagModel> {
}
