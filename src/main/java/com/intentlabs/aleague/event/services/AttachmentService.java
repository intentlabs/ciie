
package com.intentlabs.aleague.event.services;

import com.intentlabs.aleague.event.model.AttachmentModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface AttachmentService extends BaseService<AttachmentModel> {
}
