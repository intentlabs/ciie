
package com.intentlabs.aleague.event.services;

import java.util.List;

import com.intentlabs.aleague.event.model.EventUserStatusModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface EventUserStatusService extends BaseService<EventUserStatusModel> {
	EventUserStatusModel getByEventAndUser(Long id);
	
	List<EventUserStatusModel> getAllByUser();	
	
	List<EventUserStatusModel> getAllByEventAndStatus(Long eventId, Long status, Integer start, Integer end);
	
	List<EventUserStatusModel> getEventAttendeesList(Long eventId);
}
