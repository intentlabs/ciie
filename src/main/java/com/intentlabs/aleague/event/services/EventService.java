
package com.intentlabs.aleague.event.services;

import java.util.List;

import com.intentlabs.aleague.event.model.EventModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface EventService extends BaseService<EventModel> {
	EventModel getById(Long id);
	
	List<EventModel> getAllEvents(String tags, String status, Integer start, Integer end);
	
	String getName(Long id);
}
