
package com.intentlabs.aleague.event.services;

import org.hibernate.Criteria;

import com.intentlabs.aleague.event.model.EventPartnerModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventPartnerServiceImpl extends AbstractService<EventPartnerModel> implements EventPartnerService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<EventPartnerModel> getModelClass() {
		return EventPartnerModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<EventPartnerModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(EventPartnerModel model, Criteria commonCriteria) {
		return null;
	}
}
