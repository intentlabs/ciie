
package com.intentlabs.aleague.event.services;

import org.hibernate.Criteria;

import com.intentlabs.aleague.event.model.EventTagModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventTagServiceImpl extends AbstractService<EventTagModel> implements EventTagService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<EventTagModel> getModelClass() {
		return EventTagModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<EventTagModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(EventTagModel model, Criteria commonCriteria) {
		return null;
	}
}
