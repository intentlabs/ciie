
package com.intentlabs.aleague.event.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.intentlabs.aleague.event.enums.EnumEventUserStatus;
import com.intentlabs.aleague.event.model.EventUserStatusModel;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class EventUserStatusServiceImpl extends AbstractService<EventUserStatusModel> implements EventUserStatusService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<EventUserStatusModel> getModelClass() {
		return EventUserStatusModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<EventUserStatusModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(EventUserStatusModel model, Criteria commonCriteria) {
		return null;
	}

	@Override
	public EventUserStatusModel getByEventAndUser(Long id) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.createAlias("event", "event", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventPartnerModel", "eventPartners", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventSponsorModel", "eventSponsors", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventGalleryModel", "eventGalleries", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventTagModel", "eventTags", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("event.id", id));
		criteria.add(Restrictions.eq("userBasic.id", Auditor.getAuditor().getId()));		
		return (EventUserStatusModel) criteria.uniqueResult();
	}

	@Override
	public List<EventUserStatusModel> getAllByUser() {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.createAlias("event", "event", JoinType.LEFT_OUTER_JOIN);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("event.setEventPartnerModel", "eventPartners", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventSponsorModel", "eventSponsors", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventGalleryModel", "eventGalleries", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventTagModel", "eventTags", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("userBasic.id", Auditor.getAuditor().getId()));		
		return criteria.list();
	}

	@Override
	public List<EventUserStatusModel> getAllByEventAndStatus(Long eventId, Long status, Integer start, Integer end) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.createAlias("event", "event", JoinType.LEFT_OUTER_JOIN);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("event.setEventPartnerModel", "eventPartners", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventSponsorModel", "eventSponsors", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventGalleryModel", "eventGalleries", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("event.setEventTagModel", "eventTags", JoinType.LEFT_OUTER_JOIN);
		
		if(eventId != null && eventId.longValue() > 0){
			criteria.add(Restrictions.eq("event.id", eventId));	
		}
		
		if(status != null && status.longValue() > 0){
			criteria.add(Restrictions.eq("eventStatus", status));	
		}
		
		if(start != null && end != null){
			criteria.setFirstResult(start);
			criteria.setMaxResults(end - start);
		}
		
		return criteria.list();
	}

	@Override
	public List<EventUserStatusModel> getEventAttendeesList(Long eventId) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.add(Restrictions.eq("event.id", eventId));
		criteria.add(Restrictions.eq("eventStatus", EnumEventUserStatus.GOING.getId()));
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return (List<EventUserStatusModel>) criteria.list();
	}
}
