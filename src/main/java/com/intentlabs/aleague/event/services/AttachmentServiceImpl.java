
package com.intentlabs.aleague.event.services;

import org.hibernate.Criteria;

import com.intentlabs.aleague.event.model.AttachmentModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class AttachmentServiceImpl extends AbstractService<AttachmentModel> implements AttachmentService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<AttachmentModel> getModelClass() {
		return AttachmentModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<AttachmentModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(AttachmentModel model, Criteria commonCriteria) {
		return null;
	}
}
