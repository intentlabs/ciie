

package com.intentlabs.aleague.event.enums;

import com.intentlabs.common.enums.EnumType;

/**
 * This is used for defined event type.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public enum EnumEventUserStatus implements EnumType
{
	
	RSVP(1, "RSVP"),
	NOTGOING(2,"NOTGOING"),
	GOING(3,"GOING"),
	MAYBE(4,"MAYBE");

	private final long id;
    private final String name;
    
    EnumEventUserStatus(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
	 return name;
	}

	
    public static EnumEventUserStatus fromId(long id) {
        for (EnumEventUserStatus status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }

}
