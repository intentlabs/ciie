

package com.intentlabs.aleague.event.enums;

import com.intentlabs.common.enums.EnumType;

/**
 * This is used for defined event type.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public enum EnumCapacity implements EnumType
{
	
	OPEN(1, "OPEN"),
	VARIABLE(2,"VARIABLE");

	private final long id;
    private final String name;
    
    EnumCapacity(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
	 return name;
	}

	
    public static EnumCapacity fromId(long id) {
        for (EnumCapacity status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }

}
