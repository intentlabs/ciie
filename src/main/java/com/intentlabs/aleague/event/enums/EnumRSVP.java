

package com.intentlabs.aleague.event.enums;

import com.intentlabs.common.enums.EnumType;

/**
 * This is used for defined event type.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public enum EnumRSVP implements EnumType
{
	
	YES(1, "YES"),
	NO(2,"NO");

	private final long id;
    private final String name;
    
    EnumRSVP(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
	 return name;
	}

	
    public static EnumRSVP fromId(long id) {
        for (EnumRSVP status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }

}
