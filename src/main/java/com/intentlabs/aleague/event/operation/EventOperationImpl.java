
package com.intentlabs.aleague.event.operation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.event.enums.EnumCapacity;
import com.intentlabs.aleague.event.enums.EnumEventType;
import com.intentlabs.aleague.event.enums.EnumEventUserStatus;
import com.intentlabs.aleague.event.enums.EnumRSVP;
import com.intentlabs.aleague.event.model.AttachmentModel;
import com.intentlabs.aleague.event.model.EventGalleryModel;
import com.intentlabs.aleague.event.model.EventModel;
import com.intentlabs.aleague.event.model.EventPartnerModel;
import com.intentlabs.aleague.event.model.EventSponsorModel;
import com.intentlabs.aleague.event.model.EventTagModel;
import com.intentlabs.aleague.event.model.EventUserStatusModel;
import com.intentlabs.aleague.event.services.AttachmentService;
import com.intentlabs.aleague.event.services.EventGalleryService;
import com.intentlabs.aleague.event.services.EventPartnerService;
import com.intentlabs.aleague.event.services.EventService;
import com.intentlabs.aleague.event.services.EventSponsorService;
import com.intentlabs.aleague.event.services.EventTagService;
import com.intentlabs.aleague.event.services.EventUserStatusService;
import com.intentlabs.aleague.event.view.AttachmentView;
import com.intentlabs.aleague.event.view.EventGalleryView;
import com.intentlabs.aleague.event.view.EventPartnerView;
import com.intentlabs.aleague.event.view.EventSponsorView;
import com.intentlabs.aleague.event.view.EventView;
import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.aleague.tag.services.TagService;
import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.aleague.user.model.UserBasic;
import com.intentlabs.aleague.user.services.UserBasicService;
import com.intentlabs.aleague.user.services.UserService;
import com.intentlabs.aleague.user.view.UserView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.Constant;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.View;

/**
 * This is operation for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Component(value = "eventOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class EventOperationImpl extends AbstractOperation<EventModel, EventView> implements EventOperation {

	private static final long serialVersionUID = 741391010890205612L;

	@Autowired
	private EventService eventService;

	@Autowired
	private TagService tagService;

	@Autowired
	private AttachmentService attachmentService;

	@Autowired
	private EventTagService eventTagService;

	@Autowired
	private EventPartnerService eventPartnerService;

	@Autowired
	private EventSponsorService eventSponsorService;

	@Autowired
	private EventGalleryService eventGalleryService;

	@Autowired
	private EventUserStatusService eventUserStatusService;
	
	@Autowired
	private UserBasicService userBasicService;

	@Override
	public BaseService getService() {
		return eventService;
	}

	@Override
	protected EventModel getNewModel(EventView view) {
		EventModel model = new EventModel();
		model.setCreateDate(DateUtility.getCurrentDate());
		return model;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		return null;
	}

	@Override
	protected void checkInactive(EventModel model) throws Exception {
	}

	@Override
	public EventModel toModel(EventModel eventModel, EventView eventView) throws Exception {
		if (eventView.getId() == null || eventView.getId().longValue() <= 0) {
			eventModel.setCreateBy(Auditor.getAuditor());
			eventModel.setActivationStatus(ActiveInActive.ACTIVE);
			eventModel.setActivationDate(DateUtility.getCurrentDate());
			eventModel.setEnumArchive("1");
		}

		eventModel.setUpdateBy(Auditor.getAuditor());
		eventModel.setUpdateDate(DateUtility.getCurrentDate());

		eventModel.setTxtName(eventView.getName());

		if (eventView.getEventType() != null && !"".equals(eventView.getEventType())) {
			if ("PUBLIC".equalsIgnoreCase(eventView.getEventType())) {
				eventModel.setEventType(EnumEventType.PUBLIC);
			} else if ("PRIVATE".equalsIgnoreCase(eventView.getEventType())) {
				eventModel.setEventType(EnumEventType.PRIVATE);
			}
		}

		eventModel.setDateStartDate(DateUtility.toDate(eventView.getStartDate(), new SimpleDateFormat("dd-MM-yyyy")));
		eventModel.setDateEndDate(DateUtility.toDate(eventView.getEndDate(), new SimpleDateFormat("dd-MM-yyyy")));
		eventModel.setTxtLocation(eventView.getLocation());
		
		if(eventView.getDuration() != null && !"".equals(eventView.getDuration())){
			eventModel.setTxtDuration(eventView.getDuration());	
		}else{
			eventModel.setTxtDuration("");
		}		

		if (eventView.getIsrsvp() != null && !"".equals(eventView.getIsrsvp())) {
			if ("YES".equalsIgnoreCase(eventView.getIsrsvp())) {
				eventModel.setRsvp(EnumRSVP.YES);
			} else if ("NO".equalsIgnoreCase(eventView.getIsrsvp())) {
				eventModel.setRsvp(EnumRSVP.NO);
			}
		}

		eventModel.setTxtDescription(eventView.getDescription());
		eventModel.setTxtAgenda(eventView.getAgenda());

		Set<EventPartnerModel> setEventPartnerModel = new HashSet<>();

		Map<Long, EventPartnerModel> mapPartnerModel = new HashMap<>();

		if (eventModel.getSetEventPartnerModel() != null && !eventModel.getSetEventPartnerModel().isEmpty()) {
			for (EventPartnerModel eventPartner : eventModel.getSetEventPartnerModel()) {
				mapPartnerModel.put(eventPartner.getId(), eventPartner);
			}
		}

		if (eventView.getEventPartners() != null && !"".equals(eventView.getEventPartners())) {
			JSONArray jsonArrayEventPartners = new JSONArray(eventView.getEventPartners());

			for (Integer i = 0; i < jsonArrayEventPartners.length(); i++) {
				JSONObject jsonObjectEventPartner = jsonArrayEventPartners.getJSONObject(i);

				EventPartnerModel eventPartner;

				if (jsonObjectEventPartner.getLong("id") > 0) {
					if (mapPartnerModel.containsKey(jsonObjectEventPartner.getLong("id"))) {
						eventPartner = mapPartnerModel.get(jsonObjectEventPartner.getLong("id"));
						mapPartnerModel.remove(jsonObjectEventPartner.getLong("id"));
					} else {
						eventPartner = new EventPartnerModel();
						eventPartner.setCreateBy(Auditor.getAuditor());
						eventPartner.setCreateDate(DateUtility.getCurrentDate());
						eventPartner.setActivationStatus(ActiveInActive.ACTIVE);
						eventPartner.setActivationChangeBy(Auditor.getAuditor());
						eventPartner.setActivationDate(DateUtility.getCurrentDate());
						eventPartner.setEnumArchive("1");
					}
				} else {
					eventPartner = new EventPartnerModel();
					eventPartner.setCreateBy(Auditor.getAuditor());
					eventPartner.setCreateDate(DateUtility.getCurrentDate());
					eventPartner.setActivationStatus(ActiveInActive.ACTIVE);
					eventPartner.setActivationChangeBy(Auditor.getAuditor());
					eventPartner.setActivationDate(DateUtility.getCurrentDate());
					eventPartner.setEnumArchive("1");
				}

				eventPartner.setUpdateBy(Auditor.getAuditor());
				eventPartner.setUpdateDate(DateUtility.getCurrentDate());

				if(StringUtils.isBlank(jsonObjectEventPartner.getString("partnerLink"))){
					eventPartner.setTxtPartnerLink(null);
				}
				else if(jsonObjectEventPartner.getString("partnerLink").startsWith("https:")
						|| jsonObjectEventPartner.getString("partnerLink").startsWith("http:")){
					eventPartner.setTxtPartnerLink(jsonObjectEventPartner.getString("partnerLink"));
				}else{
					eventPartner.setTxtPartnerLink(Constant.HTTP_URL + jsonObjectEventPartner.getString("partnerLink"));
				}
				/*if (jsonObjectEventPartner.getString("partnerLink") != null
						&& !"".equals(jsonObjectEventPartner.getString("partnerLink").startsWith("https:"))) {
					if (jsonObjectEventPartner.getString("partnerLink").startsWith("https:")
							|| jsonObjectEventPartner.getString("partnerLink").startsWith("http:")) {
						eventPartner.setTxtPartnerLink(jsonObjectEventPartner.getString("partnerLink"));
					} else {
						eventPartner
								.setTxtPartnerLink(Constant.HTTP_URL + jsonObjectEventPartner.getString("partnerLink"));
					}
				} else {
					eventPartner.setTxtPartnerLink(null);
				}*/

				try {
					AttachmentModel attachment = null;
					if (jsonObjectEventPartner.getJSONObject("attachment") != null) {
						if (jsonObjectEventPartner.getJSONObject("attachment").getLong("id") != 0l) {
							attachment = attachmentService
									.load(jsonObjectEventPartner.getJSONObject("attachment").getLong("id"));
						}
					}

					if (attachment != null) {
						eventPartner.setAttachment(attachment);
					} else {
						eventPartner.setAttachment(null);
					}
				} catch (Exception ex) {
					LoggerService.info(EVENT_OPERATION, OperationName.EVENT_OPERATION,
							"Exception while loading attachment "
									+ jsonObjectEventPartner.getJSONObject("attachment").getLong("id") + ".");
					LoggerService.exception(ex);
					eventPartner.setAttachment(null);
				}

				setEventPartnerModel.add(eventPartner);
			}
		}

		if (!mapPartnerModel.keySet().isEmpty()) {
			for (Long key : mapPartnerModel.keySet()) {
				EventPartnerModel eventPartner = mapPartnerModel.get(key);

				if (eventPartner.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					eventPartner.setActivationStatus(ActiveInActive.INACTIVE);
					eventPartner.setActivationChangeBy(Auditor.getAuditor());
					eventPartner.setActivationDate(DateUtility.getCurrentDate());
					setEventPartnerModel.add(eventPartner);
				}
			}
		}

		eventModel.setSetEventPartnerModel(setEventPartnerModel);

		Set<EventSponsorModel> setEventSponsorModel = new HashSet<>();

		Map<Long, EventSponsorModel> mapSponsorModel = new HashMap<>();

		if (eventModel.getSetEventSponsorModel() != null && !eventModel.getSetEventSponsorModel().isEmpty()) {
			for (EventSponsorModel eventSponsor : eventModel.getSetEventSponsorModel()) {
				mapSponsorModel.put(eventSponsor.getId(), eventSponsor);
			}
		}

		if (eventView.getEventSponsors() != null && !"".equals(eventView.getEventSponsors())) {
			JSONArray jsonArrayEventSponsors = new JSONArray(eventView.getEventSponsors());

			for (Integer i = 0; i < jsonArrayEventSponsors.length(); i++) {
				JSONObject jsonObjectEventSponsor = jsonArrayEventSponsors.getJSONObject(i);

				EventSponsorModel eventSponsor;

				if (jsonObjectEventSponsor.getLong("id") > 0) {
					if (mapSponsorModel.containsKey(jsonObjectEventSponsor.getLong("id"))) {
						eventSponsor = mapSponsorModel.get(jsonObjectEventSponsor.getLong("id"));
						mapSponsorModel.remove(jsonObjectEventSponsor.getLong("id"));
					} else {
						eventSponsor = new EventSponsorModel();
						eventSponsor.setCreateBy(Auditor.getAuditor());
						eventSponsor.setCreateDate(DateUtility.getCurrentDate());
						eventSponsor.setActivationStatus(ActiveInActive.ACTIVE);
						eventSponsor.setActivationChangeBy(Auditor.getAuditor());
						eventSponsor.setActivationDate(DateUtility.getCurrentDate());
						eventSponsor.setEnumArchive("1");
					}
				} else {
					eventSponsor = new EventSponsorModel();
					eventSponsor.setCreateBy(Auditor.getAuditor());
					eventSponsor.setCreateDate(DateUtility.getCurrentDate());
					eventSponsor.setActivationStatus(ActiveInActive.ACTIVE);
					eventSponsor.setActivationChangeBy(Auditor.getAuditor());
					eventSponsor.setActivationDate(DateUtility.getCurrentDate());
					eventSponsor.setEnumArchive("1");
				}

				eventSponsor.setUpdateBy(Auditor.getAuditor());
				eventSponsor.setUpdateDate(DateUtility.getCurrentDate());

				if(StringUtils.isBlank(jsonObjectEventSponsor.getString("sponsorLink"))){
					eventSponsor.setTxtSponsorLink(null);
				}else if(jsonObjectEventSponsor.getString("sponsorLink").startsWith("https:")
						|| jsonObjectEventSponsor.getString("sponsorLink").startsWith("http:")){
					eventSponsor.setTxtSponsorLink(jsonObjectEventSponsor.getString("sponsorLink"));
				}else{
					eventSponsor.setTxtSponsorLink(Constant.HTTP_URL + jsonObjectEventSponsor.getString("sponsorLink"));
				}
				
				try {
					AttachmentModel attachment = null;
					if (jsonObjectEventSponsor.getJSONObject("attachment") != null) {
						if (jsonObjectEventSponsor.getJSONObject("attachment").getLong("id") != 0l) {
							attachment = attachmentService
									.load(jsonObjectEventSponsor.getJSONObject("attachment").getLong("id"));
						}
					}

					if (attachment != null) {
						eventSponsor.setAttachment(attachment);
					} else {
						eventSponsor.setAttachment(null);
					}
				} catch (Exception ex) {
					LoggerService.info(EVENT_OPERATION, OperationName.EVENT_OPERATION,
							"Exception while loading attachment "
									+ jsonObjectEventSponsor.getJSONObject("attachment").getLong("id") + ".");
					LoggerService.exception(ex);
					eventSponsor.setAttachment(null);
				}

				setEventSponsorModel.add(eventSponsor);
			}
		}

		if (!mapSponsorModel.keySet().isEmpty()) {
			for (Long key : mapSponsorModel.keySet()) {
				EventSponsorModel eventSponsor = mapSponsorModel.get(key);

				if (eventSponsor.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					eventSponsor.setActivationStatus(ActiveInActive.INACTIVE);
					eventSponsor.setActivationChangeBy(Auditor.getAuditor());
					eventSponsor.setActivationDate(DateUtility.getCurrentDate());
					setEventSponsorModel.add(eventSponsor);
				}
			}
		}

		eventModel.setSetEventSponsorModel(setEventSponsorModel);
		
		if(StringUtils.isBlank(eventView.getFacebookLink()) || eventView.getFacebookLink().equals("undefined")){
			eventModel.setTxtFacebookLink(null);
		}else if(eventView.getFacebookLink().startsWith("https:") || eventView.getFacebookLink().startsWith("http:")){
			eventModel.setTxtFacebookLink(eventView.getFacebookLink());
		}else{
			eventModel.setTxtFacebookLink(Constant.HTTP_URL + eventView.getFacebookLink());
		}
		
		if(StringUtils.isBlank(eventView.getInstagramLink()) || eventView.getInstagramLink().equals("undefined")){
			eventModel.setTxtInstagramLink(null);
		}else if(eventView.getInstagramLink().startsWith("https:") || eventView.getInstagramLink().startsWith("http:")){
			eventModel.setTxtInstagramLink(eventView.getInstagramLink());
		}else{
			eventModel.setTxtInstagramLink(Constant.HTTP_URL + eventView.getInstagramLink());
		}
		
		if(StringUtils.isBlank(eventView.getYoutubeLink()) || eventView.getYoutubeLink().equals("undefined")){
			eventModel.setTxtYoutubeLink(null);
		}else if(eventView.getYoutubeLink().startsWith("https:") || eventView.getYoutubeLink().startsWith("http:")){
			eventModel.setTxtYoutubeLink(eventView.getYoutubeLink());
		}else{
			eventModel.setTxtYoutubeLink(Constant.HTTP_URL + eventView.getYoutubeLink());
		}

		if(StringUtils.isBlank(eventView.getEventLink()) || eventView.getEventLink().equals("undefined")){
			eventModel.setTxtEventLink(null);
		}else if(eventView.getEventLink().startsWith("https:") || eventView.getEventLink().startsWith("http:")){
			eventModel.setTxtEventLink(eventView.getEventLink());
		}else{
			eventModel.setTxtEventLink(Constant.HTTP_URL + eventView.getEventLink());
		}

		Set<EventTagModel> setEventTagModel = new HashSet<>();

		Map<Long, EventTagModel> mapTagModel = new HashMap<>();

		if (eventModel.getSetEventTagModel() != null && !eventModel.getSetEventTagModel().isEmpty()) {
			for (EventTagModel eventTag : eventModel.getSetEventTagModel()) {
				mapTagModel.put(eventTag.getTag().getId(), eventTag);
			}
		}

		if (eventView.getTags() != null && !"".equals(eventView.getTags())) {
			JSONArray jsonArrayEventTag = new JSONArray(eventView.getTags());

			List<TagModel> listTags = tagService.getAllTags(null);

			for (Integer i = 0; i < jsonArrayEventTag.length(); i++) {
				Long tagId = jsonArrayEventTag.getLong(i);

				EventTagModel eventTag;
				if (mapTagModel.containsKey(tagId)) {
					eventTag = mapTagModel.get(tagId);

					if (eventTag.getActivationStatus().getId() == ActiveInActive.INACTIVE.getId()) {
						eventTag.setActivationStatus(ActiveInActive.ACTIVE);
					}

					mapTagModel.remove(tagId);
				} else {
					eventTag = new EventTagModel();
					eventTag.setCreateBy(Auditor.getAuditor());
					eventTag.setCreateDate(DateUtility.getCurrentDate());
					eventTag.setActivationStatus(ActiveInActive.ACTIVE);
					eventTag.setActivationChangeBy(Auditor.getAuditor());
					eventTag.setActivationDate(DateUtility.getCurrentDate());
					eventTag.setEnumArchive("1");

					for (TagModel instanceTag : listTags) {
						if (instanceTag.getId().longValue() == tagId.longValue()) {
							eventTag.setTag(instanceTag);
							break;
						}
					}
				}

				eventTag.setUpdateBy(Auditor.getAuditor());
				eventTag.setUpdateDate(DateUtility.getCurrentDate());

				setEventTagModel.add(eventTag);
			}
		}

		if (!mapTagModel.keySet().isEmpty()) {
			for (Long key : mapTagModel.keySet()) {
				EventTagModel eventTag = mapTagModel.get(key);

				if (eventTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					eventTag.setActivationStatus(ActiveInActive.INACTIVE);
					eventTag.setActivationChangeBy(Auditor.getAuditor());
					eventTag.setActivationDate(DateUtility.getCurrentDate());
					setEventTagModel.add(eventTag);
				}
			}
		}

		eventModel.setSetEventTagModel(setEventTagModel);

		if (eventView.getCapacity() != null && !"".equals(eventView.getCapacity())) {
			if ("OPEN".equalsIgnoreCase(eventView.getCapacity())) {
				eventModel.setCapacity(EnumCapacity.OPEN);
				eventModel.setTxtMaxCapacity("");
			} else if ("VARIABLE".equalsIgnoreCase(eventView.getCapacity())) {
				eventModel.setCapacity(EnumCapacity.VARIABLE);
				eventModel.setTxtMaxCapacity(eventView.getMaxCapacity());
			}
		} else {
			eventModel.setTxtMaxCapacity("");
		}

		eventModel.setTxtContactPersonName(eventView.getContactPersonName());
		eventModel.setTxtContactPersonEmail(eventView.getContactPersonEmail());
		eventModel.setTxtContactNumber(eventView.getContactNumber());

		Set<EventGalleryModel> setEventGalleryModel = new HashSet<>();

		Map<Long, EventGalleryModel> mapGalleryModel = new HashMap<>();

		if (eventModel.getSetEventGalleryModel() != null && !eventModel.getSetEventGalleryModel().isEmpty()) {
			for (EventGalleryModel eventGallery : eventModel.getSetEventGalleryModel()) {
				mapGalleryModel.put(eventGallery.getId(), eventGallery);
			}
		}

		if (eventView.getEventGalleries() != null && !"".equals(eventView.getEventGalleries())) {
			JSONArray jsonArrayEventGalleries = new JSONArray(eventView.getEventGalleries());

			for (Integer i = 0; i < jsonArrayEventGalleries.length(); i++) {
				JSONObject jsonObjectEventGallery = jsonArrayEventGalleries.getJSONObject(i);

				EventGalleryModel eventGallery;

				if (jsonObjectEventGallery.getLong("id") > 0) {
					if (mapGalleryModel.containsKey(jsonObjectEventGallery.getLong("id"))) {
						eventGallery = mapGalleryModel.get(jsonObjectEventGallery.getLong("id"));
						mapGalleryModel.remove(jsonObjectEventGallery.getLong("id"));
					} else {
						eventGallery = new EventGalleryModel();
						eventGallery.setCreateBy(Auditor.getAuditor());
						eventGallery.setCreateDate(DateUtility.getCurrentDate());
						eventGallery.setActivationStatus(ActiveInActive.ACTIVE);
						eventGallery.setActivationChangeBy(Auditor.getAuditor());
						eventGallery.setActivationDate(DateUtility.getCurrentDate());
						eventGallery.setEnumArchive("1");
					}
				} else {
					eventGallery = new EventGalleryModel();
					eventGallery.setCreateBy(Auditor.getAuditor());
					eventGallery.setCreateDate(DateUtility.getCurrentDate());
					eventGallery.setActivationStatus(ActiveInActive.ACTIVE);
					eventGallery.setActivationChangeBy(Auditor.getAuditor());
					eventGallery.setActivationDate(DateUtility.getCurrentDate());
					eventGallery.setEnumArchive("1");
				}

				eventGallery.setUpdateBy(Auditor.getAuditor());
				eventGallery.setUpdateDate(DateUtility.getCurrentDate());

				try {
					AttachmentModel attachment = null;
					if (jsonObjectEventGallery.getJSONObject("attachment") != null) {
						if (jsonObjectEventGallery.getJSONObject("attachment").getLong("id") != 0l) {
							attachment = attachmentService
									.load(jsonObjectEventGallery.getJSONObject("attachment").getLong("id"));
						}
					}

					if (attachment != null) {
						eventGallery.setAttachment(attachment);
					} else {
						eventGallery.setAttachment(null);
					}
				} catch (Exception ex) {
					LoggerService.info(EVENT_OPERATION, OperationName.EVENT_OPERATION,
							"Exception while loading attachment "
									+ jsonObjectEventGallery.getJSONObject("attachment").getLong("id") + ".");
					LoggerService.exception(ex);
					eventGallery.setAttachment(null);
				}

				setEventGalleryModel.add(eventGallery);
			}
		}

		if (!mapGalleryModel.keySet().isEmpty()) {
			for (Long key : mapGalleryModel.keySet()) {
				EventGalleryModel eventGallery = mapGalleryModel.get(key);

				if (eventGallery.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					eventGallery.setActivationStatus(ActiveInActive.INACTIVE);
					eventGallery.setActivationChangeBy(Auditor.getAuditor());
					eventGallery.setActivationDate(DateUtility.getCurrentDate());
					setEventGalleryModel.add(eventGallery);
				}
			}
		}

		eventModel.setSetEventGalleryModel(setEventGalleryModel);

		if (eventView.getFile() != null) {
			eventModel.setTxtBannerImagePath(FileUtility.storeFile(eventView.getFile(), "eventbannerimage"));
		}else{
			eventModel.setTxtBannerImagePath("");
		}

		return eventModel;
	}

	@Override
	public EventView fromModel(EventModel eventModel) {
		EventView eventView = new EventView();
		eventView.setId(eventModel.getId());

		if (eventModel.getDateStartDate() != null) {
			eventView.setStartdatemonth(
					DateUtility.toDateTimeString(eventModel.getDateStartDate(), new SimpleDateFormat("MMM")));
			eventView.setStartdateday(
					DateUtility.toDateTimeString(eventModel.getDateStartDate(), new SimpleDateFormat("dd")));
			eventView.setStartdatedayofweek(
					DateUtility.toDateTimeString(eventModel.getDateStartDate(), new SimpleDateFormat("EEEE")));
		}

		eventView.setName(eventModel.getTxtName());

		if (eventModel.getEventType() != null) {
			eventView.setEventType(eventModel.getEventType().getName());
		}

		eventView.setStartDate(
				DateUtility.toDateTimeString(eventModel.getDateStartDate(), new SimpleDateFormat("dd-MM-yyyy")));
		eventView.setEndDate(
				DateUtility.toDateTimeString(eventModel.getDateEndDate(), new SimpleDateFormat("dd-MM-yyyy")));
		eventView.setDuration(eventModel.getTxtDuration());
		eventView.setLocation(eventModel.getTxtLocation());

		if (eventModel.getRsvp() != null) {
			eventView.setIsrsvp(eventModel.getRsvp().getName());
		}

		eventView.setDescription(eventModel.getTxtDescription());
		eventView.setAgenda(eventModel.getTxtAgenda());

		Set<EventPartnerView> setEventPartners = new HashSet<>();
		if (eventModel.getSetEventPartnerModel() != null && !eventModel.getSetEventPartnerModel().isEmpty()) {
			for (EventPartnerModel eventPartner : eventModel.getSetEventPartnerModel()) {
				if (eventPartner.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					EventPartnerView partnerView = new EventPartnerView();
					partnerView.setId(eventPartner.getId());
					partnerView.setPartnerLink(eventPartner.getTxtPartnerLink());

					AttachmentView attachment = new AttachmentView();

					if (eventPartner.getAttachment() != null) {
						attachment.setId(eventPartner.getAttachment().getId());
						attachment.setAttachmentpath(
								FileUtility.setPath(eventPartner.getAttachment().getTxtAttachmentPath()));
					} else {
						attachment.setId(0l);
						attachment.setAttachmentpath(FileUtility.setPath("defaultuserprofilepic.PNG"));
					}

					partnerView.setAttachment(attachment);
					setEventPartners.add(partnerView);
				}
			}
		}

		eventView.setSetEventPartners(setEventPartners);

		Set<EventSponsorView> setEventSponsors = new HashSet<>();
		if (eventModel.getSetEventSponsorModel() != null && !eventModel.getSetEventSponsorModel().isEmpty()) {
			for (EventSponsorModel eventSponsor : eventModel.getSetEventSponsorModel()) {
				if (eventSponsor.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					EventSponsorView sponsorView = new EventSponsorView();
					sponsorView.setId(eventSponsor.getId());
					sponsorView.setSponsorLink(eventSponsor.getTxtSponsorLink());

					AttachmentView attachment = new AttachmentView();

					if (eventSponsor.getAttachment() != null) {
						attachment.setId(eventSponsor.getAttachment().getId());
						attachment.setAttachmentpath(
								FileUtility.setPath(eventSponsor.getAttachment().getTxtAttachmentPath()));
					} else {
						attachment.setId(0l);
						attachment.setAttachmentpath(FileUtility.setPath("defaultuserprofilepic.PNG"));
					}

					sponsorView.setAttachment(attachment);
					setEventSponsors.add(sponsorView);
				}
			}
		}

		eventView.setSetEventSponsors(setEventSponsors);
		eventView.setFacebookLink(eventModel.getTxtFacebookLink());
		eventView.setYoutubeLink(eventModel.getTxtYoutubeLink());
		eventView.setInstagramLink(eventModel.getTxtInstagramLink());
		eventView.setEventLink(eventModel.getTxtEventLink());

		Set<TagView> setTags = new HashSet<>();
		if (eventModel.getSetEventTagModel() != null && !eventModel.getSetEventTagModel().isEmpty()) {
			for (EventTagModel eventTag : eventModel.getSetEventTagModel()) {
				if (eventTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					TagView tagView = new TagView();
					tagView.setId(eventTag.getTag().getId());
					tagView.setTagName(eventTag.getTag().getTxtTagName());
					setTags.add(tagView);
				}
			}
		}

		eventView.setSetTags(setTags);

		if (eventModel.getCapacity() != null) {
			eventView.setCapacity(eventModel.getCapacity().getName());
			eventView.setMaxCapacity(eventModel.getTxtMaxCapacity());
		} else {
			eventView.setCapacity("");
			eventView.setMaxCapacity("");
		}

		eventView.setContactPersonName(eventModel.getTxtContactPersonName());
		eventView.setContactPersonEmail(eventModel.getTxtContactPersonEmail());
		eventView.setContactNumber(eventModel.getTxtContactNumber());

		Set<EventGalleryView> setEventGalleries = new HashSet<>();
		if (eventModel.getSetEventGalleryModel() != null && !eventModel.getSetEventGalleryModel().isEmpty()) {
			for (EventGalleryModel eventGallery : eventModel.getSetEventGalleryModel()) {
				if (eventGallery.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					EventGalleryView galleryView = new EventGalleryView();
					galleryView.setId(eventGallery.getId());

					AttachmentView attachment = new AttachmentView();

					if (eventGallery.getAttachment() != null) {
						attachment.setId(eventGallery.getAttachment().getId());
						attachment.setAttachmentpath(
								FileUtility.setPath(eventGallery.getAttachment().getTxtAttachmentPath()));
					} else {
						attachment.setId(0l);
						attachment.setAttachmentpath(FileUtility.setPath("noimageavailable.PNG"));
					}

					galleryView.setAttachment(attachment);
					setEventGalleries.add(galleryView);
				}
			}
		}
		eventView.setSetEventGalleries(setEventGalleries);
		
		if(eventModel.getTxtBannerImagePath() != null && !"".equals(eventModel.getTxtBannerImagePath())){
			eventView.setBannerImagePath(FileUtility.setPath(eventModel.getTxtBannerImagePath()));	
		}else{
			eventView.setBannerImagePath(FileUtility.setPath("defaulteventpic.JPG"));
		}		
		
		
		Long goingCount = ((PageResultListRespone) doDisplayEventUsersOperation(eventModel.getId(), "GOING", null, null)).getRecords();
		Long maybeCount = ((PageResultListRespone) doDisplayEventUsersOperation(eventModel.getId(), "MAYBE", null, null)).getRecords();
		Long notgoingCount = ((PageResultListRespone) doDisplayEventUsersOperation(eventModel.getId(), "NOTGOING", null, null)).getRecords();
		
		eventView.setGoingUsersCount(goingCount);
		eventView.setMaybeUsersCount(maybeCount);
		eventView.setNotgoingUsersCount(notgoingCount);
		
		Boolean canChangeStatus = true;

		if (Auditor.getAuditor().getId().longValue() == eventModel.getCreateBy().getId().longValue() || eventModel.getDateEndDate().before(DateUtility.getCurrentDate())) {
			canChangeStatus = false;
		}
		
		if(canChangeStatus){
			if("VARIABLE".equals(eventModel.getCapacity().getName())){
				if(Long.parseLong(eventModel.getTxtMaxCapacity()) > goingCount){
					canChangeStatus = true;
				}else{
					canChangeStatus = false;
				}
			}
		}		

		eventView.setCanChangeStatus(canChangeStatus);
		
		Boolean canEditEvent = false;
		if (Auditor.getAuditor().getId().longValue() == eventModel.getCreateBy().getId().longValue()) {
			canEditEvent = true;
		}
		eventView.setCanEditEvent(canEditEvent);

		return eventView;
	}

	@Override
	public Response doSaveOperation(EventView view) throws Exception {
		EventModel model = null;

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = getNewModel(view);
		} else {
			model = eventService.getById(view.getId());
		}

		model = toModel(model, view);

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = eventService.create(model);
		} else {
			eventService.update(model);
		}

		for (EventPartnerModel eventPartner : model.getSetEventPartnerModel()) {
			if (eventPartner.getEvent() == null) {
				eventPartner.setEvent(model);
				eventPartnerService.create(eventPartner);
			} else {
				eventPartnerService.update(eventPartner);
			}
		}

		for (EventSponsorModel eventSponsor : model.getSetEventSponsorModel()) {
			if (eventSponsor.getEvent() == null) {
				eventSponsor.setEvent(model);
				eventSponsorService.create(eventSponsor);
			} else {
				eventSponsorService.update(eventSponsor);
			}
		}

		for (EventTagModel eventTag : model.getSetEventTagModel()) {
			if (eventTag.getEvent() == null) {
				eventTag.setEvent(model);
				eventTagService.create(eventTag);
			} else {
				eventTagService.update(eventTag);
			}
		}

		for (EventGalleryModel eventGallery : model.getSetEventGalleryModel()) {
			if (eventGallery.getEvent() == null) {
				eventGallery.setEvent(model);
				eventGalleryService.create(eventGallery);
			} else {
				eventGalleryService.update(eventGallery);
			}
		}

		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Event has been saved.");
	}

	@Override
	public Response doChangeEventUserStatusOperation(Long id, String status) {
		EventUserStatusModel userStatus = eventUserStatusService.getByEventAndUser(id);
		Boolean isFresh = false;

		if (userStatus == null) {
			isFresh = true;
			userStatus = new EventUserStatusModel();
			userStatus.setCreateBy(Auditor.getAuditor());
			userStatus.setCreateDate(DateUtility.getCurrentDate());
			userStatus.setActivationStatus(ActiveInActive.ACTIVE);
			userStatus.setActivationChangeBy(Auditor.getAuditor());
			userStatus.setActivationDate(DateUtility.getCurrentDate());
			userStatus.setEnumArchive("1");
			userStatus.setEvent(eventService.getById(id));
			//userStatus.setUser(Auditor.getAuditor());
			userStatus.setUserBasic(userBasicService.get(Auditor.getAuditor().getId()));
		}

		if ("RSVP".equalsIgnoreCase(status)) {
			userStatus.setEventStatus(EnumEventUserStatus.RSVP);
		} else if ("GOING".equalsIgnoreCase(status)) {
			userStatus.setEventStatus(EnumEventUserStatus.GOING);
		} else if ("NOTGOING".equalsIgnoreCase(status)) {
			userStatus.setEventStatus(EnumEventUserStatus.NOTGOING);
		} else if ("MAYBE".equalsIgnoreCase(status)) {
			userStatus.setEventStatus(EnumEventUserStatus.MAYBE);
		}

		if (isFresh) {
			eventUserStatusService.create(userStatus);
		} else {
			eventUserStatusService.update(userStatus);
		}

		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Event status is changed successfully.");
	}

	@Override
	public Response doDisplayOperation(Long id) {
		EventModel model = eventService.getById(id);
		EventView eventView = fromModel(model);

		if (Auditor.getAuditor().getFkInstituteId() != null) {
			eventView.setInstituteName(Auditor.getAuditor().getFkInstituteId().getTxtName());
		} else {
			eventView.setInstituteName("");
		}

		eventView.setCreateByUserName(
				Auditor.getAuditor().getTxtFirstName() + " " + Auditor.getAuditor().getTxtLastName());

		EventUserStatusModel userEventStatus = eventUserStatusService.getByEventAndUser(id);
		if (userEventStatus != null) {
			eventView.setEventUserStatus(userEventStatus.getEventStatus().getName());
		} else {
			eventView.setEventUserStatus("");
		}

		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_EVENT, eventView);
	}

	@Override
	public Response doDisplayAllOperation(String tags, String status, Integer start, Integer end) {
		List<EventModel> listEvents = eventService.getAllEvents(tags, status, start, end);

		List<View> listEventViews = new ArrayList<>();

		//List<User> listUsers = userService.findAll();
		List<UserBasic> userBasicList = userBasicService.findAll();

		List<EventUserStatusModel> listUserEventStatus = eventUserStatusService.getAllByUser();
		
		for (EventModel model : listEvents) {
			EventView eventView = fromModel(model);

			for (UserBasic user : userBasicList) {
				if (user.getId().longValue() == model.getCreateBy().getId().longValue()) {
					eventView.setCreateByUserName(user.getTxtFirstName() + " " + user.getTxtLastName());

					if (user.getFkInstituteId() != null) {
						eventView.setInstituteName(user.getFkInstituteId().getTxtName());
					} else {
						eventView.setInstituteName("");
					}
					break;
				}
			}

			String eventUserstatus = "";
			for (EventUserStatusModel eventUserStatus : listUserEventStatus) {
				if (eventUserStatus.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()
						&& eventUserStatus.getEvent().getId().longValue() == model.getId().longValue()) {
					eventUserstatus = eventUserStatus.getEventStatus().getName();
					break;
				}
			}
			eventView.setEventUserStatus(eventUserstatus);

			listEventViews.add(eventView);
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_EVENTS,
				listEventViews.size(), listEventViews);
	}

	@Override
	public Response doDisplayEventUsersOperation(Long eventId, String status, Integer start, Integer end) {
		Long statusId = null;
		if (status != null && !"".equals(status)) {
			if ("RSVP".equalsIgnoreCase(status)) {
				statusId = EnumEventUserStatus.RSVP.getId();
			} else if ("GOING".equalsIgnoreCase(status)) {
				statusId = EnumEventUserStatus.GOING.getId();
			} else if ("NOTGOING".equalsIgnoreCase(status)) {
				statusId = EnumEventUserStatus.NOTGOING.getId();
			} else if ("MAYBE".equalsIgnoreCase(status)) {
				statusId = EnumEventUserStatus.MAYBE.getId();
			}
		}

		List<EventUserStatusModel> listEventUserStatus = eventUserStatusService.getAllByEventAndStatus(eventId,
				statusId, start, end);

		List<UserView> listUser = new ArrayList<>();

		for (EventUserStatusModel eventUserStatus : listEventUserStatus) {
			//User user = eventUserStatus.getUser();
			UserBasic userBasic = eventUserStatus.getUserBasic();
			UserView userView = new UserView();
			userView.setTxtFirstName(userBasic.getTxtFirstName());
			userView.setTxtLastName(userBasic.getTxtLastName());
			userView.setId(userBasic.getId());

			if (userBasic.getTxtProfilePicPath() != null && !"".equals(userBasic.getTxtProfilePicPath())) {
				userView.setTxtProfilePicPath(FileUtility.setPath(userBasic.getTxtProfilePicPath()));
			} else {
				userView.setTxtProfilePicPath(FileUtility.setPath("defaultuserprofilepic.PNG"));
			}

			listUser.add(userView);
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_EVENT_USERS,
				listUser.size(), listUser);
	}

	@Override
	public Response doDelete(Long id) {
		EventModel eventModel = eventService.get(id);
		eventModel.setActivationStatus(ActiveInActive.INACTIVE);
		eventModel.setActivationDate(DateUtility.getCurrentDate());
		eventModel.setActivationChangeBy(Auditor.getAuditor());
		eventService.update(eventModel);
		return  CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}

	@Override
	public File doExport(Long id) throws IOException {
		String name = eventService.getName(id);
		List<EventUserStatusModel> eventUserStatusModelList = eventUserStatusService.getEventAttendeesList(id);
		
		Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[] {"Sr No", "Email", "Name", "Institute Name", "Contact No"});
        int i = 2;
		for(EventUserStatusModel eventUserStatusModel : eventUserStatusModelList){
			UserBasic userBasic = eventUserStatusModel.getUserBasic();
			i++;
			data.put(String.valueOf(i), new Object[] {i, userBasic.getTxtEmail(), userBasic.getTxtFirstName() + " "+ userBasic.getTxtLastName(),
					userBasic.getFkInstituteId().getTxtName(), userBasic.getTxtContactNumber()});
		}
		
		return prepareFile(name, data);
	}
	
	public static File prepareFile(String eventName, Map<String, Object[]> data) throws IOException{
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Attendees List for " + eventName);
	
      //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        FileOutputStream out = null;
        File file = new File(eventName+".xlsx");
        try{
        	out = new FileOutputStream(file);
            workbook.write(out);
        }finally {
        	if(out != null){
        		out.close();	
        	}
		}
        return file;
	}
}
