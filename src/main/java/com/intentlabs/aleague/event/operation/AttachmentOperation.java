
package com.intentlabs.aleague.event.operation;

import com.intentlabs.aleague.event.view.AttachmentView;
import com.intentlabs.common.oparation.BaseOperation;

/**
 * This is operation for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface AttachmentOperation extends BaseOperation<AttachmentView> {

	String ATTACHMENT_OPERATION = "attachmentOperation";
}
