
package com.intentlabs.aleague.event.operation;

import java.io.File;
import java.io.IOException;

import com.intentlabs.aleague.event.view.EventView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is operation for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface EventOperation extends BaseOperation<EventView> {

	String EVENT_OPERATION = "eventOperation";
		
	Response doChangeEventUserStatusOperation(Long id, String status); 
	
	Response doDisplayOperation(Long id);
	
	Response doDisplayAllOperation(String tags, String status, Integer start, Integer end);
	
	Response doDisplayEventUsersOperation(Long eventId, String status, Integer start, Integer end);
	
	Response doDelete(Long id);
	
	File doExport(Long id) throws IOException;
}
