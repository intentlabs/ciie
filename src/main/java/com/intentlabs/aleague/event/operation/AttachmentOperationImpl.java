
package com.intentlabs.aleague.event.operation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.event.enums.EnumCapacity;
import com.intentlabs.aleague.event.enums.EnumEventType;
import com.intentlabs.aleague.event.enums.EnumEventUserStatus;
import com.intentlabs.aleague.event.enums.EnumRSVP;
import com.intentlabs.aleague.event.model.AttachmentModel;
import com.intentlabs.aleague.event.model.EventGalleryModel;
import com.intentlabs.aleague.event.model.EventModel;
import com.intentlabs.aleague.event.model.EventPartnerModel;
import com.intentlabs.aleague.event.model.EventSponsorModel;
import com.intentlabs.aleague.event.model.EventTagModel;
import com.intentlabs.aleague.event.model.EventUserStatusModel;
import com.intentlabs.aleague.event.services.AttachmentService;
import com.intentlabs.aleague.event.services.EventGalleryService;
import com.intentlabs.aleague.event.services.EventPartnerService;
import com.intentlabs.aleague.event.services.EventService;
import com.intentlabs.aleague.event.services.EventSponsorService;
import com.intentlabs.aleague.event.services.EventTagService;
import com.intentlabs.aleague.event.services.EventUserStatusService;
import com.intentlabs.aleague.event.view.AttachmentView;
import com.intentlabs.aleague.event.view.EventGalleryView;
import com.intentlabs.aleague.event.view.EventPartnerView;
import com.intentlabs.aleague.event.view.EventSponsorView;
import com.intentlabs.aleague.event.view.EventView;
import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.aleague.tag.services.TagService;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.View;

/**
 * This is operation for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Component(value = "attachmentOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class AttachmentOperationImpl extends AbstractOperation<AttachmentModel, AttachmentView>
		implements AttachmentOperation {

	private static final long serialVersionUID = 741391010890205612L;

	@Autowired
	private AttachmentService attachmentService;

	@Override
	public BaseService getService() {
		return attachmentService;
	}

	@Override
	protected AttachmentModel getNewModel(AttachmentView view) {
		return new AttachmentModel();
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		return null;
	}

	@Override
	protected void checkInactive(AttachmentModel model) throws Exception {
	}

	@Override
	public AttachmentModel toModel(AttachmentModel attachmentModel, AttachmentView attachmentView) throws Exception {
		String filename = FileUtility.storeFile(attachmentView.getFile(), "eventattachment");

		if ("".equals(filename)) {
			return null;
		}

		attachmentModel.setTxtAttachmentPath(filename);
		return attachmentModel;
	}

	@Override
	public AttachmentView fromModel(AttachmentModel attachmentModel) {
		AttachmentView attachmentView = new AttachmentView();
		attachmentView.setId(attachmentModel.getId());
		attachmentView.setAttachmentpath(FileUtility.setPath(attachmentModel.getTxtAttachmentPath()));
		return attachmentView;
	}

	@Override
	public Response doSaveOperation(AttachmentView view) throws Exception {
		AttachmentModel model = toModel(getNewModel(view), view);

		if (model != null) {
			view = fromModel(attachmentService.create(model));
			return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Attachment has been saved.", view);
		}

		return CommonResponse.create(ResponseCode.INVALID_FORM_DATA.getCode(), "Attachment cannot be saved.");
	}
}
