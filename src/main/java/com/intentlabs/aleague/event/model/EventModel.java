
package com.intentlabs.aleague.event.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.intentlabs.aleague.event.enums.EnumCapacity;
import com.intentlabs.aleague.event.enums.EnumEventType;
import com.intentlabs.aleague.event.enums.EnumRSVP;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tblEvent")
public class EventModel extends ArchiveModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;

	@Column(name = "txtName")
	private String txtName;

	@Column(name = "enumEventType")
	private long eventType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dateStartDate")
	private Date dateStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dateEndDate")
	private Date dateEndDate;

	@Column(name = "txtDuration")
	private String txtDuration;

	@Column(name = "txtLocation")
	private String txtLocation;

	@Column(name = "enumRSVP")
	private long rsvp;

	@Column(name = "txtDescription")
	private String txtDescription;

	@Column(name = "txtAgenda")
	private String txtAgenda;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkEvent")
	private Set<EventPartnerModel> setEventPartnerModel;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkEvent")
	private Set<EventSponsorModel> setEventSponsorModel;

	@Column(name = "txtFacebookLink")
	private String txtFacebookLink;

	@Column(name = "txtInstagramLink")
	private String txtInstagramLink;

	@Column(name = "txtYoutubeLink")
	private String txtYoutubeLink;

	@Column(name = "txtEventLink")
	private String txtEventLink;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkEvent")
	private Set<EventTagModel> setEventTagModel;

	@Column(name = "enumCapacity")
	private long capacity;

	@Column(name = "txtMaxCapacity")
	private String txtMaxCapacity;

	@Column(name = "txtContactPersonName")
	private String txtContactPersonName;

	@Column(name = "txtContactPersonEmail")
	private String txtContactPersonEmail;

	@Column(name = "txtContactNumber")
	private String txtContactNumber;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkEvent")
	private Set<EventGalleryModel> setEventGalleryModel;

	@Column(name = "txtBannerImagePath")
	private String txtBannerImagePath;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkEvent")
	private Set<EventUserStatusModel> setEventUserStatusModel;

	/**
	 * @return the txtName
	 */
	public String getTxtName() {
		return txtName;
	}

	/**
	 * @param txtName
	 *            the txtName to set
	 */
	public void setTxtName(String txtName) {
		this.txtName = txtName;
	}

	/**
	 * @return the eventType
	 */
	public EnumEventType getEventType() {
		return  EnumEventType.fromId(eventType);
	}

	/**
	 * @param eventType
	 *            the eventType to set
	 */
	public void setEventType(EnumEventType enumEventType) {
		this.eventType = enumEventType.getId();
	}

	/**
	 * @return the dateStartDate
	 */
	public Date getDateStartDate() {
		return dateStartDate;
	}

	/**
	 * @param dateStartDate
	 *            the dateStartDate to set
	 */
	public void setDateStartDate(Date dateStartDate) {
		this.dateStartDate = dateStartDate;
	}

	/**
	 * @return the dateEndDate
	 */
	public Date getDateEndDate() {
		return dateEndDate;
	}

	/**
	 * @param dateEndDate
	 *            the dateEndDate to set
	 */
	public void setDateEndDate(Date dateEndDate) {
		this.dateEndDate = dateEndDate;
	}

	/**
	 * @return the txtDuration
	 */
	public String getTxtDuration() {
		return txtDuration;
	}

	/**
	 * @param txtDuration
	 *            the txtDuration to set
	 */
	public void setTxtDuration(String txtDuration) {
		this.txtDuration = txtDuration;
	}

	/**
	 * @return the txtLocation
	 */
	public String getTxtLocation() {
		return txtLocation;
	}

	/**
	 * @param txtLocation
	 *            the txtLocation to set
	 */
	public void setTxtLocation(String txtLocation) {
		this.txtLocation = txtLocation;
	}

	/**
	 * @return the rsvp
	 */
	public EnumRSVP getRsvp() {
		return EnumRSVP.fromId(rsvp);
	}

	/**
	 * @param enumRSVP
	 *            the enumRSVP to set
	 */
	public void setRsvp(EnumRSVP rsvp) {
		this.rsvp = rsvp.getId();
	}

	/**
	 * @return the txtDescription
	 */
	public String getTxtDescription() {
		return txtDescription;
	}

	/**
	 * @param txtDescription
	 *            the txtDescription to set
	 */
	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	/**
	 * @return the txtAgenda
	 */
	public String getTxtAgenda() {
		return txtAgenda;
	}

	/**
	 * @param txtAgenda
	 *            the txtAgenda to set
	 */
	public void setTxtAgenda(String txtAgenda) {
		this.txtAgenda = txtAgenda;
	}

	/**
	 * @return the setEventPartnerModel
	 */
	public Set<EventPartnerModel> getSetEventPartnerModel() {
		return setEventPartnerModel;
	}

	/**
	 * @param setEventPartnerModel
	 *            the setEventPartnerModel to set
	 */
	public void setSetEventPartnerModel(Set<EventPartnerModel> setEventPartnerModel) {
		this.setEventPartnerModel = setEventPartnerModel;
	}

	/**
	 * @return the setEventSponsorModel
	 */
	public Set<EventSponsorModel> getSetEventSponsorModel() {
		return setEventSponsorModel;
	}

	/**
	 * @param setEventSponsorModel
	 *            the setEventSponsorModel to set
	 */
	public void setSetEventSponsorModel(Set<EventSponsorModel> setEventSponsorModel) {
		this.setEventSponsorModel = setEventSponsorModel;
	}

	/**
	 * @return the txtFacebookLink
	 */
	public String getTxtFacebookLink() {
		return txtFacebookLink;
	}

	/**
	 * @param txtFacebookLink
	 *            the txtFacebookLink to set
	 */
	public void setTxtFacebookLink(String txtFacebookLink) {
		this.txtFacebookLink = txtFacebookLink;
	}

	/**
	 * @return the txtInstagramLink
	 */
	public String getTxtInstagramLink() {
		return txtInstagramLink;
	}

	/**
	 * @param txtInstagramLink
	 *            the txtInstagramLink to set
	 */
	public void setTxtInstagramLink(String txtInstagramLink) {
		this.txtInstagramLink = txtInstagramLink;
	}

	/**
	 * @return the txtYoutubeLink
	 */
	public String getTxtYoutubeLink() {
		return txtYoutubeLink;
	}

	/**
	 * @param txtYoutubeLink
	 *            the txtYoutubeLink to set
	 */
	public void setTxtYoutubeLink(String txtYoutubeLink) {
		this.txtYoutubeLink = txtYoutubeLink;
	}

	/**
	 * @return the txtEventLink
	 */
	public String getTxtEventLink() {
		return txtEventLink;
	}

	/**
	 * @param txtEventLink
	 *            the txtEventLink to set
	 */
	public void setTxtEventLink(String txtEventLink) {
		this.txtEventLink = txtEventLink;
	}

	/**
	 * @return the setEventTagModel
	 */
	public Set<EventTagModel> getSetEventTagModel() {
		return setEventTagModel;
	}

	/**
	 * @param setEventTagModel
	 *            the setEventTagModel to set
	 */
	public void setSetEventTagModel(Set<EventTagModel> setEventTagModel) {
		this.setEventTagModel = setEventTagModel;
	}

	/**
	 * @return the capacity
	 */
	public EnumCapacity getCapacity() {
		return EnumCapacity.fromId(capacity);
	}

	/**
	 * @param capacity
	 *            the capacity to set
	 */
	public void setCapacity(EnumCapacity enumCapacity) {
		this.capacity = enumCapacity.getId();
	}

	/**
	 * @return the txtMaxCapacity
	 */
	public String getTxtMaxCapacity() {
		return txtMaxCapacity;
	}

	/**
	 * @param txtMaxCapacity
	 *            the txtMaxCapacity to set
	 */
	public void setTxtMaxCapacity(String txtMaxCapacity) {
		this.txtMaxCapacity = txtMaxCapacity;
	}

	/**
	 * @return the txtContactPersonName
	 */
	public String getTxtContactPersonName() {
		return txtContactPersonName;
	}

	/**
	 * @param txtContactPersonName
	 *            the txtContactPersonName to set
	 */
	public void setTxtContactPersonName(String txtContactPersonName) {
		this.txtContactPersonName = txtContactPersonName;
	}

	/**
	 * @return the txtContactPersonEmail
	 */
	public String getTxtContactPersonEmail() {
		return txtContactPersonEmail;
	}

	/**
	 * @param txtContactPersonEmail
	 *            the txtContactPersonEmail to set
	 */
	public void setTxtContactPersonEmail(String txtContactPersonEmail) {
		this.txtContactPersonEmail = txtContactPersonEmail;
	}

	/**
	 * @return the txtContactNumber
	 */
	public String getTxtContactNumber() {
		return txtContactNumber;
	}

	/**
	 * @param txtContactNumber
	 *            the txtContactNumber to set
	 */
	public void setTxtContactNumber(String txtContactNumber) {
		this.txtContactNumber = txtContactNumber;
	}

	/**
	 * @return the setEventGalleryModel
	 */
	public Set<EventGalleryModel> getSetEventGalleryModel() {
		return setEventGalleryModel;
	}

	/**
	 * @param setEventGalleryModel
	 *            the setEventGalleryModel to set
	 */
	public void setSetEventGalleryModel(Set<EventGalleryModel> setEventGalleryModel) {
		this.setEventGalleryModel = setEventGalleryModel;
	}

	/**
	 * @return the txtBannerImagePath
	 */
	public String getTxtBannerImagePath() {
		return txtBannerImagePath;
	}

	/**
	 * @param txtBannerImagePath the txtBannerImagePath to set
	 */
	public void setTxtBannerImagePath(String txtBannerImagePath) {
		this.txtBannerImagePath = txtBannerImagePath;
	}

	/**
	 * @return the setEventUserStatusModel
	 */
	public Set<EventUserStatusModel> getSetEventUserStatusModel() {
		return setEventUserStatusModel;
	}

	/**
	 * @param setEventUserStatusModel the setEventUserStatusModel to set
	 */
	public void setSetEventUserStatusModel(Set<EventUserStatusModel> setEventUserStatusModel) {
		this.setEventUserStatusModel = setEventUserStatusModel;
	}

	public static EventModel getSaveModel() {
		EventModel event = new EventModel();
		event.setCreateDate(new Date());
		return event;
	}

}
