
package com.intentlabs.aleague.event.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event sponsers.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Entity
@Table(name = "tblEventSponsor")

public class EventSponsorModel extends ArchiveModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3528610774449650642L;

	@Column(name = "txtSponsorLink")
	private String txtSponsorLink;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkAttachment")
	private AttachmentModel attachment;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkEvent")  
	private EventModel event;

	/**
	 * @return the txtSponsorLink
	 */
	public String getTxtSponsorLink() {
		return txtSponsorLink;
	}

	/**
	 * @param txtSponsorLink the txtSponsorLink to set
	 */
	public void setTxtSponsorLink(String txtSponsorLink) {
		this.txtSponsorLink = txtSponsorLink;
	}

	/**
	 * @return the attachment
	 */
	public AttachmentModel getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment(AttachmentModel attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the event
	 */
	public EventModel getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(EventModel event) {
		this.event = event;
	}
}