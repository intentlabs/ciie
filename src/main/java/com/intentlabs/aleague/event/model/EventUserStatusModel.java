
package com.intentlabs.aleague.event.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.intentlabs.aleague.event.enums.EnumEventUserStatus;
import com.intentlabs.aleague.user.model.UserBasic;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event sponsers.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Entity
@Table(name = "tblEventUserStatus")
public class EventUserStatusModel extends ArchiveModel {
	
	private static final long serialVersionUID = -3528610774449650642L;

	@Column(name = "enumEventStatus")
	private long eventStatus;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkEvent")
	private EventModel event;

	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkUser")
	private User user;*/
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkUser")
	private UserBasic userBasic;
	
	

	/**
	 * @return the enumEventStatus
	 */
	public EnumEventUserStatus getEventStatus() {
		return EnumEventUserStatus.fromId(eventStatus);
	}

	/**
	 * @param enumEventStatus
	 *            the enumEventStatus to set
	 */
	public void setEventStatus(EnumEventUserStatus enumEventStatus) {
		this.eventStatus = enumEventStatus.getId();
	}

	/**
	 * @return the event
	 */
	public EventModel getEvent() {
		return event;
	}

	/**
	 * @param event
	 *            the event to set
	 */
	public void setEvent(EventModel event) {
		this.event = event;
	}

	public UserBasic getUserBasic() {
		return userBasic;
	}

	public void setUserBasic(UserBasic userBasic) {
		this.userBasic = userBasic;
	}

	/**
	 * @return the user
	 *//*
	public User getUser() {
		return user;
	}

	*//**
	 * @param user
	 *            the user to set
	 *//*
	public void setUser(User user) {
		this.user = user;
	}*/
	
	
}