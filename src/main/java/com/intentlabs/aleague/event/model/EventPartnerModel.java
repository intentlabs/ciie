
package com.intentlabs.aleague.event.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event partners.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Entity
@Table(name = "tblEventPartner")
public class EventPartnerModel extends ArchiveModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6135151969434806576L;

	@Column(name = "txtPartnerLink")
	private String txtPartnerLink;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkAttachment")
	private AttachmentModel attachment;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkEvent")  
	private EventModel event;

	/**
	 * @return the txtPartnerLink
	 */
	public String getTxtPartnerLink() {
		return txtPartnerLink;
	}

	/**
	 * @param txtPartnerLink the txtPartnerLink to set
	 */
	public void setTxtPartnerLink(String txtPartnerLink) {
		this.txtPartnerLink = txtPartnerLink;
	}

	/**
	 * @return the attachment
	 */
	public AttachmentModel getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment(AttachmentModel attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the event
	 */
	public EventModel getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(EventModel event) {
		this.event = event;
	}

}
