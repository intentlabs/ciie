
package com.intentlabs.aleague.event.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event tags.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tblEventTag")
public class EventTagModel extends ArchiveModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkTag")
	private TagModel tag;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkEvent")  
	private EventModel event;

	public TagModel getTag() {
		return tag;
	}

	public void setTag(TagModel tag) {
		this.tag = tag;
	}

	public EventModel getEvent() {
		return event;
	}

	public void setEvent(EventModel event) {
		this.event = event;
	}
}
