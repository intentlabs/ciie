
package com.intentlabs.aleague.event.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.intentlabs.common.model.IdentifierModel;

/**
 * This is model for Event members.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Entity
@Table(name = "tblAttachment")
public class AttachmentModel extends IdentifierModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6135151969434806576L;

	@Column(name = "txtAttachmentPath")
	private String txtAttachmentPath;

	/**
	 * @return the txtAttachmentPath
	 */
	public String getTxtAttachmentPath() {
		return txtAttachmentPath;
	}

	/**
	 * @param txtAttachmentPath the txtAttachmentPath to set
	 */
	public void setTxtAttachmentPath(String txtAttachmentPath) {
		this.txtAttachmentPath = txtAttachmentPath;
	}
}
