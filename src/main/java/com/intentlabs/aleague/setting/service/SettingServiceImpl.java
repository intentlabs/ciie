/**
 * 
 */
package com.intentlabs.aleague.setting.service;

import org.hibernate.Criteria;

import com.intentlabs.aleague.UserSetting.model.SettingsModel;
import com.intentlabs.common.service.AbstractService;

/**
 * @author Dhruvang
 *
 */
public class SettingServiceImpl extends AbstractService<SettingsModel> implements SettingService {

	
	private static final long serialVersionUID = -2842305388370635935L;

	@Override
	public Class<SettingsModel> getModelClass() {
         return SettingsModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<SettingsModel> modelClass) {
	      return null;
	}

	@Override
	public Criteria setSearchCriteria(SettingsModel model, Criteria commonCriteria) {
		  return null;
	}

}
