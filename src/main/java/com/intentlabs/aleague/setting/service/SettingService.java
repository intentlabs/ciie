/**
 * 
 */
package com.intentlabs.aleague.setting.service;

import com.intentlabs.aleague.UserSetting.model.SettingsModel;
import com.intentlabs.common.service.BaseService;

/**
 * @author Dhruvang
 *
 */
public interface SettingService extends BaseService<SettingsModel> {

}
