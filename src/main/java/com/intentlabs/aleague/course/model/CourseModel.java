
package com.intentlabs.aleague.course.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Course.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 17/07/2017
 */

@Entity
@Table(name = "tblUserCourse")
public class CourseModel extends ArchiveModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7724123839076351961L;
	
	@Column(name = "txtCourse", nullable = false)
	private String txtCourse;
	
	@Column(name = "txtDescription", nullable = true)
	private String txtDescription;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fkInstituteId", nullable = false)
	private List<InstituteModel> listInstituteModel;

	public String getTxtCourse() {
		return txtCourse;
	}

	public void setTxtCourse(String txtCourse) {
		this.txtCourse = txtCourse;
	}

	public String getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	public List<InstituteModel> getListInstituteModel() {
		return listInstituteModel;
	}

	public void setListInstituteModel(List<InstituteModel> listInstituteModel) {
		this.listInstituteModel = listInstituteModel;
	}
	
	

}
