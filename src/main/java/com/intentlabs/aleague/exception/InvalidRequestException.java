package com.intentlabs.aleague.exception;

import java.util.Map;

/**
 * This is a invalid request exception which will be thrown invalid input received from client side.
 * @version 1.0
 */
public class InvalidRequestException extends CapsException{

	private static final long serialVersionUID = 1L;
		
	/**
	 * Only Code
	 * @param code
	 * @param className
	 * @param transactionName
	 */
	public InvalidRequestException(int code, String className, String transactionName){
		super(code, className, transactionName);
	}
	
	/**
	 * Only Code with identifier
	 * @param code
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public InvalidRequestException(int code, String className, String transactionName, String identificationValue){
		super(code, className, transactionName, identificationValue);
	}
	
	/**
	 * Message & Code 
	 * @param message
	 * @param code
	 * @param className
	 * @param transactionName
	 */
	public InvalidRequestException(String message, int code, String className, String transactionName){
		super(message, code, className, transactionName);
	}
	
	/**
	 * Message & code with identifier
	 * @param message
	 * @param code
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public InvalidRequestException(String message, int code, String className, String transactionName, String identificationValue){
		super(message, code, className, transactionName, identificationValue);
	}
	
	/**
	 * Code & Map message 
	 * @param code
	 * @param properties
	 * @param className
	 * @param transactionName
	 */
	public InvalidRequestException(int code, Map<String, String> properties, String className, String transactionName){
		super(code, properties, className, transactionName);
	}
	
	/**
	 * Code & Map Message with identifier
	 * @param code
	 * @param properties
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public InvalidRequestException(int code, Map<String, String> properties, String className, String transactionName, String identificationValue){
		super(code, properties, className, transactionName,identificationValue);
	}
}
