package com.intentlabs.aleague.exception;

import java.util.HashMap;
import java.util.Map;

import com.intentlabs.common.enums.ResponseCode;






/**
 * This is a custom master exception which contains error code, error message & properties as their members.
 * This can be used to handle custom generated exception.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public class CapsException extends Exception{

	private static final long serialVersionUID = 1L;
	private final int code;
	private final String message;
	private final Map<String, String> properties;
	private final String className;
	private final String transactionName;
	private final String identificationValue;
	
	
	/**
	 * Only code
	 * @param code
	 * @param className
	 * @param transactionName
	 */
	public CapsException(int code, String className, String transactionName){
		super(ResponseCode.fromId(code).getMessage());
		this.code = code;
		this.className = className;
		this.transactionName = transactionName;
		this.message=ResponseCode.fromId(code).getMessage();
		this.identificationValue="";
		this.properties= new HashMap<>();
	}
	
	/**
	 * Only code with identification value
	 * @param code
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public CapsException(int code, String className, String transactionName, String identificationValue){
		super(ResponseCode.fromId(code).getMessage());
		this.code = code;
		this.className = className;
		this.transactionName = transactionName;
		this.identificationValue = identificationValue;
		this.message=ResponseCode.fromId(code).getMessage();
		this.properties= new HashMap<>();
	}
	
	/**
	 * Code & Message
	 * @param message
	 * @param code
	 * @param className
	 * @param transactionName
	 */
	public CapsException(String message, int code, String className, String transactionName ){
		super(message);
		this.code = code;
		this.message = message;
		this.className = className;
		this.transactionName = transactionName;
		this.identificationValue = "";
		this.properties= new HashMap<>();
	}
	
	/**
	 * Code & Message with identification value
	 * @param message
	 * @param code
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public CapsException(String message, int code, String className, String transactionName, String identificationValue ){
		super(message);
		this.code = code;
		this.message = message;
		this.className = className;
		this.transactionName = transactionName;
		this.identificationValue = identificationValue;
		this.properties= new HashMap<>();
	}
	
	/**
	 * Code & Message properties
	 * @param code
	 * @param properties
	 * @param className
	 * @param transactionName
	 */
	public CapsException(int code, Map<String, String> properties, String className, String transactionName){
		super(ResponseCode.fromId(code).getMessage());
		this.code = code;
		this.properties = properties;
		this.className = className;
		this.transactionName = transactionName;
		this.identificationValue="";
		this.message=ResponseCode.fromId(code).getMessage();
	}
	
	/**
	 * Code & Message properties with identifiactionValue
	 * @param code
	 * @param properties
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public CapsException(int code, Map<String, String> properties, String className, String transactionName, String identificationValue){
		super(ResponseCode.fromId(code).getMessage());
		this.code = code;
		this.properties = properties;
		this.className = className;
		this.transactionName = transactionName;
		this.identificationValue = identificationValue;
		this.message=ResponseCode.fromId(code).getMessage();
	}

	public int getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}	
	
	public Map<String, String> getProperties() {
		return properties;
	}
	
	public void setProperties(String name, String value){
		properties.put(name, value);
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getClassName() {
		return className;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public String getIdentificationValue() {
		return identificationValue;
	}
}
