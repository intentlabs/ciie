package com.intentlabs.aleague.exception;

import java.util.Map;


/**
 * This is a unauthorized exception which will be thrown when user has performed an operation on which he/she has not been
 * granted access
 * @version 1.0
 */
public class UnAuthorizedException extends CapsException{

	private static final long serialVersionUID = 1L;
		
	/**
	 * Only Code
	 * @param code
	 * @param className
	 * @param transactionName
	 */
	public UnAuthorizedException(int code, String className, String transactionName){
		super(code, className, transactionName);
	}
	
	/**
	 * Only Code with identifier
	 * @param code
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public UnAuthorizedException(int code, String className, String transactionName, String identificationValue){
		super(code, className, transactionName, identificationValue);
	}
	
	/**
	 * Message & Code 
	 * @param message
	 * @param code
	 * @param className
	 * @param transactionName
	 */
	public UnAuthorizedException(String message, int code, String className, String transactionName){
		super(message, code, className, transactionName);
	}
	
	/**
	 * Message & code with identifier
	 * @param message
	 * @param code
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public UnAuthorizedException(String message, int code, String className, String transactionName, String identificationValue){
		super(message, code, className, transactionName, identificationValue);
	}
	
	/**
	 * Code & Map message 
	 * @param code
	 * @param properties
	 * @param className
	 * @param transactionName
	 */
	public UnAuthorizedException(int code, Map<String, String> properties, String className, String transactionName){
		super(code, properties, className, transactionName);
	}
	
	/**
	 * Code & Map Message with identifier
	 * @param code
	 * @param properties
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public UnAuthorizedException(int code, Map<String, String> properties, String className, String transactionName, String identificationValue){
		super(code, properties, className, transactionName,identificationValue);
	}
}
