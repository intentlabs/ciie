package com.intentlabs.aleague.exception;

import java.util.Map;
/**
 * This is a invalid data exception which will be thrown when input received from client is not valid during save or update operation.
 * @version 1.0
 */
public class InvalidDataException extends CapsException{

	private static final long serialVersionUID = 1L;
		
	/**
	 * Only Code
	 * @param code
	 * @param className
	 * @param transactionName
	 */
	public InvalidDataException(int code, String className, String transactionName){
		super(code, className, transactionName);
	}
	
	/**
	 * Only Code with identifier
	 * @param code
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public InvalidDataException(int code, String className, String transactionName, String identificationValue){
		super(code, className, transactionName, identificationValue);
	}
	
	/**
	 * Message & Code 
	 * @param message
	 * @param code
	 * @param className
	 * @param transactionName
	 */
	public InvalidDataException(String message, int code, String className, String transactionName){
		super(message, code, className, transactionName);
	}
	
	/**
	 * Message & code with identifier
	 * @param message
	 * @param code
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public InvalidDataException(String message, int code, String className, String transactionName, String identificationValue){
		super(message, code, className, transactionName, identificationValue);
	}
	
	/**
	 * Code & Map message 
	 * @param code
	 * @param properties
	 * @param className
	 * @param transactionName
	 */
	public InvalidDataException(int code, Map<String, String> properties, String className, String transactionName){
		super(code, properties, className, transactionName);
	}
	
	/**
	 * Code & Map Message with identifier
	 * @param code
	 * @param properties
	 * @param className
	 * @param transactionName
	 * @param identificationValue
	 */
	public InvalidDataException(int code, Map<String, String> properties, String className, String transactionName, String identificationValue){
		super(code, properties, className, transactionName,identificationValue);
	}
}
