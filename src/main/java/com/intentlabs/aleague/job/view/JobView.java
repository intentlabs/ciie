package com.intentlabs.aleague.job.view;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.common.view.ArciveView;
import com.intentlabs.common.view.KeyValueView;


/**
 * This is View used to get data for Event.
 * @author Dhruvang Joshi.
 * @version 1.0
 * @since 14/07/2017
 */

public class JobView  extends ArciveView {

	private static final long serialVersionUID = 195998038570256654L;
	
	private String nameOfPost;
	
	private String jobType;
	
	private String duration;
	
	private String posterName;
	
	private String description;
	
	private String applyLink;
	
	private String tags;
	
	private String startDate;
	
	private String endDate;
	
	private MultipartFile jobImage;

	public String getNameOfPost() {
		return nameOfPost;
	}

	public void setNameOfPost(String nameOfPost) {
		this.nameOfPost = nameOfPost;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getPosterName() {
		return posterName;
	}

	public void setPosterName(String posterName) {
		this.posterName = posterName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getApplyLink() {
		return applyLink;
	}

	public void setApplyLink(String applyLink) {
		this.applyLink = applyLink;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public MultipartFile getJobImage() {
		return jobImage;
	}

	public void setJobImage(MultipartFile jobImage) {
		this.jobImage = jobImage;
	}
	
	

}
