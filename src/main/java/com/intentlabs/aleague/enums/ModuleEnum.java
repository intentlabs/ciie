
package com.intentlabs.aleague.enums;

import com.intentlabs.common.enums.EnumType;

/**
 *
 * @author Nirav.Shah
 *
 */
public enum ModuleEnum implements EnumType {
	INSTITUTE(1, "Institute"),
	USER(2,"User"),
	BLOGPOST(3, "BlogPost"),
	JOBPOST(4, "JobPost"),
	DISCUSSION(5, "Discussion"),
	STUDENT_CLUB(6, "Student Club"),
	EVENT(7, "Event"),
	RESEARCH(8, "Research"),
	ROLE(9,"Role");

	private final long id;
    private final String name;
    
    ModuleEnum(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
	 return name;
	}

	
    public static ModuleEnum fromId(long id) {
        for (ModuleEnum status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }
}
