/**
 * 
 */
package com.intentlabs.aleague.enums;

import com.intentlabs.common.enums.EnumType;

/**
 * @author Dhruvang
 *
 */
public enum EnumProfile implements EnumType {
	SEVENTY(1, "SEVENTY"),
	EIGHTY_FIVE(2,"EIGHTY_FIVE"),
	HUNDRED(3, "HUNDRED");

	private final long id;
    private final String name;
    
    EnumProfile(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
	 return name;
	}

	
    public static EnumProfile fromId(long id) {
        for (EnumProfile status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }
}
