
package com.intentlabs.aleague.enums;

import com.intentlabs.common.enums.EnumType;

/**
 *
 * @author Nirav.Shah
 *
 */
public enum RightsEnum implements EnumType {
	ADD(1, "Add"),
	UPDATE(2,"Update"),
	VIEW(3, "View"),
	DELETE(4, "Delete"),
	CREATED_BY_OTHERS(5,"Created By Others");

	private final long id;
    private final String name;
    
    RightsEnum(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
	 return name;
	}

	
    public static RightsEnum fromId(long id) {
        for (RightsEnum status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }
}
