/**
 * 
 */
package com.intentlabs.aleague.password.service;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.user.model.UserPassword;
import com.intentlabs.common.service.AbstractService;

/**
 * @author Dhruvang
 *
 */
public class PasswordServiceImpl extends AbstractService<UserPassword> implements PasswordService {

	
	private static final long serialVersionUID = -4285850642083889563L;

	@Override
	public Class<UserPassword> getModelClass() {

		return UserPassword.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<UserPassword> modelClass) {

		return null;
	}

	@Override
	public Criteria setSearchCriteria(UserPassword model, Criteria commonCriteria) {

		return null;
	}



	/*@Override
	public UserPassword lsstpassword(long userid, String password) {
	
		  Criteria criteria = getSession().createCriteria(getModelClass());
		  criteria.add(Restrictions.eq("txtPassword", password));
		  criteria.add(Restrictions.eq("fkUserId.id", userid));
		  criteria.addOrder(Order.desc("datePasswordChange"));
		    return (UserPassword) criteria.uniqueResult();
	}*/
	
	
	@Override
	public UserPassword lsstpassword(long userid) {
	
		  Criteria criteria = getSession().createCriteria(getModelClass());
		  criteria.add(Restrictions.eq("fkUserId.id", userid));
		  criteria.addOrder(Order.desc("datePasswordChange"));
		  criteria.setMaxResults(1);
		  return (UserPassword) criteria.uniqueResult();
	}

}
