/**
 * 
 */
package com.intentlabs.aleague.password.service;

import com.intentlabs.aleague.user.model.UserPassword;
import com.intentlabs.common.service.BaseService;

/**
 * @author Dhruvang
 *
 */
public interface PasswordService extends BaseService<UserPassword>  {
	
	/*UserPassword lsstpassword(long userid,String password);*/
	
	UserPassword lsstpassword(long userid);
	
	

}
