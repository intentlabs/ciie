package com.intentlabs.aleague.jobpost.services;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.json.JSONArray;

import com.intentlabs.aleague.jobpost.model.JobPostModel;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class JobPostServiceImpl extends AbstractService<JobPostModel> implements JobPostService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<JobPostModel> getModelClass() {
		return JobPostModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<JobPostModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(JobPostModel model, Criteria commonCriteria) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobPostModel> getAllJobPosts(String tags, String location, String jobtype) {
		if(tags != null && !"".equals(tags) && !"[]".equals(tags)){
			Criteria criteriaJobIds = getSession().createCriteria(getModelClass());
			criteriaJobIds.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			criteriaJobIds.createAlias("setJobPostTagModel", "jobPostTags", JoinType.LEFT_OUTER_JOIN);
			criteriaJobIds.createAlias("jobPostTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);		
			criteriaJobIds.add(Restrictions.eq("jobPostTags.activationStatus", Long.valueOf("1")));
			criteriaJobIds.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
			
			try{
				JSONArray jsonArray = new JSONArray(tags);
				List<Long> listTagids = new ArrayList<>();
				for(Integer i = 0; i < jsonArray.length(); i++){
					listTagids.add(jsonArray.getLong(i));
				}
						
				criteriaJobIds.add(Restrictions.in("tags.id", listTagids));
			}catch (Exception e) {
				LoggerService.exception(e);
			}			
			
			if(location != null && !"".equals(location)){
				criteriaJobIds.add(Restrictions.like("txtLocation", location, MatchMode.START));
			}		
			
			if(jobtype != null && !"".equals(jobtype)){
				criteriaJobIds.add(Restrictions.eq("jobType", Long.valueOf(jobtype)));
			}
			
			criteriaJobIds.setProjection(Projections.property("id"));
			
			List<Long> listJobPosts = criteriaJobIds.list();
			
			if(!listJobPosts.isEmpty()){
				Criteria criteria = getSession().createCriteria(getModelClass());
				criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
				criteria.createAlias("setJobPostTagModel", "jobPostTags", JoinType.LEFT_OUTER_JOIN);
				criteria.createAlias("jobPostTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);		
				criteria.add(Restrictions.in("id", listJobPosts));
				criteria.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
				criteria.addOrder(Order.desc("id"));
				
				return criteria.list();
			}else{
				return new ArrayList<JobPostModel>();
			}			
		}else{
			Criteria criteria = getSession().createCriteria(getModelClass());
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			criteria.createAlias("setJobPostTagModel", "jobPostTags", JoinType.LEFT_OUTER_JOIN);
			criteria.createAlias("jobPostTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);		
			criteria.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
			
			if(location != null && !"".equals(location)){
				criteria.add(Restrictions.like("txtLocation", location, MatchMode.START));
			}
			
			if(jobtype != null && !"".equals(jobtype)){
				criteria.add(Restrictions.eq("jobType", Long.valueOf(jobtype)));
			}

			criteria.addOrder(Order.desc("id"));
			
			return criteria.list();
		}		
	}
	
	@Override
	public JobPostModel getById(Long id) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("setJobPostTagModel", "jobPostTags", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("jobPostTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);			
		criteria.add(Restrictions.eq("id", id));		
		return (JobPostModel) criteria.uniqueResult();
	}
}

