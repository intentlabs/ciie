
package com.intentlabs.aleague.jobpost.services;

import com.intentlabs.aleague.jobpost.model.JobPostTagModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface JobPostTagService extends BaseService<JobPostTagModel> {
}
