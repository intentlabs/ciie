
package com.intentlabs.aleague.jobpost.services;

import java.util.List;

import com.intentlabs.aleague.jobpost.model.JobPostModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface JobPostService extends BaseService<JobPostModel> {
	List<JobPostModel> getAllJobPosts(String tags, String location, String jobtype);
	
	JobPostModel getById(Long id);
}
