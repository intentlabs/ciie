
package com.intentlabs.aleague.jobpost.services;

import org.hibernate.Criteria;

import com.intentlabs.aleague.jobpost.model.JobPostTagModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class JobPostTagServiceImpl extends AbstractService<JobPostTagModel> implements JobPostTagService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<JobPostTagModel> getModelClass() {
		return JobPostTagModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<JobPostTagModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(JobPostTagModel model, Criteria commonCriteria) {
		return null;
	}
}