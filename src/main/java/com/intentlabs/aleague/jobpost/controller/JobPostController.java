
package com.intentlabs.aleague.jobpost.controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.jobpost.view.JobPostView;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface JobPostController extends WebController<JobPostView>
{

	String JOB_POST_CONTROLLER = "JobPostController";
	
	@RequestMapping(value = "/savejobpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response saveJobPost(@ModelAttribute JobPostView view) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getjobpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getJobPost(@RequestBody JobPostView jobPostView) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/getalljobposts.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getAllJobPosts(@RequestBody (required=false) JobPostView jobPostView) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/deletejobpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response deleteJobPost(@RequestBody JobPostView jobPost) throws InvalidDataException, Exception;
}
