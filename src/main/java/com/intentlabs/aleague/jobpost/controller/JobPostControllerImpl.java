
package com.intentlabs.aleague.jobpost.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.jobpost.operation.JobPostOperation;
import com.intentlabs.aleague.jobpost.view.JobPostView;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;

/**
 * This is controller for Event to handle request.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@SuppressWarnings("serial")
@Controller
@RequestMapping("/jobpost")
public class JobPostControllerImpl extends AbstractController<JobPostView> implements JobPostController
{

	@Autowired
	JobPostOperation jobPostOperation;
	
	@Override
	public BaseOperation<JobPostView> getOperation() {
		return jobPostOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(JobPostView webView) throws Exception {

		
	}

	@Override
	public Response saveJobPost(@ModelAttribute JobPostView view) throws InvalidDataException, Exception {
		try{
			LoggerService.info(JOB_POST_CONTROLLER, OperationName.SAVE_JOB_POST, LogMessage.BEFORE_VALIDATION);
			isValidSaveData(view);
			LoggerService.info(JOB_POST_CONTROLLER, OperationName.SAVE_JOB_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return jobPostOperation.doSaveOperation(view);
		}
		finally {
			LoggerService.info(JOB_POST_CONTROLLER, OperationName.SAVE_JOB_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response getJobPost(@RequestBody JobPostView jobPostView) throws InvalidDataException, Exception {
		return jobPostOperation.doDisplayOperation(jobPostView.getId());
	}

	@Override
	public Response getAllJobPosts(@RequestBody (required=false) JobPostView jobPostView)
			throws InvalidDataException, Exception {
		if(jobPostView != null){
			return jobPostOperation.doDisplayAllOperation(jobPostView.getTags(), jobPostView.getLocation(), jobPostView.getJobType());
		}else{
			return jobPostOperation.doDisplayAllOperation(null, null, null);
		}
	}
	


	@Override
	public Response deleteJobPost(@RequestBody JobPostView jobPost) throws InvalidDataException, Exception {
		try{
			LoggerService.info(JOB_POST_CONTROLLER, OperationName.DELETE_JOB_POST, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(JOB_POST_CONTROLLER, OperationName.DELETE_JOB_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return jobPostOperation.doDeleteOperation(jobPost.getId());
		}
		finally {
			LoggerService.info(JOB_POST_CONTROLLER, OperationName.DELETE_JOB_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}

}
