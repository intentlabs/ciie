
package com.intentlabs.aleague.jobpost.view;

import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.view.IdentifierView;

/**
 * This is View used to get data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class JobPostView extends IdentifierView {


	private static final long serialVersionUID = 8692674149531174388L;
	
	private String postName;
	
	private String jobType;
	
	private String duration;
	
	private String jobPosterName;	
	
	private String description;
	
	private String linkApply;
	
	private String tags;
	
	private Set<TagView> setTags;
	
	private String startDate;
	
	private String endDate;
	
	private String postFilePath;
	
	private String location;
	
	private MultipartFile file;
	
	private Boolean canApply;
	
	private Boolean canEditDelete;

	/**
	 * @return the postName
	 */
	public String getPostName() {
		return postName;
	}

	/**
	 * @param postName the postName to set
	 */
	public void setPostName(String postName) {
		this.postName = postName;
	}

	/**
	 * @return the jobType
	 */
	public String getJobType() {
		return jobType;
	}

	/**
	 * @param jobType the jobType to set
	 */
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	/**
	 * @return the duration
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * @return the jobPosterName
	 */
	public String getJobPosterName() {
		return jobPosterName;
	}

	/**
	 * @param jobPosterName the jobPosterName to set
	 */
	public void setJobPosterName(String jobPosterName) {
		this.jobPosterName = jobPosterName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the linkApply
	 */
	public String getLinkApply() {
		return linkApply;
	}

	/**
	 * @param linkApply the linkApply to set
	 */
	public void setLinkApply(String linkApply) {
		this.linkApply = linkApply;
	}
	
	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}
	
	/**
	 * @return the set tags
	 */
	public Set<TagView> getSettags() {
		return setTags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setSettags(Set<TagView> setTags) {
		this.setTags = setTags;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the postFilePath
	 */
	public String getPostFilePath() {
		return postFilePath;
	}

	/**
	 * @param postFilePath the postFilePath to set
	 */
	public void setPostFilePath(String postFilePath) {
		this.postFilePath = postFilePath;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * @return the canApply
	 */
	public Boolean getCanApply() {
		return canApply;
	}

	/**
	 * @param canApply the canApply to set
	 */
	public void setCanApply(Boolean canApply) {
		this.canApply = canApply;
	}
	
	/**
	 * @return the canEditDelete
	 */
	public Boolean getCanEditDelete() {
		return canEditDelete;
	}

	/**
	 * @param canEditDelete the canEditDelete to set
	 */
	public void setCanEditDelete(Boolean canEditDelete) {
		this.canEditDelete = canEditDelete;
	}	
}
