package com.intentlabs.aleague.jobpost.operation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.jobpost.enums.EnumJobType;
import com.intentlabs.aleague.jobpost.model.JobPostModel;
import com.intentlabs.aleague.jobpost.model.JobPostTagModel;
import com.intentlabs.aleague.jobpost.services.JobPostService;
import com.intentlabs.aleague.jobpost.services.JobPostTagService;
import com.intentlabs.aleague.jobpost.view.JobPostView;
import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.aleague.tag.services.TagService;
import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.Constant;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.View;

/**
 * This is operation for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Component(value = "jobPostOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class JobPostOperationImpl extends AbstractOperation<JobPostModel, JobPostView> implements JobPostOperation {

	private static final long serialVersionUID = 741391010890205612L;

	@Autowired
	private JobPostService jobPostService;

	@Autowired
	private JobPostTagService jobPostTagService;

	@Autowired
	private TagService tagService;

	@Override
	public BaseService getService() {
		return jobPostService;
	}

	@Override
	protected JobPostModel getNewModel(JobPostView view) {
		JobPostModel model = new JobPostModel();
		model.setCreateDate(DateUtility.getCurrentDate());
		return model;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		return null;
	}

	@Override
	protected void checkInactive(JobPostModel model) throws Exception {
	}

	@Override
	public JobPostModel toModel(JobPostModel jobPostModel, JobPostView jobPostView) throws Exception {
		if (jobPostView.getId() == null || jobPostView.getId().longValue() <= 0) {
			jobPostModel.setCreateBy(Auditor.getAuditor());
			jobPostModel.setActivationStatus(ActiveInActive.ACTIVE);
			jobPostModel.setActivationDate(DateUtility.getCurrentDate());
			jobPostModel.setEnumArchive("1");
		}

		jobPostModel.setUpdateBy(Auditor.getAuditor());
		jobPostModel.setUpdateDate(DateUtility.getCurrentDate());

		jobPostModel.setTxtDuration(jobPostView.getDuration());
		jobPostModel.setTxtJobDescription(jobPostView.getDescription());
		jobPostModel.setTxtJobPosterName(jobPostView.getJobPosterName());
		jobPostModel.setTxtPostName(jobPostView.getPostName());
		
		if (jobPostView.getLinkApply().startsWith("https:") || jobPostView.getLinkApply().startsWith("http:")) {
			jobPostModel.setTxtLinkApply(jobPostView.getLinkApply());
		} else {
			jobPostModel.setTxtLinkApply(Constant.HTTP_URL + jobPostView.getLinkApply());
		}
		
		Set<JobPostTagModel> setJobPostTagModel = new HashSet<>();

		Map<Long, JobPostTagModel> mapTagModel = new HashMap<>();

		if (jobPostModel.getSetJobPostTagModel() != null && !jobPostModel.getSetJobPostTagModel().isEmpty()) {
			for (JobPostTagModel jobPostTag : jobPostModel.getSetJobPostTagModel()) {
				mapTagModel.put(jobPostTag.getTag().getId(), jobPostTag);
			}
		}

		if (jobPostView.getTags() != null && !"".equals(jobPostView.getTags())) {
			JSONArray jsonArrayJobPostTag = new JSONArray(jobPostView.getTags());

			List<TagModel> listTags = tagService.getAllTags(null);

			for (Integer i = 0; i < jsonArrayJobPostTag.length(); i++) {
				Long tagId = jsonArrayJobPostTag.getLong(i);

				JobPostTagModel jobPostTag;
				if (mapTagModel.containsKey(tagId)) {
					jobPostTag = mapTagModel.get(tagId);

					if (jobPostTag.getActivationStatus().getId() == ActiveInActive.INACTIVE.getId()) {
						jobPostTag.setActivationStatus(ActiveInActive.ACTIVE);
					}

					mapTagModel.remove(tagId);
				} else {
					jobPostTag = new JobPostTagModel();
					jobPostTag.setCreateBy(Auditor.getAuditor());
					jobPostTag.setCreateDate(DateUtility.getCurrentDate());
					jobPostTag.setActivationStatus(ActiveInActive.ACTIVE);
					jobPostTag.setActivationChangeBy(Auditor.getAuditor());
					jobPostTag.setActivationDate(DateUtility.getCurrentDate());
					jobPostTag.setEnumArchive("1");

					for (TagModel instanceTag : listTags) {
						if (instanceTag.getId().longValue() == tagId.longValue()) {
							jobPostTag.setTag(instanceTag);
							break;
						}
					}
				}

				jobPostTag.setUpdateBy(Auditor.getAuditor());
				jobPostTag.setUpdateDate(DateUtility.getCurrentDate());

				setJobPostTagModel.add(jobPostTag);
			}
		}

		if (!mapTagModel.keySet().isEmpty()) {
			for (Long key : mapTagModel.keySet()) {
				JobPostTagModel jobPostTag = mapTagModel.get(key);

				if (jobPostTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					jobPostTag.setActivationStatus(ActiveInActive.INACTIVE);
					jobPostTag.setActivationChangeBy(Auditor.getAuditor());
					jobPostTag.setActivationDate(DateUtility.getCurrentDate());
					setJobPostTagModel.add(jobPostTag);
				}
			}
		}

		jobPostModel.setSetJobPostTagModel(setJobPostTagModel);

		jobPostModel
				.setDateStartDate(DateUtility.toDate(jobPostView.getStartDate(), new SimpleDateFormat("dd-MM-yyyy")));
		jobPostModel.setDateEndDate(DateUtility.toDate(jobPostView.getEndDate(), new SimpleDateFormat("dd-MM-yyyy")));
		
		if(jobPostView.getFile() != null){
			jobPostModel.setTxtPostFilePath(FileUtility.storeFile(jobPostView.getFile(), "jobpost"));
		}else{
			jobPostModel.setTxtPostFilePath("");
		}

		if (jobPostView.getJobType() != null && !"".equals(jobPostView.getJobType())) {
			if ("PERMANENT".equalsIgnoreCase(jobPostView.getJobType())) {
				jobPostModel.setTxtJobType(EnumJobType.PERMANENT);
			} else if ("TEMPORARY".equalsIgnoreCase(jobPostView.getJobType())) {
				jobPostModel.setTxtJobType(EnumJobType.TEMPORARY);
			} else if ("CONTRACT".equalsIgnoreCase(jobPostView.getJobType())) {
				jobPostModel.setTxtJobType(EnumJobType.CONTRACT);
			} else if ("INTERNSHIP".equalsIgnoreCase(jobPostView.getJobType())) {
				jobPostModel.setTxtJobType(EnumJobType.INTERNSHIP);
			}
		}

		return jobPostModel;
	}

	@Override
	public JobPostView fromModel(JobPostModel jobPostModel) {
		JobPostView jobPostView = new JobPostView();
		
		Boolean canApply = true, canEditDelete = false;				
		if(Auditor.getAuditor().getId().longValue() == jobPostModel.getCreateBy().getId().longValue()){
			canApply = false;
			canEditDelete = true;
		}
		
		if(jobPostModel.getDateEndDate().before(DateUtility.getCurrentDate())){
			canApply = false;
		}
		
		jobPostView.setCanApply(canApply);
		jobPostView.setCanEditDelete(canEditDelete);
		
		jobPostView.setId(jobPostModel.getId());
		jobPostView.setPostName(jobPostModel.getTxtPostName());
		jobPostView.setJobType(jobPostModel.getTxtJobType().getName());
		jobPostView.setDuration(jobPostModel.getTxtDuration());
		jobPostView.setJobPosterName(jobPostModel.getTxtJobPosterName());
		jobPostView.setDescription(jobPostModel.getTxtJobDescription());
		jobPostView.setLinkApply(jobPostModel.getTxtLinkApply());

		Set<TagView> setTags = new HashSet<>();
		if (jobPostModel.getSetJobPostTagModel() != null && !jobPostModel.getSetJobPostTagModel().isEmpty()) {
			for (JobPostTagModel jobPostTag : jobPostModel.getSetJobPostTagModel()) {
				if (jobPostTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					TagView tagView = new TagView();
					tagView.setId(jobPostTag.getTag().getId());
					tagView.setTagName(jobPostTag.getTag().getTxtTagName());
					setTags.add(tagView);
				}
			}
		}

		jobPostView.setSettags(setTags);

		jobPostView.setStartDate(
				DateUtility.toDateTimeString(jobPostModel.getDateStartDate(), new SimpleDateFormat("dd-MM-yyyy")));
		jobPostView.setEndDate(
				DateUtility.toDateTimeString(jobPostModel.getDateEndDate(), new SimpleDateFormat("dd-MM-yyyy")));
		
		if(!"".equals(jobPostModel.getTxtPostFilePath())){
			jobPostView.setPostFilePath(FileUtility.setPath(jobPostModel.getTxtPostFilePath()));	
		}else{
			if(jobPostModel.getCreateBy().getFkInstituteId().getTxtLogoPath() != null && !"".equals(jobPostModel.getCreateBy().getFkInstituteId().getTxtLogoPath())){
				jobPostView.setPostFilePath(FileUtility.setPath(jobPostModel.getCreateBy().getFkInstituteId().getTxtLogoPath()));	
			}else{
				jobPostView.setPostFilePath(FileUtility.setPath("defaultinstitutepic.png"));
			}			
		}

		return jobPostView;
	}

	@Override
	public Response doSaveOperation(JobPostView view) throws Exception {
		JobPostModel model = null;

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = getNewModel(view);
		} else {
			model = jobPostService.getById(view.getId());
		}

		model = toModel(model, view);

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = jobPostService.create(model);
		} else {
			jobPostService.update(model);
		}

		for (JobPostTagModel jobPostTag : model.getSetJobPostTagModel()) {
			if (jobPostTag.getJobPost() == null) {
				jobPostTag.setJobPost(model);
				jobPostTagService.create(jobPostTag);
			} else {
				jobPostTagService.update(jobPostTag);
			}
		}

		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Job Post has been saved.");
	}

	@Override
	public Response doDisplayOperation(Long id) {
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_JOB_POST,
				fromModel(jobPostService.getById(id)));
	}

	@Override
	public Response doDisplayAllOperation(String tags, String location, String jobtype) {
		List<JobPostModel> listJobPosts = jobPostService.getAllJobPosts(tags, location, jobtype);

		List<View> listJobViews = new ArrayList<>();

		for (JobPostModel model : listJobPosts) {
			listJobViews.add(fromModel(model));
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_JOB_POSTS,
				listJobViews.size(), listJobViews);
	}
		
	@Override
	public Response doDeleteOperation(Long jobPostId){
		JobPostModel model= jobPostService.getById(jobPostId);
		model.setActivationStatus(ActiveInActive.INACTIVE);
		model.setActivationDate(DateUtility.getCurrentDate());
		model.setActivationChangeBy(Auditor.getAuditor());
		jobPostService.update(model);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Job Post has been deleted.");
	}
}
