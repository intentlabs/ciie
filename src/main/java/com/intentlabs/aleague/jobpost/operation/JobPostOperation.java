
package com.intentlabs.aleague.jobpost.operation;

import com.intentlabs.aleague.jobpost.view.JobPostView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is operation for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface JobPostOperation extends BaseOperation<JobPostView> {

	String JOB_POST_OPERATION = "jobPostOperation";	

	Response doDisplayOperation(Long id);

	Response doDisplayAllOperation(String tags, String location, String jobtype);
	
	Response doDeleteOperation(Long jobPostId);
}
