

package com.intentlabs.aleague.jobpost.enums;

import com.intentlabs.common.enums.EnumType;

/**
 * This is used for defined event type.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public enum EnumJobType implements EnumType
{
	
	PERMANENT(1, "PERMANENT"),
	TEMPORARY(2,"TEMPORARY"),
	CONTRACT(3, "CONTRACT"),
	INTERNSHIP(4, "INTERNSHIP");

	private final long id;
    private final String name;
    
    EnumJobType(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
	 return name;
	}

	
    public static EnumJobType fromId(long id) {
        for (EnumJobType status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }

}
