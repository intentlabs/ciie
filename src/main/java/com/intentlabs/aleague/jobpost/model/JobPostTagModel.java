
package com.intentlabs.aleague.jobpost.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tblJobPostTag")
public class JobPostTagModel extends ArchiveModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkTag")
	private TagModel tag;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkJobPost")  
	private JobPostModel jobPost;

	public TagModel getTag() {
		return tag;
	}

	public void setTag(TagModel tag) {
		this.tag = tag;
	}

	public JobPostModel getJobPost() {
		return jobPost;
	}

	public void setJobPost(JobPostModel jobPost) {
		this.jobPost = jobPost;
	}
}
