
package com.intentlabs.aleague.jobpost.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.intentlabs.aleague.jobpost.enums.EnumJobType;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tblJobPost")
public class JobPostModel extends ArchiveModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;
	
	@Column(name = "txtPostName")
	private String txtPostName;
	
	@Column(name = "enumJobType")
	private long jobType;
	
	@Column(name = "txtDuration")
	private String txtDuration;
	
	@Column(name = "txtJobPosterName")
	private String txtJobPosterName;
	
	@Column(name = "txtJobDescription")
	private String txtJobDescription;
	
	@Column(name = "txtLinkApply")
	private String txtLinkApply;
	
	@Column(name = "dateStartDate")
	private Date dateStartDate;	
	
	@Column(name = "dateEndDate")
	private Date dateEndDate;
	
	@Column(name = "txtPostFilePath")
	private String txtPostFilePath;
	
	@Column(name = "txtLocation")
	private String txtLocation;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkJobPost")
	private Set<JobPostTagModel> setJobPostTagModel;
	
	/**
	 * @return the txtPostName
	 */
	public String getTxtPostName() {
		return txtPostName;
	}

	/**
	 * @param txtPostName the txtPostName to set
	 */
	public void setTxtPostName(String txtPostName) {
		this.txtPostName = txtPostName;
	}

	/**
	 * @return the enumJobType
	 */
	public EnumJobType getTxtJobType() {
		return  EnumJobType.fromId(jobType);
	}

	/**
	 * @param txtJobType the txtJobType to set
	 */
	public void setTxtJobType(EnumJobType enumJobType) {
		this.jobType = enumJobType.getId();
	}

	/**
	 * @return the txtDuration
	 */
	public String getTxtDuration() {
		return txtDuration;
	}

	/**
	 * @param txtDuration the txtDuration to set
	 */
	public void setTxtDuration(String txtDuration) {
		this.txtDuration = txtDuration;
	}

	/**
	 * @return the txtJobPosterName
	 */
	public String getTxtJobPosterName() {
		return txtJobPosterName;
	}

	/**
	 * @param txtJobPosterName the txtJobPosterName to set
	 */
	public void setTxtJobPosterName(String txtJobPosterName) {
		this.txtJobPosterName = txtJobPosterName;
	}

	/**
	 * @return the txtJobDescription
	 */
	public String getTxtJobDescription() {
		return txtJobDescription;
	}

	/**
	 * @param txtJobDescription the txtJobDescription to set
	 */
	public void setTxtJobDescription(String txtJobDescription) {
		this.txtJobDescription = txtJobDescription;
	}

	/**
	 * @return the txtLinkApply
	 */
	public String getTxtLinkApply() {
		return txtLinkApply;
	}

	/**
	 * @param txtLinkApply the txtLinkApply to set
	 */
	public void setTxtLinkApply(String txtLinkApply) {
		this.txtLinkApply = txtLinkApply;
	}

	/**
	 * @return the dateStartDate
	 */
	public Date getDateStartDate() {
		return dateStartDate;
	}

	/**
	 * @param dateStartDate the dateStartDate to set
	 */
	public void setDateStartDate(Date dateStartDate) {
		this.dateStartDate = dateStartDate;
	}

	/**
	 * @return the dateEndDate
	 */
	public Date getDateEndDate() {
		return dateEndDate;
	}

	/**
	 * @param dateEndDate the dateEndDate to set
	 */
	public void setDateEndDate(Date dateEndDate) {
		this.dateEndDate = dateEndDate;
	}

	/**
	 * @return the txtPostFilePath
	 */
	public String getTxtPostFilePath() {
		return txtPostFilePath;
	}

	/**
	 * @param txtPostFilePath the txtPostFilePath to set
	 */
	public void setTxtPostFilePath(String txtPostFilePath) {
		this.txtPostFilePath = txtPostFilePath;
	}

	/**
	 * @return the txtLocation
	 */
	public String getTxtLocation() {
		return txtLocation;
	}

	/**
	 * @param txtLocation the txtLocation to set
	 */
	public void setTxtLocation(String txtLocation) {
		this.txtLocation = txtLocation;
	}

	public Set<JobPostTagModel> getSetJobPostTagModel() {
		return setJobPostTagModel;
	}

	public void setSetJobPostTagModel(Set<JobPostTagModel> setJobPostTagModel) {
		this.setJobPostTagModel = setJobPostTagModel;
	}

	public static JobPostModel getSaveModel(){
		JobPostModel jobPost = new JobPostModel();
		jobPost.setCreateDate(new Date());
		return jobPost;
	}
	
}
