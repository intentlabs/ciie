/**
 * 
 */
package com.intentlabs.aleague.instutute.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.intentlabs.common.model.ArchiveModel;

/**
 * @author Dhruvang
 *
 */

@Entity
@Table(name = "tblInstitute",uniqueConstraints = {
		@UniqueConstraint(columnNames = "txtName")})
public class InstituteModel extends ArchiveModel {

	
	private static final long serialVersionUID = -8011284515198720397L;
	
	
	@Column(name = "txtName", length = 200, nullable = false)
	private String txtName;
	
	@Column(name = "txtAddress", length = 500, nullable = false)
	private String txtAddress;
	
	@Column(name = "txtDescription", nullable = true)
	private String txtDescription;
	
	@Column(name = "txtFbLink", length = 500, nullable = true)
	private String txtFbLink;
	
	@Column(name = "txtTwLink", length = 500, nullable = true)
	private String txtTwLink;
	
	@Column(name = "txtWebsiteLink", length = 500, nullable = true)
	private String txtWebsiteLink;
	
	@Column(name = "txtLogoPath", length = 100, nullable = true)
	private String txtLogoPath;
	
	@Column(name = "txtDomain", length = 20, nullable = true)
	private String txtDomain;

	public String getTxtName() {
		return txtName;
	}

	public void setTxtName(String txtName) {
		this.txtName = txtName;
	}

	public String getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(String txtAddress) {
		this.txtAddress = txtAddress;
	}

	public String getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	public String getTxtFbLink() {
		return txtFbLink;
	}

	public void setTxtFbLink(String txtFbLink) {
		this.txtFbLink = txtFbLink;
	}

	public String getTxtTwLink() {
		return txtTwLink;
	}

	public void setTxtTwLink(String txtTwLink) {
		this.txtTwLink = txtTwLink;
	}

	public String getTxtWebsiteLink() {
		return txtWebsiteLink;
	}

	public void setTxtWebsiteLink(String txtWebsiteLink) {
		this.txtWebsiteLink = txtWebsiteLink;
	}

	public String getTxtLogoPath() {
		return txtLogoPath;
	}

	public void setTxtLogoPath(String txtLogoPath) {
		this.txtLogoPath = txtLogoPath;
	}

	public String getTxtDomain() {
		return txtDomain;
	}

	public void setTxtDomain(String txtDomain) {
		this.txtDomain = txtDomain;
	}
}
