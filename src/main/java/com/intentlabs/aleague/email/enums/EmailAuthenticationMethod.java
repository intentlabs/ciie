package com.intentlabs.aleague.email.enums;

import java.util.HashMap;
import java.util.Map;

import com.intentlabs.common.enums.EnumType;



/**
 * This enum defined email server authentication method.
 * @author Dhruvang.Joshi
 * @since 26/07/2017
 */
public enum EmailAuthenticationMethod implements EnumType {
	
	PLAIN(0, "PLAIN"),
	LOGIN(1, "LOGIN"),
	CRAM_MD5(2,"CRAM MD5");
	
	private final long id;
    private final String name;
    
    public static final Map<Long, EmailAuthenticationMethod> MAP = new HashMap<>();
    
    static{
    	for(EmailAuthenticationMethod mailAuthenticationMethod : values()){
    		MAP.put(mailAuthenticationMethod.getId(), mailAuthenticationMethod);
    	}
    }
    
    EmailAuthenticationMethod(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public static EmailAuthenticationMethod fromId(String id) {
    	return MAP.get(id);
    }
}
