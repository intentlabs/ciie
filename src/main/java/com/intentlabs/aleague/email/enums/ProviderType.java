package com.intentlabs.aleague.email.enums;

import java.util.HashMap;
import java.util.Map;

import com.intentlabs.common.enums.EnumType;

public enum ProviderType implements EnumType {
	SSL_SECURE(1, "SSL Secure"),
	TLS_SECURE(2,"TLS Secure");
	
	private final long id;
    private final String name;
    
    
   public static final Map<Long, ProviderType> MAP = new HashMap<>();
    
    static{
    	for(ProviderType providerType : values()){
    		MAP.put(providerType.getId(), providerType);
    	}
    }
    
    
    ProviderType(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public static ProviderType fromId(String id) {
    	return MAP.get(id);
    }
}
