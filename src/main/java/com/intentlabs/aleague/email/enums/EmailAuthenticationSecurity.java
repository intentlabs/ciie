package com.intentlabs.aleague.email.enums;

import java.util.HashMap;
import java.util.Map;

import com.intentlabs.common.enums.EnumType;



/**
 * This enum defined email server security method.
 * @author Dhruvang.Joshi
 * @since 26/07/2017
 *
 */
public enum EmailAuthenticationSecurity implements EnumType {
	
	NONE(0, "None"),
	SSL(1, "USE SSL"),
	TLS(2,"USE TLS");
	
	private final long id;
    private final String name;
    
    EmailAuthenticationSecurity(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public static final Map<Long, EmailAuthenticationSecurity> MAP = new HashMap<>();
    
    static{
    	for(EmailAuthenticationSecurity mailAuthenticationSecurity : values()){
    		MAP.put(mailAuthenticationSecurity.getId(), mailAuthenticationSecurity);
    	}
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
    
    public static EmailAuthenticationSecurity fromId(String id) {
    	return MAP.get(id);
    }
}
