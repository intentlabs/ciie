package com.intentlabs.aleague.email.enums;

import java.util.HashMap;
import java.util.Map;

import com.intentlabs.common.enums.EnumType;


/**
 * This enum is used to maintain email/sms/push notification status. 
 * @author Dhruvang.Joshi
 * @since 26/07/2017
 */
public enum Status implements EnumType {
	
	NEW(0, "NEW"),
	INPROCESS(1, "INPROCESS"),
	FAILED(2, "FAILED"),
	SENT(3, "SENT");
	
	private final long id;
    private final String name;
    
    public static final Map<Long, Status> MAP = new HashMap<>();
    
    static{
    	for(Status status : values()){
    		MAP.put(status.getId(), status);
    	}
    }
    
    Status(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
    
    public static Status fromId(String id) {
    	return MAP.get(id);
    }

}
