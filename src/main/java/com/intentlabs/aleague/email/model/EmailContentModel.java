/*******************************************************************************
 * Copyright -2017 @IntentLabs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.intentlabs.aleague.email.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.intentlabs.common.model.IdentifierModel;

/**
 * This is Email Content which maps Email Content table of database.
 * All email related details will be stored in this table.
 * @author Dhruvang.Joshi
 * @since 12/08/2017
 */
@Entity
@Table(name = "tblEmailContent")
@DynamicUpdate(true)
@DynamicInsert(true)
public class EmailContentModel extends IdentifierModel {
	
	private static final long serialVersionUID = -4351538513633808259L;
	
	@Column(name = "txtName", nullable = false)
	private String name;
	
	@Column(name = "txtContent", nullable = false)
	private String content;

	private static Map<Long, EmailContentModel> MAP = new ConcurrentHashMap<>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public static void addEmailContent(EmailContentModel emailContentModel) {
		MAP.put(emailContentModel.getId(), emailContentModel);
	}

	public static Map<Long, EmailContentModel> getMAP() {
		return MAP;
	}
}
