/*******************************************************************************
 * Copyright -2017 @IntentLabs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.intentlabs.aleague.email.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.intentlabs.aleague.email.enums.Status;
import com.intentlabs.common.model.BulkIdentifierModel;

/**
 * This is Marketing Email model which maps Marketing email table of database.
 * Marketing email like (Advertisement etc.) will fall under this category.
 * Product Marketing will fall under this category.
 * 
 * @author Dhruvang.Joshi
 * @since 28/07/2017
 */

@Entity
@Table(name = "tblMarketingEmail")
@DynamicUpdate(true)
@DynamicInsert(true)
public class MarketingEmailModel extends BulkIdentifierModel {
	
	private static final long serialVersionUID = -4351538513633808259L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkEmailAccountId", updatable=false, nullable=false)
	private EmailAccountModel emailAccountModel;
	
	@Column(name = "txtEmailTo", nullable = false)
	private String emailTo;
	
	@Column(name = "txtEmailCc", nullable = true)
	private String emailCc;
	
	@Column(name = "txtEmailBcc", nullable = true)
	private String emailBcc;
	
	@Column(name = "txtSubject", nullable=false, length=1000)
	private String subject;
	 
	@Column(name = "txtBody", nullable=true)
	private String body;
	
	@Column(name = "enumStatus", nullable=false, length=1)
	@Access(AccessType.FIELD)
	private String status;

	@Column(name = "numberRetryCount", nullable=true, length=2)
	private Integer retryCount;
	
	@Column(name = "txtAttachmentPath", nullable=true)
	private String attachment;
	
	@Column(name = "txtError", nullable=true)
	private String txtError;

	public EmailAccountModel getEmailAccountModel() {
		return emailAccountModel;
	}

	public void setEmailAccountModel(EmailAccountModel emailAccountModel) {
		this.emailAccountModel = emailAccountModel;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailCc() {
		return emailCc;
	}

	public void setEmailCc(String emailCc) {
		this.emailCc = emailCc;
	}

	public String getEmailBcc() {
		return emailBcc;
	}

	public void setEmailBcc(String emailBcc) {
		this.emailBcc = emailBcc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Status getStatus() {
		return Status.fromId(status);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getTxtError() {
		return txtError;
	}

	public void setTxtError(String txtError) {
		this.txtError = txtError;
	}
}
