/*******************************************************************************
 * Copyright -2017 @IntentLabs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.intentlabs.aleague.email.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.intentlabs.aleague.email.enums.EmailAuthenticationMethod;
import com.intentlabs.aleague.email.enums.EmailAuthenticationSecurity;
import com.intentlabs.aleague.email.enums.ProviderType;
import com.intentlabs.common.model.IdentifierModel;
/**
 * This is Email account model which maps email account table.
 * @author Dhruvang.Joshi
 * @since 26/07/2017
 */
@Entity
@Table(name = "tblEmailAccount")
@DynamicUpdate(true)
@DynamicInsert(true)
public class EmailAccountModel extends IdentifierModel {


	private static final long serialVersionUID = 4948296735319653785L;
	
	@Column(name = "txtAccountName", length=50, unique=true, nullable=false)
	private String accountName;
	
	@Column(name = "txtReplyToEmail", length=100, nullable=false)
	private String replyToEmail;
	
	@Column(name = "numberRetryAttempt", nullable=true)
	private Integer retryAttempt;
	
	@Column(name = "txtHost", length=100, nullable=false)
	private String host;
	
	@Column(name = "numberPort", nullable=true)
	private Integer port;
	
	@Column(name = "txtUserName", length=100, nullable=false, unique=true)
	private String username;
	
	@Column(name = "txtpassword", length=100, nullable=false)
	private String password;

	@Column(name = "enumAuthenticationMethod")
	@Access(AccessType.FIELD)
	private String authenticationMethod;
	
	@Column(name = "enumAuthenticationSecurity")
	@Access(AccessType.FIELD)
	private String authenticationSecurity;
	
	@Column(name = "numberTimeout", nullable=true)
	private Integer timeOut;
	
	private static Map<Long, EmailAccountModel> MAP = new ConcurrentHashMap<>();
	
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public EmailAuthenticationMethod getAuthenticationMethod() {
		return EmailAuthenticationMethod.fromId(authenticationMethod);
	}

	public void setAuthenticationMethod(String authenticationMethod) {
		this.authenticationMethod = authenticationMethod;
	}

	public EmailAuthenticationSecurity getAuthenticationSecurity() {
		return EmailAuthenticationSecurity.fromId(authenticationSecurity);
	}

	public void setAuthenticationSecurity(String authenticationSecurity) {
		this.authenticationSecurity = authenticationSecurity;
	}

	public Integer getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(Integer timeOut) {
		this.timeOut = timeOut;
	}

	public Integer getRetryAttempt() {
		return retryAttempt;
	}

	public void setRetryAttempt(Integer retryAttempt) {
		this.retryAttempt = retryAttempt;
	}
		
	public String getReplyToEmail() {
		return replyToEmail;
	}

	public void setReplyToEmail(String replyToEmail) {
		this.replyToEmail = replyToEmail;
	}
	
	public static void addEmailAccount(EmailAccountModel emailAccountModel) {
		MAP.put(emailAccountModel.getId(), emailAccountModel);
	}

	public static Map<Long, EmailAccountModel> getMAP() {
		return MAP;		
	}

	public ProviderType getProviderType() {
		if(String.valueOf(EmailAuthenticationSecurity.TLS.getId()).equals(authenticationSecurity)){
			return ProviderType.TLS_SECURE;
		}else if(String.valueOf(EmailAuthenticationSecurity.SSL.getId()).equals(authenticationSecurity)){
			return ProviderType.SSL_SECURE;
		}else{
			return null;	
		}
	}
}
