/*******************************************************************************
 * Copyright -2017 @IntentLabs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.intentlabs.aleague.email.service;

import java.util.List;

import com.intentlabs.aleague.email.model.NotificationEmailModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is declaration of Notification Email service which defines database operation
 * which can  be performed on Notification Email table.
 * @author Nirav.Shah
 * @since 12/08/2017
 */
public interface NotificationEmailService extends BaseService<NotificationEmailModel> {
	/**
	 * This method is used to get email records list base on given count.
	 * @param limit
	 * @return
	 */
	List<NotificationEmailModel> getEmailList(int limit);
	
	/**
	 * This method is used to get failed email records list base on given count.
	 * @param limit
	 * @return
	 */
	List<NotificationEmailModel> getFailedEmailList(int limit); 
}
