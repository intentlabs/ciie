/*******************************************************************************
 * Copyright -2017 @IntentLabs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.intentlabs.aleague.email.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.email.enums.Status;
import com.intentlabs.aleague.email.model.TransactionalEmailModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is definition of Email service which defines database operation
 * which can  be performed on this table.
 * @author Nirav.Shah
 * @since 12/08/2017
 */
public class TransactionalEmailServiceImpl extends AbstractService<TransactionalEmailModel> implements TransactionalEmailService {

	@Override
	public Class<TransactionalEmailModel> getModelClass() {
		return TransactionalEmailModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<TransactionalEmailModel> modelclass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(TransactionalEmailModel transactionalEmail, Criteria commonCriteria) {
		return commonCriteria;
	}
	
	@Override
	public List<TransactionalEmailModel> getEmailList(int limit) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setLockMode(LockMode.UPGRADE);
		criteria.setMaxResults(limit);
		criteria.addOrder(Order.asc("id"));
		criteria.add(Restrictions.eq("status", Status.NEW.getId()));
		return updateStatus(criteria,false);
	}
	
	@Override
	public List<TransactionalEmailModel> getFailedEmailList(int limit) {
		Criteria criteria = getSession().createCriteria(getModelClass());		
		criteria.setLockMode(LockMode.UPGRADE);
		criteria.setMaxResults(limit);
		criteria.addOrder(Order.asc("id"));
		criteria.add(Restrictions.eq("status", Status.FAILED.getId()));
		criteria.addOrder(Order.asc("retryCount"));
		return updateStatus(criteria,true);
	}
	
	private List<TransactionalEmailModel> updateStatus(Criteria criteria,boolean isRetryAttempt){
		List<TransactionalEmailModel> emailList = criteria.list();
		for(TransactionalEmailModel transactionalEmail : emailList){
			transactionalEmail.setStatus(String.valueOf(Status.INPROCESS.getId()));
			if(isRetryAttempt){
				transactionalEmail.setRetryCount(transactionalEmail.getRetryCount() + 1);
			}
			update(transactionalEmail);
		}
		return emailList;
	}
}
