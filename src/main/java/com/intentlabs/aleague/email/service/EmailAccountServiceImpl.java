/*******************************************************************************
 * Copyright -2017 @IntentLabs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.intentlabs.aleague.email.service;

import org.hibernate.Criteria;

import com.intentlabs.aleague.email.model.EmailAccountModel;
import com.intentlabs.aleague.exception.CapsException;
import com.intentlabs.common.kernal.CustomInitializationBean;
import com.intentlabs.common.service.AbstractService;

/**
 * This is definition of Email Account service which defines database operation
 * which can  be performed on this table.
 * @author Nirav.Shah
 * @since 12/08/2017
 */
public class EmailAccountServiceImpl extends AbstractService<EmailAccountModel> implements EmailAccountService, CustomInitializationBean {

	@Override
	public Class<EmailAccountModel> getModelClass() {
		return EmailAccountModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<EmailAccountModel> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(EmailAccountModel emailAccount, Criteria commonCriteria) {
		return commonCriteria;
	}
	
	@Override
	public void onStartUp() throws CapsException {
		for(EmailAccountModel emailAccountModel : findAll()){
			EmailAccountModel.getMAP().put(emailAccountModel.getId(), emailAccountModel);
		}
	}
}
