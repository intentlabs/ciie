package com.intentlabs.aleague.exceptionhandler;


import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.exception.InvalidRequestException;
import com.intentlabs.aleague.exception.UnAuthorizedException;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.FormErrorResponse;
import com.intentlabs.common.response.Response;


/**
 * This is a common exception handler which wrapped over controller. This will be used to handle custom & other common exception.
 * This handler will return response to client with response code & message.
 * @version 1.0
 *
 */
@ControllerAdvice
public class WebExceptionHandler {

	/**
	 * Invalid Data Exception Handler 
	 * @param invalidDataException
	 * @return {@link WebResponseView}
	 */
	@ExceptionHandler(InvalidDataException.class)
	@ResponseBody
	public Response handleInvalidDataException(InvalidDataException invalidDataException){
		LoggerService.error(invalidDataException.getClassName(), invalidDataException.getTransactionName(), "Invalid Data");
		return FormErrorResponse.create(invalidDataException.getCode(), invalidDataException.getMessage(), invalidDataException.getProperties());
	}
	
	/**
	 * Invalid Request exception handler
	 * @param invalidRequestException
	 * @return {@link WebResponseView}
	 */
	@ExceptionHandler(InvalidRequestException.class)
	@ResponseBody
	public Response handleInvalidRequestException(InvalidRequestException invalidRequestException){
		LoggerService.error(invalidRequestException.getClassName(), invalidRequestException.getTransactionName(), "Invalid Request");
		return CommonResponse.create(invalidRequestException.getCode(), invalidRequestException.getMessage());
	}
	
	/**
	 * UnAuthorizedException exception
	 * @param UnAuthorizedException
	 * @return {@link WebResponseView}
	 */
	@ExceptionHandler(UnAuthorizedException.class)
	public void handleUnAuthorizedException(UnAuthorizedException unAuthorizedException, HttpServletResponse response){
		try {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		} catch (IOException e) {
			LoggerService.error(unAuthorizedException.getClass().getName(), unAuthorizedException.getLocalizedMessage(), unAuthorizedException.getMessage());
			LoggerService.exception(e);
		}
	}
	
	/**
	 * Exception handler
	 * @param exception
	 * @return {@link WebResponseView}
	 */
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Response handleException(Exception exception){
		LoggerService.exception(exception);
		return CommonResponse.create(ResponseCode.INTERNAL_SERVER_ERROR.getCode(), ResponseCode.INTERNAL_SERVER_ERROR.getMessage());
	}
	
}
