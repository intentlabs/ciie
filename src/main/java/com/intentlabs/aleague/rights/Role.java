/**
 * 
 */
package com.intentlabs.aleague.rights;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.intentlabs.common.model.ArchiveModel;

/**
 * @author Dhruvang
 *
 */

@Entity
@Table(name = "tblRole")
public class Role extends ArchiveModel {

	
	private static final long serialVersionUID = 1048350008862303978L;
	
	
	@Column(name = "txtName", nullable = true)
	private String txtName;
	
	@Column(name = "txtDescription", nullable = false, length = 256)
	private String description;
	


	public String getTxtName() {
		return txtName;
	}

	public void setTxtName(String txtName) {
		this.txtName = txtName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	
	
	

}
