
package com.intentlabs.aleague.studentclub.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tblStudentClubTag")
public class StudentClubTagModel extends ArchiveModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkTag")
	private TagModel tag;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkStudentClub")  
	private StudentClubModel studentClub;

	public TagModel getTag() {
		return tag;
	}

	public void setTag(TagModel tag) {
		this.tag = tag;
	}

	/**
	 * @return the studentClub
	 */
	public StudentClubModel getStudentClub() {
		return studentClub;
	}

	/**
	 * @param studentClub the studentClub to set
	 */
	public void setStudentClub(StudentClubModel studentClub) {
		this.studentClub = studentClub;
	}
}
