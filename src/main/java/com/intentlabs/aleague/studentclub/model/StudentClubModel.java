
package com.intentlabs.aleague.studentclub.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tblStudentClub")
public class StudentClubModel extends ArchiveModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;
	
	@Column(name = "txtName")
	private String txtName;
	
	@Column(name = "txtSocialLink")
	private String txtSocialLink;
	
	@Column(name = "txtDescription")
	private String txtDescription;
	
	@Column(name = "txtLogoPath")
	private String txtLogoPath;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkStudentClub")
	private Set<StudentClubTagModel> setStudentClubTagModel;

	/**
	 * @return the txtName
	 */
	public String getTxtName() {
		return txtName;
	}

	/**
	 * @param txtName the txtName to set
	 */
	public void setTxtName(String txtName) {
		this.txtName = txtName;
	}

	/**
	 * @return the txtSocialLink
	 */
	public String getTxtSocialLink() {
		return txtSocialLink;
	}

	/**
	 * @param txtSocialLink the txtSocialLink to set
	 */
	public void setTxtSocialLink(String txtSocialLink) {
		this.txtSocialLink = txtSocialLink;
	}

	/**
	 * @return the txtDescription
	 */
	public String getTxtDescription() {
		return txtDescription;
	}

	/**
	 * @param txtDescription the txtDescription to set
	 */
	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	/**
	 * @return the txtLogoPath
	 */
	public String getTxtLogoPath() {
		return txtLogoPath;
	}

	/**
	 * @param txtLogoPath the txtLogoPath to set
	 */
	public void setTxtLogoPath(String txtLogoPath) {
		this.txtLogoPath = txtLogoPath;
	}

	/**
	 * @return the setStudentClubTagModel
	 */
	public Set<StudentClubTagModel> getSetStudentClubTagModel() {
		return setStudentClubTagModel;
	}

	/**
	 * @param setStudentClubTagModel the setStudentClubTagModel to set
	 */
	public void setSetStudentClubTagModel(Set<StudentClubTagModel> setStudentClubTagModel) {
		this.setStudentClubTagModel = setStudentClubTagModel;
	}

	public static StudentClubModel getSaveModel(){
		StudentClubModel studentClub = new StudentClubModel();
		studentClub.setCreateDate(new Date());
		return studentClub;
	}	
}
