
package com.intentlabs.aleague.studentclub.operation;

import com.intentlabs.aleague.studentclub.view.StudentClubView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is operation for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface StudentClubOperation extends BaseOperation<StudentClubView> {

	String STUDENT_CLUB_OPERATION = "studentClubOperation";
	
	Response doDisplayOperation(Long id);

	Response doDisplayAllOperation(Integer start, Integer end);
	
	Response doDeleteOperation(Long studentClubId);
}
