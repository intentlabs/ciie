
package com.intentlabs.aleague.studentclub.operation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.studentclub.model.StudentClubModel;
import com.intentlabs.aleague.studentclub.model.StudentClubTagModel;
import com.intentlabs.aleague.studentclub.services.StudentClubService;
import com.intentlabs.aleague.studentclub.services.StudentClubTagService;
import com.intentlabs.aleague.studentclub.view.StudentClubView;
import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.aleague.tag.services.TagService;
import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.Constant;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.View;

/**
 * This is operation for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Component(value = "studentClubOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class StudentClubOperationImpl extends AbstractOperation<StudentClubModel, StudentClubView> implements StudentClubOperation {

	private static final long serialVersionUID = 741391010890205612L;

	@Autowired
	private StudentClubService studentClubService;

	@Autowired
	private StudentClubTagService studentClubTagService;

	@Autowired
	private TagService tagService;

	@Override
	public BaseService getService() {
		return studentClubService;
	}

	@Override
	protected StudentClubModel getNewModel(StudentClubView view) {
		StudentClubModel model = new StudentClubModel();
		model.setCreateDate(DateUtility.getCurrentDate());
		return model;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		return null;
	}

	@Override
	protected void checkInactive(StudentClubModel model) throws Exception {
	}

	@Override
	public StudentClubModel toModel(StudentClubModel studentClubModel, StudentClubView studentClubView) throws Exception {
		if (studentClubView.getId() == null || studentClubView.getId().longValue() <= 0) {
			studentClubModel.setCreateBy(Auditor.getAuditor());
			studentClubModel.setActivationStatus(ActiveInActive.ACTIVE);
			studentClubModel.setActivationDate(DateUtility.getCurrentDate());
			studentClubModel.setEnumArchive("1");
		} else {
			studentClubModel.setId(studentClubView.getId());
		}

		studentClubModel.setUpdateBy(Auditor.getAuditor());
		studentClubModel.setUpdateDate(DateUtility.getCurrentDate());

		studentClubModel.setTxtName(studentClubView.getName());
		
		if (studentClubView.getSocialLink().startsWith("https:") || studentClubView.getSocialLink().startsWith("http:")) {
			studentClubModel.setTxtSocialLink(studentClubView.getSocialLink());
		} else {
			studentClubModel.setTxtSocialLink(Constant.HTTP_URL + studentClubView.getSocialLink());
		}
		
		studentClubModel.setTxtDescription(studentClubView.getDescription());
		
		if(studentClubView.getFile() != null){
			studentClubModel.setTxtLogoPath(FileUtility.storeFile(studentClubView.getFile(), "studentclublogo"));
		}

		Set<StudentClubTagModel> setStudentClubTagModel = new HashSet<>();

		Map<Long, StudentClubTagModel> mapTagModel = new HashMap<>();

		if (studentClubModel.getSetStudentClubTagModel() != null && !studentClubModel.getSetStudentClubTagModel().isEmpty()) {
			for (StudentClubTagModel studentClubTag : studentClubModel.getSetStudentClubTagModel()) {
				mapTagModel.put(studentClubTag.getTag().getId(), studentClubTag);
			}
		}

		if (studentClubView.getTags() != null && !"".equals(studentClubView.getTags())) {
			JSONArray jsonArrayStudentClubTag = new JSONArray(studentClubView.getTags());

			List<TagModel> listTags = tagService.getAllTags(null);

			for (Integer i = 0; i < jsonArrayStudentClubTag.length(); i++) {
				Long tagId = jsonArrayStudentClubTag.getLong(i);

				StudentClubTagModel studentClubTag;
				if (mapTagModel.containsKey(tagId)) {
					studentClubTag = mapTagModel.get(tagId);

					if (studentClubTag.getActivationStatus().getId() == ActiveInActive.INACTIVE.getId()) {
						studentClubTag.setActivationStatus(ActiveInActive.ACTIVE);
					}

					mapTagModel.remove(tagId);
				} else {
					studentClubTag = new StudentClubTagModel();
					studentClubTag.setCreateBy(Auditor.getAuditor());
					studentClubTag.setCreateDate(DateUtility.getCurrentDate());
					studentClubTag.setActivationStatus(ActiveInActive.ACTIVE);
					studentClubTag.setActivationChangeBy(Auditor.getAuditor());
					studentClubTag.setActivationDate(DateUtility.getCurrentDate());
					studentClubTag.setEnumArchive("1");

					for (TagModel instanceTag : listTags) {
						if (instanceTag.getId().longValue() == tagId.longValue()) {
							studentClubTag.setTag(instanceTag);
							break;
						}
					}
				}

				studentClubTag.setUpdateBy(Auditor.getAuditor());
				studentClubTag.setUpdateDate(DateUtility.getCurrentDate());

				setStudentClubTagModel.add(studentClubTag);
			}
		}

		if (!mapTagModel.keySet().isEmpty()) {
			for (Long key : mapTagModel.keySet()) {
				StudentClubTagModel studentClubTag = mapTagModel.get(key);

				if (studentClubTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					studentClubTag.setActivationStatus(ActiveInActive.INACTIVE);
					studentClubTag.setActivationChangeBy(Auditor.getAuditor());
					studentClubTag.setActivationDate(DateUtility.getCurrentDate());
					setStudentClubTagModel.add(studentClubTag);
				}
			}
		}

		studentClubModel.setSetStudentClubTagModel(setStudentClubTagModel);

		return studentClubModel;
	}

	@Override
	public StudentClubView fromModel(StudentClubModel studentClubModel) {
		StudentClubView studentClubView = new StudentClubView();
		studentClubView.setId(studentClubModel.getId());
		
		studentClubView.setName(studentClubModel.getTxtName());
		studentClubView.setDescription(studentClubModel.getTxtDescription());
		studentClubView.setSocialLink(studentClubModel.getTxtSocialLink());
		studentClubView.setLogoPath(FileUtility.setPath(studentClubModel.getTxtLogoPath()));

		Set<TagView> setTags = new HashSet<>();
		if (studentClubModel.getSetStudentClubTagModel() != null && !studentClubModel.getSetStudentClubTagModel().isEmpty()) {
			for (StudentClubTagModel studentClubTag : studentClubModel.getSetStudentClubTagModel()) {
				if (studentClubTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					TagView tagView = new TagView();
					tagView.setId(studentClubTag.getTag().getId());
					tagView.setTagName(studentClubTag.getTag().getTxtTagName());
					setTags.add(tagView);
				}
			}
		}

		studentClubView.setSetTags(setTags);		

		return studentClubView;
	}

	@Override
	public Response doSaveOperation(StudentClubView view) throws Exception {
		StudentClubModel model = null;

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = getNewModel(view);
		} else {
			model = studentClubService.getById(view.getId());
		}

		model = toModel(model, view);

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = studentClubService.create(model);
		} else {
			studentClubService.update(model);
		}

		for (StudentClubTagModel studentClubTag : model.getSetStudentClubTagModel()) {
			if (studentClubTag.getStudentClub() == null) {
				studentClubTag.setStudentClub(model);
				studentClubTagService.create(studentClubTag);
			} else {
				studentClubTagService.update(studentClubTag);
			}
		}

		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Student Club has been saved.");
	}

	@Override
	public Response doDisplayOperation(Long id) {
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_STUDENT_CLUB, fromModel(studentClubService.getById(id)));
	}

	@Override
	public Response doDisplayAllOperation(Integer start, Integer end) {
		List<StudentClubModel> listStudentClubs = studentClubService.getAllStudentClubs(null, start, end);

		List<View> listStudentClubViews = new ArrayList<>();

		for (StudentClubModel model : listStudentClubs) {			
			listStudentClubViews.add(fromModel(model));
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_STUDENT_CLUBS,
				listStudentClubViews.size(), listStudentClubViews);
	}
		
	@Override
	public Response doDeleteOperation(Long studentClubId){
		StudentClubModel model = studentClubService.getById(studentClubId);
		model.setActivationStatus(ActiveInActive.INACTIVE);
		model.setActivationDate(DateUtility.getCurrentDate());
		model.setActivationChangeBy(Auditor.getAuditor());
		studentClubService.update(model);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Student Club has been deleted.");
	}
}
