
package com.intentlabs.aleague.studentclub.view;

import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.view.ArciveView;

/**
 * This is View used to get data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class StudentClubView extends ArciveView {


	private static final long serialVersionUID = 8692674149531174388L;
	
	private String name;
	
	private String socialLink;
	
	private String tags;
	
	private Set<TagView> setTags;
	
	private String description;
	
	private String logoPath;
	
	private MultipartFile file;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the socialLink
	 */
	public String getSocialLink() {
		return socialLink;
	}

	/**
	 * @param socialLink the socialLink to set
	 */
	public void setSocialLink(String socialLink) {
		this.socialLink = socialLink;
	}

	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * @return the setTags
	 */
	public Set<TagView> getSetTags() {
		return setTags;
	}

	/**
	 * @param setTags the setTags to set
	 */
	public void setSetTags(Set<TagView> setTags) {
		this.setTags = setTags;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the logoPath
	 */
	public String getLogoPath() {
		return logoPath;
	}

	/**
	 * @param logoPath the logoPath to set
	 */
	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}
}