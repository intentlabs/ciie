
package com.intentlabs.aleague.studentclub.services;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.aleague.studentclub.model.StudentClubModel;
import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class StudentClubServiceImpl extends AbstractService<StudentClubModel> implements StudentClubService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<StudentClubModel> getModelClass() {
		return StudentClubModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<StudentClubModel> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(StudentClubModel model, Criteria commonCriteria) {
		return commonCriteria;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentClubModel> getAllStudentClubs(Long instituteId, Integer start, Integer end) {
		InstituteModel institute = null;
		if(instituteId == null){
			if(Auditor.getAuditor().getFkInstituteId() != null){
				institute = Auditor.getAuditor().getFkInstituteId();
			}
		}else{
			Criteria criteriaInstitute = getSession().createCriteria(InstituteModel.class);		
			criteriaInstitute.add(Restrictions.eq("id", instituteId));
			institute = (InstituteModel) criteriaInstitute.uniqueResult();
		}
		
		if(institute != null){
			Criteria criteriaUserInstitute = getSession().createCriteria(User.class);		
			criteriaUserInstitute.add(Restrictions.eq("fkInstituteId", institute));		
			List<User> userInstituteIds = criteriaUserInstitute.list();
			
			if(!userInstituteIds.isEmpty()){
				Criteria criteria = getSession().createCriteria(getModelClass());
				criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
				criteria.createAlias("setStudentClubTagModel", "studentClubTags", JoinType.LEFT_OUTER_JOIN);
				criteria.createAlias("studentClubTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);		
				criteria.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
				criteria.addOrder(Order.desc("id"));
				criteria.add(Restrictions.in("createBy", userInstituteIds));
				
				if(start != null && end != null){
					criteria.setFirstResult(start);
					criteria.setMaxResults(end - start);
				}
				
				return criteria.list();	
			}else{
				return new ArrayList<>();
			}			
		}else{
			return new ArrayList<>();
		}
		
	}

	@Override
	public StudentClubModel getById(Long id) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("setStudentClubTagModel", "studentClubTags", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("studentClubTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);		
		criteria.add(Restrictions.eq("id", id));
		return (StudentClubModel) criteria.uniqueResult();
	}
}