
package com.intentlabs.aleague.studentclub.services;

import java.util.List;

import com.intentlabs.aleague.studentclub.model.StudentClubModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface StudentClubService extends BaseService<StudentClubModel> {
	List<StudentClubModel> getAllStudentClubs(Long instituteId, Integer start, Integer end);
	
	StudentClubModel getById(Long id);
}
