
package com.intentlabs.aleague.studentclub.services;

import org.hibernate.Criteria;

import com.intentlabs.aleague.studentclub.model.StudentClubTagModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class StudentClubTagServiceImpl extends AbstractService<StudentClubTagModel> implements StudentClubTagService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<StudentClubTagModel> getModelClass() {
		return StudentClubTagModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<StudentClubTagModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(StudentClubTagModel model, Criteria commonCriteria) {
		return null;
	}
}