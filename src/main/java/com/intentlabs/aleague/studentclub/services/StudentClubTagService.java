
package com.intentlabs.aleague.studentclub.services;

import com.intentlabs.aleague.studentclub.model.StudentClubTagModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface StudentClubTagService extends BaseService<StudentClubTagModel> {
}
