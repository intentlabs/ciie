
package com.intentlabs.aleague.studentclub.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.studentclub.operation.StudentClubOperation;
import com.intentlabs.aleague.studentclub.view.StudentClubView;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;

/**
 * This is controller for Blog Post to handle request.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@SuppressWarnings("serial")
@Controller
@RequestMapping("/studentclub")
public class StudentClubControllerImpl extends AbstractController<StudentClubView> implements StudentClubController
{

	@Autowired
	StudentClubOperation studentClubOperation;
	
	@Override
	public BaseOperation<StudentClubView> getOperation() {
		return studentClubOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(StudentClubView webView) throws Exception {

		
	}

	@Override
	public Response getStudentClub(@RequestBody StudentClubView studentClub) throws InvalidDataException, Exception {
		return studentClubOperation.doDisplayOperation(studentClub.getId());
	}

	@Override
	public Response getAllStudentClubs(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception {
		return studentClubOperation.doDisplayAllOperation(start, end);
	}

	@Override
	public Response saveStudentClub(@ModelAttribute StudentClubView view) throws InvalidDataException, Exception {
		try{
			LoggerService.info(STUDENT_CLUB_CONTROLLER, OperationName.SAVE_STUDENT_CLUB, LogMessage.BEFORE_VALIDATION);
			isValidSaveData(view);
			LoggerService.info(STUDENT_CLUB_CONTROLLER, OperationName.SAVE_STUDENT_CLUB, LogMessage.BEFORE_CALLING_OPERATION);
			return studentClubOperation.doSaveOperation(view);
		}
		finally {
			LoggerService.info(STUDENT_CLUB_CONTROLLER, OperationName.SAVE_STUDENT_CLUB, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response deleteStudentClub(@RequestBody StudentClubView studentClub) throws InvalidDataException, Exception {
		try{
			LoggerService.info(STUDENT_CLUB_CONTROLLER, OperationName.DELETE_STUDENT_CLUB, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(STUDENT_CLUB_CONTROLLER, OperationName.DELETE_STUDENT_CLUB, LogMessage.BEFORE_CALLING_OPERATION);
			return studentClubOperation.doDeleteOperation(studentClub.getId());
		}
		finally {
			LoggerService.info(STUDENT_CLUB_CONTROLLER, OperationName.DELETE_STUDENT_CLUB, LogMessage.REQUEST_COMPLETED);			
		}
	}
}
