
package com.intentlabs.aleague.studentclub.controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.studentclub.view.StudentClubView;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface StudentClubController extends WebController<StudentClubView>
{

	String STUDENT_CLUB_CONTROLLER = "StudentClubController";
	
	@RequestMapping(value = "/savestudentclub.htm", method = RequestMethod.POST)
	@ResponseBody
	Response saveStudentClub(@ModelAttribute StudentClubView view) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getstudentclub.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getStudentClub(@RequestBody StudentClubView studentClub) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/getallstudentclubs.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getAllStudentClubs(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/deletestudentclub.htm", method = RequestMethod.POST)
	@ResponseBody
	Response deleteStudentClub(@RequestBody StudentClubView studentClub) throws InvalidDataException, Exception;
}
