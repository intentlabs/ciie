
package com.intentlabs.aleague.researchresource.controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.researchresource.view.ResearchResourceView;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface ResearchResourceController extends WebController<ResearchResourceView>
{

	String RESEARCH_RESOURCE_CONTROLLER = "ResearchResourceController";
	
	@RequestMapping(value = "/saveresearchresource.htm", method = RequestMethod.POST)
	@ResponseBody
	Response saveResearchResource(@ModelAttribute ResearchResourceView view) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getresearchresource.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getResearchResource(@RequestBody ResearchResourceView researchResource) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/getallresearchresources.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getAllResearchResources() throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/deleteresearchresource.htm", method = RequestMethod.POST)
	@ResponseBody
	Response deleteResearchResource(@RequestBody ResearchResourceView researchResource) throws InvalidDataException, Exception;
}
