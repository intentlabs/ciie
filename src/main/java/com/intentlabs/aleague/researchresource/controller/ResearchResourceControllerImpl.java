
package com.intentlabs.aleague.researchresource.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.researchresource.operation.ResearchResourceOperation;
import com.intentlabs.aleague.researchresource.view.ResearchResourceView;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;

/**
 * This is controller for Blog Post to handle request.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@SuppressWarnings("serial")
@Controller
@RequestMapping("/researchresource")
public class ResearchResourceControllerImpl extends AbstractController<ResearchResourceView> implements ResearchResourceController
{

	@Autowired
	ResearchResourceOperation researchResourceOperation;
	
	@Override
	public BaseOperation<ResearchResourceView> getOperation() {
		return researchResourceOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(ResearchResourceView webView) throws Exception {

		
	}

	@Override
	public Response getResearchResource(@RequestBody ResearchResourceView researchResource) throws InvalidDataException, Exception {
		return researchResourceOperation.doDisplayOperation(researchResource.getId());
	}

	@Override
	public Response getAllResearchResources() throws InvalidDataException, Exception {
		return researchResourceOperation.doDisplayAllOperation();
	}

	@Override
	public Response saveResearchResource(@ModelAttribute ResearchResourceView view) throws InvalidDataException, Exception {
		try{
			LoggerService.info(RESEARCH_RESOURCE_CONTROLLER, OperationName.SAVE_RESEARCH_RESOURCE, LogMessage.BEFORE_VALIDATION);
			isValidSaveData(view);
			LoggerService.info(RESEARCH_RESOURCE_CONTROLLER, OperationName.SAVE_RESEARCH_RESOURCE, LogMessage.BEFORE_CALLING_OPERATION);
			return researchResourceOperation.doSaveOperation(view);
		}
		finally {
			LoggerService.info(RESEARCH_RESOURCE_CONTROLLER, OperationName.SAVE_RESEARCH_RESOURCE, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response deleteResearchResource(@RequestBody ResearchResourceView researchResource) throws InvalidDataException, Exception {
		try{
			LoggerService.info(RESEARCH_RESOURCE_CONTROLLER, OperationName.DELETE_RESEARCH_RESOURCE, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(RESEARCH_RESOURCE_CONTROLLER, OperationName.DELETE_RESEARCH_RESOURCE, LogMessage.BEFORE_CALLING_OPERATION);
			return researchResourceOperation.doDeleteOperation(researchResource.getId());
		}
		finally {
			LoggerService.info(RESEARCH_RESOURCE_CONTROLLER, OperationName.DELETE_RESEARCH_RESOURCE, LogMessage.REQUEST_COMPLETED);			
		}
	}
}
