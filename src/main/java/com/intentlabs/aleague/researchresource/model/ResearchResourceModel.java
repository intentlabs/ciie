
package com.intentlabs.aleague.researchresource.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tblResearchResource")
public class ResearchResourceModel extends ArchiveModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;
	
	@Column(name = "txtTitle")
	private String txtTitle;
	
	@Column(name = "txtAuthors")
	private String txtAuthors;
	
	@Column(name = "txtType")
	private String txtType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datePublishedOn")
	private Date datePublishedOn;
	
	@Column(name = "txtAbstract")
	private String txtAbstract;
	
	@Column(name = "txtResearchResourcePath")
	private String txtResearchResourcePath;
	
	@Column(name = "txtResearchLink")
	private String txtResearchLink;

	/**
	 * @return the txtTitle
	 */
	public String getTxtTitle() {
		return txtTitle;
	}

	/**
	 * @param txtTitle the txtTitle to set
	 */
	public void setTxtTitle(String txtTitle) {
		this.txtTitle = txtTitle;
	}

	/**
	 * @return the txtAuthors
	 */
	public String getTxtAuthors() {
		return txtAuthors;
	}

	/**
	 * @param txtAuthors the txtAuthors to set
	 */
	public void setTxtAuthors(String txtAuthors) {
		this.txtAuthors = txtAuthors;
	}

	/**
	 * @return the txtType
	 */
	public String getTxtType() {
		return txtType;
	}

	/**
	 * @param txtType the txtType to set
	 */
	public void setTxtType(String txtType) {
		this.txtType = txtType;
	}

	/**
	 * @return the datePublishedOn
	 */
	public Date getDatePublishedOn() {
		return datePublishedOn;
	}

	/**
	 * @param datePublishedOn the datePublishedOn to set
	 */
	public void setDatePublishedOn(Date datePublishedOn) {
		this.datePublishedOn = datePublishedOn;
	}

	/**
	 * @return the txtAbstract
	 */
	public String getTxtAbstract() {
		return txtAbstract;
	}

	/**
	 * @param txtAbstract the txtAbstract to set
	 */
	public void setTxtAbstract(String txtAbstract) {
		this.txtAbstract = txtAbstract;
	}

	/**
	 * @return the txtResearchResourcePath
	 */
	public String getTxtResearchResourcePath() {
		return txtResearchResourcePath;
	}

	/**
	 * @param txtResearchResourcePath the txtResearchResourcePath to set
	 */
	public void setTxtResearchResourcePath(String txtResearchResourcePath) {
		this.txtResearchResourcePath = txtResearchResourcePath;
	}

	/**
	 * @return the txtResearchLink
	 */
	public String getTxtResearchLink() {
		return txtResearchLink;
	}

	/**
	 * @param txtResearchLink the txtResearchLink to set
	 */
	public void setTxtResearchLink(String txtResearchLink) {
		this.txtResearchLink = txtResearchLink;
	}
	
	
}
