
package com.intentlabs.aleague.researchresource.view;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.common.view.ArciveView;

/**
 * This is View used to get data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class ResearchResourceView extends ArciveView {


	private static final long serialVersionUID = 8692674149531174388L;
	
	private String title;
	
	private String authors;
	
	private String type;
	
	private String publishedOn;
	
	private String researchResourceAbstract;
	
	private String researchResourcePath;
	
	private String txtResearchLink;
	
	private MultipartFile file;
	
	private Boolean canEditDelete;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the authors
	 */
	public String getAuthors() {
		return authors;
	}

	/**
	 * @param authors the authors to set
	 */
	public void setAuthors(String authors) {
		this.authors = authors;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the publishedOn
	 */
	public String getPublishedOn() {
		return publishedOn;
	}

	/**
	 * @param publishedOn the publishedOn to set
	 */
	public void setPublishedOn(String publishedOn) {
		this.publishedOn = publishedOn;
	}

	/**
	 * @return the researchResourceAbstract
	 */
	public String getResearchResourceAbstract() {
		return researchResourceAbstract;
	}

	/**
	 * @param researchResourceAbstract the researchResourceAbstract to set
	 */
	public void setResearchResourceAbstract(String researchResourceAbstract) {
		this.researchResourceAbstract = researchResourceAbstract;
	}

	/**
	 * @return the researchResourcePath
	 */
	public String getResearchResourcePath() {
		return researchResourcePath;
	}

	/**
	 * @param researchResourcePath the researchResourcePath to set
	 */
	public void setResearchResourcePath(String researchResourcePath) {
		this.researchResourcePath = researchResourcePath;
	}
	
	/**
	 * @return the txtResearchLink
	 */
	public String getTxtResearchLink() {
		return txtResearchLink;
	}

	/**
	 * @param txtResearchLink the txtResearchLink to set
	 */
	public void setTxtResearchLink(String txtResearchLink) {
		this.txtResearchLink = txtResearchLink;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * @return the canEditDelete
	 */
	public Boolean getCanEditDelete() {
		return canEditDelete;
	}

	/**
	 * @param canEditDelete the canEditDelete to set
	 */
	public void setCanEditDelete(Boolean canEditDelete) {
		this.canEditDelete = canEditDelete;
	}
}