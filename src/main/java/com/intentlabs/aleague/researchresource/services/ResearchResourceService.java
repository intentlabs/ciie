
package com.intentlabs.aleague.researchresource.services;

import java.util.List;

import com.intentlabs.aleague.researchresource.model.ResearchResourceModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface ResearchResourceService extends BaseService<ResearchResourceModel> {
	List<ResearchResourceModel> getAllResearchResources();
	
	ResearchResourceModel getById(Long id);
}
