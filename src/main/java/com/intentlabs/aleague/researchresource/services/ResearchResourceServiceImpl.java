
package com.intentlabs.aleague.researchresource.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.researchresource.model.ResearchResourceModel;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class ResearchResourceServiceImpl extends AbstractService<ResearchResourceModel> implements ResearchResourceService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<ResearchResourceModel> getModelClass() {
		return ResearchResourceModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<ResearchResourceModel> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(ResearchResourceModel model, Criteria commonCriteria) {
		return commonCriteria;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ResearchResourceModel> getAllResearchResources() {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
		criteria.addOrder(Order.desc("id"));
		return criteria.list();
	}

	@Override
	public ResearchResourceModel getById(Long id) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);		
		criteria.add(Restrictions.eq("id", id));
		return (ResearchResourceModel) criteria.uniqueResult();
	}
}