
package com.intentlabs.aleague.researchresource.operation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.researchresource.model.ResearchResourceModel;
import com.intentlabs.aleague.researchresource.services.ResearchResourceService;
import com.intentlabs.aleague.researchresource.view.ResearchResourceView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.Constant;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.View;

/**
 * This is operation for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Component(value = "researchResourceOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class ResearchResourceOperationImpl extends AbstractOperation<ResearchResourceModel, ResearchResourceView> implements ResearchResourceOperation {

	private static final long serialVersionUID = 741391010890205612L;

	@Autowired
	private ResearchResourceService researchResourceService;

	@Override
	public BaseService getService() {
		return researchResourceService;
	}

	@Override
	protected ResearchResourceModel getNewModel(ResearchResourceView view) {
		ResearchResourceModel model = new ResearchResourceModel();
		model.setCreateDate(DateUtility.getCurrentDate());
		return model;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		return null;
	}

	@Override
	protected void checkInactive(ResearchResourceModel model) throws Exception {
	}

	@Override
	public ResearchResourceModel toModel(ResearchResourceModel researchResourceModel, ResearchResourceView researchResourceView) throws Exception {
		if (researchResourceView.getId() == null || researchResourceView.getId().longValue() <= 0) {
			researchResourceModel.setCreateBy(Auditor.getAuditor());
			researchResourceModel.setActivationStatus(ActiveInActive.ACTIVE);
			researchResourceModel.setActivationDate(DateUtility.getCurrentDate());
			researchResourceModel.setEnumArchive("1");
		} else {
			researchResourceModel.setId(researchResourceView.getId());
		}

		researchResourceModel.setUpdateBy(Auditor.getAuditor());
		researchResourceModel.setUpdateDate(DateUtility.getCurrentDate());
        if(researchResourceView.getTxtResearchLink() !=null){
        	if(researchResourceView.getTxtResearchLink().startsWith("https:")|| researchResourceView.getTxtResearchLink().startsWith("http:")){
        		researchResourceModel.setTxtResearchLink(researchResourceView.getTxtResearchLink());
        	}else{
        		researchResourceModel.setTxtResearchLink(Constant.HTTP_URL + researchResourceView.getTxtResearchLink());
        	}
        	
        }
		researchResourceModel.setTxtTitle(researchResourceView.getTitle());
		researchResourceModel.setTxtAuthors(researchResourceView.getAuthors());
		researchResourceModel.setTxtType(researchResourceView.getType());
		researchResourceModel.setDatePublishedOn(DateUtility.toDate(researchResourceView.getPublishedOn(), new SimpleDateFormat("dd-MM-yyyy")));
		researchResourceModel.setTxtAbstract(researchResourceView.getResearchResourceAbstract());
		
		if(researchResourceView.getFile() != null){
			researchResourceModel.setTxtResearchResourcePath(FileUtility.storeFile(researchResourceView.getFile(), "researchresource"));
		}else{
			researchResourceModel.setTxtResearchResourcePath("");
		}

		return researchResourceModel;
	}

	@Override
	public ResearchResourceView fromModel(ResearchResourceModel researchResourceModel) {
		ResearchResourceView researchResourceView = new ResearchResourceView();
		researchResourceView.setId(researchResourceModel.getId());
		
		researchResourceView.setTitle(researchResourceModel.getTxtTitle());
		researchResourceView.setAuthors(researchResourceModel.getTxtAuthors());
		researchResourceView.setType(researchResourceModel.getTxtType());
		if(researchResourceModel.getTxtResearchLink() !=null){
			researchResourceView.setTxtResearchLink(researchResourceModel.getTxtResearchLink());
		}
		researchResourceView.setPublishedOn(DateUtility.toDateTimeString(researchResourceModel.getDatePublishedOn(), new SimpleDateFormat("dd-MM-yyyy")));
		researchResourceView.setResearchResourceAbstract(researchResourceModel.getTxtAbstract());
		
		if(!"".equals(researchResourceModel.getTxtResearchResourcePath())){
			researchResourceView.setResearchResourcePath(FileUtility.setPath(researchResourceModel.getTxtResearchResourcePath()));	
		}else{
			researchResourceView.setResearchResourcePath("");
		}		
		
		return researchResourceView;
	}

	@Override
	public Response doSaveOperation(ResearchResourceView view) throws Exception {
		ResearchResourceModel model = null;

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = getNewModel(view);
		} else {
			model = researchResourceService.getById(view.getId());
		}

		model = toModel(model, view);

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = researchResourceService.create(model);
		} else {
			researchResourceService.update(model);
		}

		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Research Resource has been saved.");
	}

	@Override
	public Response doDisplayOperation(Long id) {
		ResearchResourceModel model = researchResourceService.load(id);
		ResearchResourceView view = fromModel(model);
		Boolean canEditDelete = false;			
		if(Auditor.getAuditor().getId().longValue() == model.getCreateBy().getId().longValue()){
			canEditDelete = true;
		}			
		view.setCanEditDelete(canEditDelete);
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_RESEARCH_RESOURCE, view);
	}

	@Override
	public Response doDisplayAllOperation() {
		List<ResearchResourceModel> listResearchResources = researchResourceService.getAllResearchResources();

		List<View> listResearchResourceViews = new ArrayList<>();
		
		for (ResearchResourceModel model : listResearchResources) {	
			ResearchResourceView view = fromModel(model);
			Boolean canEditDelete = false;			
			if(Auditor.getAuditor().getId().longValue() == model.getCreateBy().getId().longValue()){
				canEditDelete = true;
			}			
			view.setCanEditDelete(canEditDelete);
			
			listResearchResourceViews.add(view);
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_RESEARCH_RESOURCES,
				listResearchResourceViews.size(), listResearchResourceViews);
	}
		
	@Override
	public Response doDeleteOperation(Long researchResourceId){
		ResearchResourceModel model = researchResourceService.getById(researchResourceId);
		model.setActivationStatus(ActiveInActive.INACTIVE);
		model.setActivationDate(DateUtility.getCurrentDate());
		model.setActivationChangeBy(Auditor.getAuditor());
		researchResourceService.update(model);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Research Resource has been deleted.");
	}
}
