
package com.intentlabs.aleague.researchresource.operation;

import com.intentlabs.aleague.researchresource.view.ResearchResourceView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is operation for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface ResearchResourceOperation extends BaseOperation<ResearchResourceView> {

	String RESEARCH_RESOURCE_OPERATION = "researchResourceOperation";
	
	Response doDisplayOperation(Long id);

	Response doDisplayAllOperation();
	
	Response doDeleteOperation(Long researchResourceId);
}
