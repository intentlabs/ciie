package com.intentlabs.aleague.filter;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.usersession.service.SessionService;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.util.HTTPUtil;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.util.PasswordUtil;
import com.intentlabs.common.util.WebUtil;


/**
 * This is context root request filter. Root context request coming from any client will pass through this filter. 
 * It validates user session inside do filter method.
 * @version 1.0
 *
 */

public class ContextRootFilter implements Filter {
	private static final String CONTEXT_ROOT_FILTER = "ContextRootFilter";
	private ApplicationContext applicationContext;
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		HttpSession session = request.getSession(true);
		LoggerService.info(CONTEXT_ROOT_FILTER, request.getRequestURI(), "session id : "+session.getId());
		
		if(request.getRequestURI().endsWith("/")){
			String salt = (String) WebUtil.getFromCurrentSession("salt");
			if(salt == null){
				WebUtil.setSaltToCurrentSession("salt",PasswordUtil.getDynamicSalt());
			}
		}
		
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Expose-Headers", "Content-Type,usersession,sessionId");
        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
          
            // CORS "pre-flight" request
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            // response.addHeader("Access-Control-Allow-Headers", "Authorization");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type,usersession,sessionId");
            response.addHeader("Access-Control-Max-Age", "1");
            
            return;
        }
		
		if (validateHttpRequest(request)) {
			chain.doFilter(req, res);
			return;
		}

		String sessionId = HTTPUtil.extractAuthTokenFromRequest(request);

		LoggerService.info(CONTEXT_ROOT_FILTER, request.getRequestURI(), "session id : " + sessionId);

		User user = getCurrentUser(sessionId, request);

		if (user == null) {
			LoggerService.error(CONTEXT_ROOT_FILTER, request.getRequestURI(),
					"No user binded with session : " + sessionId);
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		
		//User user = (User)WebUtil.getFromCurrentSession(Constant.USER_SESSION_KEY); 
		/*if(!checkValidSessionUser(user,session.getId())){
	        WebUtil.invalidatSession();
	        response.sendRedirect(HttpRequestParam.getContextPath()+"error");
			return;
		}*/
		if (user != null) {
			Auditor.setAuditor(user);
		}
		chain.doFilter(req, res);
	}
	
	@Override
	public void init(FilterConfig config) throws ServletException {
		ImageIO.scanForPlugins();
		this.setApplicationContext(WebApplicationContextUtils.getWebApplicationContext(config.getServletContext()));	
	}

	@Override
	public void destroy() {
		LoggerService.info(CONTEXT_ROOT_FILTER, "Tomcat shutdown", "Destroy portal authorization filter");
	}
	
	private boolean checkValidSessionUser(User user,String sessionId) {
		try {
		/*	UserOperation userOperation = (UserOperation) applicationContext.getBean("userOperation");
			CommonResponse commonResponse = (CommonResponse) userOperation.doValidateUserSession(user.getId(), sessionId);
			if(ResponseCode.SUCCESSFUL.getCode() == commonResponse.getResponseCode()){
				return true;
		}
			return false;*/
		User usersesssion = new User();
			return usersesssion!=null ? true : false;
		} catch (Exception e) {
			LoggerService.error(CONTEXT_ROOT_FILTER,OperationName.USER_SESSION ,"error in checkValidSessionUser");
			LoggerService.exception(e);
			return false;
		}
				
	}
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	
	/**
	 * It is used to validate http servlet request.
	 * @request object of HttpServletRequest
	 * @return boolean
	 */
	private boolean validateHttpRequest(HttpServletRequest request){
		return request.getRequestURI().endsWith("/") ||
				request.getRequestURI().endsWith("user/login.htm") ||
				request.getRequestURI().endsWith("forgotpassword.htm") ||
				request.getRequestURI().endsWith("updatenewPassword.htm") ||
				request.getRequestURI().endsWith("logout.htm") ||
				request.getRequestURI().endsWith("getAllInstitutes.htm") ||
				request.getRequestURI().endsWith("user/checkEmailid.htm") ||
				request.getRequestURI().endsWith("role/display.htm") ||
				request.getRequestURI().endsWith("role/getAllRoles.htm") ||
				request.getRequestURI().endsWith("user/verification.htm") ||
				request.getRequestURI().endsWith("loggedinUser.htm") ||
				request.getRequestURI().endsWith("user/save.htm") ||
				request.getRequestURI().endsWith(".css") || 
				request.getRequestURI().contains("attachments") || 
				request.getRequestURI().endsWith(".tiff")||
				request.getRequestURI().endsWith(".json")||
				request.getRequestURI().endsWith(".woff")||
				request.getRequestURI().endsWith("error")|| 
				request.getRequestURI().endsWith(".js");
	}
	
	
	private User getCurrentUser(String userSessionId, HttpServletRequest httpRequest) {
		/*UserSessionServiceImpl userSessionService = (UserSessionServiceImpl) ApplicationContextProvider
				.getApplicationContext().getBean("userSessionTransactionalService");*/
		
		SessionService userOperation = (SessionService) applicationContext.getBean("sessionServiceImpl");
		
		if (!StringUtils.isBlank(userSessionId)) {
			User currentUser = userOperation.getUserBySessionId(userSessionId);
			
			return currentUser;

		}
		return null;
	}
}