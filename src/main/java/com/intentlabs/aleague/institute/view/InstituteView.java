package com.intentlabs.aleague.institute.view;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.aleague.studentclub.view.StudentClubView;
import com.intentlabs.aleague.user.view.UserView;
import com.intentlabs.common.view.ArciveView;

public class InstituteView extends ArciveView  {

	
	private static final long serialVersionUID = 629301577639855950L;
	
	
	private String txtName;
	
	private String txtAddress;
	
	private String txtDescription;
	
	private String txtFbLink;
	
	private String txtTwLink;
	
	private String txtWebsiteLink;
	
	private MultipartFile file;
		
	private String txtDomain;
	
	private String logoPath;
	
	private List<UserView> userList;
	
	private List<StudentClubView> studentClubList;
	
	private Boolean isstudentcoordinator;
	
	private int userCount;
	
	private String studentCo;
	
	private String facultyCo;

	public String getTxtName() {
		return txtName;
	}

	public void setTxtName(String txtName) {
		this.txtName = txtName;
	}

	public String getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(String txtAddress) {
		this.txtAddress = txtAddress;
	}

	public String getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	public String getTxtFbLink() {
		return txtFbLink;
	}

	public void setTxtFbLink(String txtFbLink) {
		this.txtFbLink = txtFbLink;
	}

	public String getTxtTwLink() {
		return txtTwLink;
	}

	public void setTxtTwLink(String txtTwLink) {
		this.txtTwLink = txtTwLink;
	}

	public String getTxtWebsiteLink() {
		return txtWebsiteLink;
	}

	public void setTxtWebsiteLink(String txtWebsiteLink) {
		this.txtWebsiteLink = txtWebsiteLink;
	}

	

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getTxtDomain() {
		return txtDomain;
	}

	public void setTxtDomain(String txtDomain) {
		this.txtDomain = txtDomain;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public List<UserView> getUserList() {
		return userList;
	}

	public void setUserList(List<UserView> userList) {
		this.userList = userList;
	}

	/**
	 * @return the studentClubList
	 */
	public List<StudentClubView> getStudentClubList() {
		return studentClubList;
	}

	/**
	 * @param studentClubList the studentClubList to set
	 */
	public void setStudentClubList(List<StudentClubView> studentClubList) {
		this.studentClubList = studentClubList;
	}

	/**
	 * @return the isstudentcoordinator
	 */
	public Boolean getIsstudentcoordinator() {
		return isstudentcoordinator;
	}

	/**
	 * @param isstudentcoordinator the isstudentcoordinator to set
	 */
	public void setIsstudentcoordinator(Boolean isstudentcoordinator) {
		this.isstudentcoordinator = isstudentcoordinator;
	}

	public int getUserCount() {
		return userCount;
	}

	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}

	public String getStudentCo() {
		return studentCo;
	}

	public void setStudentCo(String studentCo) {
		this.studentCo = studentCo;
	}

	/**
	 * @return the facultyCo
	 */
	public String getFacultyCo() {
		return facultyCo;
	}

	/**
	 * @param facultyCo the facultyCo to set
	 */
	public void setFacultyCo(String facultyCo) {
		this.facultyCo = facultyCo;
	}
}
