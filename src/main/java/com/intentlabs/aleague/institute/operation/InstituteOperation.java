
package com.intentlabs.aleague.institute.operation;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.intentlabs.aleague.institute.view.InstituteView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is operation for Institute.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 17/07/2017
 */

public interface InstituteOperation extends BaseOperation<InstituteView>
{

	String INSTITUTE_OPERATION = "instituteOperation";

	Response saveInstitute(InstituteView instituteView, HttpServletRequest request);

	Response getAllInstitutes();

	Response doDeleteInstituteOperation(Long id);
	
}
