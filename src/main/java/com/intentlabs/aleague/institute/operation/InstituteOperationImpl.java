
package com.intentlabs.aleague.institute.operation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.institute.services.InstituteService;
import com.intentlabs.aleague.institute.view.InstituteView;
import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.aleague.rights.Role;
import com.intentlabs.aleague.role.services.RoleService;
import com.intentlabs.aleague.studentclub.model.StudentClubModel;
import com.intentlabs.aleague.studentclub.services.StudentClubService;
import com.intentlabs.aleague.studentclub.view.StudentClubView;
import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.user.model.UserBasic;
import com.intentlabs.aleague.user.services.UserBasicService;
import com.intentlabs.aleague.user.services.UserService;
import com.intentlabs.aleague.user.view.UserView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.EnumArchive;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultResponse;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.view.KeyValueView;

/**
 * This is operation for Event to handle request.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 17/07/2017
 */

@Component(value = "instituteOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class InstituteOperationImpl extends AbstractOperation<InstituteModel, InstituteView> implements InstituteOperation
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2597763074752764256L;
	
	@Autowired
	InstituteService instituteService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserBasicService userBasicService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	StudentClubService studentClubService;

	@Override
	protected InstituteModel getNewModel(InstituteView view) {
		return new InstituteModel();
	}
	
	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		User user = userService.get(Auditor.getAuditor().getId());
		if(user.getFkRoleId().getId() != Long.valueOf(1l)){
			if(user.getFkInstituteId().getId() != id){
				return CommonResponse.create(ResponseCode.UNAUTHORIZE_USER.getCode(), ResponseCode.UNAUTHORIZE_USER.getMessage());
			}	
		}
		
		if(user.getFkRoleId().getId() != Long.valueOf(5l) &&
				user.getFkRoleId().getId() != Long.valueOf(6l) && user.getFkRoleId().getId() != Long.valueOf(1l)){
			return CommonResponse.create(ResponseCode.UNAUTHORIZE_USER.getCode(), ResponseCode.UNAUTHORIZE_USER.getMessage());
		}
	 InstituteView instituteView = fromModel(instituteService.get(id));
	 InstituteModel insti  = instituteService.get(id);
	 insti  = toModel(insti, instituteView);
	 instituteService.update(insti);
		
		//InstituteView instituteView = fromModel(instituteService.get(id));
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage(), instituteView);
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		InstituteModel instituteModel=instituteService.get(id);
		InstituteView instituteView=fromModel(instituteModel);
		List<Long> facultyList = new ArrayList<>();
		facultyList.add(3l);
		facultyList.add(5l);
		List<UserBasic> userBasicList = userBasicService.fetchInstituteFaculty(facultyList, instituteModel.getId(), 0, 10);
		List<UserView> userViewList = new ArrayList<>();
		for(UserBasic userBasic : userBasicList){
			UserView userView = new UserView();
			userView.setTxtFirstName(userBasic.getTxtFirstName());
			userView.setTxtLastName(userBasic.getTxtLastName());
			
			if(userBasic.getTxtProfilePicPath() != null && !"".equals(userBasic.getTxtProfilePicPath())){
				userView.setTxtProfilePicPath(FileUtility.setPath(userBasic.getTxtProfilePicPath()));	
			}else{
				userView.setTxtProfilePicPath(FileUtility.setPath("defaultuserprofilepic.PNG"));
			}
			
			if(userBasic.getTxtEmail() != null && !"".equals(userBasic.getTxtEmail())){
				userView.setEmailId(userBasic.getTxtEmail());
			}else{
				userView.setEmailId("");
			}
			userView.setAbout(userBasic.getAbout());
			userView.setId(userBasic.getId());
			userView.setTxtLinkdinLink(userBasic.getTxtLinkdinLink());
			userViewList.add(userView);
		}
				
		instituteView.setUserList(userViewList);
		
		List<StudentClubModel> listStudentClubs = studentClubService.getAllStudentClubs(id, null, null);
		List<StudentClubView> studentClubs = new ArrayList<>();
		
		for(StudentClubModel studentClubInstance : listStudentClubs){
			StudentClubView studentClub = new StudentClubView();
			studentClub.setName(studentClubInstance.getTxtName());
			studentClub.setDescription(studentClubInstance.getTxtDescription());
			studentClub.setId(studentClubInstance.getId());
			studentClub.setSocialLink(studentClubInstance.getTxtSocialLink());
			if(studentClubInstance.getTxtLogoPath() != null && !"".equals(studentClubInstance.getTxtLogoPath())){
				studentClub.setLogoPath(FileUtility.setPath(studentClubInstance.getTxtLogoPath()));	
			}else{
				studentClub.setLogoPath(FileUtility.setPath("defaultinstitutepic.PNG"));
			}			
			
			studentClubs.add(studentClub);
		}
		
		instituteView.setStudentClubList(studentClubs);
		
		if(Auditor.getAuditor().getFkInstituteId() != null && Auditor.getAuditor().getFkInstituteId().getId().longValue() == id && Auditor.getAuditor().getFkRoleId().getId().longValue() == 6l){
			instituteView.setIsstudentcoordinator(true);
		}else{
			instituteView.setIsstudentcoordinator(false);
		}
		
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Get Institutebyid completed", instituteView);
	}

	@Override
	protected void checkInactive(InstituteModel model) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public BaseService getService() {
		return instituteService;
	}
	
	@Override
	public InstituteModel toModel(InstituteModel model, InstituteView view) throws Exception {
		model.setId(view.getId());
		model.setTxtName(view.getTxtName());
		model.setTxtAddress(view.getTxtAddress());
		model.setTxtDescription(view.getTxtDescription());
		model.setTxtFbLink(view.getTxtFbLink());
		if (view.getFile() != null) {
			model.setTxtLogoPath(FileUtility.storeFile(view.getFile(), "institute"));
		}
		model.setTxtWebsiteLink(view.getTxtWebsiteLink());
		model.setTxtDomain(view.getTxtDomain());		
		return model;
	}

	@Override
	public InstituteView fromModel(InstituteModel model) {
		InstituteView view=new InstituteView();
		view.setId(model.getId());
		view.setVersion(model.getLockVersion());
		view.setTxtName(model.getTxtName());
		view.setTxtAddress(model.getTxtAddress());
		view.setTxtDescription(model.getTxtDescription());
		view.setTxtFbLink(model.getTxtFbLink());
		view.setTxtWebsiteLink(model.getTxtWebsiteLink());
		view.setTxtDomain(model.getTxtDomain());
		if(model.getTxtLogoPath() == null){
			view.setLogoPath(null);
		}else{
			view.setLogoPath(FileUtility.setPath(model.getTxtLogoPath()));	
		}
		/*view.setUserCount(userService.userCount(model.getId()));
		List<User> studentCoList = userService.getStudentCo(model.getId());
		List<User> facultyCoList = userService.getFacultyCo(model.getId());
		if(! studentCoList.isEmpty()){
			String studentCo = null;
			for(User user : studentCoList){
				if(studentCo == null){
					studentCo = user.getTxtFirstName() + " "+user.getTxtLastName();
				}else{
					studentCo = studentCo + ", " + user.getTxtFirstName() + " "+user.getTxtLastName();
				}
				view.setStudentCo(studentCo);
			}	
		}else{
			view.setStudentCo(null);
		}
		
		if(! facultyCoList.isEmpty()){
			String facultyCo = null;
			for(User user : facultyCoList){
				if(facultyCo == null){
					facultyCo = user.getTxtFirstName() + " "+user.getTxtLastName();
				}else{
					facultyCo = facultyCo + ", " + user.getTxtFirstName() + " "+user.getTxtLastName();
				}
				view.setFacultyCo(facultyCo);
			}	
		}else{
			view.setFacultyCo(null);
		}*/
		return view;
	}

	@Override
	public Response saveInstitute(InstituteView instituteView,HttpServletRequest request) {
		Role role = roleService.getRoleByName("Super Admin");
		if(Auditor.getAuditor() == null){
			return CommonResponse.create(ResponseCode.INVALID_REQUEST.getCode(), ResponseCode.INVALID_REQUEST.getMessage());
		}
		User user = userService.get(Auditor.getAuditor().getId());
		if(role.getId() != user.getFkRoleId().getId()){
			return CommonResponse.create(ResponseCode.UNAUTHORIZE_USER.getCode(), ResponseCode.UNAUTHORIZE_USER.getMessage());
		}
		
		InstituteModel instituteModel;
		try {
			instituteModel = toModel(getNewModel(instituteView), instituteView);
			instituteModel.setActivationStatus(ActiveInActive.ACTIVE);
			instituteModel.setEnumArchive(String.valueOf(EnumArchive.ARCHIVE.getId()));
		} catch (Exception e) {
			LoggerService.exception(e);
			return CommonResponse.create(ResponseCode.INTERNAL_SERVER_ERROR.getCode(), ResponseCode.INTERNAL_SERVER_ERROR.getMessage());
		}
		instituteService.create(instituteModel);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), ResponseCode.SUCCESSFUL.getMessage());
	}	

	@Override
	public Response getAllInstitutes() {
		List<InstituteModel> insttues = instituteService.fatchForDropDown();
		List<KeyValueView> intituteList = new ArrayList<>();
		for (InstituteModel instituteModel : insttues) {
			intituteList.add(KeyValueView.create(instituteModel.getId(), instituteModel.getTxtName()));
		}
	
		return PageResultResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Get All Institute is Successfull", intituteList.size(), intituteList);
	}

	@Override
	public Response doDeleteInstituteOperation(Long id) {
		// TODO Auto-generated method stub
		return null;
	}


}
