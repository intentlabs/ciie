
package com.intentlabs.aleague.institute.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.institute.operation.InstituteOperation;
import com.intentlabs.aleague.institute.view.InstituteView;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;

/**
 * This is controller for Institute to handle request.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 17/07/2017
 */

@SuppressWarnings("serial")
@Controller
@RequestMapping("/institute")
public class InstituteControllerImpl extends AbstractController<InstituteView> implements InstituteController
{
	
	@Autowired
	InstituteOperation instituteOperation;

	@Override
	public BaseOperation<InstituteView> getOperation() {
		return instituteOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {
		return instituteOperation.doViewOperation(id);
	}

	@Override
	public void isValidSaveData(InstituteView webView) throws Exception {

		
	}

	@Override
	public Response saveInstitute(@ModelAttribute InstituteView instituteView,HttpServletRequest request) throws InvalidDataException, Exception {
		return instituteOperation.saveInstitute(instituteView,request);
	}

	@Override
	public Response getAllInstitutes() throws InvalidDataException, Exception {
		try{
			LoggerService.info(INSTITUTE_CONTROLLER, OperationName.GET_ALL_INSTITUTES, LogMessage.BEFORE_CALLING_OPERATION);
			
			return instituteOperation.getAllInstitutes();
		}
		finally {
			LoggerService.info(INSTITUTE_CONTROLLER, OperationName.GET_ALL_INSTITUTES, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response getInstituteDetailsId(@RequestBody InstituteView view) throws InvalidDataException, Exception {
		return instituteOperation.doViewOperation(view.getId());
	}

	@Override
	public Response deleteInstituteDetailsId(Long id) throws InvalidDataException, Exception {
		return instituteOperation.doDeleteInstituteOperation(id);
	}

	@Override
	public Response editInstituteDetailsId(@ModelAttribute InstituteView view) throws InvalidDataException, Exception {
		return instituteOperation.doEditOpeartion(view.getId());
	}

	@Override
	public Response updateInstitiue(@ModelAttribute InstituteView view) throws Exception {
		return instituteOperation.doUpdateOperation(view);
	}


}
