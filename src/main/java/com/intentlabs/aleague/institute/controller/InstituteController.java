
package com.intentlabs.aleague.institute.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.institute.view.InstituteView;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Institute.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 17/07/2017
 */

public interface InstituteController extends WebController<InstituteView>
{

	String INSTITUTE_CONTROLLER = "InstituteController";
	
	@RequestMapping(value = "/saveInstitute.htm", method = RequestMethod.POST)
	@ResponseBody
	public Response saveInstitute(@ModelAttribute InstituteView instituteView,HttpServletRequest request) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getAllInstitutes.htm", method = RequestMethod.GET)
	@ResponseBody
	Response getAllInstitutes() throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getInstituteDetailsId.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getInstituteDetailsId(@RequestBody InstituteView view) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/deleteInstituteDetailsId.htm", method = RequestMethod.GET)
	@ResponseBody
	Response deleteInstituteDetailsId(@RequestParam Long id) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/editInstituteDetailsId.htm", method = RequestMethod.POST)
	@ResponseBody
	Response editInstituteDetailsId(@RequestBody InstituteView view) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/update.htm", method = RequestMethod.POST)
	@ResponseBody
	public Response update(@ModelAttribute InstituteView view) throws Exception;
	
	@RequestMapping(value = "/updateInstitute.htm", method = RequestMethod.POST)
	@ResponseBody
	public Response updateInstitiue(@ModelAttribute InstituteView view) throws Exception;
	
}
