/**
 * 
 */
package com.intentlabs.aleague.institute.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.service.AbstractService;

/**
 * @author Dhruvang
 *
 */
public class InstituteServiceImpl extends AbstractService<InstituteModel> implements InstituteService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2091270089773832112L;

	@Override
	public Class<InstituteModel> getModelClass() {
		return InstituteModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<InstituteModel> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		if(Auditor.getAuditor().getFkInstituteId() != null){
			criteria.add(Restrictions.eq("id", Auditor.getAuditor().getFkInstituteId().getId()));	
		}
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(InstituteModel model, Criteria commonCriteria) {
		return commonCriteria;
	}

	@Override
	public List<InstituteModel> fatchForDropDown() {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria.list();
	}

	@Override
	public InstituteModel checkDomainName(String txtDomain) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		//criteria.add(Restrictions.eq("txtDomain", txtDomain));
		criteria.add(Restrictions.like("txtDomain", txtDomain,MatchMode.ANYWHERE));
		return (InstituteModel) criteria.uniqueResult();
	}

}
