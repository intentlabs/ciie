package com.intentlabs.aleague.institute.services;

import java.util.List;

import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.common.service.BaseService;

public interface InstituteService extends BaseService<InstituteModel> {
	
	List<InstituteModel> fatchForDropDown();
	InstituteModel checkDomainName(String txtDomain);

}
