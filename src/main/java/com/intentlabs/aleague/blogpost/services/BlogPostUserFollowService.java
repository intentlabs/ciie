
package com.intentlabs.aleague.blogpost.services;

import java.util.List;

import com.intentlabs.aleague.blogpost.model.BlogPostUserFollowModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface BlogPostUserFollowService extends BaseService<BlogPostUserFollowModel> {
	BlogPostUserFollowModel getByBlogPostAndUser(Long id);
	
	List<BlogPostUserFollowModel> getAllByUser();
}
