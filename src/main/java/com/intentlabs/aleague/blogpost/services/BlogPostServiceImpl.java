
package com.intentlabs.aleague.blogpost.services;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.json.JSONArray;

import com.intentlabs.aleague.blogpost.model.BlogPostModel;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.service.AbstractService;
import com.intentlabs.common.util.OperationName;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class BlogPostServiceImpl extends AbstractService<BlogPostModel> implements BlogPostService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<BlogPostModel> getModelClass() {
		return BlogPostModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<BlogPostModel> modelClass) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		return criteria;
	}

	@Override
	public Criteria setSearchCriteria(BlogPostModel model, Criteria commonCriteria) {
		return commonCriteria;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BlogPostModel> getAllBlogPosts(String tags, String ordertype) {
		if(tags != null && !"".equals(tags) && !"[]".equals(tags)){
			Criteria criteriaBlogIds = getSession().createCriteria(getModelClass());
			criteriaBlogIds.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			criteriaBlogIds.createAlias("setBlogPostTagModel", "blogPostTags", JoinType.LEFT_OUTER_JOIN);
			criteriaBlogIds.createAlias("blogPostTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);		
			criteriaBlogIds.add(Restrictions.eq("blogPostTags.activationStatus", Long.valueOf("1")));
			criteriaBlogIds.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
			
			try{
				JSONArray jsonArray = new JSONArray(tags);
				List<Long> listTagids = new ArrayList<>();
				for(Integer i = 0; i < jsonArray.length(); i++){
					listTagids.add(jsonArray.getLong(i));
				}
						
				criteriaBlogIds.add(Restrictions.in("tags.id", listTagids));
			}catch (Exception e) {
				LoggerService.exception(e);
			}			
			
			criteriaBlogIds.setProjection(Projections.property("id"));
			
			List<Long> listBlogPosts = criteriaBlogIds.list();
			
			if(!listBlogPosts.isEmpty()){
				Criteria criteria = getSession().createCriteria(getModelClass());
				criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
				criteria.createAlias("setBlogPostTagModel", "blogPostTags", JoinType.LEFT_OUTER_JOIN);
				criteria.createAlias("blogPostTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);	
				criteria.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
				
				if(ordertype != null && !"".equals(ordertype)){
					criteria.addOrder(Order.desc(ordertype));
				}

				criteria.addOrder(Order.desc("id"));				
				criteria.add(Restrictions.in("id", listBlogPosts));				
				return criteria.list();
			}else{
				return new ArrayList<BlogPostModel>();
			}
		}else{
			Criteria criteria = getSession().createCriteria(getModelClass());
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			criteria.createAlias("setBlogPostTagModel", "blogPostTags", JoinType.LEFT_OUTER_JOIN);
			criteria.add(Restrictions.eq("activationStatus", ActiveInActive.ACTIVE.getId()));
			criteria.createAlias("blogPostTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);		
			
			if(ordertype != null && !"".equals(ordertype)){
				criteria.addOrder(Order.desc(ordertype));
			}

			criteria.addOrder(Order.desc("id"));			
			return criteria.list();
		}
	}

	@Override
	public BlogPostModel getById(Long id) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("setBlogPostTagModel", "blogPostTags", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("blogPostTags.tag", "tags", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("id", id));		
		return (BlogPostModel) criteria.uniqueResult();
	}
}