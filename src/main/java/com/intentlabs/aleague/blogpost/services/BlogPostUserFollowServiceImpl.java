
package com.intentlabs.aleague.blogpost.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.intentlabs.aleague.blogpost.model.BlogPostUserFollowModel;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class BlogPostUserFollowServiceImpl extends AbstractService<BlogPostUserFollowModel> implements BlogPostUserFollowService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<BlogPostUserFollowModel> getModelClass() {
		return BlogPostUserFollowModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<BlogPostUserFollowModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(BlogPostUserFollowModel model, Criteria commonCriteria) {
		return null;
	}

	@Override
	public BlogPostUserFollowModel getByBlogPostAndUser(Long id) {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.createAlias("blogPost", "blogPost", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("blogPost.setBlogPostTagModel", "blogPostTags", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("blogPost.id", id));
		criteria.add(Restrictions.eq("user", Auditor.getAuditor()));		
		return (BlogPostUserFollowModel) criteria.uniqueResult();
	}
	
	@Override
	public List<BlogPostUserFollowModel> getAllByUser() {
		Criteria criteria = getSession().createCriteria(getModelClass());
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("blogPost", "blogPost", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("blogPost.setBlogPostTagModel", "blogPostTags", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("user", Auditor.getAuditor()));		
		return criteria.list();
	}
}