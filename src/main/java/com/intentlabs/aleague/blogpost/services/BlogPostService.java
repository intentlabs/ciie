
package com.intentlabs.aleague.blogpost.services;

import java.util.List;

import com.intentlabs.aleague.blogpost.model.BlogPostModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface BlogPostService extends BaseService<BlogPostModel> {
	List<BlogPostModel> getAllBlogPosts(String tags, String ordertype);
	
	BlogPostModel getById(Long id);
}
