
package com.intentlabs.aleague.blogpost.services;

import java.util.List;

import com.intentlabs.aleague.blogpost.model.BlogPostUserLikeModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface BlogPostUserLikeService extends BaseService<BlogPostUserLikeModel> {
	BlogPostUserLikeModel getByBlogPostAndUser(Long id);
	
	List<BlogPostUserLikeModel> getAllByUser();
}
