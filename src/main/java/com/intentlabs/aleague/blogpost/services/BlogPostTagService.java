
package com.intentlabs.aleague.blogpost.services;

import com.intentlabs.aleague.blogpost.model.BlogPostTagModel;
import com.intentlabs.common.service.BaseService;

/**
 * This is service for storing events.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface BlogPostTagService extends BaseService<BlogPostTagModel> {
}
