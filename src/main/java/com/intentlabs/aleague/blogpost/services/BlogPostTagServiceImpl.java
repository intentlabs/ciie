
package com.intentlabs.aleague.blogpost.services;

import org.hibernate.Criteria;

import com.intentlabs.aleague.blogpost.model.BlogPostTagModel;
import com.intentlabs.common.service.AbstractService;

/**
 * This is service for storing events.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class BlogPostTagServiceImpl extends AbstractService<BlogPostTagModel> implements BlogPostTagService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3643666686457822446L;

	@Override
	public Class<BlogPostTagModel> getModelClass() {
		return BlogPostTagModel.class;
	}

	@Override
	public Criteria setCommonCriteria(Class<BlogPostTagModel> modelClass) {
		return null;
	}

	@Override
	public Criteria setSearchCriteria(BlogPostTagModel model, Criteria commonCriteria) {
		return null;
	}
}