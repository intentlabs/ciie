/**
 * 
 */
package com.intentlabs.aleague.blogpost.enums;

import com.intentlabs.common.enums.EnumType;

/**
 * @author Dhruvang
 *
 */
public enum EnumArchive implements EnumType {
	ARCHIVE(0, "ARCHIV"),
	NON_ARCHIV(1,"NON_ARCHIV");
	
	private final long id;
    private final String name;
    
    
    EnumArchive(long id, String name) {
        this.id = id;
        this.name = name;
    }

	@Override
	public long getId() {
	
		return id;
	}

	@Override
	public String getName() {
	
		return name;
	}
	
	
	   public static EnumArchive fromId(long id) {
	        for (EnumArchive status : values()) {
	            if (status.getId() == id) {
	                return status;
	            }
	        }
	        return null;
	    }

}
