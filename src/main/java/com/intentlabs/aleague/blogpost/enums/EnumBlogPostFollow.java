

package com.intentlabs.aleague.blogpost.enums;

import com.intentlabs.common.enums.EnumType;

/**
 * This is used for defined event type.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public enum EnumBlogPostFollow implements EnumType
{
	
	YES(1, "YES"),
	NO(2,"NO");

	private final long id;
    private final String name;
    
    EnumBlogPostFollow(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
	 return name;
	}

	
    public static EnumBlogPostFollow fromId(long id) {
        for (EnumBlogPostFollow status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }

}
