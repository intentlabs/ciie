
package com.intentlabs.aleague.blogpost.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Formula;

import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */
@Entity
@Table(name = "tblBlogPost")
public class BlogPostModel extends ArchiveModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7612139860119577011L;
	
	@Column(name = "txtBlogTitle", length = 500)
	private String txtBlogTitle;
	
	@Column(name = "txtAuthor", length = 200)
	private String txtAuthor;
	
	@Column(name = "txtLinkBlog", length = 100)
	private String txtLinkBlog;
	
	@Column(name = "txtDescription")
	private String txtDescription;
	
	@Column(name = "txtPostFilePath")
	private String txtPostFilePath;
	
	@Formula("(SELECT COUNT(*) FROM tblBlogPostUserFollow e WHERE e.fkBlogPost = pkId)")
	private long countFollowers;
	
	@Formula("(SELECT COUNT(*) FROM tblBlogPostUserLike e WHERE e.fkBlogPost = pkId)")
	private long countLikes;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datePostedOn")
	private Date datePostedOn;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkBlogPost")
	private Set<BlogPostTagModel> setBlogPostTagModel;

	/**
	 * @return the txtBlogTitle
	 */
	public String getTxtBlogTitle() {
		return txtBlogTitle;
	}

	/**
	 * @param txtBlogTitle the txtBlogTitle to set
	 */
	public void setTxtBlogTitle(String txtBlogTitle) {
		this.txtBlogTitle = txtBlogTitle;
	}

	/**
	 * @return the txtAuthor
	 */
	public String getTxtAuthor() {
		return txtAuthor;
	}

	/**
	 * @param txtAuthor the txtAuthor to set
	 */
	public void setTxtAuthor(String txtAuthor) {
		this.txtAuthor = txtAuthor;
	}

	/**
	 * @return the txtLinkBlog
	 */
	public String getTxtLinkBlog() {
		return txtLinkBlog;
	}

	/**
	 * @param txtLinkBlog the txtLinkBlog to set
	 */
	public void setTxtLinkBlog(String txtLinkBlog) {
		this.txtLinkBlog = txtLinkBlog;
	}

	/**
	 * @return the txtDescription
	 */
	public String getTxtDescription() {
		return txtDescription;
	}

	/**
	 * @param txtDescription the txtDescription to set
	 */
	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	/**
	 * @return the txtPostFilePath
	 */
	public String getTxtPostFilePath() {
		return txtPostFilePath;
	}

	/**
	 * @param txtPostFilePath the txtPostFilePath to set
	 */
	public void setTxtPostFilePath(String txtPostFilePath) {
		this.txtPostFilePath = txtPostFilePath;
	}

	/**
	 * @return the countFollowers
	 */
	public long getCountFollowers() {
		return countFollowers;
	}

	/**
	 * @param countFollowers the countFollowers to set
	 */
	public void setCountFollowers(long countFollowers) {
		this.countFollowers = countFollowers;
	}

	/**
	 * @return the countLikes
	 */
	public long getCountLikes() {
		return countLikes;
	}

	/**
	 * @param countLikes the countLikes to set
	 */
	public void setCountLikes(long countLikes) {
		this.countLikes = countLikes;
	}

	public Date getDatePostedOn() {
		return datePostedOn;
	}

	public void setDatePostedOn(Date datePostedOn) {
		this.datePostedOn = datePostedOn;
	}

	public Set<BlogPostTagModel> getSetBlogPostTagModel() {
		return setBlogPostTagModel;
	}

	public void setSetBlogPostTagModel(Set<BlogPostTagModel> setBlogPostTagModel) {
		this.setBlogPostTagModel = setBlogPostTagModel;
	}

	public static BlogPostModel getSaveModel(){
		BlogPostModel blogPost = new BlogPostModel();
		blogPost.setCreateDate(new Date());
		return blogPost;
	}	
}
