
package com.intentlabs.aleague.blogpost.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.intentlabs.aleague.blogpost.enums.EnumBlogPostFollow;
import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.model.ArchiveModel;

/**
 * This is model for Event sponsers.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Entity
@Table(name = "tblBlogPostUserFollow")

public class BlogPostUserFollowModel extends ArchiveModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3528610774449650642L;

	@Column(name = "enumBlogPostFollow")
	private long blogPostFollow;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkBlogPost")
	private BlogPostModel blogPost;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkUser")
	private User user;

	/**
	 * @return the blogPostLike
	 */
	public EnumBlogPostFollow getBlogPostFollow() {
		return EnumBlogPostFollow.fromId(blogPostFollow);
	}

	/**
	 * @param blogPostLike the blogPostLike to set
	 */
	public void setBlogPostFollow(EnumBlogPostFollow blogPostFollow) {
		this.blogPostFollow = blogPostFollow.getId();
	}

	/**
	 * @return the blogPost
	 */
	public BlogPostModel getBlogPost() {
		return blogPost;
	}

	/**
	 * @param blogPost the blogPost to set
	 */
	public void setBlogPost(BlogPostModel blogPost) {
		this.blogPost = blogPost;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
}