
package com.intentlabs.aleague.blogpost.view;

import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.view.IdentifierView;

/**
 * This is View used to get data for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public class BlogPostView extends IdentifierView {


	private static final long serialVersionUID = 8692674149531174388L;
	
	private String blogTitle;
	
	private String author;
	
	private String tags;
	
	private Set<TagView> setTags;
	
	private String linkblog;
	
	private String description;
	
	private String postFilePath;
	
	private MultipartFile file;
	
	private String postedOn;
	
	private long countFollowers;
	
	private long countLikes;
	
	private String orderType;
	
	private Boolean isLiked;
	
	private Boolean isFollowed;
	
	private Boolean canEditDelete;

	/**
	 * @return the blogTitle
	 */
	public String getBlogTitle() {
		return blogTitle;
	}

	/**
	 * @param blogTitle the blogTitle to set
	 */
	public void setBlogTitle(String blogTitle) {
		this.blogTitle = blogTitle;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}
	
	/**
	 * @return the set tags
	 */
	public Set<TagView> getSettags() {
		return setTags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setSettags(Set<TagView> setTags) {
		this.setTags = setTags;
	}

	/**
	 * @return the linkblog
	 */
	public String getLinkblog() {
		return linkblog;
	}

	/**
	 * @param linkblog the linkblog to set
	 */
	public void setLinkblog(String linkblog) {
		this.linkblog = linkblog;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the postFilePath
	 */
	public String getPostFilePath() {
		return postFilePath;
	}

	/**
	 * @param postFilePath the postFilePath to set
	 */
	public void setPostFilePath(String postFilePath) {
		this.postFilePath = postFilePath;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * @return the postedOn
	 */
	public String getPostedOn() {
		return postedOn;
	}

	/**
	 * @param postedOn the postedOn to set
	 */
	public void setPostedOn(String postedOn) {
		this.postedOn = postedOn;
	}

	/**
	 * @return the countFollowers
	 */
	public long getCountFollowers() {
		return countFollowers;
	}

	/**
	 * @param countFollowers the countFollowers to set
	 */
	public void setCountFollowers(long countFollowers) {
		this.countFollowers = countFollowers;
	}

	/**
	 * @return the countLikes
	 */
	public long getCountLikes() {
		return countLikes;
	}

	/**
	 * @param countLikes the countLikes to set
	 */
	public void setCountLikes(long countLikes) {
		this.countLikes = countLikes;
	}

	/**
	 * @return the orderType
	 */
	public String getOrderType() {
		return orderType;
	}

	/**
	 * @param orderType the orderType to set
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	/**
	 * @return the isLiked
	 */
	public Boolean getIsLiked() {
		return isLiked;
	}

	/**
	 * @param isLiked the isLiked to set
	 */
	public void setIsLiked(Boolean isLiked) {
		this.isLiked = isLiked;
	}

	/**
	 * @return the isFollowed
	 */
	public Boolean getIsFollowed() {
		return isFollowed;
	}

	/**
	 * @param isFollowed the isFollowed to set
	 */
	public void setIsFollowed(Boolean isFollowed) {
		this.isFollowed = isFollowed;
	}

	/**
	 * @return the canEditDelete
	 */
	public Boolean getCanEditDelete() {
		return canEditDelete;
	}

	/**
	 * @param canEditDelete the canEditDelete to set
	 */
	public void setCanEditDelete(Boolean canEditDelete) {
		this.canEditDelete = canEditDelete;
	}
}
