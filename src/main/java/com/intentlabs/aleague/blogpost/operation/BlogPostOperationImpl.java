
package com.intentlabs.aleague.blogpost.operation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.aleague.blogpost.enums.EnumBlogPostFollow;
import com.intentlabs.aleague.blogpost.enums.EnumBlogPostLike;
import com.intentlabs.aleague.blogpost.model.BlogPostModel;
import com.intentlabs.aleague.blogpost.model.BlogPostTagModel;
import com.intentlabs.aleague.blogpost.model.BlogPostUserFollowModel;
import com.intentlabs.aleague.blogpost.model.BlogPostUserLikeModel;
import com.intentlabs.aleague.blogpost.services.BlogPostService;
import com.intentlabs.aleague.blogpost.services.BlogPostTagService;
import com.intentlabs.aleague.blogpost.services.BlogPostUserFollowService;
import com.intentlabs.aleague.blogpost.services.BlogPostUserLikeService;
import com.intentlabs.aleague.blogpost.view.BlogPostView;
import com.intentlabs.aleague.tag.model.TagModel;
import com.intentlabs.aleague.tag.services.TagService;
import com.intentlabs.aleague.tag.view.TagView;
import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.enums.ActiveInActive;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.model.PageModel;
import com.intentlabs.common.oparation.AbstractOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultListRespone;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.response.ViewResponse;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.Constant;
import com.intentlabs.common.util.DateUtility;
import com.intentlabs.common.util.FileUtility;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.View;

/**
 * This is operation for Event to handle request.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@Component(value = "blogPostOperation")
@Transactional(propagation = Propagation.REQUIRED)
public class BlogPostOperationImpl extends AbstractOperation<BlogPostModel, BlogPostView> implements BlogPostOperation {

	private static final long serialVersionUID = 741391010890205612L;

	@Autowired
	private BlogPostService blogPostService;

	@Autowired
	private BlogPostTagService blogPostTagService;

	@Autowired
	private BlogPostUserLikeService blogPostUserLikeService;

	@Autowired
	private BlogPostUserFollowService blogPostUserFollowService;

	@Autowired
	private TagService tagService;

	@Override
	public BaseService getService() {
		return blogPostService;
	}

	@Override
	protected BlogPostModel getNewModel(BlogPostView view) {
		BlogPostModel model = new BlogPostModel();
		model.setCreateDate(DateUtility.getCurrentDate());
		return model;
	}

	@Override
	public Response doAddOperation() throws IOException, Exception {
		return null;
	}

	@Override
	public Response doEditOpeartion(Long id) throws IOException, Exception {
		return null;
	}

	@Override
	public Response doViewOperation(Long id) throws Exception {
		return null;
	}

	@Override
	protected void checkInactive(BlogPostModel model) throws Exception {
	}

	@Override
	public BlogPostModel toModel(BlogPostModel blogPostModel, BlogPostView blogPostView) throws Exception {
		if (blogPostView.getId() == null || blogPostView.getId().longValue() <= 0) {
			blogPostModel.setCreateBy(Auditor.getAuditor());
			blogPostModel.setActivationStatus(ActiveInActive.ACTIVE);
			blogPostModel.setActivationDate(DateUtility.getCurrentDate());
			blogPostModel.setEnumArchive("1");
			blogPostModel.setCountFollowers(0l);
			blogPostModel.setCountLikes(0l);
		} else {
			blogPostModel.setId(blogPostView.getId());
		}

		blogPostModel.setUpdateBy(Auditor.getAuditor());
		blogPostModel.setUpdateDate(DateUtility.getCurrentDate());

		blogPostModel.setTxtAuthor(blogPostView.getAuthor());
		blogPostModel.setTxtBlogTitle(blogPostView.getBlogTitle());
		blogPostModel.setTxtDescription(blogPostView.getDescription());

		if (blogPostView.getLinkblog().startsWith("https:") || blogPostView.getLinkblog().startsWith("http:")) {
			blogPostModel.setTxtLinkBlog(blogPostView.getLinkblog());
		} else {
			blogPostModel.setTxtLinkBlog(Constant.HTTP_URL + blogPostView.getLinkblog());
		}

		blogPostModel
				.setDatePostedOn(DateUtility.toDate(blogPostView.getPostedOn(), new SimpleDateFormat("dd-MM-yyyy")));

		if (blogPostView.getFile() != null) {
			blogPostModel.setTxtPostFilePath(FileUtility.storeFile(blogPostView.getFile(), "blogpost"));
		}

		Set<BlogPostTagModel> setBlogPostTagModel = new HashSet<>();

		Map<Long, BlogPostTagModel> mapTagModel = new HashMap<>();

		if (blogPostModel.getSetBlogPostTagModel() != null && !blogPostModel.getSetBlogPostTagModel().isEmpty()) {
			for (BlogPostTagModel blogPostTag : blogPostModel.getSetBlogPostTagModel()) {
				mapTagModel.put(blogPostTag.getTag().getId(), blogPostTag);
			}
		}

		if (blogPostView.getTags() != null && !"".equals(blogPostView.getTags())) {
			JSONArray jsonArrayBlogPostTag = new JSONArray(blogPostView.getTags());

			List<TagModel> listTags = tagService.getAllTags(null);

			for (Integer i = 0; i < jsonArrayBlogPostTag.length(); i++) {
				Long tagId = jsonArrayBlogPostTag.getLong(i);

				BlogPostTagModel blogPostTag;
				if (mapTagModel.containsKey(tagId)) {
					blogPostTag = mapTagModel.get(tagId);

					if (blogPostTag.getActivationStatus().getId() == ActiveInActive.INACTIVE.getId()) {
						blogPostTag.setActivationStatus(ActiveInActive.ACTIVE);
					}

					mapTagModel.remove(tagId);
				} else {
					blogPostTag = new BlogPostTagModel();
					blogPostTag.setCreateBy(Auditor.getAuditor());
					blogPostTag.setCreateDate(DateUtility.getCurrentDate());
					blogPostTag.setActivationStatus(ActiveInActive.ACTIVE);
					blogPostTag.setActivationChangeBy(Auditor.getAuditor());
					blogPostTag.setActivationDate(DateUtility.getCurrentDate());
					blogPostTag.setEnumArchive("1");

					for (TagModel instanceTag : listTags) {
						if (instanceTag.getId().longValue() == tagId.longValue()) {
							blogPostTag.setTag(instanceTag);
							break;
						}
					}
				}

				blogPostTag.setUpdateBy(Auditor.getAuditor());
				blogPostTag.setUpdateDate(DateUtility.getCurrentDate());

				setBlogPostTagModel.add(blogPostTag);
			}
		}

		if (!mapTagModel.keySet().isEmpty()) {
			for (Long key : mapTagModel.keySet()) {
				BlogPostTagModel blogPostTag = mapTagModel.get(key);

				if (blogPostTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					blogPostTag.setActivationStatus(ActiveInActive.INACTIVE);
					blogPostTag.setActivationChangeBy(Auditor.getAuditor());
					blogPostTag.setActivationDate(DateUtility.getCurrentDate());
					setBlogPostTagModel.add(blogPostTag);
				}
			}
		}

		blogPostModel.setSetBlogPostTagModel(setBlogPostTagModel);

		return blogPostModel;
	}

	@Override
	public BlogPostView fromModel(BlogPostModel blogPostModel) {
		BlogPostView blogPostView = new BlogPostView();

		Boolean canEditDelete = false;
		if (Auditor.getAuditor().getId().longValue() == blogPostModel.getCreateBy().getId().longValue()) {
			canEditDelete = true;
		}
		blogPostView.setCanEditDelete(canEditDelete);

		blogPostView.setId(blogPostModel.getId());
		blogPostView.setAuthor(blogPostModel.getTxtAuthor());
		blogPostView.setBlogTitle(blogPostModel.getTxtBlogTitle());
		blogPostView.setDescription(blogPostModel.getTxtDescription());
		blogPostView.setLinkblog(blogPostModel.getTxtLinkBlog());
		blogPostView.setPostFilePath(FileUtility.setPath(blogPostModel.getTxtPostFilePath()));

		Set<TagView> setTags = new HashSet<>();
		if (blogPostModel.getSetBlogPostTagModel() != null && !blogPostModel.getSetBlogPostTagModel().isEmpty()) {
			for (BlogPostTagModel blogPostTag : blogPostModel.getSetBlogPostTagModel()) {
				if (blogPostTag.getActivationStatus().getId() == ActiveInActive.ACTIVE.getId()) {
					TagView tagView = new TagView();
					tagView.setId(blogPostTag.getTag().getId());
					tagView.setTagName(blogPostTag.getTag().getTxtTagName());
					setTags.add(tagView);
				}
			}
		}

		blogPostView.setSettags(setTags);
		blogPostView.setPostedOn(
				DateUtility.toDateTimeString(blogPostModel.getDatePostedOn(), new SimpleDateFormat("dd-MM-yyyy")));

		blogPostView.setCountFollowers(blogPostModel.getCountFollowers());
		blogPostView.setCountLikes(blogPostModel.getCountLikes());

		return blogPostView;
	}

	@Override
	public Response doSaveOperation(BlogPostView view) throws Exception {
		BlogPostModel model = null;

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = getNewModel(view);
		} else {
			model = blogPostService.getById(view.getId());
		}

		model = toModel(model, view);

		if (view.getId() == null || view.getId().longValue() <= 0) {
			model = blogPostService.create(model);
		} else {
			blogPostService.update(model);
		}

		for (BlogPostTagModel blogPostTag : model.getSetBlogPostTagModel()) {
			if (blogPostTag.getBlogPost() == null) {
				blogPostTag.setBlogPost(model);
				blogPostTagService.create(blogPostTag);
			} else {
				blogPostTagService.update(blogPostTag);
			}
		}

		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Blog Post has been saved.");
	}

	@Override
	public Response doDisplayOperation(Long id) {
		BlogPostModel model = blogPostService.load(id);
		BlogPostView blogPostView = fromModel(model);
		blogPostView.setIsFollowed(isBlogPostFollow(model, blogPostUserFollowService.getByBlogPostAndUser(id)));
		blogPostView.setIsLiked(isBlogPostLike(model, blogPostUserLikeService.getByBlogPostAndUser(id)));
		return ViewResponse.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_BLOG_POST, blogPostView);
	}

	@Override
	public Response doDisplayAllOperation(String tags, String ordertype) {
		List<BlogPostModel> listBlogPosts = blogPostService.getAllBlogPosts(tags, ordertype);

		List<View> listBlogViews = new ArrayList<>();

		List<BlogPostUserFollowModel> listBlogPostUserFollow = blogPostUserFollowService.getAllByUser();

		List<BlogPostUserLikeModel> listBlogPostUserLike = blogPostUserLikeService.getAllByUser();

		for (BlogPostModel model : listBlogPosts) {
			BlogPostView blogPostView = fromModel(model);
			blogPostView.setIsFollowed(isBlogPostFollow(model, listBlogPostUserFollow));
			blogPostView.setIsLiked(isBlogPostLike(model, listBlogPostUserLike));
			listBlogViews.add(blogPostView);
		}

		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_BLOG_POSTS,
				listBlogViews.size(), listBlogViews);
	}

	public Boolean isBlogPostFollow(BlogPostModel model, List<BlogPostUserFollowModel> listBlogPostUserFollow) {
		for (BlogPostUserFollowModel follow : listBlogPostUserFollow) {
			if (follow.getBlogPost().getId() == model.getId()) {
				return true;
			}
		}
		return false;
	}

	public Boolean isBlogPostLike(BlogPostModel model, List<BlogPostUserLikeModel> listBlogPostUserLike) {
		for (BlogPostUserLikeModel like : listBlogPostUserLike) {
			if (like.getBlogPost().getId() == model.getId()) {
				return true;
			}
		}
		return false;
	}

	public Boolean isBlogPostFollow(BlogPostModel model, BlogPostUserFollowModel blogPostUserFollow) {
		if (blogPostUserFollow != null) {
			return true;
		}
		return false;
	}

	public Boolean isBlogPostLike(BlogPostModel model, BlogPostUserLikeModel blogPostUserLike) {
		if (blogPostUserLike != null) {
			return true;
		}
		return false;
	}

	@Override
	public Response doFollowOperation(Long id) {
		BlogPostUserFollowModel model = blogPostUserFollowService.getByBlogPostAndUser(id);

		if (model == null) {
			BlogPostModel blogPost = blogPostService.getById(id);
			model = new BlogPostUserFollowModel();
			model.setCreateBy(Auditor.getAuditor());
			model.setActivationStatus(ActiveInActive.ACTIVE);
			model.setActivationDate(DateUtility.getCurrentDate());
			model.setEnumArchive("1");
			model.setUpdateBy(Auditor.getAuditor());
			model.setUpdateDate(DateUtility.getCurrentDate());
			model.setBlogPost(blogPost);
			model.setBlogPostFollow(EnumBlogPostFollow.YES);
			model.setUser(Auditor.getAuditor());
			blogPostUserFollowService.create(model);

			return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Blog Post is followed successfully.");
		} else {
			return CommonResponse.create(ResponseCode.ALREADY_EXISTS.getCode(), "Blog Post already followed.");
		}
	}

	@Override
	public Response doLikeOperation(Long id) {
		BlogPostUserLikeModel model = blogPostUserLikeService.getByBlogPostAndUser(id);

		if (model == null) {
			BlogPostModel blogPost = blogPostService.getById(id);
			model = new BlogPostUserLikeModel();
			model.setCreateBy(Auditor.getAuditor());
			model.setActivationStatus(ActiveInActive.ACTIVE);
			model.setActivationDate(DateUtility.getCurrentDate());
			model.setEnumArchive("1");
			model.setUpdateBy(Auditor.getAuditor());
			model.setUpdateDate(DateUtility.getCurrentDate());
			model.setBlogPost(blogPost);
			model.setBlogPostLike(EnumBlogPostLike.YES);
			model.setUser(Auditor.getAuditor());
			blogPostUserLikeService.create(model);

			return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Blog Post is liked successfully.");
		} else {
			return CommonResponse.create(ResponseCode.ALREADY_EXISTS.getCode(), "Blog Post already liked.");
		}
	}

	@Override
	public Response doSearch(int start, int end, String tags, String orderType) {
		PageModel pageModel = blogPostService.search(new BlogPostModel(), start, end);
		return PageResultListRespone.create(ResponseCode.SUCCESSFUL.getCode(), OperationName.GET_ALL_BLOG_POSTS,
				pageModel.getRecords(), fromModelList((List<BlogPostModel>) pageModel.getList()));
	}

	@Override
	public Response doDeleteOperation(Long blogPostId) {
		BlogPostModel model = blogPostService.getById(blogPostId);
		model.setActivationStatus(ActiveInActive.INACTIVE);
		model.setActivationDate(DateUtility.getCurrentDate());
		model.setActivationChangeBy(Auditor.getAuditor());
		blogPostService.update(model);
		return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), "Blog Post has been deleted.");
	}
}
