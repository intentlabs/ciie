
package com.intentlabs.aleague.blogpost.operation;

import com.intentlabs.aleague.blogpost.view.BlogPostView;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;

/**
 * This is operation for Event.
 * 
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface BlogPostOperation extends BaseOperation<BlogPostView> {

	String BLOG_POST_OPERATION = "blogPostOperation";
	
	Response doDisplayOperation(Long id);

	Response doDisplayAllOperation(String tags, String ordertype);
	
	Response doFollowOperation(Long id);
	
	Response doLikeOperation(Long id);
	
	Response doSearch(int start, int end, String tags, String orderType);
	
	Response doDeleteOperation(Long blogPostId);
}
