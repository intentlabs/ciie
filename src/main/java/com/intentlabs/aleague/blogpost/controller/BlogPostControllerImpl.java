
package com.intentlabs.aleague.blogpost.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.intentlabs.aleague.blogpost.operation.BlogPostOperation;
import com.intentlabs.aleague.blogpost.view.BlogPostView;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.common.controller.AbstractController;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;

/**
 * This is controller for Blog Post to handle request.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

@SuppressWarnings("serial")
@Controller
@RequestMapping("/blogpost")
public class BlogPostControllerImpl extends AbstractController<BlogPostView> implements BlogPostController
{

	@Autowired
	BlogPostOperation blogPostOperation;
	
	@Override
	public BaseOperation<BlogPostView> getOperation() {
		return blogPostOperation;
	}

	@Override
	public Response getModelForAdd() throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForEdit(Long id) throws IOException, Exception {

		return null;
	}

	@Override
	public Response getModelForView(Long id) throws Exception {

		return null;
	}

	@Override
	public void isValidSaveData(BlogPostView webView) throws Exception {

		
	}

	@Override
	public Response getBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception {
		return blogPostOperation.doDisplayOperation(blogPost.getId());
	}

	@Override
	public Response getAllBlogPosts(@RequestBody (required=false) BlogPostView blogPost) throws InvalidDataException, Exception {
		if(blogPost != null){
			return blogPostOperation.doDisplayAllOperation(blogPost.getTags(), blogPost.getOrderType());	
		}else{
			return blogPostOperation.doDisplayAllOperation(null, null);
		}		
	}

	@Override
	public Response followBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception {
		try{
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.FOLLOW_BLOG_POST, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.FOLLOW_BLOG_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return blogPostOperation.doFollowOperation(blogPost.getId());
		}
		finally {
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.FOLLOW_BLOG_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response likeBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception {
		try{
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.LIKE_BLOG_POST, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.LIKE_BLOG_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return blogPostOperation.doLikeOperation(blogPost.getId());
		}
		finally {
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.LIKE_BLOG_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response saveBlogPost(@ModelAttribute BlogPostView view) throws InvalidDataException, Exception {
		try{
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.SAVE_BLOG_POST, LogMessage.BEFORE_VALIDATION);
			isValidSaveData(view);
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.SAVE_BLOG_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return blogPostOperation.doSaveOperation(view);
		}
		finally {
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.SAVE_BLOG_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}

	@Override
	public Response search(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end,
			@RequestParam(required = false, value="tags") String tags, @RequestParam(required = false, value="orderType") String orderType) throws InvalidDataException, Exception {
		if(start == null){
			start = 0;
		}
		if(end == null){
			end = 10;
		}
		return blogPostOperation.doSearch(start, end, tags, orderType);
	}

	@Override
	public Response deleteBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception {
		try{
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.DELETE_BLOG_POST, LogMessage.BEFORE_VALIDATION);
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.DELETE_BLOG_POST, LogMessage.BEFORE_CALLING_OPERATION);
			return blogPostOperation.doDeleteOperation(blogPost.getId());
		}
		finally {
			LoggerService.info(BLOG_POST_CONTROLLER, OperationName.DELETE_BLOG_POST, LogMessage.REQUEST_COMPLETED);			
		}
	}
}
