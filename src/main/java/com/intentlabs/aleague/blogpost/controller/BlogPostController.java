
package com.intentlabs.aleague.blogpost.controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.blogpost.view.BlogPostView;
import com.intentlabs.aleague.exception.InvalidDataException;
import com.intentlabs.aleague.researchresource.view.ResearchResourceView;
import com.intentlabs.common.controller.WebController;
import com.intentlabs.common.response.Response;

/**
 * This is controller for Event.
 * @author Vishwa Shah.
 * @version 1.0
 * @since 14/07/2017
 */

public interface BlogPostController extends WebController<BlogPostView>
{

	String BLOG_POST_CONTROLLER = "BlogPostController";
	
	@RequestMapping(value = "/saveblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response saveBlogPost(@ModelAttribute BlogPostView view) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/getblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/getallblogposts.htm", method = RequestMethod.POST)
	@ResponseBody
	Response getAllBlogPosts(@RequestBody (required=false) BlogPostView blogPost) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/followblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response followBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception;	
	
	@RequestMapping(value = "/likeblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response likeBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/search.htm", method = RequestMethod.GET)
	@ResponseBody
	Response search(@RequestParam(required = false, value="start") Integer start, @RequestParam(required = false, value="end") Integer end,
			@RequestParam(required = false, value="tags") String tags, @RequestParam(required = false, value="orderType") String orderType) throws InvalidDataException, Exception;
	
	@RequestMapping(value = "/deleteblogpost.htm", method = RequestMethod.POST)
	@ResponseBody
	Response deleteBlogPost(@RequestBody BlogPostView blogPost) throws InvalidDataException, Exception;
}
