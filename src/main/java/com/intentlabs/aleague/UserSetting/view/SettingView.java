/**
 * 
 */
package com.intentlabs.aleague.UserSetting.view;

import com.intentlabs.common.view.AuditableView;

/**
 * @author Dhruvang
 *
 */
public class SettingView extends AuditableView {


	private static final long serialVersionUID = -5615441360694163135L;
	
	private String key;
	
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
