/**
 * 
 */
package com.intentlabs.aleague.UserSetting.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.intentlabs.common.model.AuditableModel;

/**
 * @author Dhruvang
 *
 */

@Entity
@Table(name = "tblSettings")
public class SettingsModel extends AuditableModel {

	
	private static final long serialVersionUID = 7920187949861499317L;
	

	
	@Column(name = "txtKey")
	private String Key;
	
	@Column(name = "txtValue")
	private String Value;



	public String getKey() {
		return Key;
	}

	public void setKey(String key) {
		Key = key;
	}

	public String getValue() {
		return Value;
	}

	public void setValue(String value) {
		Value = value;
	}
	
	

}
