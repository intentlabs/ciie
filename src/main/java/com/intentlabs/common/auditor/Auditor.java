/**
 * 
 */
package com.intentlabs.common.auditor;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.model.ActivationModel;
import com.intentlabs.common.model.AuditableModel;
import com.intentlabs.common.model.CreateModel;
import com.intentlabs.common.model.Model;
import com.intentlabs.common.util.DateUtility;

/**
 * @author Dhruvang
 *
 */
public class Auditor {
	
	private static ThreadLocal<User> userAuditor = new ThreadLocal<>();
	
	
	 private Auditor(){
	    }
	    /**
	     * It is used to store activation by user details when any model is activated/de-activated.
	     * @param model
	     */
	    public static void activationAudit(Model model){
	    	User user = Auditor.userAuditor.get();
	    	((ActivationModel) model).setActivationChangeBy(user);
//	    	((ActivationModel) model).setActivationChangeByUserName(user.getUserId());
	    	((ActivationModel) model).setActivationDate(DateUtility.getCurrentDate());
	    }
	    
	    /**
	     * It is used to audit user details when any module data is getting updated.
	     * @param model
	     */
	    public static void updateAudit(Model model){
		   	if(model instanceof AuditableModel ){
			    	User user = Auditor.userAuditor.get();
			    	((AuditableModel) model).setUpdateBy(user);
			        ((AuditableModel) model).setUpdateDate(DateUtility.getCurrentDate());
//			        ((AuditableModel) model).setUpdateByUserName(user.getUserId());
		   	}
	    }
	    
	    /**
	     * It is used to store create by user details when any model is getting created.
	     * @param model
	     */
	    public static void createAudit(Model model) {
	    	if(model instanceof CreateModel ){
		        User user = Auditor.userAuditor.get();
		        if(user != null){
		        	((CreateModel) model).setCreateBy(user);
		        }
//			    ((CreateModel) model).setCreateByUserName(user.getUserId());
		        ((CreateModel) model).setCreateDate(DateUtility.getCurrentDate());	        
	    	}
	    }

	    /**
	     * To get current auditor details
	     * @return User
	     */
	    public static User getAuditor() {
	        return userAuditor.get();
	    }

	    /**
	     * To set user details into thread local.
	     * @return User
	     */
	    public static void setAuditor(User user) {
	        userAuditor.set(user);
	    }

}
