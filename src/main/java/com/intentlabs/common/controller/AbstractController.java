package com.intentlabs.common.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.intentlabs.aleague.exception.CapsException;
import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.oparation.BaseOperation;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.util.LogMessage;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.IdentifierView;




/**
 * Its abstract class of controller. It provides abstract methods & implementation of basic methods used by any controller. 
 * @author Core team.
 * @version 1.0
 * @param <IdentifierView>
 */
public abstract class AbstractController<V extends IdentifierView> {
	static final String ABSTRACT_CONTROLLER = "AbstractController";
    
	/**
	 * It returns operation's name which can be called from controller.
	 * @return WebOperation
	 */
    public abstract BaseOperation<V> getOperation();

    /**
     * This methods returns add form used by any module. All data inside form will be binded through spring mvc feature.
     * @return ModelAndView render jsp form with data to web client.
     */
    @RequestMapping(value = "/addForm.htm", method = RequestMethod.GET)
   // @Authorization(rights=RightsEnum.ADD)
    @ResponseBody
    public abstract Response getModelForAdd()throws IOException, Exception;

    /**
     * This methods returns edit form used by any module. All data inside form will be binded through spring mvc feature.
     * @param id unique value which is used to get model details.
     * @return Response render jsp form with data to web client.
     * @throws InvalidModelAndViewRequestException
     * @throws InactiveModelException
     * @throws IOException 
     */
    @RequestMapping(value = "/editForm.htm", method = RequestMethod.POST)
    @ResponseBody
    public abstract Response getModelForEdit(@RequestParam("id") Long id) throws IOException, Exception;

    /**
     * This methods returns view form used by any module. All data inside form will be binded through spring mvc feature.
     * @param id unique value which is used to display model details.
     * @return Response render jsp form with data to web client.
     * @throws InvalidModelAndViewRequestException
     */
    @RequestMapping(value = "/viewForm.htm", method = RequestMethod.POST)
    @ResponseBody
    public abstract Response getModelForView(@RequestParam("id") Long id) throws Exception;

    /**
     * This methods is used handle save request coming from client for any module. It validates the data & pass the same
     * to operation where database related operation is performed.
     * @param view data received from web client will be mapped to view.
     * @return Response it provides a response to web client in form of json.
     * @throws Exception 
     */
    @RequestMapping(value = "/save.htm", method = RequestMethod.POST)
    @ResponseBody
    public Response save(@RequestBody V view) throws Exception {
    	try {
    		LoggerService.info(this.getClass().getSimpleName(), OperationName.SAVE, LogMessage.BEFORE_VALIDATION);
    		isValidSaveData(view);
    		LoggerService.info(this.getClass().getSimpleName(), OperationName.SAVE, LogMessage.BEFORE_CALLING_OPERATION);
            return getOperation().doSaveOperation(view);
    	}finally{
    		LoggerService.info(this.getClass().getSimpleName(), OperationName.SAVE, LogMessage.REQUEST_COMPLETED);
    	}
    }

    /**
     * This methods is used handle update request coming from client for any module. It validates the data & pass the same
     * to operation where database related operation is performed.
     * @param view data received from web client will be mapped to view.
     * @return Response it provides a response to web client in form of json.
     * @throws Exception 
     */
    @RequestMapping(value = "/update.htm", method = RequestMethod.POST)
    @ResponseBody
    public Response update(@RequestBody V view) throws Exception {
    	try{
    		
    		if(view.getId() == null){
    
    			throw new CapsException(ResponseCode.INVALID_REQUEST.getCode(), this.getClass().getSimpleName(),OperationName.UPDATE);
    		}
    		isValidSaveData(view);
    
    		return getOperation().doUpdateOperation(view);
    	}finally{
    
    	}            
    }

    
    /**
     * This methods is used handle display request coming from client for any module. It validates the data & pass the same
     * to operation where database related operation is performed.
     * @param start  starting to fetch records from this number.
     * @param end    maximum records to fetched from start number.
     * @return Response it provides a response to web client in form of json.
     */
    @RequestMapping(value = "/display.htm", method = RequestMethod.GET)
    @ResponseBody
    public Response displayGrid(@RequestParam(value = "start", required = false) Integer start, @RequestParam(value = "end", required = false) Integer end) {
    	try{
    		LoggerService.info(this.getClass().getSimpleName(), OperationName.DISPLAY_GRID, LogMessage.BEFORE_CALLING_OPERATION);
    		return getOperation().doDisplayGridOperation(start, end);
    	}finally{
    		LoggerService.info(this.getClass().getSimpleName(), OperationName.DISPLAY_GRID, LogMessage.REQUEST_COMPLETED);
    	}
    }

    /**
     * This methods is used handle delete request coming from client for any module. It validates the data & pass the same
     * to operation where database related operation is performed.
     * @param id  unique value which is used to get model details.
     * @return Response it provides a response to web client in form of json.
     */
    @RequestMapping(value = "/delete.htm", method = RequestMethod.POST)
    @ResponseBody
    public Response delete(@RequestParam("id") Long id) {
    	try{
    		
    		if(id == null){
    			return CommonResponse.create(ResponseCode.INTERNAL_SERVER_ERROR.getCode(), ResponseCode.INVALID_REQUEST.getMessage());
    		}
    		
    		return getOperation().doDeleteOperation(id);
    	}finally{
    		
    	}
    }
    
    /**
     * This methods is used to validate data received from client side before saving or updating a module.
     * @param webView data received from web client will be mapped to view.
     * @throws Exception
     */
    

    
    public abstract void isValidSaveData(V webView) throws Exception;
}