package com.intentlabs.common.controller;

import java.io.IOException;

import com.intentlabs.common.response.Response;
import com.intentlabs.common.view.IdentifierView;




/**
 * This is a web controller which handles request coming from web client. Some basic methods defination has been provided inside
 * this interface.Controller will handle all request coming from client end. It does basic validation on given data & transfer the same to operation layer where
 * database related operation will be done.It renders the view based on request to the client. 
 * 
 * @author Core team.
 * @version 1.0
 */
public interface WebController<V extends IdentifierView> extends Controller {

	/**
     * This methods returns add form used by any module. All data inside form will be binded through spring mvc feature.
     * @return ModelAndView render jsp form with data to web client.
     */
	 Response getModelForAdd() throws IOException, Exception; 

	/**
     * This methods returns edit form used by any module. All data inside form will be binded through spring mvc feature.
     * @param id unique value which is used to get model details.
     * @return Response render jsp form with data to web client.
     * @throws InvalidModelAndViewRequestException
     * @throws Exception
     */
	 Response getModelForEdit(Long id) throws IOException, Exception;

	/**
     * This methods returns view form used by any module. All data inside form will be binded through spring mvc feature.
     * @param id unique value which is used to display model details.
     * @return Response render jsp form with data to web client.
     * @throws Exception
     */
	 Response getModelForView(Long id) throws Exception;

	/**
     * This methods is used handle display request coming from client for any module. It validates the data & pass the same
     * to operation where database related operation is performed.
     * @param start  starting to fetch records from this number.
     * @param end    maximum records to fetched from start number.
     * @return Response it provides a response to web client in form of json.
     */
	 Response displayGrid(Integer start, Integer end);

	/**
     * This methods is used handle save request coming from client for any module. It validates the data & pass the same
     * to operation where database related operation is performed.
     * @param view data received from web client will be mapped to view.
     * @return Response it provides a response to web client in form of json.
     * @throws Exception
     */
	 Response save(V view) throws Exception; 

	/**
     * This methods is used handle update request coming from client for any module. It validates the data & pass the same
     * to operation where database related operation is performed.
     * @param view data received from web client will be mapped to view.
     * @return Response it provides a response to web client in form of json.
     * @throws Exception
     */
	 Response update(V view) throws Exception;

	/**
     * This methods is used handle delete request coming from client for any module. It validates the data & pass the same
     * to operation where database related operation is performed.
     * @param id  unique value which is used to get model details.
     * @return Response it provides a response to web client in form of json.
     */
	 Response delete(Long id);
}
