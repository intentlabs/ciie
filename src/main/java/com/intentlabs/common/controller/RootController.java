

package com.intentlabs.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.response.CommonResponse;





@Controller
public class RootController {
	
	/*@RequestMapping(value = "/siportal", method = RequestMethod.GET)
	public ModelAndView si() {
		ModelAndView mav = new ModelAndView("welcome");
		return mav;
	}*/
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() {
		
	
			ModelAndView mav = new ModelAndView("login");
			return mav;
		
	
		
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.POST)
	public CommonResponse error() {
		return CommonResponse.create(ResponseCode.HTTP_UNAUTHORIZED.getCode(), "Untuthorized user.");
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView adminHome() {
		ModelAndView mav = new ModelAndView("profile_page");
		return mav;
	}
}
