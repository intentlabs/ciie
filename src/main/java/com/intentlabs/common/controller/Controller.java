package com.intentlabs.common.controller;

import java.io.Serializable;

/**
 * This is a marker interface used to identifies controller.Controller will 
 * handle all request coming from client end. It does basic validation on given data & transfer the same to operation layer where
 * database related operation will be done.It renders the view based on request to the client. 
 * 
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public interface Controller extends Serializable{

}
