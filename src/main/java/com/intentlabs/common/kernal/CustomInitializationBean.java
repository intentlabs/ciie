package com.intentlabs.common.kernal;

import com.intentlabs.aleague.exception.CapsException;

/**
 * This is used to define Custom Initialization bean when ApplicationEvent fired.
 * @version 1.0
 */

public interface CustomInitializationBean {
	
	/**
	 * This method is called when ApplicationEvent is fired by spring.
	 * @author CapsCore team. 
	 * @throws {@link CapsException}
	 */
	public void onStartUp() throws CapsException;
}

