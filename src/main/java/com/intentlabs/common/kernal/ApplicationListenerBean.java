package com.intentlabs.common.kernal;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.util.CustomMessage;

/**
 * This is used to catch any ApplicationEvents fired by spring.
 * @version 1.0
 */
@Component
public class ApplicationListenerBean implements ApplicationListener<ContextRefreshedEvent> {

	private static final String APPLICATION_LISTENER_BEAN = "ApplicationListenerBean";
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent paramE) {
		 
		ApplicationContext applicationContext = ((ContextRefreshedEvent) paramE).getApplicationContext();
		String[] bean = applicationContext.getBeanNamesForType(CustomInitializationBean.class);

		for (String name : bean) {
			try {
				((CustomInitializationBean) applicationContext.getBean(name)).onStartUp();
			} catch (Exception exception) {
				LoggerService.error(APPLICATION_LISTENER_BEAN, CustomMessage.ON_START_UP, "Unable to start server properly");
				LoggerService.exception(exception);
			}
		}
	}
}
