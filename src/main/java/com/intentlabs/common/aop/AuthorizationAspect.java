/*******************************************************************************
 * Copyright -2017 @IntentLabs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.intentlabs.common.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;


import com.intentlabs.common.logger.LoggerService;

import com.intentlabs.common.util.WebUtil;




/**
 * This will be used to perform access control validation.
 * This aspect will be applied when method has been annotated with Authorization Annotation.
 * @version 1.0
 */
@Component
@Aspect
public class AuthorizationAspect {
	
	
	@Before("@annotation(authorization)")
    public void authorized(JoinPoint joinPoint, Authorization authorization) throws Exception {
		/*UserModel userModel = Auditor.getAuditor();
		if(userModel == null){
			WebUtil.invalidatSession();
			LoggerService.error(joinPoint.getTarget().getClass().getName(), joinPoint.getSignature().getName().toUpperCase(), "user has no access :- "+userModel.getId());
		}*/
    }	
}
