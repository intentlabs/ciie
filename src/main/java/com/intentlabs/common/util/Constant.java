package com.intentlabs.common.util;


/**
 * @author Dhruvang
 *
 */
public interface Constant {
	
	
	String USER_SESSION_KEY = "user";
	String SALT_STRING = "#";
	String USER_SESSION_STRING = "usersession";
	String USER_AGENT = "Mozilla/5.0";
	String url = "https://www.google.com/recaptcha/api/siteverify";
	String DIGITS = "0123456789";
	String ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
	String HTTP_URL ="http://";
	//String MAINURL = HTTP_URL + "portal.a-league.org/ALeague";
	//String MAINURL = HTTP_URL + "portal.a-league.org/#/login";
	String MAINURL = HTTP_URL + "localhost/aleaguejs/#/login";
	//String SERVER_URL = HTTP_URL + "portal.a-league.org/aleaguejs/#/pages/verify/";
	String SERVER_URL = HTTP_URL + "portal.a-league.org/#/pages/verify/";	
	//String SERVER_LOCAL_URL = HTTP_URL + "localhost/aleaguejs/#/pages/verify/";	
	int sessionExpire = 30;



}
