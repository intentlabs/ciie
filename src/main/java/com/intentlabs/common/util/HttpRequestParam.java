/**
 * 
 */
package com.intentlabs.common.util;

/**
 * @author Dhruvang
 *
 */
public class HttpRequestParam {
	
	
	private HttpRequestParam(){
		//To create singleton instance
	}

	
	/**
     * Will return remote address
     * @return
     */
	public static String getRemoteAddress() {
		return WebUtil.getCurrentRequest().getRemoteAddr();
	}
	
	/**
     * Will return user agent
     * @return
     */
	public static String getUserAgent() {
		return WebUtil.getCurrentRequest().getHeader("User-Agent");
	}
	
	/**
     * Will return context path
     * @return
     */
	public static String getContextPath() {
		return WebUtil.getCurrentRequest().getContextPath() + "/";
	}
}
