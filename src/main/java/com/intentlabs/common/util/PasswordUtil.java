/**
 * 
 */
package com.intentlabs.common.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author Dhruvang
 *
 */
public class PasswordUtil {
	
	private PasswordUtil() {
		
	}
	
	
	private static ThreadLocal<MessageDigest> md5MessageDigest = new ThreadLocal<MessageDigest>(){
		@Override
		protected MessageDigest initialValue() {
			try{
				return MessageDigest.getInstance("MD5"); 
			}catch (NoSuchAlgorithmException e){ 
				throw new RuntimeException(e);  
			}
		}
	};
	
	private static ThreadLocal<MessageDigest> sha512MessageDigest = new ThreadLocal<MessageDigest>(){
		@Override
		protected MessageDigest initialValue() {
			try{
				return MessageDigest.getInstance("SHA-512"); 
			}catch (NoSuchAlgorithmException e){ 
				throw new RuntimeException(e); 
			}
		}
	};
	
	
	/**
	 * Generate md5 hash for given string & return hash string.
	 * @param saltedPassword
	 * @return
	 */
	public static String getmd5(String saltedPassword){
		md5MessageDigest.get().update(saltedPassword.getBytes());
		return toHex(md5MessageDigest.get().digest());
	}
	
	/**
	 * Generate sha512 hash for given string & return hash string.
	 * @param saltedPassword
	 * @return
	 */
	public static String getSha512(String saltedPassword){
		sha512MessageDigest.get().update(saltedPassword.getBytes());
		return toHex(sha512MessageDigest.get().digest());
	}
	
	
	/**
	 * This method converted byte array into hexa decimal string.
	 * @param array
	 * @return
	 */
	public static String toHex(byte[] array){
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0){
        	String format = "%0"  +paddingLength + "d";
            return String.format(format, 0) + hex;
        }else{
            return hex;
        }
    }
	
	
	/**
	 * This method converts given string value into byte array
	 * @param hex
	 * @return byte[]
	 */
	public static byte[] fromHex(String hex){
        byte[] bytes = new byte[hex.length() / 2];
        for(int i = 0; i<bytes.length ;i++){
            bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
	/**
	 * This will generated dynamic alphanumeric salt.
	 * @return
	 */
	public static String getDynamicSalt(){
		return RandomStringUtils.randomAlphanumeric(8);
	}
	
	/**
	 * This method is use to convert string to sha512 hash string with default salt.
	 * @param emailId emailId
	 * @param password password
	 * @return sha512 string
	 */
	public static String generatSecurePassword(String emailId, String password){
		return getSha512(emailId + Constant.SALT_STRING + getmd5(emailId + Constant.SALT_STRING + password));
	}
	
	/**
	 * This method is use to compare client & server password
	 * @param dbPassword storePassword
	 * @param clientPassword  clientPassword
	 * @return boolean
	 */
	public static boolean passwordVerification(String dbPassword, String clientPassword, String dynamicSalt){
		String securePassword = getSha512(dbPassword + dynamicSalt);
		if(clientPassword.equals(securePassword)){
			return true;
		}
		return false;
	}

}
