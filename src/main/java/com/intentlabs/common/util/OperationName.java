/**
 * 
 */
package com.intentlabs.common.util;

/**
 * @author Dhruvang
 *
 */
public interface OperationName {
	
	String GET  = "Get";
	String SAVE = "Save";
	String UPDATE = "Update";
	String DELETE = "Delete";
	String EDIT = 	"Edit";
	String VIEW = 	"View";
	String USER_REGISTRATION = "userRegistration";
	String INVITE_USER = "inviteUser";
	String APPROVE_USER = "approveUser";
	String ENABLE_DISABLE_USER = "enableDisableUser";
	String PRODUCT_ADDING = "productAdding";
	String ALL_PRODUCTS = "allProducts";
	String USER_LOGIN = "userLogin";
	String USER_LOGOUT = "userLogout";
	String DISPLAY_GRID = "Display Table";
	String USER_SESSION = "Fetch User Session";
	String GET_ALLCOUNTRY = "gellAllCountry";
	String GET_AllROLES = "getallRoles";
	String GET_ROL_FOR_CORDINATIRS = "getRolForCordinatirs";
	String GET_ALL_PRODUCTCTA = "getallProCat";
	String GET_COUNTRY_STATES = "getStates";
	String GET_STATE_DISTRICTS = "getDistricts";
	String GET_DISTRICTS_CITY = "getCity";
	String GET_CITY_VILLAGE = "getVillage";
	String DESIGN_FORM_TEMPLATE = "designFormTemplate";
	String GET_ALL_COMPANIES = "getAllCompanies";
	String GET_ALL_COMPANY_CATEGORY = "getAllCompanyCategory";
	String GET_ALLCPRODUCTS = "getAllproducts";
	String GET_ALLSUBCPRODUCTS = "getAllSubProducts";
	String GET_ALLSERVICES = "getAllServices";
	String GET_ALLSUBSERVICES = "getAll SubServices";
	String VERIFY_OTP = "VerigyOtp";
	String VERIFY_INCUBATOR_KYC = "verifyIncubatorKyc";
	String USER_FORGET_PASSWORD = "setForgetPassword";
	String USER_OTP = "otpGenerated";
	String CHANGE_PASSWORD = "ChangePassword";
	String CHECK_EMAIL = "checkEmail";
	String SAVE_USER_FIELDS = "saveUserFields";
	String USER_SET_NEW_PASSWOED = "set new password";
	String EVENT_OPERATION = "eventOperation";
	String INSTITUTE_OPERATION = "instituteOperation";
	String SI_USER_LOGOUT = "User logout";
	String GET_ALL_INSTITUTES = "getIntitutes";
	String DISPLAY_IMAGE = "showImages";
	String STORE_FILE = "Store File";
	String SAVE_IMAGE = "saveImages";
	String EDIT_AND_VIEW_OPERATION = "Edit and view Event";
	String EDIT_AND_VIEW_OOARATION = "editAndView";
	String GET_ALL_UPCOMING_EVENTS = "Get All Upcoming Events";
	String GET_ALL_PAST_EVENTS = "Get All Past Events";
	String GET_ALL_INSTITUTE_EVENTS = "Get All Institute Events";
	String SAVE_BLOG_POST = "Save Blog Post";
	String LIKE_BLOG_POST = "Like Blog Post";
	String FOLLOW_BLOG_POST = "Follow Blog Post";
	String GET_ALL_BLOG_POSTS = "Get All Blog Post";
	String GET_BLOG_POST = "Get Blog Post";
	String SAVE_JOB_POST = "Save Job Post";
	String GET_ALL_JOB_POSTS = "Get All Job Post";
	String GET_ALL_TAGS = "Get All Tag";
	String GET_JOB_POST = "Get Job Post";
	String CHANGE_EVENT_USER_STATUS = "Change Event User Status";
	String GET_ALL_EVENTS = "Get All Event";
	String GET_EVENT = "Get Event";
	String SAVE_EVENT = "Save Event";
	String SAVE_ATTACHMENT = "Save Attachment";
	String GET_ALL_EVENT_USERS = "Get All Event Users";
	String GET_STUDENT_CLUB = "Get Student Club";
	String GET_ALL_STUDENT_CLUBS = "Get All Student Clubs";
	String SAVE_STUDENT_CLUB = "Save Student Club";
	String DELETE_STUDENT_CLUB = "Delete Student Club";
	String GET_RESEARCH_RESOURCE = "Get Research Resource";
	String GET_ALL_RESEARCH_RESOURCES = "Get All Research Resources";
	String DELETE_RESEARCH_RESOURCE = "Delete Research Resource";
	String SAVE_RESEARCH_RESOURCE = "Save Research Resource";
	String GET_USER_PROFILE = "Get User Profile";
	String SAVE_USER_PROFILE = "Save User Profile";
	String DELETE_BLOG_POST = "Delete Blog Post";
	String DELETE_JOB_POST = "Delete Job Post";
	String GET_ALL_FACULTIES = "Get All Faculties";
}
