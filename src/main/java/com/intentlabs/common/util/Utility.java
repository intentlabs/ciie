/**
 * 
 */
package com.intentlabs.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.jsoup.Jsoup;

import com.intentlabs.common.enums.EnumType;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.view.KeyValueView;

/**
 * @author Dhruvang
 *
 */
public class Utility {

	private static final String UTILITY = "Utility";

	private Utility() {
		//
	}

	private static ThreadLocal<MessageDigest> md5MessageDigest = new ThreadLocal<MessageDigest>() {
		@Override
		protected MessageDigest initialValue() {
			try {
				return MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
		}
	};

	/**
	 * This methods is used to convert EnumType list into KeyValueView list.
	 * 
	 * @param list
	 *            list of EnumType
	 * @return listView list of KeyValueView
	 */
	public static List<KeyValueView> toKeyValueView(EnumType[] list) {
		List<KeyValueView> listView = new ArrayList<>(list.length);
		for (EnumType enumType : list) {
			listView.add(KeyValueView.create((long) enumType.getId(), enumType.getName().toUpperCase()));
		}
		Collections.sort(listView, KeyValueView.idComparator);
		return listView;
	}

	public static SecureRandom rnd = new SecureRandom();

	/**
	 * This method is used to create a new folder on given path. If folder is
	 * already exist then return's the current folder.
	 * 
	 * @param pathName
	 *            path where folder needs to be created
	 * @return {@link File}
	 */
	public static File createNewFolder(String pathName) {
		File dir = new File(pathName);
		if (dir.exists() == false) {
			dir.mkdirs();
		}
		return dir;
	}

	/**
	 * This method is used to create a new file on give path.
	 * 
	 * @param pathName
	 *            path where file needs to be created
	 * @return {@link File}
	 * @throws IOException
	 */
	public static File createNewFile(String pathName) throws IOException {
		File file = new File(pathName);
		if (file.exists()) {
			return null;
		}
		file.createNewFile();
		return file;
	}

	/**
	 * This method gives file name without extension
	 * 
	 * @param file
	 * @return fileName without extension
	 */
	public static String getFileNameWithOutExtension(File file) {
		return FilenameUtils.removeExtension(file.getName());
	}

	/**
	 * This methods is used to convert EnumType list into KeyValueView list.
	 * 
	 * @param list
	 *            list of EnumType
	 * @return listView list of KeyValueView
	 */
	public static List<KeyValueView> toKeyValueView(EnumSet list) {
		List<KeyValueView> listView = new ArrayList<>(list.size());

		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			EnumType object = (EnumType) iterator.next();
			listView.add(KeyValueView.create((long) object.getId(), object.getName().toUpperCase()));
		}

		Collections.sort(listView, KeyValueView.idComparator);
		return listView;
	}

	public static void copyFileUsingFileChannels(File source, File dest) throws IOException {

		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			inputChannel.close();
			outputChannel.close();
		}
	}

	public static String generateRandomNumber(Integer length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++)
			sb.append(Constant.DIGITS.charAt(rnd.nextInt(Constant.DIGITS.length())));
		return sb.toString();
	}
	
	public static String generateRandomNumberAndString(Integer length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++)
			sb.append(Constant.ALPHABETS.charAt(rnd.nextInt(Constant.DIGITS.length())));
		//sb.append(Constant.ALPHABETS.charAt(rnd.nextInt(Constant.ALPHABETS.length())));
		return sb.toString();
	}

	public static boolean isOtpExpired(Date otpGeneratedDate) {
		/*
		 * if(otpExpired == CommonStatus.YES.getId()){ return true; }
		 */
		Date currentDate = new Date();
		// return
		// currentDate.compareTo(Utility.getDateAfterHours(otpGeneratedDate,
		// SystemConfig.getOtpExpiredTimeInHours())) > 0;
		return currentDate.compareTo(Utility.getDateAfterHours(otpGeneratedDate, 1)) > 0;
	}

	public static Date getDateAfterHours(Date date, int hours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + hours);
		return calendar.getTime();
	}

	/**
	 * This method is used to generating random Token.
	 * 
	 * @param otpLength
	 * @return String number
	 */
	public static String generateCode(int codeLength) {
		return RandomStringUtils.randomAlphanumeric(codeLength);
	}

	public static String toHex(byte[] array) {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			String format = "%0" + paddingLength + "d";
			return String.format(format, 0) + hex;
		} else {
			return hex;
		}
	}

	public static String getMD5(String hash) {
		md5MessageDigest.get().update(hash.getBytes());
		return toHex(md5MessageDigest.get().digest());
	}

	public static String generateSession(String userId) {
		return getMD5(userId + Constant.SALT_STRING
				+ DateUtility.getDateAfterDays(DateUtility.getCurrentDate(), 365 * 10) + generateRandomNumber(8));
	}

	public static Long getLongFromString(String value) {
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			LoggerService.exception(e);
			return null;
		}
	}

	public static String convertHtmltestToSimpleText(String html) {

		return Jsoup.parse(html).text();

	}

	public static void deleteFile(File file) {
		if (file.exists()) {
			file.delete();
		}
	}
}
