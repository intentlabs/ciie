/**
 * 
 */
package com.intentlabs.common.util;

/**
 * @author Dhruvang
 *
 */
public interface LogMessage {

	
	String REQUEST_COMPLETED = "request completed";
	String BEFORE_VALIDATION = "Before validating data";
	String BEFORE_CALLING_OPERATION = "Before calling operation";
	String BEFORE_CREATING_FILE = "Before File Creation";
	String FILE_CREATED_SUCCESSFULLY = "File created successfully";
	String BEFORE_REDIRECT_TO_SOCIAL_LOGIN = "Before Redirect to  Social Login";
}
