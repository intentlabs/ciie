package com.intentlabs.common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;




public class CookieUtility {
	
	public static final String COOKIE_FOR_USER_ID = "ALeague";
	public static final String COOKIE_PATH = HttpRequestParam.getContextPath()+"portal/home/";
    
	private CookieUtility(){
	}
	
	/**
	 * THis method will get cookie value from cookie name.
	 * @param cookieName
	 * @param defaultVal
	 * @return
	 */
    public static String getCookie(String cookieName, String defaultVal) {
        Cookie[] cookies = WebUtil.getCurrentRequest().getCookies();
        if (cookies == null) {
            return defaultVal;
        }
        for (Cookie cookie : cookies) {
            if (cookie.getName().equalsIgnoreCase(cookieName) && StringUtils.isNotBlank(cookie.getValue())) {
                return cookie.getValue();
            }
        }
        return defaultVal;
    }
    
    /**
     * this method will return all the cookies whose name contains  specific string.
     * @param defaultVal
     * @return
     */
    public static String getCookiesBasedOnSufix(String defaultVal) {
   	 String cookiesNames ="";
        Cookie[] cookies = WebUtil.getCurrentRequest().getCookies();
        if (cookies == null) {
            return defaultVal;
        }
        for (Cookie cookie : cookies) {
            if (cookie.getName().contains(COOKIE_FOR_USER_ID)) {
           	 cookiesNames += cookie.getName() + ",";
            }
        }
        return cookiesNames;
    }

    public static void setCookie(HttpServletResponse response, String cookieName, String value) {
        if (response == null || StringUtils.isBlank(cookieName) || StringUtils.isBlank(value)) {
            return;
        }
        response.addCookie(createCookie(cookieName, value, null, null));
    }

    public static void setCookie(HttpServletResponse response, String cookieName, String value, String path) {
        if (response == null || StringUtils.isBlank(cookieName) || StringUtils.isBlank(value)) {
            return;
        }
        response.addCookie(createCookie(cookieName, value, null, path));
    }

    public static void setCookie(HttpServletResponse response, String cookieName, String value, Integer maxAge, String path) {
        if (response == null || StringUtils.isBlank(cookieName) || StringUtils.isBlank(value)) {
            return;
        }
        response.addCookie(createCookie(cookieName, value, maxAge, path));
    }

    /**
     * THis method will add cookie to response object.
     * @param cookieName
     * @param value
     * @param maxAge  : life time of cookie.
     * @param path
     * @return
     */
    private static Cookie createCookie(String cookieName, String value, Integer maxAge, String path) {
        Cookie cookie = new Cookie(cookieName, value);
        if (maxAge == null) {
            return cookie;
        }
        cookie.setMaxAge(maxAge.intValue() < 1 ? Integer.MAX_VALUE : maxAge.intValue());
        cookie.setPath(StringUtils.isNotBlank(path) ? path : "/");
        return cookie;
    }

    /**
     * it will remove all cookies from browser.
     * @param response
     */
    public static void removeAllCookies(HttpServletResponse response) {
        Cookie[] cookies = WebUtil.getCurrentRequest().getCookies();
        for (Cookie cookie : cookies) {
        	cookie.setMaxAge(0);
            response.addCookie(cookie);
        }
    }
    
    /**
     * it will remove specific cookie from browser.
     * @param response
     * @param cookieTobeDeleted
     */
    public static void removeSpecifiedCookie(HttpServletResponse response, String cookieTobeDeleted) {
        Cookie[] cookies = WebUtil.getCurrentRequest().getCookies();
        for (Cookie cookie : cookies) {
       	 if(cookie.getName().equals(cookieTobeDeleted)){
       		cookie.setMaxAge(0);
       		cookie.setPath(COOKIE_PATH);
            response.addCookie(cookie);
       	 }
       }
    }

    /**
     * @return : expire time of cookies.
     */
    public static int getCookieExpirationTime(){
    	int cookieExpTimeInDays = 5;//SystemConfig.getForcePasswordChangeDays();
		return getCookieExpirationTimeEquation(cookieExpTimeInDays);
    }
    
    /*
     * equation of cookie expire time.
     */
    public static int getCookieExpirationTimeEquation(int cookieExpTimeInDays){
    	return cookieExpTimeInDays * 24 * 60 * 60;
    }
    
    /*
     * return cookie name.
     */
    public static String getCookieName(Long userId){
     	return COOKIE_FOR_USER_ID+String.valueOf((userId*214)+214);
     }
     
    /**
     * return cookie value.
     */
    public static String getCookieValue(String uuid){
     	return uuid;
     }

    /**
     * will return user id from cookie name.
     * @param cookieName
     * @return
     */
    public static Long getUserIdFromCookieName(String cookieName){
    	 String id = cookieName.replace(COOKIE_FOR_USER_ID, "");
    	 return (Utility.getLongFromString(id)==null)? null : ((Utility.getLongFromString(id)-214)/214);
     }
   
}