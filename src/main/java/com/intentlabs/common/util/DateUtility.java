/**
 * 
 */
package com.intentlabs.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Dhruvang
 *
 */
public class DateUtility {
	
	private static DateFormat dateFormatWithTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DateFormat dateFormatWithOutTime = new SimpleDateFormat("yyyy-MM-dd");
	
	private static DateFormat discussion = new SimpleDateFormat("dd MMMM yyyy, HH:mm");
	private static DateFormat onlyDate = new SimpleDateFormat("dd/MM/yyyy");
	private static final ThreadLocal<DateFormat> DATE_FORMAT_WITHOUT_TIME = new ThreadLocal<DateFormat>() {
		@Override
    	protected DateFormat initialValue() {
			return dateFormatWithOutTime;
		}
	};
	
	   private static final ThreadLocal<DateFormat> DATE_FORMAT_WITH_TIME = new ThreadLocal<DateFormat>() {
			@Override
	    	protected DateFormat initialValue() {
				return dateFormatWithTime;
			}
		};
		
		private static final ThreadLocal<DateFormat> DISCUSSION = new ThreadLocal<DateFormat>() {
			@Override
	    	protected DateFormat initialValue() {
				return discussion;
			}
		};
		
		private static final ThreadLocal<DateFormat> ONLYDATE = new ThreadLocal<DateFormat>() {
			@Override
	    	protected DateFormat initialValue() {
				return onlyDate;
			}
		};
		
		private DateUtility(){
		}
		
		/**
	     * Will convert date to string base on given locale
	     * @param date
	     * @return
	     */
	    public static String toDateTimeString(Date date) {
	        if (date == null) {
	            return null;
	        }
	        return DATE_FORMAT_WITH_TIME.get().format(date);
	    }
	    
	    public static String toDateTimeString(Date date, SimpleDateFormat dateFormat) {
	        if (date == null) {
	            return null;
	        }
	        
	        if(dateFormat != null){
	        	return dateFormat.format(date);
	        }
	        
	        return DATE_FORMAT_WITH_TIME.get().format(date);
	    }
	    
	    /**
	     * Will convert date to string base on given locale
	     * @param date
	     * @return
	     */
	    public static String toDateString(Date date) {
	        if (date == null) {
	            return null;
	        }
	        return DATE_FORMAT_WITHOUT_TIME.get().format(date);
	    }
	    
	    /**
	     * To convert date into string
	     * @param date
	     * @return
	     */
	    public static String toDateStringForDiscussion(Date date) {
	        if (date == null) {
	            return null;
	        }
	        return DISCUSSION.get().format(date);
	    }
	    
	    
	    /**
	     * Will return current date.
	     * @return Date
	     */
	    public static Date getCurrentDate() {
	       return new Date();
	    }
	    
	    /**
	     * This method used to get date after given days. 
	     * @param date
	     * @param day
	     * @return Date
	     */
	    public static Date getDateAfterDays(Date date, int day) {
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + day);
	        return calendar.getTime();
	    }
	    
	    
	    
	    public static Date getDateBeforeDays(Date date, int day) {
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - day);
	        return calendar.getTime();
	    }
	    
	    public static long getDiffDaysFromCurrentDate(Date date){
			long diff = getCurrentDate().getTime() - date.getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);
			return diffDays;		
	    }
	    
	    public static Date toDate(String date, SimpleDateFormat dateFormat){
	    	try {
	    		if(date != null && !"".equals(date)){
	    			if(dateFormat != null){
	    				return dateFormat.parse(date);
	    	    	}
	    	        return DATE_FORMAT_WITHOUT_TIME.get().parse(date);
	    	    	
	    		}else{
	    			return null;
	    		}
	    	} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
	    
	    }
	    
	    public static Date toDateTime(String date, SimpleDateFormat dateFormat) {
	    	
			try {
				if (dateFormat != null) {
					return dateFormat.parse(date);
				}
				return DATE_FORMAT_WITH_TIME.get().parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return null;
			
	    }
	    
	    /**
	     * This method is used to get date before given mintues
	     * @param date
	     * @param minute
	     * @return Date
	     */
	    public static Date getDateBeforeMinute(Date date, int minute) {
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - minute);
	        return calendar.getTime();
	    }
	    
	    public static Date getOnlyDate(){
	    	try {
				return ONLYDATE.get().parse((ONLYDATE.get().format(getCurrentDate())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    	return null;
	    }

}
