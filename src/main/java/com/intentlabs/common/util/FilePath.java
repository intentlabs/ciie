/**
 * 
 */
package com.intentlabs.common.util;

import java.io.File;

/**
 * @author Dhruvang
 *
 */
public interface FilePath {
	
	String DIRECTORY_SEPERATOR = File.separator;
	String PNG_EXTENSION = ".PNG";
    //String DIRECTORY_ROOT_PATH =  "/home/A-Leauge" ;
	String DIRECTORY_ROOT_PATH =  "/home/A-League" ;
    String STARTUP_IMAGE_PATH = DIRECTORY_ROOT_PATH + DIRECTORY_SEPERATOR + "images";
    //String SERVER_URL = "http://localhost:8080/ALeague/attachments/";
    //String SERVER_URL = "http://192.168.43.32:8080/ALeague/attachments/";
    String SERVER_URL = "http://portal.a-league.org/ALeague/attachments/";
}
