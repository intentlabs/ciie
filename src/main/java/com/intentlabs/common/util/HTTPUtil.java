package com.intentlabs.common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.intentlabs.common.logger.LoggerService;



public final class HTTPUtil {
	
	static String  HTTPUtil = "HTTPUtil";
	
	public static String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
		String sessionId = null;
		Cookie cookies[] = httpRequest.getCookies();

		if(!StringUtils.isBlank(httpRequest.getHeader(Constant.USER_SESSION_STRING))){
			return httpRequest.getHeader(Constant.USER_SESSION_STRING);
		}else if (cookies == null) {
			LoggerService.error(HTTPUtil, OperationName.USER_SESSION,
					"Unable to find cookies, Fos ID, " + httpRequest.getRequestedSessionId());
			return sessionId;
		}

		for (Cookie cookie : cookies) {
			if (Constant.USER_SESSION_STRING.equals(cookie.getName())) {
				sessionId = cookie.getValue();
				LoggerService.debug(HTTPUtil, OperationName.USER_SESSION, "sessionID, " + sessionId);
				break;
			}
		}
		return sessionId;
	}

}
