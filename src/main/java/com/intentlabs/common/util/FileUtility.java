/**
 * 
 */
package com.intentlabs.common.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import org.springframework.web.multipart.MultipartFile;

import com.intentlabs.common.logger.LoggerService;

/**
 * @author Dhruvang
 *
 */
public class FileUtility {
	public static String setPath(String path){
		return FilePath.SERVER_URL + path;
	}
	
	public static String storeFile(MultipartFile file, String filetype) throws Exception {
		/* /home/aleague/images/ */
		String destinationPath = Utility
				.createNewFolder(
						FilePath.STARTUP_IMAGE_PATH /*+ FilePath.DIRECTORY_SEPERATOR + Auditor.getAuditor().getId()*/)
				.getPath() + FilePath.DIRECTORY_SEPERATOR;

		String filename = "";
		try {
			File tempFile = new File(destinationPath);
			if (tempFile.exists() && tempFile.isDirectory()) {

			} else {
				tempFile.mkdirs();
			}

			String originalFileName = file.getOriginalFilename();
			String ext = originalFileName.substring(originalFileName.lastIndexOf(".")).toUpperCase();
			filename = filetype + originalFileName.replaceAll(" ", "").substring(0, originalFileName.lastIndexOf(".")) + DateUtility.toDateTimeString(DateUtility.getCurrentDate(), new SimpleDateFormat("ddMMyyyyhhmmss")) + ext;
			File attachmentFile = new File(destinationPath + File.separator + filename);
			file.transferTo(attachmentFile);
		} catch (IOException e) {
			LoggerService.exception(e);
			filename = "";
		}
		
		return filename;
	}

}
