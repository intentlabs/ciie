package com.intentlabs.common.oparation;

import java.io.Serializable;

/**
 * This is a marker interface used to identifies operation.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */

public interface Operation extends Serializable { }
