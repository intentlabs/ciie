package com.intentlabs.common.oparation;

import java.io.IOException;

import com.intentlabs.common.response.Response;
import com.intentlabs.common.view.IdentifierView;




/**
 * This is a marker interface used to identifies operation.
 * @author Core team.
 * @version 1.0
 * @param <V>
 */
public interface BaseOperation <V extends IdentifierView> extends Operation{
	/**
	 * This method used when to perform add operation
	 * @return Response
	 */
	 Response doAddOperation() throws IOException, Exception;
	/**
	 * This method used when to perform edit operation
	 * @param id model unique identifier
	 * @return Response
	 * @throws InvalidModelAndViewRequestException
	 * @throws InvalidModelException
	 */
	 Response doEditOpeartion(Long id) throws IOException, Exception;
    /**
     * This method use for when  view operation is perform on model
     * @param id model unique identifier
     * @return Response
	 * @throws InvalidModelAndViewRequestException
     */
	 Response doViewOperation(Long id) throws Exception;

    /**
     * This method is used when save operation performed 
     * @param view using view set all model property
     * @return Response
     */
	 Response doSaveOperation(V view) throws Exception;
	 /**
     * This method is used when edit operation performed 
     * @param view using view set all model property
     * @return WebResponseView
     * @throws InactiveModelException
     */
	 Response doUpdateOperation(V view) throws Exception;
    /**
     * This method used for grid display with pagination and Search Purpose 
     * @param start starting  value of fetch record use for limit purpose
     * @param end end value of fetch record use for limit purposes
     * @return Response
     */
	 Response doDisplayGridOperation(Integer start, Integer end);

    /**
     * This method use to perform delete operation 
     * @param id unique identifier of model to be delete
     * @return  Response
     */
	 Response doDeleteOperation(Long id);
    
    
}