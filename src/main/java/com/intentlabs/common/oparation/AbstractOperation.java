package com.intentlabs.common.oparation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.intentlabs.common.enums.ResponseCode;
import com.intentlabs.common.logger.LoggerService;
import com.intentlabs.common.model.AuditableModel;
import com.intentlabs.common.model.Model;
import com.intentlabs.common.model.PageModel;
import com.intentlabs.common.response.CommonResponse;
import com.intentlabs.common.response.PageResultResponse;
import com.intentlabs.common.response.Response;
import com.intentlabs.common.service.BaseService;
import com.intentlabs.common.util.CustomMessage;
import com.intentlabs.common.util.OperationName;
import com.intentlabs.common.view.IdentifierView;




/**
 * This class is provide transaction wrapper ,Actual transaction begin over here 
 * contains common operation and list of abstract method    
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 * @param <M> model parameter
 * @param <V> view of that model
 */

@Component
@Transactional(propagation=Propagation.REQUIRED)
public abstract class AbstractOperation<M extends Model, V extends IdentifierView> {
	
	static final String ABSTRACT_OPERATION = "AbstractOperation";
	 /**
	 * This method use to convert view to model 
	 * @param model model of entity
	 * @param view view of model 
	 * @return model 
	 */
	
	public abstract M toModel(M model, V view) throws Exception;
	/**
	 * This method use to get original model from view of entity
	 * @param view view of model
	 * @return model
	 */
	
	protected M getModel(V view) throws Exception {
        return toModel(getNewModel(view), view);
    }
	
	/**
	 *This method used when require new model for view 
	 * @param view view of model
	 * @return model
	 */
    protected abstract M getNewModel(V view);

    /**
     * This method load original model from view  
     * @param view view of model
     * @return model
     */
    @SuppressWarnings("unchecked")
	protected M loadModel(V view) {
        return (M) getService().get(view.getId());
    }
    
	/**
	 * This method used when need to convert model to view
	 * @param model model
	 * @return view 
	 */
	public abstract V fromModel(M model);
	
	/**
	 * This method convert list of model to list of view
	 * @param modelList list of model
	 * @return list of view
	 */
	public List<V> fromModelList(List<M> modelList) {
        List<V> viewList = new ArrayList<>(modelList.size());
        for (M model : modelList) {
            viewList.add(fromModel(model));
        }
        return viewList;
    }
	
	/**
	 * This method use for get Service respected operation
	 * @return BaseService
	 */
	@SuppressWarnings("rawtypes")
	public abstract BaseService getService();
	
	/**
	 * This method used when to perform add operation
	 * @return ModelAndView
	 */
	public abstract Response doAddOperation()  throws IOException, Exception;

	/**
	 * This method used when to perform edit operation
	 * @param id model unique identifier
	 * @return Response
	 * @throws InvalidModelAndViewRequestException
	 * @throws InactiveModelException
	 */
    public abstract Response doEditOpeartion(Long id) throws IOException, Exception;

    /**
     * This method use for when  view operation is perform on model
     * @param id model unique identifier
     * @return Response
	 * @throws InvalidModelAndViewRequestException
     */
    public abstract Response doViewOperation(Long id) throws Exception;

    /**
     * This method is used when save operation performed 
     * @param view using view set all model property
     * @return Response
     */
    @SuppressWarnings("unchecked")
	public Response doSaveOperation(V view) throws Exception{
    	Model model = getModel(view);
        getService().create(model);
        return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), CustomMessage.DATA_SAVED);
    }
    	

    /**
     * This method is used when edit operation performed 
     * @param view using view set all model property
     * @return Response
	 * @throws InactiveModelException
     */
    @SuppressWarnings("unchecked")
	public Response doUpdateOperation(V view) throws Exception{
        M model = loadModel(view);
        if (model == null) {
            return CommonResponse.create(ResponseCode.INTERNAL_SERVER_ERROR.getCode(), ResponseCode.INTERNAL_SERVER_ERROR.getMessage());
        }
     /*   if (model instanceof AuditableModel && isSameVersion(view, (AuditableModel) model) == false) {
        	return CommonResponse.create(ResponseCode.INTERNAL_SERVER_ERROR.getCode(), CustomMessage.ALREADY_UPDATED);
        }*/
        checkInactive(model);
        getService().update(toModel(model, view));
        return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), CustomMessage.DATA_UPDATED);
    }
    
    /**
     * This method used for grid display with pagination and Search Purpose 
     * @param start starting  value of fetch record use for limit purpose
     * @param end end value of fetch record use for limit purposes
     * @return Response
     */
    @SuppressWarnings("unchecked")
	public Response doDisplayGridOperation(Integer start, Integer end) {
        PageModel result = getService().getGridData(start, end);
        if (result.getRecords() == 0) {
        	LoggerService.info(this.getClass().getSimpleName(), OperationName.DISPLAY_GRID, "No Data Found");
            return PageResultResponse.create(ResponseCode.INTERNAL_SERVER_ERROR.getCode(), ResponseCode.INTERNAL_SERVER_ERROR.getMessage(), 0, Collections.EMPTY_LIST);
        }
        return PageResultResponse.create(ResponseCode.SUCCESSFUL.getCode(),ResponseCode.SUCCESSFUL.getMessage(), result.getRecords(), fromModelList((List<M>) result.getList()));
    }
    
    

    /**
     * This method use to perform delete operation 
     * @param id unique identifier of model to be delete
     * @return  Response
     */
    public Response doDeleteOperation(Long id){
        getService().delete(id);    
        return CommonResponse.create(ResponseCode.SUCCESSFUL.getCode(), CustomMessage.DATA_DELETED);
    }

    /**
     * This method use to check while edit operation whether model is same version 
     * @param view view of model
     * @param model actual model
     * @return boolean
     */
    protected boolean isSameVersion(IdentifierView view, AuditableModel model) {
        return model.getLockVersion().equals(view.getVersion());
    }
    
    /**
     * This method is used to validate active or inactive state of model.
     * @param model
     * @throws InactiveModelException
     */
    protected abstract void checkInactive(M model) throws Exception;
}
