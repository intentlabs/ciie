package com.intentlabs.common.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author Dhruvang
 *
 */
@MappedSuperclass
public abstract class UUIDIdentifierModel implements Model {

	
	private static final long serialVersionUID = -1708648662915539470L;
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "pkUUID", updatable = false, nullable = false,length=128)
	private String uuid;

}
