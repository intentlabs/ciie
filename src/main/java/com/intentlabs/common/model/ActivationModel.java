/**
 * 
 */
package com.intentlabs.common.model;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.common.enums.ActiveInActive;

/**
 * @author Dhruvang
 *
 */

@MappedSuperclass
public abstract class ActivationModel extends AuditableModel {

	
	private static final long serialVersionUID = 6510352715534906544L;
	
	@Column(name = "enumAct")
	@Access(AccessType.FIELD)
	private long activationStatus;
	
	@Column(name = "dateActChange")
    private Date activationDate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fkActChangeBy")
	private User activationChangeBy;

	public ActiveInActive getActivationStatus() {
		return ActiveInActive.fromId(activationStatus);
	}

	public void setActivationStatus(ActiveInActive activationStatus) {
		this.activationStatus = activationStatus.getId();
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}

	public User getActivationChangeBy() {
		return activationChangeBy;
	}

	public void setActivationChangeBy(User activationChangeBy) {
		this.activationChangeBy = activationChangeBy;
	}
	
	

}
