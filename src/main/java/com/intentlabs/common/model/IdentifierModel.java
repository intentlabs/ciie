/**
 * 
 */
package com.intentlabs.common.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author Dhruvang
 *
 */

@MappedSuperclass
public abstract class IdentifierModel implements Model {

	
	private static final long serialVersionUID = -7471747631485155136L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pkId", updatable = false, nullable = false,length=20)
	private Long id;
	
	public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

}
