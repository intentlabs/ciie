package com.intentlabs.common.model;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.intentlabs.aleague.user.model.User;

/**
 * @author Dhruvang
 *
 */

@MappedSuperclass
public class ArchiveModel extends ActivationModel {


	private static final long serialVersionUID = 2598384568984369681L;
	
	
	@Column(name = "enumArchive")
	@Access(AccessType.FIELD)
	private String enumArchive;
	
	@Column(name = "dateArchive")
	private Date dateArchive;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fkArchiveBy")
	private User fkArchiveBy;


	public String getEnumArchive() {
		return enumArchive;
	}


	public void setEnumArchive(String enumArchive) {
		this.enumArchive = enumArchive;
	}


	public Date getDateArchive() {
		return dateArchive;
	}


	public void setDateArchive(Date dateArchive) {
		this.dateArchive = dateArchive;
	}


	public User getFkArchiveBy() {
		return fkArchiveBy;
	}


	public void setFkArchiveBy(User fkArchiveBy) {
		this.fkArchiveBy = fkArchiveBy;
	}
	
	

}
