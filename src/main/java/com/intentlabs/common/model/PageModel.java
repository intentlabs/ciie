/**
 * 
 */
package com.intentlabs.common.model;

import java.util.List;

/**
 * @author Dhruvang
 *
 */
public class PageModel implements Model  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9014141070685701820L;
	
	private List<? extends Model> list;
    private long records;

    public PageModel(List<? extends Model> list, long records) {
        this.list = list;
        this.records = records;
    }

    public static PageModel create(List<? extends Model> list, long records) {
        return new PageModel(list, records);
    }

    public List<? extends Model> getList() {
        return list;
    }

    public void setList(List<? extends Model> list) {
        this.list = list;
    }

    public void setRecords(long records) {
        this.records = records;
    }

    public long getRecords() {
        return records;
    }

}
