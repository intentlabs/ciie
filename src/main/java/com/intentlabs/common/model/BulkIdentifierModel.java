/*******************************************************************************
 * Copyright -2017 @IntentLabs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.intentlabs.common.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * This is bulk identifier interface which maps primary key of table to model.
 * This model will be used in case bulk insert as hibernate doesn't support
 * batch insert in case of auto_increment.
 * @author Nirav.Shah
 * @since 02/08/2017
 */
@MappedSuperclass
public abstract class BulkIdentifierModel implements Model {

	
	private static final long serialVersionUID = -7471747631485155136L;
	
	@Id
    @Column(name = "pkId", updatable = false, nullable = false,length=10)
	private Long id;
	
	public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
}
