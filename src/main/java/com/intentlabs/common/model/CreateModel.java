/**
 * 
 */
package com.intentlabs.common.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.intentlabs.aleague.user.model.User;

/**
 * @author Dhruvang
 *
 */

@MappedSuperclass
public abstract class CreateModel extends IdentifierModel {

	
	private static final long serialVersionUID = 7484427777263634009L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkCreateBy")
	private User createBy;
	
	@Column(name = "dateCreate")
	private Date createDate;

	public User getCreateBy() {
		return createBy;
	}

	public void setCreateBy(User createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
}
