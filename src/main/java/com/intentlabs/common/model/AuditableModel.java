/**
 * 
 */
package com.intentlabs.common.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.intentlabs.aleague.user.model.User;

/**
 * @author Dhruvang
 *
 */

@MappedSuperclass
public class AuditableModel extends CreateModel {

	
	private static final long serialVersionUID = 4830734796526520159L;
	
	@Version
	private Long lockVersion;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fkUpdateBy")
	private User updateBy;
	
	@Column(name = "dateUpdate")
    private Date updateDate;

	public Long getLockVersion() {
		return lockVersion;
	}

	public void setLockVersion(Long lockVersion) {
		this.lockVersion = lockVersion;
	}

	public User getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(User updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
	

}
