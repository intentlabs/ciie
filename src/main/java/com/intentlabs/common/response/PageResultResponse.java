package com.intentlabs.common.response;

import java.util.List;

import com.intentlabs.common.view.KeyValueView;
import com.intentlabs.common.view.View;




/**
 * This is used to render grid data on screen.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public class PageResultResponse extends CommonResponse {

	private static final long serialVersionUID = -1698438611739275048L;
	private List<? extends View> list;
	private List<KeyValueView> keyValueList;
    private long records;

    private PageResultResponse(int responseCode, String message, long records, List<? extends View> list){
    	super(responseCode, message);
    	this.records = records;
    	this.list = list;
    }

    public static PageResultResponse create(int responseCode, String message,long records, List<? extends View> list) {
        return new PageResultResponse(responseCode, message, records, list);
    }

	public List<? extends View> getList() {
		return list;
	}

	public long getRecords() {
		return records;
	}
	
	public List<KeyValueView> getCountyList(){
		return keyValueList;
	}
}
