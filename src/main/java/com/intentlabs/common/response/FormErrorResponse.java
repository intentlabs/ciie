package com.intentlabs.common.response;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a form response view which used to validation message inside form.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public class FormErrorResponse extends CommonResponse{

	private static final long serialVersionUID = 3944857319523952086L;
	private Map<String, String> formValidationDetailMap = new HashMap<>();
    
    protected FormErrorResponse(int responseCode, String message, Map<String, String> formValidationDetailMap) {
    	super(responseCode,message);
        this.formValidationDetailMap = formValidationDetailMap;
    }
    
    public static FormErrorResponse create(int responseCode, String message, Map<String, String> formValidationDetailMap) {
        return new FormErrorResponse(responseCode, message, formValidationDetailMap);
    }
    
	public Map<String, String> getFormValidationDetailMap() {
		return formValidationDetailMap;
	}  
}
