package com.intentlabs.common.response;

import com.intentlabs.common.view.View;

/**
 * This is a form response on single view.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public class ViewResponse extends CommonResponse{

	private static final long serialVersionUID = 3944857319523952086L;
    private View view;
    
	protected ViewResponse(int responseCode, String message, View view) {
		super(responseCode, message);
		this.view = view;
	}
	
	public static ViewResponse create(int responseCode, String message, View view){
		return new ViewResponse(responseCode, message, view);
	}
    public View getView(){
    	return view;
    }
    
}
