package com.intentlabs.common.response;

import java.awt.image.BufferedImage;

public class ImageResponse extends CommonResponse {
	
	private static final long serialVersionUID = -1698438611739275048L;
	private BufferedImage img;
	
	
	private ImageResponse(int responseCode, String message, BufferedImage img) {
		super(responseCode, message);
		this.img = img;
	}


	public static ImageResponse create(int responseCode, String message, BufferedImage img) {
	return new ImageResponse(responseCode, message, img);
	}


	public BufferedImage getImg() {
		return img;
	}


	public void setImg(BufferedImage img) {
		this.img = img;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

}
