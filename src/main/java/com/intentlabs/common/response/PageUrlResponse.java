package com.intentlabs.common.response;

public class PageUrlResponse extends CommonResponse {
	
	
	private static final long serialVersionUID = -1698438611739275048L;
	private String url;
	
    private PageUrlResponse(int responseCode, String message, String url){
    	super(responseCode, message);
    	this.url = url;
    }

    public static PageUrlResponse create(int responseCode, String message, String url) {
        return new PageUrlResponse(responseCode, message,url);
    }
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
