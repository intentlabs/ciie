package com.intentlabs.common.response;

import java.util.List;

public class PageEnumsListResponse extends CommonResponse {

	
	private static final long serialVersionUID = -8148207878979024229L;
	
	
	
	private List<String> list;
	private long records;
	
	

	protected PageEnumsListResponse(int responseCode, String message, long records, List<String> list) {
		super(responseCode, message);
    	this.records = records;
    	this.list = list;

	}

	
	   public static PageEnumsListResponse create(int responseCode, String message,long records, List<String> list) {
	        return new PageEnumsListResponse(responseCode, message, records, list);
	    }
	   

		public List<String> getList() {
			return list;
		}

		public long getRecords() {
			return records;
		}

}
