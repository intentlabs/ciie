package com.intentlabs.common.response;


/**
 * This is a common response which is used to response every request.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public class CommonResponse implements Response{
	
	private static final long serialVersionUID = 3217452268355902474L;
	private int responseCode;
    private String message;
    
    protected CommonResponse(int responseCode, String message) {
        this.responseCode = responseCode;
        this.message = message;
    }
 
    public static CommonResponse create(int responseCode, String message) {
        return new CommonResponse(responseCode, message);
    }
       
	public int getResponseCode() {
		return responseCode;
	}

	public String getMessage() {
		return message;
	}
}
