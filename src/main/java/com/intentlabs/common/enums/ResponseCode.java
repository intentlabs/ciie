package com.intentlabs.common.enums;

import java.io.Serializable;

/**
 * This is used to give response code & message to client request.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public enum ResponseCode implements Serializable{
	
	SUCCESSFUL(1000,"Successful"),
	ALREADY_EXISTS(2000,"Already Exists"),
	INTERNAL_SERVER_ERROR(1001,"System is unable to process the request."),
	INVALID_REQUEST(1003, "Invalid request."),
	INVALID_FORM_DATA(1004,"Invalid Form Data."),
	INVALID_PAGE_REQUEST(1005, "Unable to load requested page."),
	INVALID_OTP(1008, "Invalid OTP."),
	INVALID_PASSWORD(1010, "Invalid Old Password."),
	FIELD_EXECUTIVE_INVALID_SESSION(1012, "Session is expired. Please login again."),
	ALREADY_USED_PASSWORD(1009, "Password is already used. Can't use from last five passwords."),
	HTTP_UNAUTHORIZED(401, "Unauthorized User"),
	DOMAIN_NAME_IS_NOT_FOUND(0, "Domain name is not found"),
	PROVIDE_INSTITUTE_DETAILS(1010,"Institute detail is required."),
	EMAIL_ALREADY_EXISTS(1014,"Your email is already registered on the portal. Please login using registered email id."),
	INACTIVE_ACCOUNT(1011,"Your email id is already registered on the portal. Please verify and activate your account by the link sent in your mail."),
	APPROVAL_REQUIRED(1012,"Your account approval is pending."),
	UNAUTHORIZE_USER(1013,"Unauthorized user"),
	LINK_EXPIRED(1015, "Token is already used.");
	
	
	private final int code;
	private final String message;
	
	ResponseCode(int code, String message){
		this.code = code;
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	/**
	 * This methods is used to fetch Enum base on given id.
	 * @param code enum key
	 * @return ResponseCode enum
	 */
	public static ResponseCode fromId(int code) {
        for (ResponseCode responseCode : values()) {
            if (responseCode.code == code) {
                return responseCode;
            }
        }
        return null;
    }
}
