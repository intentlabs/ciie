package com.intentlabs.common.enums;

/**
 * This is used to mark model as active or inactive.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public enum ActiveInActive implements EnumType {
	
	INACTIVE(0, "INACTIVE"),
	ACTIVE(1, "ACTIVE");
	
	private final long id;
    private final String name;
    
    ActiveInActive(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
    
    /**
	 * This methods is used to fetch Enum base on given id.
	 * @param id enum key
	 * @return ActiveInActive enum
	 */
    public static ActiveInActive fromId(long id) {
        for (ActiveInActive status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }
}
