package com.intentlabs.common.enums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import com.intentlabs.common.logger.Notification;



/**
 * This is used to defines type of logger which is used inside this project.
 * @version 1.0
 * @since	29/03/2016
 */
public enum LoggerType implements EnumType {
	
	INFO(1, "Info"), 
	DEBUG(2, "Debug"), 
	ERROR(3, "Error"), 
	EXCEPTION(4, "Exception"),
	PERFORMANCE(5, "Performance"),
	SCHEDULER(6, "Scheduler");

	private final long id;
	private final String name;
	private final LinkedBlockingQueue<Notification> message = new LinkedBlockingQueue<>(10000);

	
	/**
     * code
     * @param id key of LoggerType.
     * @param name value of LoggerType.  
     */
	private LoggerType(long id, String name) {
		this.id = id;
		this.name = name;
	}

    @Override
	public long getId() {
		return this.id;
	}

    @Override
	public String getName() {
		return this.name;
	}

    
    /**
     * This methods is used for add Notification.
     * @param notification object of Notification. 
     */
	public void addNotification(Notification notification) {
		if (message.size() >= 100) {
			 for (int i = 0; i <= message.size() - 100; i++) {
				 message.remove();
			 }
		}
		message.add(notification);
	}

	public List<Notification> getMessages() {
		return Arrays.asList(message.toArray(new Notification[0]));
	}
	 /**
	  * This methods is used to fetch Enum base on given id.
	  * @param id enum key
	  * @return LoggerType enum
	  */
	public static LoggerType getFromId(int id) {
		for (LoggerType status : values()) {
			if (status.getId() == id) {
				return status;
			}
		}
		return null;
	}

	 /**
	  * This methods is used to fetch key value pair of notificationType.	
	  * @return Map<Long, String> key value of notificationType.
	  */
	
	public static Map<Long, String> getAll() {
		Map<Long, String> notificationType = new HashMap<>();
		for (LoggerType type : LoggerType.values()) {
			if (!(type.getId() == -1)) {
				notificationType.put(type.getId(), type.getName());
			}
		}
		return notificationType;
	}

	 /**
	  * This methods is used to notify any notification which is requested.	
	  * @param notification	object of Notification
	  * @author MToolKit Core team.
	  */
	
	public void notify(Notification notification) {
		addNotification(notification);
	}

}
