/**
 * 
 */
package com.intentlabs.common.enums;

/**
 * @author Dhruvang
 *
 */
public enum CommonStatus implements EnumType{
	NO(0, "NO"),
	YES(1, "YES");
	
	private final long id;
    private final String name;
    
    CommonStatus(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
    
    /**
	 * This methods is used to fetch Enum base on given id.
	 * @param id enum key
	 * @return ActiveInActive enum
	 */
    public static CommonStatus fromId(long id) {
        for (CommonStatus status : values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        return null;
    }

}
