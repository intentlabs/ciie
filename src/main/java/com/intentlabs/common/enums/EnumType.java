package com.intentlabs.common.enums;

import java.io.Serializable;

/**
 * Base interface of Enum.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public interface EnumType extends Serializable {
	
	/**
	 * This method returns id of enum
	 * @return id of enum
	 */
    public long getId();
    
    /**
	 * This method returns name of enum
	 * @return name of enum
	 */
    public String getName();
}