/**
 * 
 */
package com.intentlabs.common.enums;

/**
 * @author Dhruvang
 *
 */
public enum EnumArchive implements EnumType {
	ARCHIVE(0, "ARCHIVE"),
	NON_ARCHIVE(1, "NON_ARCHIVE");
	
	
	private final long id;
    private final String name;
    
    
    EnumArchive(long id, String name) {
        this.id = id;
        this.name = name;
    }


	@Override
	public long getId() {
	   return id;
	}

	@Override
	public String getName() {
		return name;
	}
	
	  public static EnumArchive fromId(long id) {
	        for (EnumArchive status : values()) {
	            if (status.getId() == id) {
	                return status;
	            }
	        }
	        return null;
	    }

}
