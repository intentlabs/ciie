/**
 * 
 */
package com.intentlabs.common.view;

import java.util.List;

/**
 * @author Dhruvang
 *
 */
public class PageResultView extends IdentifierView {

	
	private static final long serialVersionUID = 1026428637764248569L;
	
	private List<? extends View> list;
    private long records;

    private PageResultView(long records, List<? extends View> list){
    	this.records = records;
    	this.list = list;
    }
    
    public static PageResultView create(long records, List<? extends View> list) {
        return new PageResultView(records, list);
    }

    public static PageResultView create(int responseCode, String message,long records, List<? extends View> list) {
        return new PageResultView(records, list);
    }

	public List<? extends View> getList() {
		return list;
	}

	public long getRecords() {
		return records;
	}

}
