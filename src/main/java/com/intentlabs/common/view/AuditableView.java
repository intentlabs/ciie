/**
 * 
 */
package com.intentlabs.common.view;

/**
 * @author Dhruvang
 *
 */
public class AuditableView extends CreateView {

	
	private static final long serialVersionUID = 7982485400162432811L;
	

    private String updateDate;
    private String updateByUserName;
    
    
    
    
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateByUserName() {
		return updateByUserName;
	}
	public void setUpdateByUserName(String updateByUserName) {
		this.updateByUserName = updateByUserName;
	}

}
