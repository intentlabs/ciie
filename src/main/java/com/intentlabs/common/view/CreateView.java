/**
 * 
 */
package com.intentlabs.common.view;

/**
 * @author Dhruvang
 *
 */
public abstract class CreateView extends IdentifierView  {

	
	private static final long serialVersionUID = -6553054616121310042L;
	private String createDate;
	private String createByUserName;
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateByUserName() {
		return createByUserName;
	}
	public void setCreateByUserName(String createByUserName) {
		this.createByUserName = createByUserName;
	}
	
	

}
