/**
 * 
 */
package com.intentlabs.common.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author Dhruvang
 *
 */
public class KeyValueView implements Comparable<KeyValueView>, View  {


	private static final long serialVersionUID = -4330905917750411429L;
	
	private String name;
    private Long id;

    public static final Comparator<KeyValueView> idComparator = new Comparator<KeyValueView>() {
		@Override
		public int compare(KeyValueView o1, KeyValueView o2) {			
			return o1.getId().compareTo(o2.getId());
		}
	};
	
	public KeyValueView(){
		super();
	}
	
    public KeyValueView(Long id, String name) {
    	this.id = id;
        this.name = name;
    }

    public static KeyValueView create(Long id, String name) {
        return new KeyValueView(id, name);
    }

    public static List<Long> getKeyFromKeyValuePair(List<KeyValueView> keyValueView){
    	List<Long> keys = new ArrayList<>();
    	for(KeyValueView temp : keyValueView){
    		keys.add(temp.getId());
    	}
    	return keys;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
    public int compareTo(KeyValueView that) {
        return name.compareTo(that.getName());
    }
	
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == 0L) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		KeyValueView other = (KeyValueView) obj;
		if (getId() == null) {
			if (other.getId() != null){
				return false;
			}
		} else if (!getId().equals(other.getId())){
			return false;
		}
		return true;
	}

	@Override
    public String toString() {
        return "KeyValueView [Id=" + getId() + ", name=" + getName() + "]";
    }

}
