package com.intentlabs.common.view;

public abstract class ArciveView extends ActivationView {

	
	private static final long serialVersionUID = 5678365620021239583L;
	
	
	private KeyValueView  arciveStatus;
	private String arcivetionDate;
    private String arcivetionChangeByUserNam;
	public KeyValueView getArciveStatus() {
		return arciveStatus;
	}
	public void setArciveStatus(KeyValueView arciveStatus) {
		this.arciveStatus = arciveStatus;
	}
	public String getArcivetionDate() {
		return arcivetionDate;
	}
	public void setArcivetionDate(String arcivetionDate) {
		this.arcivetionDate = arcivetionDate;
	}
	public String getArcivetionChangeByUserNam() {
		return arcivetionChangeByUserNam;
	}
	public void setArcivetionChangeByUserNam(String arcivetionChangeByUserNam) {
		this.arcivetionChangeByUserNam = arcivetionChangeByUserNam;
	}
    
    

}
