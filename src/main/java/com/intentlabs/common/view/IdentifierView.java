/**
 * 
 */
package com.intentlabs.common.view;

/**
 * @author Dhruvang
 *
 */
public abstract class IdentifierView implements View {


	private static final long serialVersionUID = -8162299429464943398L;
	
	private Long id;
	private String uuid;
	private Long version;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	

}
