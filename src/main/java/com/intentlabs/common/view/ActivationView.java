package com.intentlabs.common.view;

public abstract class ActivationView extends AuditableView {


	private static final long serialVersionUID = 4528679895572287481L;
	
	private KeyValueView  activationStatus;
	private String activationDate;
    private String activationChangeByUserNam;
	public KeyValueView getActivationStatus() {
		return activationStatus;
	}
	public void setActivationStatus(KeyValueView activationStatus) {
		this.activationStatus = activationStatus;
	}
	public String getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}
	public String getActivationChangeByUserNam() {
		return activationChangeByUserNam;
	}
	public void setActivationChangeByUserNam(String activationChangeByUserNam) {
		this.activationChangeByUserNam = activationChangeByUserNam;
	}
    
    

}
