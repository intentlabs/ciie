package com.intentlabs.common.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import com.intentlabs.common.auditor.Auditor;
import com.intentlabs.common.model.Model;
import com.intentlabs.common.model.PageModel;




/**
 * This is abstract service class used to provide implementation or definition of basic services used to perform
 * database related activities.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public abstract class AbstractService<M extends Model> {
	
    private SessionFactory sessionFactory;

    /**
     * Setter method to set session factory object of hibernate.
     * @param sessionFactory
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * it returns current session bound with transaction boundary.
     * @return Session hibernate session
     */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * This methods returns model class against which all database related operation will performed through hibernate.
     * @return String modelClass
     */
    public abstract Class<M> getModelClass();
    
    /**
     * It is used to set common search criteria. This criteria will be set base on logged user.
     * @param modelClass against which database operation will be performed
     * @return {@link Criteria}
     */
    public abstract Criteria setCommonCriteria(Class<M> modelClass);
    
    /**
     * It is used to set search criteria base on model.
     * @param model model on which search criteria should be applied.     
     * @param commonCriteria return common criteria set by setCommonCriteria method
     * @return {@link Criteria}
     */
    public abstract Criteria setSearchCriteria(M model, Criteria commonCriteria);
    
    /**
     * It is used to insert a single record into database using model.
     * @param model 
     * @return model 
     */
    public M create(M model) {
    	saveModel(model);
        return model;
    }
    
    /**
     * It is used to insert bulk record into database using default entity.
     * @param models list of models
     * @return models list of models
     */
    public List<M> createBulk(List<M> models) {
    	for (M model : models) {
        	saveModel(model);
        }
        return models;
    }
    
    /**
     * It is used to update single record into database using default entity.
     * @param model
     */
    public void update(M model) {
    	updateModel(model);
    }
    
    /**
     * It is used to update bulk record into database using default entity.
     * @param models
     */
    public void updateBulk(List<M> models){
    	for (M model : models) {
    		updateModel(model);
        }
    }
    
    /**
     * It is used to get single record base on given id using default entity.
     * @param id unique value to identify model
     * @return model
     */
    public M get(long id){
    	return get(id, getModelClass());
    }
    
    /**
     * It is used to get single record base on given uuid using default entity.
     * @param uuid unique value to identify model
     * @return model
     */
    public M get(String uuid){
    	return get(uuid, getModelClass());
    }
    
    /**
     * It is used to get single record base on given id using given entity.
     * @param id unique value to identify model
     * @param entityName name of entity define inside hbm file which needs to mapped to model.
     * @return model
     */
    @SuppressWarnings("unchecked")
	protected M get(long id, Class<M> modelClass){
    	return (M) getSession().get(modelClass, id);
    }
    
    /**
     * It is used to get single record base on given uuid using given entity.
     * @param uuid unique value to identify model
     * @param entityName name of entity define inside hbm file which needs to mapped to model.
     * @return model
     */
    @SuppressWarnings("unchecked")
	protected M get(String uuid, Class<M> modelClass){
    	return (M) getSession().get(modelClass, uuid);
    }
    
    /**
     * It is used to delete single record base on given id using default entity.
     * @param id unique value to identify model
     */
    public void delete(long id){
    	delete(id, getModelClass());
    }
    
    /**
     * It is used to delete single record base on given id using default entity.
     * @param id unique value to identify model
     * @param entityName name of entity define inside hbm file which needs to mapped to model.
     */
    protected void delete(long id, Class<M> modelClass){
    	getSession().delete(get(id, modelClass));
    }
    
    /**
     * It is used to delete bulk record base on given model using default entity.
     * @param models list of models
     */
    public void deleteBulk(List<M> models){
    	for (M model : models) {
    		getSession().delete(model);
    	}
    }
    
    /**
     * This is used to fetch data that needs to be displayed on grid using default entity.
     * @param start starting row from where to fetch record
     * @param end end row of record
     * @return {@link PageModel}
     */
    public PageModel getGridData(Integer start, Integer end){
    	return getGridData(start, end, getModelClass());
    }
    
    /**
     * This is used to fetch data that needs to be displayed on grid using given entity.
     * @param start starting row from where to fetch record
     * @param end end row of record
     * @param entityName name of entity define inside hbm file which needs to mapped to model.
     * @return {@link PageModel}
     */
    protected PageModel getGridData(Integer start, Integer end, Class<M> modelClass){
    	return getResults(setCommonCriteria(modelClass), start, end);
    }

    /**
     * This is used to search model data on given criteria using default entity.
     * @param model {@link Model}
     * @param start starting row from where to fetch record
     * @param end end row of record
     * @return {@link PageModel}
     */
    public PageModel search(M model, Integer start, Integer end){
    	return search(model, start, end, getModelClass());
    }
    
    /**
     * This is used to search model data on given criteria using given entity.
     * @param model {@link Model}
     * @param start starting row from where to fetch record
     * @param end end row of record
     * @param entityName name of entity define inside hbm file which needs to mapped to model.
     * @return {@link PageModel}
     */
    protected PageModel search(M model, Integer start, Integer end, Class<M> modelClass){
    	Criteria commonCriteria = setCommonCriteria(modelClass);
    	setSearchCriteria(model, commonCriteria);
    	return getResults(commonCriteria, start, end);
    }
    
    /**
     * This is used to fetch data that using hql query that needs to be displayed on grid.
     * @param query hibernate query object
     * @param startNo starting row from where to fetch record
     * @param endNo end row of record
     * @return {@link PageModel}
     */
    public PageModel getResultUsingHQL(Query query, Integer startNo, Integer endNo) {
 		long records = query.list().size();
        if (startNo != null && endNo != null) {
        	query.setFirstResult(startNo);
        	query.setMaxResults((1 + endNo) - startNo);
        }
        List<? extends M> results = query.list();
        return PageModel.create(results, records);
    }

    /**
     * This methods returns number of records base on given criteria.
     * @param criteria {@link Criteria}
     * @return long number of records
     */
    public long getRawCount(Criteria criteria){
    	return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }
    /**
     *  @deprecated (Fetch all records using default entity.)
     * @return List
     */
    @Deprecated
    public List<M> findAll(){
    	return findAll(getModelClass());
    }

    /**
     * @deprecated (Fetch all records using given entity.)
     * @param entityName  name of entity define inside hbm file which needs to mapped to model.
     * @return List
     */

    @Deprecated
    protected List<M> findAll(Class<M> modelClass){
    	Criteria criteria = getSession().createCriteria(modelClass);
    	return (List<M>)criteria.list();
    }

    protected PageModel getResults(Criteria criteria, Integer startNo, Integer endNo) {
    	long records = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
        criteria.setProjection(null);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        if (startNo != null && endNo != null) {
            criteria.setFirstResult(startNo);
            criteria.setMaxResults((1 + endNo) - startNo);
        }
        setOrder(criteria);
        List<? extends M> results = criteria.list(); 
        return PageModel.create(results, records);
    }

    protected void setOrder(Criteria criteria) {
    	criteria.addOrder(Order.desc("id"));
    }
    
 	private void saveModel(Model model){
 		Auditor.createAudit(model);
        getSession().save(model);
 	}
 	 
 	private void updateModel(Model model){
 		Auditor.updateAudit(model);
        getSession().update(model);
 	}

 	public void updateEntityWithoutAuditor(M model){
    	getSession().update(model);
    }
 	
 	@SuppressWarnings("unchecked")
	public M load(long id){
 		return (M) getSession().load(getModelClass(), new Long(id));
 	}
 	
 	@SuppressWarnings("unchecked")
	public List<M> loadBulk(List<Long> idList){
 		List<M> modelList = new ArrayList<>();
 		for(Long id : idList){
 			modelList.add((M) getSession().load(getModelClass(), new Long(id)));
 		}
 		return modelList;
 	}
 	
 	@SuppressWarnings("unchecked")
	public M load(String uuid){
 		return (M) getSession().load(getModelClass(), uuid);
 	}
}
