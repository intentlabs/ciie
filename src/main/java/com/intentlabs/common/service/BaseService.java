package com.intentlabs.common.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;

import com.intentlabs.common.model.Model;
import com.intentlabs.common.model.PageModel;




/**
 * This is base service definition of basic services used to perform database related activities.
 * It works on given model.
 * @version 1.0
 */
public interface BaseService<M extends Model> extends Service {
	
	 /**
     * It is used to insert a single record into database using default entity.
     * @param model 
     * @return model 
     */
    M create(M model);

    /**
     * It is used to insert bulk record into database using default entity.
     * @param models list of models
     * @return models
     */
    List<M> createBulk(List<M> models);
    
    /**
     * It is used to update single record into database using default entity.
     * @param model
     */
    void update(M model);
    
    /**
     * It is used to update bulk record into database using default entity.
     * @param models
     */
    void updateBulk(List<M> models);
    
    /**
     * This is used to search model data on given criteria using default entity.
     * @param model {@link Model}
     * @param start starting row from where to fetch record
     * @param end end row of record
     * @return {@link PageModel}
     */
    PageModel search(M model, Integer start, Integer end);

    /**
     * This is used to fetch data that needs to be displayed on grid using default entity.
     * @param start starting row from where to fetch record
     * @param end end row of record
     * @return {@link PageModel}
     */
    PageModel getGridData(Integer start, Integer end);

    /**
     * It is used to get single record base on given id using default entity.
     * @param id unique value to identify model
     * @return model
     */
    M get(long id);
    
    /**
     * It is used to get single record base on given uuid using default entity.
     * @param uuid unique value to identify model
     * @return model
     */
    M get(String uuid);
        
    /**
     * It is used to delete single record base on given id using default entity.
     * @param id unique value to identify model
     */
    void delete(long id);
    
    /**
     * It is used to delete bulk record base on given model using default entity.
     * @param models list of models
     */
    void deleteBulk(List<M> models);

    /**
     * This is used to fetch data that using hql query that needs to be displayed on grid.
     * @param query hibernate query object
     * @param start starting row from where to fetch record
     * @param end end row of record
     * @return {@link PageModel}
     */
    PageModel getResultUsingHQL(Query query, Integer startNo, Integer endNo);
    
    /**
     * This methods returns number of records base on given criteria.
     * @param criteria {@link Criteria}
     * @return long number of records
     */
    long getRawCount(Criteria criteria);
    
    /**
     * @deprecated (Fetch all records using default entity.)
     * @return List
     */
    @Deprecated
    List<M> findAll();
    
    /**
     * It is used to update single record without auditor into database using given entity .
     * @param model
     * @param entityName name of entity define inside hbm file which needs to mapped to model.
     */
 	void updateEntityWithoutAuditor(M model);
 	
 	/**
 	 * This method creates a proxy object as per hibernate's load method functionality which can be 
 	 * used during save & update method when just reference of entities are required.
 	 * @param id
 	 * @param entityName
 	 * @return
 	 */
 	M load(long id);
 	
 	/**
 	 * This method creates a proxy objects as per hibernate's load method functionality which can be 
 	 * used during save & update method when just reference of entities are required.
 	 * @param id
 	 * @param entityName
 	 * @return
 	 */
 	List<M> loadBulk(List<Long> idList);
 	
 	/**
 	 * This method creates a proxy object as per hibernate's load method functionality which can be 
 	 * used during save & update method when just reference of entities are required.
 	 * @param uuid
 	 * @param entityName
 	 * @return
 	 */
 	M load(String uuid);
}
