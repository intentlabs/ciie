package com.intentlabs.common.service;

import java.io.Serializable;

/**
 * This is a marker interface used to identifies service.
 * @author Core team.
 * @version 1.0
 * @since 21/09/2016
 */
public interface Service extends Serializable { }
