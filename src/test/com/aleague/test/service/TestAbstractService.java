package com.aleague.test.service;

import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.user.services.UserService;
import com.intentlabs.common.auditor.Auditor;





/**
 * This is abstract class which is used to test services. This class prepares basic data which will be used 
 * during unit testing of module services.
 * @author Caps Core team.
 * @version 1.0
 */
public abstract class TestAbstractService {
	protected static ApplicationContext applicationContext = null;
	protected static EmbeddedDatabase db = null;
	protected static SessionFactory sessionFactory = null;

	@BeforeClass
	public static void setUp() {
		applicationContext = new ClassPathXmlApplicationContext("/testApplicationContext.xml");
		sessionFactory = (SessionFactory) applicationContext.getBean("sessionFactory");		
		UserService userService = (UserService) applicationContext.getBean("userService");
		User user = userService.get(1L);
		Auditor.setAuditor(user);
	}
	
	@AfterClass
	public static void tearDown() {
		if(db != null) {
			db.shutdown();
		}
	}
	
	@Before 
	public abstract void prepareData() throws Exception;
	
	@After
	public abstract void removeData() throws Exception;
}
