package com.aleague.test.service.test;
import java.util.Date;

import org.junit.Test;

import com.aleague.test.service.TestAbstractService;
import com.intentlabs.aleague.institute.services.InstituteService;
import com.intentlabs.aleague.instutute.model.InstituteModel;
import com.intentlabs.aleague.user.services.UserService;

public class TestInstituteService extends TestAbstractService {
	
	InstituteService instituteService;
	UserService userService;

	@Override
	public void prepareData() throws Exception {
		instituteService = (InstituteService) applicationContext.getBean("instituteService");
		userService = (UserService) applicationContext.getBean("userService");

		
	}

	@Override
	public void removeData() throws Exception {
	
		
	}
	
	@Test
	public void addInstituteTest(){
		
		try {
			InstituteModel im = new InstituteModel();
			im.setCreateBy(userService.get(1l));
			im.setCreateDate(new Date());
			im.setTxtName("SIEM");
			im.setActivationStatus(1l);
			im.setTxtAddress("Ahmedabad");
			im.setTxtDescription("ofjfwfjwefpwjprojpogerjg");
			im.setTxtWebsiteLink("portal.ciieindia.org");
			im.setEnumArchive(1l);
			instituteService.create(im);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		
		
	}

}
