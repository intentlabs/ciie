package com.aleague.test.kernal;

/**
 * This class contains basic methods which will be used to prepare common data that can be used to test any services.
 * @author Caps Core team.
 * @version 1.0
 */
public class CommonDataPreparation{
	
	private CommonDataPreparation(){
	}
}
