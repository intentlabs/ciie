package com.aleague.test.controller.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.aleague.test.controller.TestAbstractController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intentlabs.aleague.event.view.EventView;
import com.intentlabs.aleague.user.services.UserService;

public class TestEventController extends TestAbstractController  {
	
	private ObjectMapper objectMapper;
	UserService userService;

	@Override
	public void prepareData() throws Exception {
	
		objectMapper = new ObjectMapper();
		userService = (UserService) applicationContext.getBean("userService");
	}
	
	@Test
	public void textImagePath() {
		
		
	}

	@Override
	public void removeData() throws Exception {
		try {
			final String requestUri = "/user/login.htm";
			request.setContentType("application/json");
			request.setMethod("POST");
			request.setRequestURI(requestUri);
			
			EventView userView = new EventView();
			userView.setId(4l);
			//userView.setCreateBy(userService.get(4l));;
			
			ModelMap modelMap = new ModelMap();
			
			modelMap.put("userView",userView);
			
			request.setContent(objectMapper.writeValueAsString(modelMap).getBytes());
			
			Object handler = requestMappingHandlerMapping.getHandler(request).getHandler();
			
			ModelAndView modelAndView = requestMappingHandlerAdapter.handle(request, response, handler);
			Assert.assertTrue("response Message", response.getContentAsString().contains("1000"));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue("Exception", false);
		}
	}
		
	}


