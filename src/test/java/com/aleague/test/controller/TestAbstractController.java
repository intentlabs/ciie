/**
 * 
 */
package com.aleague.test.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.intentlabs.aleague.user.model.User;
import com.intentlabs.aleague.user.services.UserService;
import com.intentlabs.common.auditor.Auditor;




/**
 * @author Dhruvang
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"classpath:testApplicationContext.xml","classpath:dispatcher-servlet.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,TransactionalTestExecutionListener.class})
public abstract class TestAbstractController {
	
	@Autowired
	protected ApplicationContext applicationContext;
	
	protected MockHttpServletRequest request = null;
	protected MockHttpServletResponse response = null;
	protected MockMvc mockMvc;
	protected MockFilterChain mockChain = null;
	
	@Autowired
	protected RequestMappingHandlerAdapter requestMappingHandlerAdapter;
	@Autowired
	protected RequestMappingHandlerMapping requestMappingHandlerMapping;
	
	@Before
	public void setUp(){
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		mockChain = new MockFilterChain();
		requestMappingHandlerAdapter = applicationContext.getBean(RequestMappingHandlerAdapter.class);
		MockitoAnnotations.initMocks(this);
		
		UserService userService = (UserService) applicationContext.getBean("userService");
		User user = userService.get(2);
		Auditor.setAuditor(user);
	}
	
	@Before 
	public abstract void prepareData() throws Exception;
	
	@After
	public abstract void removeData() throws Exception;

}
