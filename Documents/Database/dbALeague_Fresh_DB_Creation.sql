CREATE DATABASE dbALeague;
USE dbALeague;
SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE tblUser(
    pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	lockVersion INT(10) unsigned NOT NULL,
	txtEmail VARCHAR(100) NOT NULL,
	txtFirstName VARCHAR(30) NOT NULL,	
	txtLastName VARCHAR(30) NOT NULL,
	txtProfilePicPath VARCHAR(100) DEFAULT NULL,
	txtContactNumber VARCHAR(12) DEFAULT NULL,
	fkRoleId INT(10) unsigned NOT NULL,
	fkInstituteId INT(10) unsigned DEFAULT NULL,
    numberBatchYear INT(4)  DEFAULT NULL,
    enrolmentNumber INT(50) DEFAULT NULL,
	txtLinkdinLink VARCHAR(500) DEFAULT NULL,
 	txtAbout VARCHAR(1000) DEFAULT NULL,
 	txtCourse VARCHAR(100) DEFAULT NULL,
    txtActToken VARCHAR(32) NOT NULL,
	enumActTokenUsed ENUM('0','1') NOT NULL DEFAULT '0', /* 0-No 1-Yes*/
    fkCreateBy INT(10) unsigned DEFAULT NULL,
    dateCreate DATETIME NOT NULL,
    fkUpdateBy INT(10) unsigned DEFAULT NULL,
    dateUpdate DATETIME DEFAULT NULL,
    enumAct INT(1) NOT NULL DEFAULT '0', /* 0-Inactive, 1-Active*/ 
    fkActChangeBy INT(10) unsigned DEFAULT NULL,
    dateActChange DATETIME DEFAULT NULL,
    enumArchive ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Archive, 1-NonArchive*/ 
	fkArchiveBy INT(10) unsigned DEFAULT NULL, 
	dateArchive DATETIME DEFAULT NULL,
	enumApprove ENUM('1','0') NOT NULL DEFAULT '0', /* 1- Approval Required, 0-Not Required*/ 
    PRIMARY KEY (pkId),
    UNIQUE KEY (txtEmail),
    UNIQUE KEY(txtActToken),
	FOREIGN KEY(fkCreateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkUpdateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkActChangeBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkArchiveBy) REFERENCES tblUser(pkId) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE tblUserSession(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	fkUserId INT(10) unsigned NOT NULL,
	txtSession VARCHAR(100) NOT NULL,
	txtDevice TEXT DEFAULT NULL,
	txtDeviceCookie VARCHAR(100) DEFAULT NULL,
	dateCookieExpireTime DATETIME DEFAULT NULL,
	txtIp VARCHAR(50) DEFAULT NULL,
	PRIMARY KEY(pkId),
	FOREIGN KEY(fkUserId) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	UNIQUE KEY(txtSession),
	UNIQUE KEY(txtDeviceCookie)
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE tblUserOTP(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	fkUserId INT(10) unsigned NOT NULL,
	txtForgotPassOtp VARCHAR(8) DEFAULT NULL,
	dateForgotOtpGeneration DATETIME DEFAULT NULL,
	enumForgotPassOtpUsed ENUM('0','1') DEFAULT '0', /* 0-No 1-Yes*/
	txtTwoFactorOtpGeneration VARCHAR(8) DEFAULT NULL,
	dateTwoFactorOtp DATETIME DEFAULT NULL,
	enumTwoFactorOtpUsed ENUM('0','1') DEFAULT '0', /* 0-No 1-Yes*/
	PRIMARY KEY(pkId),
	FOREIGN KEY(fkUserId) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	UNIQUE KEY(fkUserId, txtForgotPassOtp),
	UNIQUE KEY(fkUserId, txtTwoFactorOtpGeneration)
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE tblUserPassword(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	fkUserId INT(10) unsigned NOT NULL,
	txtPassword TEXT NOT NULL,
	datePasswordChange DATETIME NOT NULL,
	PRIMARY KEY(pkId),
	FOREIGN KEY(fkUserId) REFERENCES tblUser(pkId) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE tblRole(
    pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
    lockVersion INT(10) unsigned NOT NULL,
    txtName VARCHAR(30) NOT NULL,
    txtDescription VARCHAR(256) DEFAULT NULL,
    fkCreateBy INT(10) unsigned NOT NULL,
    dateCreate DATETIME NOT NULL,
    fkUpdateBy INT(10) unsigned DEFAULT NULL,
    dateUpdate DATETIME DEFAULT NULL,
    enumAct ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Inactive, 1-Active*/ 
    fkActChangeBy INT(10) unsigned DEFAULT NULL,
    dateActChange DATETIME DEFAULT NULL,
    enumArchive ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Archive, 1-NonArchive*/ 
	fkArchiveBy INT(10) unsigned DEFAULT NULL, 
	dateArchive DATETIME DEFAULT NULL,
    PRIMARY KEY (pkId),
    FOREIGN KEY(fkCreateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkUpdateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkActChangeBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkArchiveBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
    UNIQUE KEY(txtName)
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE tblRoleModuleRight(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	pkfkRoleId INT(10) unsigned NOT NULL ,
	pkModuleId SMALLINT(5) unsigned NOT NULL ,
	pkSubModuleId SMALLINT(5) unsigned NOT NULL ,
	pkEnumType ENUM('1','2','3','4') NOT NULL DEFAULT '4', /* 1-Maker, 2-Checker, 3-Approver, 4-None*/
	txtRights TEXT NOT NULL,
    PRIMARY KEY(pkId),
	UNIQUE KEY(pkfkRoleId,pkModuleId,pkSubModuleId,pkEnumType),
    FOREIGN KEY(pkfkRoleId) REFERENCES tblRole (pkId) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=UTF8;


INSERT INTO tblUser 
(pkId,lockVersion,txtEmail,txtFirstName,txtLastName,fkRoleId,fkInstituteId,txtActToken,enumActTokenUsed,fkCreateBy,dateCreate,fkUpdateBy,dateUpdate,enumAct) 
VALUES 
(1,'0','superadmin@intentlabs.com','Dhruvang', 'Joshi', '1', '1', '1','1','1', NOW(), '1',NOW(),'1');

ALTER TABLE tblUser
ADD CONSTRAINT FOREIGN KEY(fkRoleId) REFERENCES tblRole(pkId);
 
INSERT INTO tblRole(pkId, lockVersion, txtName, txtDescription, fkCreateBy, dateCreate)
VALUES(1, 0, 'Super Admin','Super Admin', 1, NOW());

INSERT INTO tblRole(pkId, lockVersion, txtName, txtDescription, fkCreateBy, dateCreate)
VALUES(2, 0, 'Student','Student', 1, NOW());

INSERT INTO tblRole(pkId, lockVersion, txtName, txtDescription, fkCreateBy, dateCreate)
VALUES(3, 0, 'Faculty','Faculty', 1, NOW());

INSERT INTO tblRole(pkId, lockVersion, txtName, txtDescription, fkCreateBy, dateCreate)
VALUES(4, 0, 'Others','Others', 1, NOW());

INSERT INTO tblRole(pkId, lockVersion, txtName, txtDescription, fkCreateBy, dateCreate)
VALUES(5, 0, 'Faculty Co-ordinate','Faculty Co-ordinate', 1, NOW());

INSERT INTO tblRole(pkId, lockVersion, txtName, txtDescription, fkCreateBy, dateCreate)
VALUES(6, 0, 'Student Co-ordinate','Student Co-ordinate', 1, NOW());

CREATE TABLE tblSettings(
 pkId INT(10) NOT NULL AUTO_INCREMENT,
 lockVersion BIGINT(10) NOT NULL,
 txtKey VARCHAR(30) DEFAULT NULL,
 txtValue VARCHAR(30) DEFAULT NULL,
 fkUserId BIGINT(10) NOT NULL,
 PRIMARY KEY (pkId)
 )ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE tblInstitute(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	lockVersion INT(10) unsigned NOT NULL,
	txtName VARCHAR(200) NOT NULL,
	txtAddress VARCHAR(500) NOT NULL,
	txtDescription VARCHAR(500) DEFAULT NULL,
	txtFbLink VARCHAR(500) DEFAULT NULL,
	txtTwLink VARCHAR(500) DEFAULT NULL,
	txtWebsiteLink VARCHAR(500) DEFAULT NULL,
	txtLogoPath VARCHAR(100) DEFAULT NULL,
	txtDomain VARCHAR(20) DEFAULT NULL,
	fkCreateBy INT(10) unsigned NOT NULL,
    dateCreate DATETIME NOT NULL,
    fkUpdateBy INT(10) unsigned DEFAULT NULL,
    dateUpdate DATETIME DEFAULT NULL,
    enumAct ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Inactive, 1-Active*/ 
    fkActChangeBy INT(10) unsigned DEFAULT NULL,
    dateActChange DATETIME DEFAULT NULL,
    enumArchive ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Archive, 1-NonArchive*/ 
	fkArchiveBy INT(10) unsigned DEFAULT NULL, 
	dateArchive DATETIME DEFAULT NULL,
	PRIMARY KEY(pkId),
	UNIQUE KEY(txtName),
	FOREIGN KEY(fkCreateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkUpdateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkActChangeBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkArchiveBy) REFERENCES tblUser(pkId) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Adani Institute of Infrastructure Management', 'Adani Institute of Infrastructure Management Shantigram Township Nr Vaishnodevi Cirlce SG Highway Ahmedabad Gujarat 382421 ','aiim.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'CEPT University', 'Kasturba Lalabhai Campus, University Road, Vasant Vihar, Navrangpura, Ahmedabad, Gujarat 380009 ','cept.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Dhirubhai Ambani Institute of Information and Communication Technology', 'Near Indroda Circle Gandhinagar Gujarat 382007 ','daiict.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Pandit Deendayal Petroleum University', 'Raisan, Gandhinagar, Gujarat 382421 ','pdpu.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Entrepreneurship Development Institute Of India', 'Gandhinagar-Ahmedabad Rd , Next to Apollo Hospital, Bhat, Ahmedabad Gujarat 382428','ediindia.org', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Indian Institute of Technology Gandhinagar', 'Village Palaj Simkheda, Gandhinagar Gujarat 382355','iitgn.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Indian Institute of Management Ahmedabad', 'Vastrapur, Ahmedabad, Gujarat 380015','iima.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'MICA', 'Shela, Ahmedabad - 380058, Gujarat','micamail.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Gujarat National Law University', 'Attalika Avenue, PDPU Road, Knowledge Corridor, Koba, Gandhinagar, Gujarat 382007','gnlu.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Ahmedabad University', 'Commerce Six Roads Navrangpura, Ahmedabad - 380009 Gujarat','ahduni.edu.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'National Institute of Design', 'Opposite Tagore Hall, Paldi, Ahmedabad, Gujarat 380007','nid.edu', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'National Institute of Fashion Technology', 'Gandhinagar, Gujarat 382010','nift.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Nirma University', 'Sarkhej-Gandhinagar Highway, Chandlodia, Gota, Ahmedabad, Gujarat 382481','nirmauni.ac.in', 1, NOW());

INSERT INTO tblInstitute(lockVersion, txtName, txtAddress, txtDomain, fkCreateBy, dateCreate)
VALUES(0,'Indian Institute of Information Technology, Vadodara', 'c/o Block No.9, Government Engineering College, Sector-28, Gandhinagar, Gujarat - 382028','iiitvadodara.ac.in', 1, NOW());
CREATE TABLE tblInsituteGallery(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	lockVersion INT(10) unsigned NOT NULL,
	fkInstituteId INT(10) unsigned NOT NULL,
	txtImagePath VARCHAR(100) NOT NULL,
	fkUploadBy INT(10) unsigned NOT NULL,
	dateUpload DATETIME NOT NULL,
	enumArchive ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Archive, 1-NonArchive*/ 
	fkArchiveBy INT(10) unsigned DEFAULT NULL,
	dateArchive DATETIME DEFAULT NULL,
	PRIMARY KEY(pkId),
	FOREIGN KEY tblInsituteGallery_fkInstituteId(fkInstituteId) REFERENCES tblInstitute(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkUploadBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkArchiveBy) REFERENCES tblUser(pkId) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

ALTER TABLE tblUser
ADD CONSTRAINT FOREIGN KEY(fkInstituteId) REFERENCES tblInstitute(pkId);

CREATE TABLE `tblJobPost` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblJobPost` ADD COLUMN `txtPostName` VARCHAR(500) AFTER `dateArchive`,
 ADD COLUMN `enumJobType` INTEGER(1) AFTER `txtPostName`,/*ENUM('1','2', '3', '4')*/
 ADD COLUMN `txtDuration` VARCHAR(20) AFTER `enumJobType`,
 ADD COLUMN `txtJobPosterName` VARCHAR(500) AFTER `txtDuration`,
 ADD COLUMN `txtJobDescription` TEXT AFTER `txtJobPosterName`,
 ADD COLUMN `txtLinkApply` VARCHAR(100) AFTER `txtJobDescription`,
 ADD COLUMN `txtTags` TEXT AFTER `txtLinkApply`,
 ADD COLUMN `dateStartDate` DATETIME AFTER `txtTags`,
 ADD COLUMN `dateEndDate` DATETIME AFTER `dateStartDate`,
 ADD COLUMN `txtPostFilePath` VARCHAR(100) AFTER `dateEndDate`,
 ADD COLUMN `txtLocation` VARCHAR(100) AFTER `txtPostFilePath`;
 
 CREATE TABLE `tblBlogPost` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

 ALTER TABLE `tblBlogPost` ADD COLUMN `txtBlogTitle` VARCHAR(500),
 ADD COLUMN `txtAuthor` VARCHAR(200),
 ADD COLUMN `txtTags` TEXT,
 ADD COLUMN `txtLinkBlog` VARCHAR(100),
 ADD COLUMN `txtDescription` TEXT,
 ADD COLUMN `txtPostFilePath` VARCHAR(100),
 ADD COLUMN `datePostedOn` DATETIME,
 ADD COLUMN `countFollowers` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `datePostedOn`,
 ADD COLUMN `countLikes` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `countFollowers`;
 /*
 CREATE TABLE `tblTag` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;*/

CREATE TABLE tblTag (
  	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	lockVersion INT(10) unsigned NOT NULL DEFAULT 0,
	txtTagName VARCHAR(100) NOT NULL,
  	fkCreateBy INT(10) unsigned NOT NULL DEFAULT 1,
    dateCreate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fkUpdateBy INT(10) unsigned DEFAULT NULL,
    dateUpdate DATETIME DEFAULT NULL,
    enumAct ENUM('1','0') NOT NULL DEFAULT '0', /* 0-Inactive, 1-Active*/ 
    fkActChangeBy INT(10) unsigned DEFAULT NULL,
    dateActChange DATETIME DEFAULT NULL,
    enumArchive ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Archive, 1-NonArchive*/ 
	fkArchiveBy INT(10) unsigned DEFAULT NULL, 
	dateArchive DATETIME DEFAULT NULL,
  	PRIMARY KEY(pkId),
  	UNIQUE KEY(txtTagName),
  	FOREIGN KEY(fkCreateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkUpdateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkActChangeBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkArchiveBy) REFERENCES tblUser(pkId) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;
 
insert into tbltag(`txtTagName`)
values(
'Food&Agri'),('
floriculture'),('
fruits and vegetables'),('
poultry'),('
bio fuel '),('
fishery and sea food'),('
post harvest infrastructure'),('
agri service centres'),('
dedicated agri ports'),('
agri research/ quality centres'),('
Construction'),('
Industrial & Infrastructural Construction'),('
Real Estate'),('
Defence'),('
Education'),('
Journalism'),('
Tutorials'),('
online education businesses'),('
pre school'),('
education content'),('
skill upgradation/ vocational education'),('
professional education'),('
higher education'),('
Financial services'),('
Asset financing services'),('
Banking services  '),('
Insurance'),('
Investment services  '),('
Mutual Funds'),('
Securities broking  '),('
Investment banking'),('
equity research'),('
Government'),('
Life Sciences'),('
Healthcare'),('
Biotechnology'),('
Biological Research'),('
Drugs & pharmaceuticals'),('
Health Services '),('
Hospitals '),('
Medical Laboratories '),('
Hospitality'),('
Hotels'),('
Restaurants & Bar'),('
Tours & Travels '),('
Infrastructure'),('
Alternate/ renewable energy'),('
Power distribution'),('
Power generation'),('
Power transmission'),('
Rural infrastructure'),('
Telecom'),('
Transport infrastructure'),('
IOT'),('
IT Products'),('
ITES'),('
Tech'),('
Aerospace'),('
Automobiles'),('
Printing/Publishing  '),('
Chemicals'),('
Consumer durables'),('
Defence Equipments'),('
Engineering'),('
FMCG'),('
Food and beverages'),('
Leather products  '),('
Machinery'),('
Metal and metal products'),('
Non metallic mineral products'),('
Paper'),(' newsprint & paper products  '),('
Petroleum products'),('
Textiles'),('
Media and Entertainment'),('
Advertising '),('
Amusement & Recreation Services '),('
Commercial Art & Photography '),('
Film Production & distribution '),('
Media & Media Content '),('
Movie & Music Rental '),('
Sports'),(' Fitness & Recreation Clubs '),('
Television Media'),('
Mining'),('
Coal & lignite'),('
Minerals'),('
Oil & Gas Exploration'),('
Retail'),('
Food retail'),('
Apparel retail'),('
multi product retail'),('
medicine/ heathcare'),('
single brand'),('
multi brand retail'),('
gift items'),('
confectionary'),('
sports equipment'),('
computer hardware/ accessories'),('
electronic appliances'),('
furniture/ furnishings'),('
auto dealers/ filling stations'),('
Retail Consumer'),('
Music'),('
Services'),('
Business Consulting Services '),('
Employment/HR Agencies '),('
Engineering Services'),('
Equipment Rental & Leasing '),('
Event Management '),('
Legal Services'),('
Management Consulting Services'),('
Personal Services '),('
Photocopying and Duplicating Services '),('
Security Agencies'),('
Talent Management'),('
Transport Services'),('
Sports Marketing/Management'),('
Fitness centres'),('
project advisory'),('
market research'),('
product research'),('
Entertainment'),('
CIIE'),('
Ahmedabad'),('
Surat'),('
Vadodara'),('
Rajkot'),('
Bhavnagar'),('
Jamnagar'),('
Junagadh'),('
Gandhidham'),('
Nadiad'),('
Gandhinagar'),('
Anand'),('
Morbi'),('
Mehsana'),('
Surendranagar'),('
Bharuch'),('
Vapi'),('
Navsari'),('
Veraval'),('
Porbandar'),('
Godhra'),('
Bhuj'),('
Ankleshwar'),('
Botad'),('
Palanpur'),('
Patan'),('
Dahod'),('
Jetpur'),('
Valsad'),('
Kalol'),('
Gondal'),('
Amreli'),('
IIMA'),('
A-league'),('
AIIM'),('
CEPT'),('
DAIICT'),('
EDI'),('
PDPU'),('
IIT-Gn'),('
MICA'),('
GNLU'),('
AU'),('
NID'),('
NIFT'),('
NIRMA'),('
IIIT-V'),('
AhdUniv');

UPDATE tblTag SET txtTagName = TRIM(txtTagName);

ALTER TABLE `tblBlogPost` DROP COLUMN `txtTags`;
 
ALTER TABLE `tblJobPost` DROP COLUMN `txtTags`;
 
CREATE TABLE `tblBlogPostTag` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` INTEGER(1) NOT NULL DEFAULT '0',/*ENUM('1','0')*/
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblBlogPostTag` ADD COLUMN `fkTag` INTEGER(10) UNSIGNED AFTER `pkId`,
 ADD COLUMN `fkBlogPost` INTEGER(10) UNSIGNED AFTER `fkTag`,
 ADD CONSTRAINT `FK_tblBlogPostTag_Tag` FOREIGN KEY `FK_tblBlogPostTag_Tag` (`fkTag`)
    REFERENCES `tblTag` (`pkId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_tblBlogPostTag_BlogPost` FOREIGN KEY `FK_tblBlogPostTag_BlogPost` (`fkBlogPost`)
    REFERENCES `tblBlogPostTag` (`pkId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;
    
 ALTER TABLE `tblBlogPostTag`
 DROP FOREIGN KEY `FK_tblBlogPostTag_BlogPost`;

ALTER TABLE `tblBlogPostTag` ADD CONSTRAINT `FK_tblBlogPostTag_BlogPost` FOREIGN KEY `FK_tblBlogPostTag_BlogPost` (`fkBlogPost`)
    REFERENCES `tblBlogPostTag` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
 ALTER TABLE `tblBlogPostTag`
 DROP FOREIGN KEY `FK_tblBlogPostTag_Tag`;

ALTER TABLE `tblBlogPostTag` ADD CONSTRAINT `FK_tblBlogPostTag_Tag` FOREIGN KEY `FK_tblBlogPostTag_Tag` (`fkTag`)
    REFERENCES `tblTag` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
ALTER TABLE `tblBlogPostTag`
 DROP FOREIGN KEY `FK_tblBlogPostTag_BlogPost`;

ALTER TABLE `tblBlogPostTag` ADD CONSTRAINT `FK_tblBlogPostTag_BlogPost` FOREIGN KEY `FK_tblBlogPostTag_BlogPost` (`fkBlogPost`)
    REFERENCES `tblBlogPost` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
CREATE TABLE `tblJobPostTag` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` INTEGER(1) NOT NULL DEFAULT '0', /*ENUM('1','0')*/
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblJobPostTag` ADD COLUMN `fkTag` INTEGER(10) UNSIGNED AFTER `pkId`,
 ADD COLUMN `fkJobPost` INTEGER(10) UNSIGNED AFTER `fkTag`,
 ADD CONSTRAINT `FK_tblJobPostTag_Tag` FOREIGN KEY `FK_tblJobPostTag_Tag` (`fkTag`)
    REFERENCES `tblTag` (`pkId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_tblJobPostTag_JobPost` FOREIGN KEY `FK_tblJobPostTag_JobPost` (`fkJobPost`)
    REFERENCES `tblJobPostTag` (`pkId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

 ALTER TABLE `tblJobPostTag`
 DROP FOREIGN KEY `FK_tblJobPostTag_JobPost`;

ALTER TABLE `tblJobPostTag` ADD CONSTRAINT `FK_tblJobPostTag_JobPost` FOREIGN KEY `FK_tblJobPostTag_JobPost` (`fkJobPost`)
    REFERENCES `tblJobPost` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
 ALTER TABLE `tblJobPostTag`
 DROP FOREIGN KEY `FK_tblJobPostTag_Tag`;

ALTER TABLE `tblJobPostTag` ADD CONSTRAINT `FK_tblJobPostTag_Tag` FOREIGN KEY `FK_tblJobPostTag_Tag` (`fkTag`)
    REFERENCES `tblTag` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
 CREATE TABLE `tblEvent` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblEvent` ADD COLUMN `txtName` VARCHAR(500) AFTER `dateArchive`,
 ADD COLUMN `countGoingUsers` INTEGER(10) UNSIGNED AFTER `txtName`,
 ADD COLUMN `countMaybeUsers` INTEGER(10) UNSIGNED AFTER `countGoingUsers`,
 ADD COLUMN `countNotGoingUsers` INTEGER(10) UNSIGNED AFTER `countMaybeUsers`,
 ADD COLUMN `enumEventType` INTEGER(1) UNSIGNED AFTER `countNotGoingUsers`,
 ADD COLUMN `dateStartDate` DATETIME AFTER `enumEventType`,
 ADD COLUMN `dateEndDate` DATETIME AFTER `dateStartDate`,
 ADD COLUMN `txtDuration` VARCHAR(20) AFTER `dateEndDate`,
 ADD COLUMN `txtLocation` VARCHAR(100) AFTER `txtDuration`,
 ADD COLUMN `enumRSVP` INTEGER(1) UNSIGNED AFTER `txtLocation`,
 ADD COLUMN `txtDescription` TEXT AFTER `enumRSVP`,
 ADD COLUMN `txtAgenda` TEXT AFTER `txtDescription`,
 ADD COLUMN `txtFacebookLink` VARCHAR(200) AFTER `txtAgenda`,
 ADD COLUMN `txtInstagramLink` VARCHAR(200) AFTER `txtFacebookLink`,
 ADD COLUMN `txtYoutubeLink` VARCHAR(200) AFTER `txtInstagramLink`,
 ADD COLUMN `txtEventLink` VARCHAR(200) AFTER `txtYoutubeLink`,
 ADD COLUMN `enumCapacity` INTEGER(1) UNSIGNED AFTER `txtEventLink`,
 ADD COLUMN `txtMaxCapacity` VARCHAR(5) AFTER `enumCapacity`,
 ADD COLUMN `txtContactPersonName` VARCHAR(500) AFTER `txtMaxCapacity`,
 ADD COLUMN `txtContactPersonEmail` VARCHAR(500) AFTER `txtContactPersonName`,
 ADD COLUMN `txtContactNumber` VARCHAR(25) AFTER `txtContactPersonEmail`,
 ADD COLUMN `txtBannerImagePath` VARCHAR(100) AFTER `txtContactNumber`;
 
  CREATE TABLE `tblEventPartner` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblEventPartner` ADD COLUMN `txtPartnerLink` VARCHAR(45) AFTER `dateArchive`,
 ADD COLUMN `fkAttachment` INTEGER(10) UNSIGNED AFTER `txtPartnerLink`,
 ADD COLUMN `fkEvent` INTEGER(10) UNSIGNED AFTER `fkAttachment`,
 ADD CONSTRAINT `FK_tblEventPartner_Event` FOREIGN KEY `FK_tblEventPartner_Event` (`fkEvent`)
    REFERENCES `tblEvent` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
CREATE TABLE `tblAttachment` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblAttachment` ADD COLUMN `txtAttachmentPath` VARCHAR(100) AFTER `pkId`;

ALTER TABLE `tblEventPartner` ADD CONSTRAINT `FK_tblEventPartner_Attachment` FOREIGN KEY `FK_tblEventPartner_Attachment` (`fkAttachment`)
    REFERENCES `tblAttachment` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
CREATE TABLE `tblEventSponsor` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblEventSponsor` ADD COLUMN `txtSponsorLink` VARCHAR(45) AFTER `dateArchive`,
 ADD COLUMN `fkAttachment` INTEGER(10) UNSIGNED AFTER `txtSponsorLink`,
 ADD COLUMN `fkEvent` INTEGER(10) UNSIGNED AFTER `fkAttachment`,
 ADD CONSTRAINT `FK_tblEventSponsor_Event` FOREIGN KEY `FK_tblEventSponsor_Event` (`fkEvent`)
    REFERENCES `tblEvent` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `tblEventSponsor` ADD CONSTRAINT `FK_tblEventSponsor_Attachment` FOREIGN KEY `FK_tblEventSponsor_Attachment` (`fkAttachment`)
    REFERENCES `tblAttachment` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
CREATE TABLE `tblEventTag` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` INTEGER(1) NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblEventTag` ADD COLUMN `fkTag` INTEGER(10) UNSIGNED AFTER `dateArchive`,
 ADD COLUMN `fkEvent` INTEGER(10) UNSIGNED AFTER `fkTag`,
 ADD CONSTRAINT `FK_tblEventTag_Tag` FOREIGN KEY `FK_tblEventTag_Tag` (`fkTag`)
    REFERENCES `tblTag` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_tblEventTag_Event` FOREIGN KEY `FK_tblEventTag_Event` (`fkEvent`)
    REFERENCES `tblEvent` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
CREATE TABLE `tblEventGallery` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblEventGallery` ADD COLUMN `fkAttachment` INTEGER(10) UNSIGNED AFTER `dateArchive`,
 ADD COLUMN `fkEvent` INTEGER(10) UNSIGNED AFTER `fkAttachment`,
 ADD CONSTRAINT `FK_tblEventGallery_Attachment` FOREIGN KEY `FK_tblEventGallery_Attachment` (`fkAttachment`)
    REFERENCES `tblattachment` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_tblEventGallery_Event` FOREIGN KEY `FK_tblEventGallery_Event` (`fkEvent`)
    REFERENCES `tblevent` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
CREATE TABLE `tblEventUserStatus` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblEventUserStatus` ADD COLUMN `enumEventStatus` INTEGER(1) UNSIGNED AFTER `dateArchive`,
 ADD COLUMN `fkEvent` INTEGER(10) UNSIGNED AFTER `enumEventStatus`,
 ADD COLUMN `fkUser` INTEGER(10) UNSIGNED AFTER `fkEvent`,
 ADD CONSTRAINT `FK_tblEventUserStatus_Event` FOREIGN KEY `FK_tblEventUserStatus_Event` (`fkEvent`)
    REFERENCES `tblEvent` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_tblEventUserStatus_User` FOREIGN KEY `FK_tblEventUserStatus_User` (`fkUser`)
    REFERENCES `tblUser` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
ALTER TABLE `tblEvent` DROP COLUMN `countGoingUsers`,
 DROP COLUMN `countMaybeUsers`,
 DROP COLUMN `countNotGoingUsers`;
 
ALTER TABLE `tblEventGallery` MODIFY COLUMN `enumAct` INTEGER(1) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `tblEventPartner` MODIFY COLUMN `enumAct` INTEGER(1) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `tblEventSponsor` MODIFY COLUMN `enumAct` INTEGER(1) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `tblEventTag` MODIFY COLUMN `enumAct` INTEGER(1) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `tblEventUserStatus` MODIFY COLUMN `enumAct` INTEGER(1) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `tblBlogPost` DROP COLUMN `countFollowers`,
 DROP COLUMN `countLikes`;
 
CREATE TABLE `tblBlogPostUserFollow` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE `tblBlogPostUserLike` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` ENUM('1','0') NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblBlogPostUserFollow` ADD COLUMN `enumBlogPostFollow` INTEGER(1) UNSIGNED AFTER `dateArchive`,
 ADD COLUMN `fkBlogPost` INTEGER(10) UNSIGNED AFTER `enumBlogPostFollow`,
 ADD COLUMN `fkUser` INTEGER(10) UNSIGNED AFTER `fkBlogPost`,
 ADD CONSTRAINT `FK_tblBlogPostUserFollow_BlogPost` FOREIGN KEY `FK_tblBlogPostUserFollow_BlogPost` (`fkBlogPost`)
    REFERENCES `tblBlogPost` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_tblBlogPostUserFollow_User` FOREIGN KEY `FK_tblBlogPostUserFollow_User` (`fkUser`)
    REFERENCES `tblUser` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
 ALTER TABLE `tblBlogPostUserLike` ADD COLUMN `enumBlogPostLike` INTEGER(1) UNSIGNED AFTER `dateArchive`,
 ADD COLUMN `fkBlogPost` INTEGER(10) UNSIGNED AFTER `enumBlogPostLike`,
 ADD COLUMN `fkUser` INTEGER(10) UNSIGNED AFTER `fkBlogPost`,
 ADD CONSTRAINT `FK_tblBlogPostUserLike_BlogPost` FOREIGN KEY `FK_tblBlogPostUserLike_BlogPost` (`fkBlogPost`)
    REFERENCES `tblBlogPost` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_tblBlogPostUserLike_User` FOREIGN KEY `FK_tblBlogPostUserLike_User` (`fkUser`)
    REFERENCES `tblUser` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    

    
CREATE TABLE `tblStudentClub` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` INTEGER(1) NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblStudentClub` ADD COLUMN `txtName` VARCHAR(500) AFTER `dateArchive`,
 ADD COLUMN `txtSocialLink` VARCHAR(200) AFTER `txtName`,
 ADD COLUMN `txtDescription` TEXT AFTER `txtSocialLink`,
 ADD COLUMN `txtLogoPath` VARCHAR(100) AFTER `txtDescription`;
 
CREATE TABLE `tblStudentClubTag` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` INTEGER(1) NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblStudentClubTag` ADD COLUMN `fkTag` INTEGER(10) UNSIGNED AFTER `dateArchive`,
 ADD COLUMN `fkStudentClub` INTEGER(10) UNSIGNED AFTER `fkTag`,
 ADD CONSTRAINT `FK_tblStudentClubTag_Tag` FOREIGN KEY `FK_tblStudentClubTag_Tag` (`fkTag`)
    REFERENCES `tblTag` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_tblStudentClubTag_Event` FOREIGN KEY `FK_tblStudentClubTag_StudentClub` (`fkStudentClub`)
    REFERENCES `tblStudentClub` (`pkId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
CREATE TABLE `tblResearchResource` (
  `pkId` INTEGER(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lockVersion` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `fkCreateBy` INTEGER(10) UNSIGNED NOT NULL DEFAULT 0,
  `dateCreate` DATETIME NOT NULL DEFAULT 0,
  `fkUpdateBy` INTEGER(10) UNSIGNED,
  `dateUpdate` DATETIME,
  `enumAct` INTEGER(1) NOT NULL DEFAULT '0',
  `fkActChangeBy` INTEGER(10) UNSIGNED,
  `dateActChange` DATETIME,
  `enumArchive` ENUM('1','0') NOT NULL DEFAULT '1',
  `fkArchiveBy` INTEGER(10) UNSIGNED,
  `dateArchive` DATETIME,
  PRIMARY KEY(`pkId`)
)
ENGINE = InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblResearchResource` ADD COLUMN `txtTitle` VARCHAR(100) AFTER `dateArchive`,
 ADD COLUMN `txtAuthors` VARCHAR(500) AFTER `txtTitle`,
 ADD COLUMN `txtType` VARCHAR(100) AFTER `txtAuthors`,
 ADD COLUMN `datePublishedOn` DATETIME AFTER `txtType`,
 ADD COLUMN `txtAbstract` TEXT AFTER `datePublishedOn`,
 ADD COLUMN `txtResearchResourcePath` VARCHAR(100) AFTER `txtAbstract`;
 
ALTER TABLE `tblUser` ADD COLUMN `txtBatch` VARCHAR(100) AFTER `enumApprove`;
CREATE TABLE tblEmailAccount(  
  pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
  txtAccountName VARCHAR(50) NOT NULL,
  txtReplyToEmail VARCHAR(100) NOT NULL,
  numberRetryAttempt INT(2) NOT NULL DEFAULT 1,
  txtHost VARCHAR(100) NOT NULL,
  numberPort INT(5) NOT NULL DEFAULT 25,
  txtUserName VARCHAR(100) NOT NULL,
  txtpassword VARCHAR(100) NOT NULL,
  enumAuthenticationMethod ENUM('0','1','2') NOT NULL DEFAULT '0',  /* 0 - PLAIN, 1-LOGIN, 2- CRAM MD5*/
  enumAuthenticationSecurity ENUM('0','1','2') NOT NULL DEFAULT '0', /* 0-No, 1-SSL, 2-TSL */
  numberTimeout INT(5) NOT NULL DEFAULT 60000, /* define in millisecond */
  fkCreateBy INT(10) unsigned NOT NULL,
  dateCreate DATETIME NOT NULL,
  fkUpdateBy INT(10) unsigned DEFAULT NULL,
  dateUpdate DATETIME DEFAULT NULL,
  enumAct ENUM('1','0') NOT NULL DEFAULT '0', /* 0-Inactive, 1-Active*/ 
  fkActChangeBy INT(10) unsigned DEFAULT NULL,
  dateActChange DATETIME DEFAULT NULL,
  enumArchive ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Archive, 1-NonArchive*/ 
  fkArchiveBy INT(10) unsigned DEFAULT NULL, 
  dateArchive DATETIME DEFAULT NULL,
  PRIMARY KEY (pkId),
  UNIQUE KEY(txtAccountName),
  UNIQUE KEY(txtUserName)
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

INSERT INTO tblEmailAccount(pkId, txtAccountName, txtReplyToEmail,
txtHost, numberPort, txtUserName, txtpassword, enumAuthenticationMethod, enumAuthenticationSecurity, fkCreateBy, dateCreate) VALUES
(1,"Testing Account","noreply.sddesign@gmail.com","smtp.gmail.com",465,"noreply.sddesign@gmail.com",
"sddesign2015",'1','1',1,NOW());

CREATE TABLE tblTransactionEmail(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
 	fkEmailAccountId INT(10) unsigned NOT NULL,
 	txtEmailTo TEXT NOT NULL,
 	txtEmailCc TEXT DEFAULT NULL,
 	txtEmailBcc TEXT DEFAULT NULL,
 	txtSubject VARCHAR(1000) NOT NULL,
 	txtBody TEXT NOT NULL,
 	enumStatus ENUM('0','1','2','3') NOT NULL DEFAULT '0', /* 0-New, 1-INPROCESS, 2-FAILED, 3-SENT*/
 	numberRetryCount INT(2) NOT NULL DEFAULT 0,
 	txtAttachmentPath TEXT DEFAULT NULL,
 	txtError TEXT DEFAULT NULL,
 	PRIMARY KEY (pkId),
 	INDEX(enumStatus),
 	FOREIGN KEY(fkEMailAccountId) REFERENCES tblEmailAccount(pkId) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE tblNotificationEmail(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
 	fkEmailAccountId INT(10) unsigned NOT NULL,
 	txtEmailTo TEXT NOT NULL,
 	txtEmailCc TEXT DEFAULT NULL,
 	txtEmailBcc TEXT DEFAULT NULL,
 	txtSubject VARCHAR(1000) NOT NULL,
 	txtBody TEXT NOT NULL,
 	enumStatus ENUM('0','1','2','3') NOT NULL DEFAULT '0', /* 0-New, 1-INPROCESS, 2-FAILED, 3-SENT*/
 	numberRetryCount INT(2) NOT NULL DEFAULT 0,
 	txtAttachmentPath TEXT DEFAULT NULL,
 	txtError TEXT DEFAULT NULL,
 	PRIMARY KEY (pkId),
 	INDEX(enumStatus),
 	FOREIGN KEY(fkEMailAccountId) REFERENCES tblEmailAccount(pkId) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=UTF8;


CREATE TABLE tblMarketingEmail(
	pkId INT(10) unsigned NOT NULL,
 	fkEmailAccountId INT(10) unsigned NOT NULL,
 	txtEmailTo TEXT NOT NULL,
 	txtEmailCc TEXT DEFAULT NULL,
 	txtEmailBcc TEXT DEFAULT NULL,
 	txtSubject VARCHAR(1000) NOT NULL,
 	txtBody TEXT NOT NULL,
 	enumStatus ENUM('0','1','2','3') NOT NULL DEFAULT '0', /* 0-New, 1-INPROCESS, 2-FAILED, 3-SENT*/
 	numberRetryCount INT(2) NOT NULL DEFAULT 0,
 	txtAttachmentPath TEXT DEFAULT NULL,
 	txtError TEXT DEFAULT NULL,
 	PRIMARY KEY (pkId),
 	INDEX(enumStatus),
 	FOREIGN KEY(fkEMailAccountId) REFERENCES tblEmailAccount(pkId) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE tblEmailContent(
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
	txtName VARCHAR(100) NOT NULL,
	txtContent TEXT NOT NULL,
	PRIMARY KEY (pkId)
)ENGINE=INNODB DEFAULT CHARSET=UTF8;

ALTER TABLE `tblBlogPost` MODIFY COLUMN `enumAct` INTEGER(1) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `tblJobPost` MODIFY COLUMN `enumAct` INTEGER(1) UNSIGNED NOT NULL DEFAULT 0;