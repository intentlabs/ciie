
USE dbALeague;

ALTER TABLE tblRoleModuleRight
DROP COLUMN pkSubModuleId;

/** Student Role Right**/
INSERT INTO tblRoleModuleRight(pkfkRoleId, pkModuleId, pkEnumType, txtRights)
VALUES (2, 1, '4', '3');

INSERT INTO tblRoleModuleRight(pkfkRoleId, pkModuleId, pkEnumType, txtRights)
VALUES (2, 2, '4', '3');

INSERT INTO tblRoleModuleRight(pkfkRoleId, pkModuleId, pkEnumType, txtRights)
VALUES (2, 3, '4', '1,2,3,4');

INSERT INTO tblRoleModuleRight(pkfkRoleId, pkModuleId, pkEnumType, txtRights)
VALUES (2, 4, '4', '1,2,3,4');

INSERT INTO tblRoleModuleRight(pkfkRoleId, pkModuleId, pkEnumType, txtRights)
VALUES (2, 5, '4', '1,2,3,4');

INSERT INTO tblRoleModuleRight(pkfkRoleId, pkModuleId, pkEnumType, txtRights)
VALUES (2, 6, '4', '1,2,3,4');

INSERT INTO tblRoleModuleRight(pkfkRoleId, pkModuleId, pkEnumType, txtRights)
VALUES (2, 7, '4', '1,2,3,4');

INSERT INTO tblRoleModuleRight(pkfkRoleId, pkModuleId, pkEnumType, txtRights)
VALUES (2, 8, '4', '1,2,3,4');


CREATE TABLE tblQuestion (
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
    lockVersion INT(10) unsigned NOT NULL,
	fkCreateBy INT(10) unsigned NOT NULL,
    dateCreate DATETIME NOT NULL,
    fkUpdateBy INT(10) unsigned DEFAULT NULL,
    dateUpdate DATETIME DEFAULT NULL,
    enumAct INT(1) NOT NULL DEFAULT '1', /* 0-Inactive, 1-Active*/ 
    fkActChangeBy INT(10) unsigned DEFAULT NULL,
    dateActChange DATETIME DEFAULT NULL,
    enumArchive ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Archive, 1-NonArchive*/ 
	fkArchiveBy INT(10) unsigned DEFAULT NULL, 
	dateArchive DATETIME DEFAULT NULL,
	txtQuestion TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	fkReportedBy INT(10) unsigned DEFAULT NULL,
	dateReportedDate DATETIME DEFAULT NULL,
	fkModeratedBy INT(10) unsigned DEFAULT NULL,
	dateModeratedDate DATETIME DEFAULT NULL,
	enumAppropriate ENUM('0','1') NOT NULL DEFAULT '0', /* 0-appropriate, 1-Inappropriate*/ 
	PRIMARY KEY (pkId),
    FOREIGN KEY(fkCreateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkUpdateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkActChangeBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkReportedBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkModeratedBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	INDEX(enumAppropriate)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tblAnswer (
	pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
    lockVersion INT(10) unsigned NOT NULL,
	fkCreateBy INT(10) unsigned NOT NULL,
    dateCreate DATETIME NOT NULL,
    fkUpdateBy INT(10) unsigned DEFAULT NULL,
    dateUpdate DATETIME DEFAULT NULL,
    enumAct INT(1) NOT NULL DEFAULT '1', /* 0-Inactive, 1-Active*/ 
    fkActChangeBy INT(10) unsigned DEFAULT NULL,
    dateActChange DATETIME DEFAULT NULL,
    enumArchive ENUM('1','0') NOT NULL DEFAULT '1', /* 0-Archive, 1-NonArchive*/ 
	fkArchiveBy INT(10) unsigned DEFAULT NULL, 
	dateArchive DATETIME DEFAULT NULL,
	txtAnswer TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	fkQuestionId INT(10) unsigned NOT NULL,
	fkReportedBy INT(10) unsigned DEFAULT NULL,
	dateReportedDate DATETIME DEFAULT NULL,
	fkModeratedBy INT(10) unsigned DEFAULT NULL,
	dateModeratedDate DATETIME DEFAULT NULL,
	enumAppropriate ENUM('0','1') NOT NULL DEFAULT '1', /* 0-appropriate, 1-Inappropriate*/ 
	PRIMARY KEY (pkId),
    FOREIGN KEY(fkCreateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkUpdateBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkActChangeBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkReportedBy) REFERENCES tblUser(pkId) ON DELETE CASCADE,
	FOREIGN KEY(fkQuestionId) REFERENCES tblQuestion(pkId) ON DELETE CASCADE,
	INDEX(enumAppropriate)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE tblQuestionLike(
  pkId INT(10) unsigned NOT NULL AUTO_INCREMENT,
  fkUserId int(10) unsigned NOT NULL,
  fkQuestionId INT(10) unsigned NOT NULL,
  enumLike tinyint(1) NOT NULL,
  PRIMARY KEY (pkId),
  FOREIGN KEY(fkQuestionId) REFERENCES tblQuestion(pkId) ON DELETE CASCADE,
  FOREIGN KEY(fkUserId) REFERENCES tblUser(pkId) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





ALTER TABLE `dbALeague`.`tblEvent` MODIFY COLUMN `enumAct` INTEGER UNSIGNED NOT NULL DEFAULT 0;
ALTER TABLE `dbALeague`.`tblInstitute` MODIFY COLUMN `txtDomain` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci;

UPDATE dbALeague.tblInstitute SET txtDomain=',aiim.ac.in,' WHERE pkId = 1;
UPDATE dbALeague.tblInstitute SET txtDomain=',cept.ac.in,' WHERE pkId = 2;
UPDATE dbALeague.tblInstitute SET txtDomain=',daiict.ac.in,' WHERE pkId = 3;
UPDATE dbALeague.tblInstitute SET txtDomain=',pdpu.ac.in,sot.pdpu.ac.in,spm.pdpu.ac.in,spt.pdpu.ac.in,iic.pdpu.ac.in,sls.pdpu.ac.in,' WHERE pkId = 4;
UPDATE dbALeague.tblInstitute SET txtDomain=',ediindia.org,' WHERE pkId = 5;
UPDATE dbALeague.tblInstitute SET txtDomain=',iitgn.ac.in,' WHERE pkId = 6;
UPDATE dbALeague.tblInstitute SET txtDomain=',iima.ac.in,' WHERE pkId = 7;
UPDATE dbALeague.tblInstitute SET txtDomain=',micamail.in,' WHERE pkId = 8;
UPDATE dbALeague.tblInstitute SET txtDomain=',gnlu.ac.in,' WHERE pkId = 9;
UPDATE dbALeague.tblInstitute SET txtDomain=',ahduni.edu.in,' WHERE pkId = 10;
UPDATE dbALeague.tblInstitute SET txtDomain=',nid.edu,' WHERE pkId = 11;
UPDATE dbALeague.tblInstitute SET txtDomain=',nift.ac.in,' WHERE pkId = 12;
UPDATE dbALeague.tblInstitute SET txtDomain=',nirmauni.ac.in,' WHERE pkId = 13;
UPDATE dbALeague.tblInstitute SET txtDomain=',iiitvadodara.ac.in,' WHERE pkId = 14;


INSERT INTO `tblEmailContent` VALUES (3,'User Forgot Password','<h3><strong>{ApplicantName},</strong></h3><p>Greetings from A-League community !</p><p>We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below. <br><br>Your new temporary password is : <strong>{password}</strong><br/><br/><br>Please Note : Your temporary password will expire in 48 hours. Change your password after your Login.<br/><br/></p><p>Sincerely,<br/>A-League</p>');

UPDATE dbALeague.tblEmailContent SET txtContent='<h3><strong>{ApplicantName},</strong></h3><p>Greetings from A-League community !</p><p>We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below. <br><br>Your new temporary password is : <strong>{password}</strong><br/><br/><br>Please Note : Your temporary password will expire in 48 hours. Change your password after your Login.<br/><br/></p><p>Sincerely,<br/>A-League</p>' WHERE pkId = 3;

ALTER TABLE `dbALeague`.`tblUserSession` MODIFY COLUMN `fkUserId` INTEGER UNSIGNED;
ALTER TABLE `dbALeague`.`tblUser` MODIFY COLUMN `enumApprove` ENUM('1','0','2') NOT NULL DEFAULT '0';


ALTER TABLE `dbALeague`.`tblEvent` MODIFY COLUMN `txtFacebookLink` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci,
 MODIFY COLUMN `txtInstagramLink` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci,
 MODIFY COLUMN `txtYoutubeLink` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci,
 MODIFY COLUMN `txtEventLink` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci;
 
 ALTER TABLE `dbALeague`.`tblEvent` MODIFY COLUMN `dateStartDate` DATE,
 MODIFY COLUMN `dateEndDate` DATE;
 
 
 ALTER TABLE `dbALeague`.`tblUser` ADD COLUMN `txtUserprofileLink` VARCHAR(1000) AFTER `txtBatch`;
